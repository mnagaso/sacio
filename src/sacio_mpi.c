#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <complex.h>
#include <mpi.h>
#include "sacio_mpi.h"

/*
#ifndef _DI
#define _DI (__extension__ 1.0i)
#endif
#ifndef DCMPLX
#define DCMPLX(r,i) ((double) (r) + (double) (i)*_DI)
#endif
*/

/*!
 * @defgroup sac_mpi SAC MPI
 * @brief SAC MPI communication utilities.
 * @author Ben Baker
 * @copyright ISTI distributed under the Apache 2 license.
 */
/*!
 * @defgroup sac_mpi_broadcast Broadcast
 * @brief Broadcasts SAC data structures to all processes on a communicator.
 * @ingroup sac_mpi
 * @author Ben Baker
 * @copyright ISTI distributed under the Apache 2 license.
 */

/*!
 * @brief Serializes a SAC header for communication via MPI
 * @result Serialized MPI datatype representing the SAC header.
 * @ingroup sac_mpi
 */
MPI_Datatype sacio_mpi_createHeader(void)
{
    MPI_Datatype mpiHeader, tempType;
    MPI_Aint lb, extent;
    int nitems = 135;
    int blockLengths[135] =
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, //20
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, //40
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, //60
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1,                               //70
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, //90
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,                //105
         1, 1, 1, 1, 1,                                              //110
         8, 16, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,//130
         8, 8, 8,                                                    //133
         1, 1                                                        //135
        };
      
    MPI_Datatype types[135]
       ={MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, //5
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, //10
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, //15
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, //20
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, //25
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, //30
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, //35
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, //40
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, //45
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, //50
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, //55
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, //60
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, //65
         MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE, //70
         MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT,                //75
         MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT,                //80
         MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT,                //85
         MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT,                //90
         MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT,                //95
         MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT,                //100
         MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT,                //105
         MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT,                //110
         MPI_CHAR,  MPI_CHAR,                                        //112
         MPI_CHAR,  MPI_CHAR, MPI_CHAR,                              //115
         MPI_CHAR,  MPI_CHAR, MPI_CHAR,                              //118
         MPI_CHAR,  MPI_CHAR, MPI_CHAR,                              //121
         MPI_CHAR,  MPI_CHAR, MPI_CHAR,                              //124
         MPI_CHAR,  MPI_CHAR, MPI_CHAR,                              //127
         MPI_CHAR,  MPI_CHAR, MPI_CHAR,                              //130
         MPI_CHAR,  MPI_CHAR, MPI_CHAR,                              //133
         MPI_C_BOOL, MPI_CHAR};                                      //135 
    MPI_Aint offset[135];
    // doubles
    offset[0]   = offsetof(struct sacHeader_struct, delta);
    offset[1]   = offsetof(struct sacHeader_struct, depmin);
    offset[2]   = offsetof(struct sacHeader_struct, depmax);
    offset[3]   = offsetof(struct sacHeader_struct, scale);
    offset[4]   = offsetof(struct sacHeader_struct, odelta);
    offset[5]   = offsetof(struct sacHeader_struct, b);
    offset[6]   = offsetof(struct sacHeader_struct, e);
    offset[7]   = offsetof(struct sacHeader_struct, o);
    offset[8]   = offsetof(struct sacHeader_struct, a);
    offset[9]   = offsetof(struct sacHeader_struct, internal1);
    offset[10]  = offsetof(struct sacHeader_struct, t0);
    offset[11]  = offsetof(struct sacHeader_struct, t1); 
    offset[12]  = offsetof(struct sacHeader_struct, t2);
    offset[13]  = offsetof(struct sacHeader_struct, t3);
    offset[14]  = offsetof(struct sacHeader_struct, t4);
    offset[15]  = offsetof(struct sacHeader_struct, t5);
    offset[16]  = offsetof(struct sacHeader_struct, t6);
    offset[17]  = offsetof(struct sacHeader_struct, t7);
    offset[18]  = offsetof(struct sacHeader_struct, t8);
    offset[19]  = offsetof(struct sacHeader_struct, t9);
    offset[20]  = offsetof(struct sacHeader_struct, f);
    offset[21]  = offsetof(struct sacHeader_struct, resp0);
    offset[22]  = offsetof(struct sacHeader_struct, resp1); 
    offset[23]  = offsetof(struct sacHeader_struct, resp2);
    offset[24]  = offsetof(struct sacHeader_struct, resp3);
    offset[25]  = offsetof(struct sacHeader_struct, resp4);
    offset[26]  = offsetof(struct sacHeader_struct, resp5);
    offset[27]  = offsetof(struct sacHeader_struct, resp6);
    offset[28]  = offsetof(struct sacHeader_struct, resp7);
    offset[29]  = offsetof(struct sacHeader_struct, resp8);
    offset[30]  = offsetof(struct sacHeader_struct, resp9);
    offset[31]  = offsetof(struct sacHeader_struct, stla);
    offset[32]  = offsetof(struct sacHeader_struct, stlo);
    offset[33]  = offsetof(struct sacHeader_struct, stel);
    offset[34]  = offsetof(struct sacHeader_struct, stdp);
    offset[35]  = offsetof(struct sacHeader_struct, evla);
    offset[36]  = offsetof(struct sacHeader_struct, evlo);
    offset[37]  = offsetof(struct sacHeader_struct, evel);
    offset[38]  = offsetof(struct sacHeader_struct, evdp);
    offset[39]  = offsetof(struct sacHeader_struct, mag);
    offset[40]  = offsetof(struct sacHeader_struct, user0);
    offset[41]  = offsetof(struct sacHeader_struct, user1); 
    offset[42]  = offsetof(struct sacHeader_struct, user2);
    offset[43]  = offsetof(struct sacHeader_struct, user3);
    offset[44]  = offsetof(struct sacHeader_struct, user4);
    offset[45]  = offsetof(struct sacHeader_struct, user5);
    offset[46]  = offsetof(struct sacHeader_struct, user6);
    offset[47]  = offsetof(struct sacHeader_struct, user7);
    offset[48]  = offsetof(struct sacHeader_struct, user8);
    offset[49]  = offsetof(struct sacHeader_struct, user9);
    offset[50]  = offsetof(struct sacHeader_struct, dist);
    offset[51]  = offsetof(struct sacHeader_struct, az);
    offset[52]  = offsetof(struct sacHeader_struct, baz);
    offset[53]  = offsetof(struct sacHeader_struct, gcarc);
    offset[54]  = offsetof(struct sacHeader_struct, internal1);
    offset[55]  = offsetof(struct sacHeader_struct, internal2);
    offset[56]  = offsetof(struct sacHeader_struct, depmen);
    offset[57]  = offsetof(struct sacHeader_struct, cmpaz);
    offset[58]  = offsetof(struct sacHeader_struct, cmpinc);
    offset[59]  = offsetof(struct sacHeader_struct, xminimum);
    offset[60]  = offsetof(struct sacHeader_struct, xmaximum);
    offset[61]  = offsetof(struct sacHeader_struct, yminimum);
    offset[62]  = offsetof(struct sacHeader_struct, ymaximum);
    offset[63]  = offsetof(struct sacHeader_struct, unused0);
    offset[64]  = offsetof(struct sacHeader_struct, unused1);
    offset[65]  = offsetof(struct sacHeader_struct, unused2);
    offset[66]  = offsetof(struct sacHeader_struct, unused3);
    offset[67]  = offsetof(struct sacHeader_struct, unused4);
    offset[68]  = offsetof(struct sacHeader_struct, unused5);
    offset[69]  = offsetof(struct sacHeader_struct, unused6);
    // ints
    offset[70]  = offsetof(struct sacHeader_struct, nzyear);
    offset[71]  = offsetof(struct sacHeader_struct, nzjday);
    offset[72]  = offsetof(struct sacHeader_struct, nzhour);
    offset[73]  = offsetof(struct sacHeader_struct, nzmin);
    offset[74]  = offsetof(struct sacHeader_struct, nzsec);
    offset[75]  = offsetof(struct sacHeader_struct, nzmsec);
    offset[76]  = offsetof(struct sacHeader_struct, nvhdr);
    offset[77]  = offsetof(struct sacHeader_struct, norid);
    offset[78]  = offsetof(struct sacHeader_struct, nevid);
    offset[79]  = offsetof(struct sacHeader_struct, npts);
    offset[80]  = offsetof(struct sacHeader_struct, ninternal1);
    offset[81]  = offsetof(struct sacHeader_struct, nwfid);
    offset[82]  = offsetof(struct sacHeader_struct, nxsize);
    offset[83]  = offsetof(struct sacHeader_struct, nysize);
    offset[84]  = offsetof(struct sacHeader_struct, nunused0);
    offset[85]  = offsetof(struct sacHeader_struct, iftype);
    offset[86]  = offsetof(struct sacHeader_struct, idep);
    offset[87]  = offsetof(struct sacHeader_struct, iztype);
    offset[88]  = offsetof(struct sacHeader_struct, nunused1);
    offset[89]  = offsetof(struct sacHeader_struct, iinst);
    offset[90]  = offsetof(struct sacHeader_struct, istreg);
    offset[91]  = offsetof(struct sacHeader_struct, ievreg);
    offset[92]  = offsetof(struct sacHeader_struct, ievtyp);
    offset[93]  = offsetof(struct sacHeader_struct, iqual);
    offset[94]  = offsetof(struct sacHeader_struct, isynth);
    offset[95]  = offsetof(struct sacHeader_struct, imagtyp);
    offset[96]  = offsetof(struct sacHeader_struct, imagsrc);
    offset[97]  = offsetof(struct sacHeader_struct, nunused2);
    offset[98]  = offsetof(struct sacHeader_struct, nunused3);
    offset[99]  = offsetof(struct sacHeader_struct, nunused4);
    offset[100] = offsetof(struct sacHeader_struct, nunused5);
    offset[101] = offsetof(struct sacHeader_struct, nunused6);
    offset[102] = offsetof(struct sacHeader_struct, nunused7);
    offset[103] = offsetof(struct sacHeader_struct, nunused8);
    offset[104] = offsetof(struct sacHeader_struct, nunused9); 
    // logicals
    offset[105] = offsetof(struct sacHeader_struct, leven);
    offset[106] = offsetof(struct sacHeader_struct, lpspol);
    offset[107] = offsetof(struct sacHeader_struct, lovrok);
    offset[108] = offsetof(struct sacHeader_struct, lcalda);
    offset[109] = offsetof(struct sacHeader_struct, lunused);
    // characters
    offset[110] = offsetof(struct sacHeader_struct, kstnm);
    offset[111] = offsetof(struct sacHeader_struct, kevnm);
    offset[112] = offsetof(struct sacHeader_struct, khole);
    offset[113] = offsetof(struct sacHeader_struct, ko);
    offset[114] = offsetof(struct sacHeader_struct, ka);
    offset[115] = offsetof(struct sacHeader_struct, kt0);
    offset[116] = offsetof(struct sacHeader_struct, kt1);
    offset[117] = offsetof(struct sacHeader_struct, kt2);
    offset[118] = offsetof(struct sacHeader_struct, kt3);
    offset[119] = offsetof(struct sacHeader_struct, kt4);
    offset[120] = offsetof(struct sacHeader_struct, kt5);
    offset[121] = offsetof(struct sacHeader_struct, kt6);
    offset[122] = offsetof(struct sacHeader_struct, kt7);
    offset[123] = offsetof(struct sacHeader_struct, kt8);
    offset[124] = offsetof(struct sacHeader_struct, kt9);
    offset[125] = offsetof(struct sacHeader_struct, kf);
    offset[126] = offsetof(struct sacHeader_struct, kuser0);
    offset[127] = offsetof(struct sacHeader_struct, kuser1);
    offset[128] = offsetof(struct sacHeader_struct, kuser2);
    offset[129] = offsetof(struct sacHeader_struct, kcmpnm);
    offset[130] = offsetof(struct sacHeader_struct, knetwk);
    offset[131] = offsetof(struct sacHeader_struct, kdatrd);
    offset[132] = offsetof(struct sacHeader_struct, kinst);
    offset[133] = offsetof(struct sacHeader_struct, lhaveHeader);
    offset[134] = offsetof(struct sacHeader_struct, pad);

    // Chars and bools can create potential holes in alignment
    MPI_Type_create_struct(nitems, blockLengths, offset, types, &tempType);
    MPI_Type_get_extent(tempType, &lb, &extent);
    MPI_Type_create_resized(tempType, lb, extent, &mpiHeader);
    MPI_Type_commit(&mpiHeader);
    MPI_Type_free(&tempType);
    return mpiHeader;
}
//============================================================================//
/*!
 * @brief Broadcasts the SAC pole-zero structure.
 *
 * @param[in,out] pz     On input contains the SAC pole-zero structure
 *                       defined on the root node.
 * @param[in,out] pz     On exit contains the SAC Pole-zero structures
 *                       on all processes on communicator.
 * @param[in] npz        Number of pole-zero structures to broadcast.
 * @param[in] root       Root process ID on communicator.
 * @param[in] comm       MPI communicator.
 * @result MPI_SUCCESS indicates success.
 * @ingroup sac_mpi_broadcast
 */ 
int sacio_mpi_broadcastPZ(struct sacPoleZero_struct *pz, const int npz,
                          const int root, const MPI_Comm comm)
{
    int i, ierr, inum, myid;
    ierr = MPI_SUCCESS;
    MPI_Comm_rank(comm, &myid);
    for (i=0; i<npz; i++)
    {
        if (myid != root){sacio_freePoleZeros(&pz[i]);}
        // chars
        MPI_Bcast(pz[i].network,        64, MPI_CHAR, root, comm);  
        MPI_Bcast(pz[i].station,        64, MPI_CHAR, root, comm);
        MPI_Bcast(pz[i].location,       64, MPI_CHAR, root, comm); 
        MPI_Bcast(pz[i].channel,        64, MPI_CHAR, root, comm);
        MPI_Bcast(pz[i].comment,        64, MPI_CHAR, root, comm);
        MPI_Bcast(pz[i].description,    64, MPI_CHAR, root, comm);
        MPI_Bcast(pz[i].instrumentType, 64, MPI_CHAR, root, comm);
        // doubles
        MPI_Bcast(&pz[i].constant,    1, MPI_DOUBLE, root, comm);
        MPI_Bcast(&pz[i].created,     1, MPI_DOUBLE, root, comm);
        MPI_Bcast(&pz[i].start,       1, MPI_DOUBLE, root, comm);
        MPI_Bcast(&pz[i].end,         1, MPI_DOUBLE, root, comm);
        MPI_Bcast(&pz[i].latitude,    1, MPI_DOUBLE, root, comm);
        MPI_Bcast(&pz[i].longitude,   1, MPI_DOUBLE, root, comm);
        MPI_Bcast(&pz[i].elevation,   1, MPI_DOUBLE, root, comm);
        MPI_Bcast(&pz[i].depth,       1, MPI_DOUBLE, root, comm);
        MPI_Bcast(&pz[i].dip,         1, MPI_DOUBLE, root, comm);
        MPI_Bcast(&pz[i].azimuth,     1, MPI_DOUBLE, root, comm);
        MPI_Bcast(&pz[i].sampleRate,  1, MPI_DOUBLE, root, comm);
        MPI_Bcast(&pz[i].instgain,    1, MPI_DOUBLE, root, comm);
        MPI_Bcast(&pz[i].sensitivity, 1, MPI_DOUBLE, root, comm);
        MPI_Bcast(&pz[i].a0,          1, MPI_DOUBLE, root, comm);
        // ints
        MPI_Bcast(&pz[i].npoles, 1, MPI_INT, root, comm);
        MPI_Bcast(&pz[i].nzeros, 1, MPI_INT, root, comm);
        // enums
        if (myid == root){inum = (int) pz[i].inputUnits;}
        MPI_Bcast(&inum, 1, MPI_INT, root, comm);
        if (myid != root){pz[i].inputUnits = (enum sacUnits_enum) inum;}
        if (myid == root){inum = (int) pz[i].instgainUnits;}
        MPI_Bcast(&inum, 1, MPI_INT, root, comm);
        if (myid != root){pz[i].instgainUnits = (enum sacUnits_enum) inum;}
        if (myid == root){inum = (int) pz[i].sensitivityUnits;}
        MPI_Bcast(&inum, 1, MPI_INT, root, comm);
        if (myid != root){pz[i].sensitivityUnits = (enum sacUnits_enum) inum;}
        // bools
        MPI_Bcast(&pz[i].lhavePZ, 1, MPI_C_BOOL, root, comm);
        // double complex
        if (pz[i].npoles > 0)
        {
            //nalloc = (size_t) pz[i].npoles*sizeof(double complex);
            if (myid != root)
            {
                pz[i].poles = sacio_malloc64z(pz[i].npoles);
/*
#ifdef USE_POSIX
                posix_memalign((void **) &pz[i].poles, 64, nalloc);
#else
                pz[i].poles = (double complex *) aligned_alloc(64, nalloc);
#endif
*/
            }
            MPI_Bcast(pz[i].poles, pz[i].npoles, MPI_C_DOUBLE_COMPLEX,
                      root, comm);
        }
        if (pz[i].nzeros > 0)
        {
            //nalloc = (size_t) pz[i].nzeros*sizeof(double complex);
            if (myid != root)
            {
                pz[i].zeros = sacio_malloc64z(pz[i].nzeros);
/*
#ifdef USE_POSIX
                posix_memalign((void **) &pz[i].zeros, 64, nalloc);
#else
                pz[i].zeros = (double complex *) aligned_alloc(64, nalloc);
#endif
*/
            }
            MPI_Bcast(pz[i].zeros, pz[i].nzeros, MPI_C_DOUBLE_COMPLEX,
                      root, comm);
        }
    }
    return ierr;
}
//============================================================================//
/*!
 * @brief Broadcasts the SAC FAP data structure.
 * @param[in,out] fap     On input contains the SAC FAP data structures defined
 *                        on the root process.
 * @param[in,out] fap     On output all processes on communicator have same
 *                        SAC FAP structures.
 * @param[in] nfap        Number of FAP data structures to broadcast.
 * @param[in] root        Root process ID on communicator.
 * @param[in] comm        MPI communicator.
 * @result MPI_SUCCESS indicates success.
 * @ingroup sac_mpi_broadcast
 */
int sacio_mpi_broadcastFAP(struct sacFAP_struct *fap, const int nfap,
                           const int root, const MPI_Comm comm)
{
    int i, ierr, myid;
    ierr = MPI_SUCCESS;
    MPI_Comm_rank(comm, &myid);
    for (i=0; i<nfap; i++)
    {
        if (myid != root){sacio_freeFAP(&fap[i]);}
        MPI_Bcast(&fap[i].nf, 1, MPI_INT, root, comm);
        MPI_Bcast(&fap[i].lhaveFAP, 1, MPI_C_BOOL, root, comm);
        if (fap[i].nf > 0)
        {
            if (myid != root)
            {
                fap[i].freqs = sacio_malloc64f(fap[i].nf);
                fap[i].amp   = sacio_malloc64f(fap[i].nf);
                fap[i].phase = sacio_malloc64f(fap[i].nf);
            }
            MPI_Bcast(fap[i].freqs, fap[i].nf, MPI_DOUBLE, root, comm);
            MPI_Bcast(fap[i].amp,   fap[i].nf, MPI_DOUBLE, root, comm);
            MPI_Bcast(fap[i].phase, fap[i].nf, MPI_DOUBLE, root, comm); 
        }
    }
    return ierr;
}
/*!
 * @brief Broadcasts a SAC header.
 * @param[in,out] sacHeader  On input this is the SAC header defined on the
 *                           root process.
 * @param[in,out] sacHeader  On exit all processes on communicator have a 
 *                           copy of the header.
 * @param[in] nheader        Number of headers to broadcast.
 * @param[in] root           Root process ID on communicator on which sacHeader
 *                           is defined.
 * @param[in] comm           MPI communicator.
 * @result MPI_SUCCESS indicates success.
 * @ingroup sac_mpi_broadcast
 */
int sacio_mpi_broadcastHeader(struct sacHeader_struct *sacHeader,
                              const int nheader,
                              const int root, const MPI_Comm comm)
{
    int i, ierr;
    MPI_Datatype mpiHeader = sacio_mpi_createHeader();
    ierr = 0;
    for (i=0; i<nheader; i++)
    {
        ierr = MPI_Bcast(&sacHeader[i], 1, mpiHeader, root, comm);
        if (ierr != MPI_SUCCESS)
        {
            printf("%s: Error broadcasting header\n", __func__);
            goto ERROR;
        }
    }
ERROR:;
    return ierr;
}
//============================================================================//
/*!
 * @brief Broadcasts a SAC IO data structure.
 * @param[in,out] sac     On input contains the SAC data structures defined
 *                        on the root process.
 * @param[in,out] sac     On output all processes on communicator have same
 *                        sac data structure.
 * 
 * @param[in] nsac        Number of data structures to broadcast.
 * @param[in] root        Root process ID on communicator.
 * @param[in] comm        MPI communicator on which communication happens.
 * @result MPI_SUCCESS indicates success.
 * @ingroup sac_mpi_broadcast
 */
int sacio_mpi_broadcast(struct sacData_struct *sac, const int nsac,
                        const int root, const MPI_Comm comm)
{
    MPI_Datatype mpiHeader;
    //size_t nalloc;
    int i, ierr, myid;
    ierr = MPI_SUCCESS;
    MPI_Comm_rank(comm, &myid);
    mpiHeader = sacio_mpi_createHeader(); 
    for (i=0; i<nsac; i++)
    {
        if (myid != root){sacio_free(&sac[i]);}
        ierr = MPI_Bcast(&sac[i].header, 1, mpiHeader, root, comm);
        if (ierr != MPI_SUCCESS)
        {
            printf("%s: Error broadcasting header\n", __func__);
            goto ERROR;
        }
        ierr = MPI_Bcast(&sac[i].npts, 1, MPI_INT, root, comm);
        if (ierr != MPI_SUCCESS)
        {
            printf("%s: Error broadcasting npts\n", __func__);
            goto ERROR;
        }
        ierr = sacio_mpi_broadcastPZ(&sac[i].pz, 1, root, comm);
        if (ierr != MPI_SUCCESS)
        {
            printf("%s: Error broadcasting poles/zeros\n", __func__);
            goto ERROR;
        }
        ierr = sacio_mpi_broadcastFAP(&sac[i].fap, 1, root, comm);
        if (ierr != MPI_SUCCESS)
        {
            printf("%s: Error broadcasting FAP\n", __func__);
            goto ERROR;
        }
        if (sac[i].npts > 0)
        {
            if (myid != root)
            {
                sac[i].data = sacio_malloc64f(sac[i].npts);
/*
                nalloc = (size_t) sac[i].npts*sizeof(double);
#ifdef USE_POSIX
                posix_memalign((void **) &sac[i].data, 64, nalloc);
#else
                sac[i].data = (double *) aligned_alloc(64, nalloc);
#endif
*/
            }
            ierr = MPI_Bcast(sac[i].data, sac[i].npts, MPI_DOUBLE, root, comm);
            if (ierr != MPI_SUCCESS)
            {
                printf("%s: Error broadcasting data\n", __func__);
                goto ERROR;
            }
        }
    }
ERROR:;
    if (ierr != MPI_SUCCESS){printf("sacio_mpi_broadcast: Errors detected\n");}
    MPI_Type_free(&mpiHeader);
    return ierr;
}
//============================================================================//
int sacio_mpi_send(struct sacData_struct *sac, const int nsac,
                   const int dest, const int tag, const MPI_Comm comm)
{
    MPI_Datatype mpiHeader;
    int i, ierr, nsacSend;
    ierr = MPI_SUCCESS;
    mpiHeader = sacio_mpi_createHeader();
    nsacSend = nsac;
    ierr = MPI_Send(&nsacSend, 1, MPI_INT, dest, tag, comm);
    if (ierr != MPI_SUCCESS){goto ERROR;}
    for (i=0; i<nsac; i++)
    {
        ierr = MPI_Send(&sac[i].header, 1, mpiHeader, dest, tag, comm); 
        if (ierr != MPI_SUCCESS){goto ERROR;}
        ierr = MPI_Send(&sac[i].npts, 1, MPI_INT, dest, tag, comm);
        if (ierr != MPI_SUCCESS){goto ERROR;}
        if (sac[i].npts > 0)
        {
            ierr = MPI_Send(sac[i].data, sac[i].npts, MPI_DOUBLE,
                            dest, tag, comm);
            if (ierr != MPI_SUCCESS){goto ERROR;}
        }
    }
ERROR:;
    if (ierr != MPI_SUCCESS){printf("sacio_mpi_send: Errors detected\n");}
    MPI_Type_free(&mpiHeader);
    return ierr;
}
//============================================================================//
int sacio_mpi_recv(struct sacData_struct *sac, const int nsac,
                   const int source, const int tag, const MPI_Comm comm,
                   MPI_Status *status)
{
    MPI_Datatype mpiHeader;
    //size_t nalloc;
    int i, ierr, nsacRef;
    ierr = MPI_SUCCESS;
    mpiHeader = sacio_mpi_createHeader();
    ierr = MPI_Recv(&nsacRef, 1, MPI_INT, source, tag, comm, status);
    if (ierr != MPI_SUCCESS){goto ERROR;}
    if (nsacRef != nsac){printf("sacio_mpi_recv: inconsistent sizes\n");}
    for (i=0; i<nsacRef; i++)
    {
        sacio_free(&sac[i]);
        ierr = MPI_Recv(&sac[i].header, 1, mpiHeader, source,
                        tag, comm, status);
        if (ierr != MPI_SUCCESS){goto ERROR;}
        ierr = MPI_Recv(&sac[i].npts, 1, MPI_INT, source, tag, comm, status);
        if (ierr != MPI_SUCCESS){goto ERROR;}
        if (sac[i].npts > 0)
        {
            sac[i].data = sacio_malloc64f(sac[i].npts);
/*
            nalloc = (size_t) sac[i].npts*sizeof(double);
#ifdef USE_POSIX
            posix_memalign((void **) &sac[i].data, 64, nalloc);
#else
            sac[i].data = (double *) aligned_alloc(64, nalloc);
#endif
*/
            ierr = MPI_Recv(&sac[i].data, sac[i].npts, MPI_DOUBLE,
                            source, tag, comm, status);
            if (ierr != MPI_SUCCESS){goto ERROR;}
        }
    }
ERROR:;
    if (ierr != MPI_SUCCESS){printf("sacio_mpi_recv: Errors detected\n");}
    MPI_Type_free(&mpiHeader);
    return ierr;
}
