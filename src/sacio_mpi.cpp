#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "sacio_config.h"
#include "sacio_mpi.hpp"
#include "sacio_mpi.h"
#include "sacio.hpp"

/*!
 * @defgroup SacMPIpp SAC MPI Communication
 * @brief Allows for communication of SAC data structures with MPI.
 * @ingroup SacMPIpp
 */
/*!
 * @brief Default constructor.
 * @ingroup SacMPIpp
 */
SacMPI::SacMPI(void)
{
    SacMPI::clear();
    return;
}
/*!
 * @brief Default destructor.
 * @ingroup SacMPIpp
 */
SacMPI::~SacMPI(void)
{
    header_.clear();
    pz_.clear();
    data_.clear();
    return;
}
/*!
 * @brief Constructs a SAC MPI class from a given C SAC data struture.
 * @param[in] sac   SAC data structure from which to initialize class.
 * @ingroup SacMPIpp
 */
SacMPI::SacMPI(const sacData_struct sac)
{
    // Reset the class
    clear();
    // Ensure the array sizes are consistent
    int npts;
    int ierr = sacio_getIntegerHeader(SAC_INT_NPTS, sac.header, &npts);
    if (ierr != 0 || npts != sac.npts)
    {    
        if (ierr != 0){fprintf(stderr, "%s: Failed to read npts\n", __func__);}
        if (npts != sac.npts)
        {
            fprintf(stderr, "%s: Size inconsistency\n", __func__);
        }
        return;
    }
    // Initialize the header
    header_.clear();
    header_ = SacHeader(sac.header);
    // Copy poles and zeros
    pz_ = SacPoleZero(sac.pz);
    // Copy the data
    if (npts > 0)
    {    
        ierr = data_.setData(sac.npts, sac.data);
        if (ierr != 0)
        {
            fprintf(stderr, "%s: Failed to set data\n", __func__);
            clear();
        }
    }
    return;
}
/*!
 * @brief Copy constructor.
 * @param[in] sacmpi   SAC MPI data structure from which to initialize class.
 *                     This will perform a deep copy.
 * @ingroup SacMPIpp
 */
SacMPI& SacMPI::operator=(const SacMPI &sacmpi)
{
    // Self copy
    if (&sacmpi == this){return *this;}
    // Clear the class
    clear();
    // Copy
    header_ = sacmpi.header_;
    pz_     = sacmpi.pz_;
    data_   = sacmpi.data_;
    return *this;
}
/*!
 * @brief Copy constructor.
 * @param[in] sacmpi   SAC MPI data structure from which to initialize class.
 *                     This will perform a deep copy.
 * @ingroup SacMPIpp
 */
void SacMPI::clear(void)
{
    header_.clear();
    pz_.clear();
    data_.clear();
    return;
}
/*!
 * @brief Broadcasts the contents of a class defined on root to all other
 *        processes on the communicator.
 * @param[in] root   The root process ID on which the SAC class is defined.
 * @param[in] comm   The MPI communicator.
 * @result 0 indicates success.
 * @ingroup SacMPIpp
 */
int SacMPI::broadcast(const int root, const MPI_Comm comm)
{
    int ierr, ierrLoc, myid;
    // Get rank and null out data structure to broadcast
    MPI_Comm_rank(comm, &myid);
    // Assert the number of points makes sense
    int npts = 0;
    if (myid == root)
    {
        ierr = getNumberOfPoints(&npts);
        if (ierr != 0)
        {
            fprintf(stderr, "%s: Failed to get number of points\n", __func__);
        }
    }
    else
    {
        clear();
    }
    MPI_Bcast(&ierr, 1, MPI_INTEGER, root, comm);
    if (ierr != 0){return MPI_ERR_INTERN;}
    // Broadcast the header 
    ierr = sacio_mpi_broadcastHeader(&header_.header_, 1, root, comm);
    ierrLoc = 0;
    if (ierr != MPI_SUCCESS){ierrLoc = 1;} 
    if (ierrLoc != 0)
    {
        if (myid == root)
        {
            fprintf(stderr, "%s: Failed to broadcast SAC header\n",
                    __func__);
        }
        return MPI_ERR_INTERN;
    }
    // Broadcast the pole-zeros
    ierr = sacio_mpi_broadcastPZ(&pz_.pz_, 1, root, comm);
    ierrLoc = 0;
    if (ierr != MPI_SUCCESS){ierrLoc = 1;} 
    if (ierrLoc != 0)
    {
        if (myid == root)
        {
            fprintf(stderr, "%s: Failed to broadcast SAC pole-zeros\n",
                    __func__);
        }
        return MPI_ERR_INTERN;
    }
    // Broadcast the data
    uint64_t nbytes = data_.nbytes_; 
    MPI_Bcast(&nbytes, 1, MPI_UINT64_T, root, comm);
    MPI_Bcast(&data_.npts_, 1, MPI_INT, root, comm);
    if (myid != root)
    {
        data_.nbytes_ = static_cast<size_t> (nbytes);
        data_.data_ = static_cast<double *> (aligned_alloc(64, data_.nbytes_));
        memset(data_.data_, 0, data_.nbytes_);
    }
    if (data_.npts_ > 0)
    {
        MPI_Bcast(data_.data_, data_.npts_, MPI_DOUBLE, root, comm);
    }
    return MPI_SUCCESS;
}
