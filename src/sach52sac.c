#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <stdbool.h>
#include <limits.h>
#include "sacio.h"
#include "sacio_os.h"

struct options_struct
{
    char inFile[SAC_PATH_MAX];
    char outFile[SAC_PATH_MAX];
    char h5GroupName[SAC_PATH_MAX];
    char h5Dataset[SAC_PATH_MAX];
    int verbose;
};
static int parseArguments(int argc, char *argv[],
                          struct options_struct *options);
static void printUsage(void);


/*!
 * @brief Convenience function that extracts the SAC file from the H5
 *        virtual directory list. 
 *
 * @author Ben Baker
 *
 * @copyright ISTI distributed under Apache 2
 *
 */
int main(int argc, char *argv[])
{
    struct options_struct options;
    struct sacData_struct sac;
    int ierr;
    hid_t groupID, h5fl;
    // Initialize and parse the arguments
    memset(&sac, 0, sizeof(struct sacData_struct));
    ierr = parseArguments(argc, argv, &options);
    if (ierr ==-2){return EXIT_SUCCESS;}
    if (ierr ==-1)
    {
        printf("Failed parsing command line arguments\n");
        return EXIT_FAILURE;
    }
    // Verify the input file exists
    if (!sacio_os_path_isfile(options.inFile))
    {
        printf("Input HDF5 file %s does not exist\n", options.inFile);
        return EXIT_FAILURE;
    }
    // Warn if the output file will be overwritten
    if (sacio_os_path_isfile(options.outFile) && options.verbose > 0)
    {
        printf("Will overwrite %s\n", options.outFile);
    }
    // Open the HDF5 file
    h5fl = H5Fopen(options.inFile, H5F_ACC_RDONLY, H5P_DEFAULT);
    // Open the HDF5 group
    if (H5Lexists(h5fl, options.h5GroupName, H5P_DEFAULT) <= 0)
    {
        printf("Group %s does not exist\n", options.h5GroupName);
        ierr = EXIT_FAILURE;
        goto ERROR;
    }
    groupID = H5Gopen2(h5fl, options.h5GroupName, H5P_DEFAULT);
    if (H5Lexists(groupID, options.h5Dataset, H5P_DEFAULT) <= 0)
    {
        printf("Dataset %s does not exist\n", options.h5Dataset);
        ierr = EXIT_FAILURE;
        goto ERROR;
    }
    // Read the  dataset
    ierr = sacioh5_readTimeSeries2(options.h5Dataset, groupID, &sac);
    if (ierr != 0)
    {
        printf("Error loading dataset %s\n", options.h5Dataset);
        goto ERROR;
    }
    H5Gclose(groupID);
    // Write it
    ierr = sacio_writeTimeSeriesFile(options.outFile, sac);
    if (ierr != 0)
    {
        printf("Error writing file %s\n", options.outFile);
        goto ERROR;
    }
ERROR:;
    sacio_free(&sac);
    H5Fclose(h5fl);
    return EXIT_SUCCESS;
}
//============================================================================//
static int parseArguments(int argc, char *argv[],
                          struct options_struct *options)
{
    char dirName[SAC_PATH_MAX], temp[SAC_PATH_MAX];
    size_t lenos;
    int i;
    bool linFile, ldataSet, loutDir, loutFile;
    linFile = false;
    loutFile = false;
    loutDir = false;
    ldataSet = false;
    memset(options, 0, sizeof(struct options_struct));
    memset(dirName, 0, SAC_PATH_MAX*sizeof(char));
    while (true)
    {
        static struct option longOptions[] =
        {
            {"help", no_argument, 0, '?'},
            {"help", no_argument, 0, 'h'},
            {"verbose", no_argument, 0, 'v'},
            {"h5Path", required_argument, 0, 'd'},
            {"inputFile", required_argument, 0, 'i'},
            {"outputDir",  optional_argument, 0, 'o'},
            {"outputFile", optional_argument, 0, 'f'},
            {0, 0, 0, 0}
        };
        int c, optionIndex;
        c = getopt_long(argc, argv, "?hvi:d:o:f:",
                        longOptions, &optionIndex);
        if (c ==-1){break;}
        if (c == 'i')
        {
            strcpy(options->inFile, (const char *) optarg);
            linFile = true;
        }
        else if (c == 'd')
        {
            strcpy(options->h5Dataset, (const char *) optarg);
            ldataSet = true;
        }
        else if (c == 'o')
        {
            strcpy(dirName, (const char *) optarg);
            loutDir = true;
        }
        else if (c == 'f')
        {
            strcpy(options->outFile, (const char *) optarg);
            loutFile = true;
        }
        else if (c == 'h' || c == '?')
        {
            printUsage();
            return -2;
        }
        else if (c == 'v')
        {
            options->verbose = 1;
        }
        else
        {
            printf("Unknown options: %s\n", argv[optionIndex]);
        }
    }
    if (!linFile || !ldataSet)
    {
        if (!linFile){printf("Input file must be specified\n");}
        if (!ldataSet){printf("HDF5 dataset must be specified\n");}
        printUsage();
        return -1; 
    }
    lenos = strlen(options->h5Dataset);
    if (lenos == 0)
    {
        printf("H5 dataset not specified\n");
        return -1;
    }
    if (options->h5Dataset[lenos-1] == '/')
    {
        printf("H5 dataset cannot end with '/'\n");
        return -1;
    }
    // Set the output directory
    if (!loutDir)
    {
        strcpy(dirName, "./\0");
    }
    else
    {
        if (strlen(dirName) > 0)
        {
            if (sacio_os_makedirs(dirName) != 0) //ISCL_SUCCESS)
            {
                printf("Failed to make directory: %s\n", dirName);
            }
            if (dirName[strlen(dirName)-1] != '/')
            {
                strcat(dirName, "/\0");
            }
        }
        else
        {
            strcpy(dirName, "./\0");
        }
    }
    // Verify the directory exists
    if (!sacio_os_path_isdir(dirName))
    {   
        printf("Output directory %s does not exist\n", dirName);
        return -1; 
    }
    // Set the output file from
    if (!loutFile)
    {
        strcpy(options->outFile, "./\0");
        // Find the last "/"
        memset(dirName, 0, SAC_PATH_MAX*sizeof(char));
        lenos = strlen(options->h5Dataset);
        for (i=(int) lenos-1; i>=0; i--)
        {
            if (options->h5Dataset[i] == '/')
            {
                strcat(options->outFile, &options->h5Dataset[i+1]);
                break;
            }
        }
        //if (options->verbose)
        {
            printf("Setting output file to: %s\n", options->outFile);
        }
    }
    // Output file exists - require the directory exists
    else
    {
        // Find the last "/"
        memset(temp, 0, SAC_PATH_MAX*sizeof(char));
        strcpy(temp, dirName);
        strcat(temp, options->outFile);
        memset(options->outFile, 0, SAC_PATH_MAX*sizeof(char));
        strcpy(options->outFile, temp);
/*
        memset(dirName, 0, SAC_PATH_MAX*sizeof(char));
        lenos = strlen(options->outFile);
        sacio_os_dirname_work(options->outFile, dirName);
        for (i=lenos-1; i>=0; i--)
        {
            if (options->outFile[i] == '/')
            {
                strncpy(dirName, options->outFile, i);
                break;
            }
        }
        if (strlen(dirName) > 0)
        {
            if (!sacio_os_path_isdir(dirName))
            {
                printf("Output directory %s does not exist\n", dirName);
                return -1;
            }
        }
*/
    }
    // Figure out the group's directory
    lenos = strlen(options->h5Dataset);
    for (i=(int) lenos-1; i>=0; i--)
    {
        if (options->h5Dataset[i] == '/')
        {
            strncpy(options->h5GroupName, options->h5Dataset, (size_t) i);
            strcpy(temp, &options->h5Dataset[i+1]);
            memset(options->h5Dataset, 0, SAC_PATH_MAX*sizeof(char));
            strcpy(options->h5Dataset, temp);
            break;
        }
    }
    return 0;
}

static void printUsage(void)
{
    printf("Usage: sach52sac -i input_file -d /path/to/hdf5/dataset -o output_dir -f output_file\n");
    printf("\n");
    printf("Required arguments:\n");
    printf("    -i input_file specifies the input HDF5 file\n");
    printf("    -d /path/to/hdf5/dataset specifies the dataset's path the H5 file\n");
    printf("\n");
    printf("Optional arguments:\n");
    printf("    -v toggles verbose mode\n");
    printf("    -o output_dir specifies the name of the output directory\n");
    printf("    -f output_file specifies the name of the output file\n");
    printf("    -h displays this message\n\n");
    return;
}
