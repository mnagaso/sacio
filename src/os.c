#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <libgen.h>
#include <limits.h>
#include <sys/stat.h>
#include <errno.h>
#include "sacio_os.h"
#if defined WINNT || defined WIN32 || defined WIN64
#include <windows.h>
#endif

#ifndef MAX
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#endif
#ifndef MIN
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#endif

/*!
 * @defgroup sac_io_os OS
 * @brief File handling utilities.  These files have been copied from
 *        ISCL and their names appropriately modified.
 * @ingroup sac
 * @copyright ISTI distributed under the Apache 2 license.
 */

/*! 
 * @brief Tests if filenm is a file.
 * @param[in] filenm    Name of file to test.
 * @retval If true then filenm is an existing file.
 * @retval If false then filenm is not a file.
 * @ingroup sac_io_os
 */
bool sacio_os_path_isfile(const char *filenm)
{
    struct stat info;
    if (filenm == NULL){return false;}
    if (strlen(filenm) == 0){return false;}
    // Doesn't exist
    if (stat(filenm, &info) ==-1)
    {
        return false;
    }
    // Exists -> check it is a file
    else
    {
        if (S_ISREG(info.st_mode))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
/*! 
 * @brief Tests if dirnm is a directory.
 * @param[in] dirnm    Name of directory to test.
 * @retval If true true then dirnm is an existing directory.
 * @retval If false then dirnm is not a directory.
 * @ingroup sac_io_os
 */
bool sacio_os_path_isdir(const char *dirnm)
{
    struct stat s;
    int err;
    if (dirnm == NULL){return false;}
    if (strlen(dirnm) == 0){return false;}
    err = stat(dirnm, &s);
    // Doesn't exist
    if (err == -1)
    {
        if (ENOENT == errno)
        {
            return false;
        }
        // Exists
        else
        {
            return true;
        }
    }
    // Exists
    else
    {
        // Test it is a directory
        if (S_ISDIR(s.st_mode))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
/*! 
 * Makes a directory named dirnm with full permissions.
 * @param[in] dirnm    Name of directory to make.
 * @result  0 indicates success.
 * @result -1 indicates the directory name is NULL.
 * @result -2 indicates the path is empty.
 * @result -3 indicates an internal failure.
 * @ingroup sac_io_os
 */
int sacio_os_mkdir(const char *dirnm)
{
    int ierr;
    if (dirnm == NULL)
    {   
        fprintf(stderr, "%s: Directory name is NULL", __func__);
        return -1;
    }
    if (strlen(dirnm) == 0)
    {
        fprintf(stderr,  "%s: Directory name is empty", __func__);
        return -2;
    }
#if defined WINNT || defined WIN32 || defined WIN64
    ierr = mkdir(dirnm);
#else
    ierr = mkdir(dirnm, 0777);
#endif
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Error making directory %s", __func__, dirnm);
        return -3;
    }
    return 0;
}
/*!
 * @brief Breaks a null-terminated pathname string into a directory.
 *        For example, /usr/include/lapacke.h will return 
 *        /usr/include
 * @param[in] path      Name of file path to be parsed.
 * @param[out] dirName  Name of directory containing path.
 * @retval  0 indicates success.
 * @retval -1 indicates that input string is NULL.
 * @ingroup sac_io_os
 */
int sacio_os_dirname_work(const char *path, char dirName[PATH_MAX])
{
    char *dname, *temp;
    size_t lenos;
    // Initialize output
    memset(dirName, 0, PATH_MAX*sizeof(char));
    // Input string is NULL - this may yield some weirdness
    if (path == NULL)
    {
        strcpy(dirName, ".\0");
        return -1;
    }
    // Save copy of string - glibc can destroy it
    dname = NULL;
    lenos = strlen(path);
    temp = (char *) calloc(MAX(16, lenos+1), sizeof(char));
    strcpy(temp, path);
    dname = dirname(temp);
    if (dname != NULL)
    {
        lenos = strlen(dname);
        strncpy(dirName, dname, MIN(lenos, PATH_MAX));
    }
    free(temp);
    return 0;
}

/*!
 * @brief Recursive directory creation function.
 * @param[in] path    Directory tree to make.
 * @result -1 indicates the directory name is NULL.
 * @result -2 indicates the path is empty.
 * @result -3 indicates path is too long.
 * @result -4 indicates an internal failure.
 */
int sacio_os_makedirs(const char *path)
{
    char *where, *dname, *work, directory[SAC_PATH_MAX];
#if defined WINNT || defined WIN32 || defined WIN64
    const char find[] = "\\/";
#else
    const char find[] = "/\0";
#endif
    size_t indx, lenos;
    int ierr = 0;
    //------------------------------------------------------------------------//    //
    // Errors 
    ierr = 0;
    if (path == NULL)
    {
        fprintf(stderr, "%s: Directory name is NULL\n", __func__);
        return -1;
    }
    lenos = strlen(path);
    if (lenos == 0)
    {
        fprintf(stderr, "%s: Directory name is empty\n", __func__);
        return -2;
    }
    if (lenos > SAC_PATH_MAX - 2)
    {
        fprintf(stderr, "%s: Directory %s is too long", __func__, path);
        return -3;
    }
    // Already exists
    if (sacio_os_path_isdir(path)){return 0;}
    // Initialize
    work = (char *) calloc(lenos+2, sizeof(char));
    strcpy(work, path);
    memset(directory, 0, SAC_PATH_MAX*sizeof(char));
    dname = work;
#if defined WINNT || defined WIN32 || defined WIN64
    dname[lenos] = '\0'; // No idea what to do
#else
    dname[lenos] = '/'; // Try to catch the final case
#endif
    where = strpbrk(dname, find);
    while ((where != NULL))
    {
        indx = (size_t) (where - dname);
        lenos = strlen(directory);
        strncat(directory, dname, indx);
        // Windows does nonsense like: C:
#if defined WINNT || defined WIN32 || defined WIN64
        if (directory[strlen(directory) - 1] != ':')
#endif
        {
            // If directory doesn't exist then make it
            if (!sacio_os_path_isdir(directory))
            {
                ierr = sacio_os_mkdir(directory);
                if (ierr != 0)
                {
                    fprintf(stderr, "%s: Error making subdirectory: %s",
                            __func__, directory);
                    fprintf(stderr, "%s: Error making directory: %s\n",
                            __func__, path);
                    free(dname);
                    return ierr;
                }
            } // End check on if directory exists
        }
        // Add directory delimiter
#if defined WINNT || defined WIN32 || defined WIN64
        strcat(directory, "//\0");
#else
        strcat(directory, "/\0");
#endif
        dname = dname + indx + 1;
        where = strpbrk(dname, find);
    } // End while
    // Add name of final subdirectory and make it
    strcat(directory, dname);
    if (!sacio_os_path_isdir(directory))
    {
         ierr = sacio_os_mkdir(directory);
         if (ierr != 0)
         {
             fprintf(stderr, "%s: Error making directory: %s",
                     __func__, directory);
             free(work);
             return ierr;
         }
    }
    // Free space
    dname = NULL;
    free(work);
    return ierr;
}
