#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include "sacioh5.h"

#define PAD 0

/*!
 * @defgroup sac_h5 SAC HDF5
 * @brief SAC and HDF5 file IO utilities.
 * @copyright ISTI distributed under the Apache 2 license. 
 */
/*!
 * @defgroup sac_h5_header_utils Header Access
 * @brief Accesses and modifies header variables in the HDF5 SAC structure.
 * @ingroup sac_h5
 * @copyright ISTI distributed under the Apache 2 license.
 */
/*!
 * @defgroup sac_h5_memory Memory
 * @brief Utilties for allocating and deallocating memory on the HDF5
 *        SAC structure.
 * @ingroup sac_h5
 * @copyright ISTI distributed under the Apache 2 license.
 */
/*!
 * @defgroup sac_h5_data_type Data Type
 * @brief Creates data types for reading/writing SAC data from/to HDF5 files.
 * @ingroup sac_h5
 * @copyright ISTI distributed under the Apache 2 license.
 */
/*!
 * @defgroup sac_h5_io Input/Output
 * @brief Utilities for reading/writing SAC structures from/to an HDF5 file.
 * @ingroup sac_h5
 * @copyright ISTI distributed under the Apache 2 license.
 */

#define SACH5_HEADER_STRUCT_NAME "sacH5Header_struct"
#define SACH5_DATA_STRUCT_NAME "sacH5Data_struct"
#define SACH5_PZ_STRUCT_NAME "sacH5PoleZero_struct"
#define SACH5_FAP_STRUCT_NAME "sacH5FAP_struct"
#ifndef MAX
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#endif
#ifndef MIN
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#endif
#ifndef DI
#define DI (__extension__ 1.0i)
#endif
#ifndef DCMPLX
#define DCMPLX(r,i) ((double) (r) + (double) (i)*DI)
#endif


static char *strdup_private(const char *s);
static char *strdup_private(const char *s)
{
    char *sdup;
    size_t lenos;
    lenos = strlen(s) + 1;
    sdup = (char *) calloc(lenos, sizeof(char)); 
    strcpy(sdup, s);
    return sdup;    
}

static size_t PAD_FLOAT64(const size_t ALIGNMENT, const size_t n)
{
    size_t paddingElements = ALIGNMENT/sizeof(double); 
    size_t padding = n + (paddingElements - n%paddingElements);
    return padding;
}
/*
#define PAD_FLOAT64(ALIGNMENT, n) \
({ \
 size_t paddingElements = ALIGNMENT/sizeof(double); \
 size_t padding = n + (paddingElements - n%paddingElements); \
 padding; \
})
*/

/*
#define PAD_FLOAT32(ALIGNMENT, n) \
({ \
 size_t paddingElements = ALIGNMENT/sizeof(float); \
 size_t padding = n + (paddingElements - n%paddingElements); \
 padding; \
})
*/

static void readChar8(const hvl_t t, char *__restrict__ kvar);
static void readChar16(const hvl_t t, char *__restrict__ kvar);
static void readChar64(const hvl_t t, char *__restrict__ kvar);
static void writeChar8(const char *__restrict__ kvar, hvl_t *t);
static void writeChar16(const char *__restrict__ kvar, hvl_t *t);
static void writeChar64(const char *__restrict__ kvar, hvl_t *t);
static void freeHVLt(hvl_t *t);
static void sacPZ2sach5PZ(const struct sacPoleZero_struct pz,
                          struct sacH5PoleZero_struct *pzh5);
static void sacFAP2sach5FAP(const struct sacFAP_struct fap, 
                            struct sacH5FAP_struct *faph5);
static void sacHeader2sach5Header(const struct sacHeader_struct sac, 
                                  struct sacH5Header_struct *sach5);
static void sach5PZ2sacPZ(const struct sacH5PoleZero_struct sach5PZ,
                          struct sacPoleZero_struct *pz);
static void sach5Header2sacHeader(const struct sacH5Header_struct sach5,
                                  struct sacHeader_struct *sac);
static void sach5FAP2sacFAP(const struct sacH5FAP_struct sach5FAP,
                            struct sacFAP_struct *fap);
struct fileList_struct
{
    char **files;
    int nfiles;
    int maxSpace;
};

static herr_t fileOpFunc(hid_t locID, const char *name, const H5L_info_t *info,
                         void *operatorData);
/*
int sacio_freeH5(struct sacH5Data )
{
    int ierr;
    ierr = sacio_freeH5header(&sac->header);
    return 0;
}
*/

/*!
 * @brief Frees the SAC H5 data structure.
 *
 * @param[out] data     On exit all memory has been released and values
 *                      set to 0 or NULL.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_memory 
 *
 * @author Ben Baker
 *
 */
int sacioh5_freeData(struct sacH5Data_struct *data)
{
    struct sacH5Header_struct *header = NULL;
    struct sacH5PoleZero_struct *pz = NULL;
    struct sacH5FAP_struct *fap = NULL;
    double *x = NULL;
    header = (struct sacH5Header_struct *) data->header.p;
    if (header != NULL && data->header.len > 0)
    {
        sacioh5_freeHeader(header);
        free(header);
    }
    pz = (struct sacH5PoleZero_struct *) data->pz.p;
    if (pz != NULL && data->pz.len > 0)
    {
        sacioh5_freePoleZero(pz);
        free(pz);
    }
    fap = (struct sacH5FAP_struct *) data->fap.p;
    if (fap != NULL && data->fap.len > 0)
    {
        sacioh5_freeFAP(fap);
        free(fap);
    }
    x = (double *) data->data.p;
    if (x != NULL){free(x);} //memory_free64f(&x);
    memset(data, 0, sizeof(struct sacH5Data_struct));
    x = NULL;
    return 0;
}
//============================================================================//
/*!
 * @brief Frees the SAC H5 header.
 *
 * @param[out] header    On exit all memory has been released and values
 *                       set to 0 or NULL.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_memory
 *
 * @author Ben Baker
 *
 */
int sacioh5_freeHeader(struct sacH5Header_struct *header)
{
    freeHVLt(&header->kstnm);
    freeHVLt(&header->kevnm);
    freeHVLt(&header->khole);
    freeHVLt(&header->ko);
    freeHVLt(&header->ka);
    freeHVLt(&header->kt0);
    freeHVLt(&header->kt1);
    freeHVLt(&header->kt2);
    freeHVLt(&header->kt3);
    freeHVLt(&header->kt4);
    freeHVLt(&header->kt5);
    freeHVLt(&header->kt6);
    freeHVLt(&header->kt7);
    freeHVLt(&header->kt8);
    freeHVLt(&header->kt9);
    freeHVLt(&header->kf);
    freeHVLt(&header->kuser0);
    freeHVLt(&header->kuser1);
    freeHVLt(&header->kuser2);
    freeHVLt(&header->kcmpnm);
    freeHVLt(&header->knetwk);
    freeHVLt(&header->kdatrd);
    freeHVLt(&header->kinst);
    memset(header, 0, sizeof(struct sacH5Header_struct));
    return 0;
}
//============================================================================//
/*!
 * @brief Frees the SAC H5 pole-zero structure.
 *
 * @param[out] pz    On exit all memory has been released and values
 *                   set to 0 or NULL.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_memory
 *
 * @author Ben Baker
 *
 */
int sacioh5_freePoleZero(struct sacH5PoleZero_struct *pz)
{
    freeHVLt(&pz->network);
    freeHVLt(&pz->station);
    freeHVLt(&pz->location);
    freeHVLt(&pz->channel);
    freeHVLt(&pz->comment);
    freeHVLt(&pz->description);
    freeHVLt(&pz->instrumentType);
    freeHVLt(&pz->polesRe);
    freeHVLt(&pz->polesIm);
    freeHVLt(&pz->zerosRe);
    freeHVLt(&pz->zerosIm);
    memset(pz, 0, sizeof(struct sacH5PoleZero_struct));
    return 0;
}
//============================================================================//
/*!
 * @brief Frees the SAC H5 FAP structure.
 *
 * @param[out] fap   On exit all memory has been released and values
 *                   set to 0 or NULL.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_memory
 *
 * @author Ben Baker
 *
 */
int sacioh5_freeFAP(struct sacH5FAP_struct *fap)
{
    freeHVLt(&fap->freqs);
    freeHVLt(&fap->amp);
    freeHVLt(&fap->phase);
    memset(fap, 0, sizeof(struct sacH5FAP_struct));
    return 0;
}
//============================================================================//
/*!
 * @brief Loads SAC data where the traces are stored in a data chunk.
 *
 * @param[in] dataSetName   Name of dataset to read.
 * @param[in] groupID       HDF5 group in which the dataset resides.
 * @param[out] ntrace       Number of SAC traces read.
 * @param[out] sacOut       An array of SAC data structures of dimension
 *                          [ntrace].  This can be freed with free().
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_io
 *
 * @author Ben Baker
 *
 */
int sacioh5_readChunkedTraces(const char *dataSetName,
                              const hid_t groupID,
                              int *ntrace,
                              struct sacData_struct **sacOut)
{
    char *fapName, *dataName, *hdrName, *pzName;
    struct sacH5Header_struct *sacH5Header;
    struct sacData_struct *sac = NULL;
    double *data;
    bool lhaveFAP, lhavePZ;
    int i, nptsMax, rank;
    hid_t dataSet, dataSpace, dataType, memSpace;
    hsize_t dims[2];
    herr_t status;
    // Set the data set names
    *ntrace = 0;
    lhaveFAP = false;
    lhavePZ  = false;
    dataName = (char *) calloc(strlen(dataSetName) + 64, sizeof(char));
    hdrName  = (char *) calloc(strlen(dataSetName) + 64, sizeof(char));
    pzName   = (char *) calloc(strlen(dataSetName) + 64, sizeof(char));
    fapName  = (char *) calloc(strlen(dataSetName) + 64, sizeof(char));
    strcpy(dataName, dataSetName);
    strcat(dataName, "_data\0");
    strcpy(hdrName,  dataSetName);
    strcat(hdrName,  "_header\0");
    strcpy(pzName,   dataSetName);
    strcat(pzName,   "_pz\0");
    strcpy(fapName,  dataSetName);
    strcat(fapName,  "_fap\0");
    if (H5Lexists(groupID, dataName, H5P_DEFAULT) < 0)
    {
        fprintf(stderr, "%s: Data %s does not exist\n", __func__, dataName);
        goto CLEAN_CHAR;
    }
    if (H5Lexists(groupID, hdrName, H5P_DEFAULT) < 0)
    {
        fprintf(stderr, "%s: Headers %s do not exist\n", __func__, hdrName);
        goto CLEAN_CHAR;
    }
    if (H5Lexists(groupID, pzName, H5P_DEFAULT) > 0){lhavePZ = true;}
    if (H5Lexists(groupID, fapName, H5P_DEFAULT) > 0){lhaveFAP = true;}
    if (lhaveFAP)
    {
        fprintf(stderr, "%s: fap read isn't done yet\n", __func__);
    }
    if (lhavePZ)
    {
        fprintf(stderr, "%s: pz read isn't done yet\n", __func__);
    }
    // Read the headers
    dataSet   = H5Dopen(groupID, hdrName, H5P_DEFAULT);
    dataSpace = H5Dget_space(dataSet);
    dataType  = H5Topen(groupID, SACH5_HEADER_STRUCT_NAME, H5P_DEFAULT);
    // Verify size
    rank = H5Sget_simple_extent_ndims(dataSpace);
    if (rank != 1)
    {
        fprintf(stderr, "%s: headers must be 1d; rank=%d\n", __func__, rank);
        return -1;
    } 
    status = H5Sget_simple_extent_dims(dataSpace, dims, NULL);
    *ntrace = (int) dims[0];
    // Read it
    memSpace = H5Screate_simple(rank, dims, NULL);
    sacH5Header = (struct sacH5Header_struct *)
                  calloc((size_t) *ntrace, sizeof(struct sacH5Header_struct)); 
    status = H5Dread(dataSet, dataType, memSpace, dataSpace,
                     H5P_DEFAULT, sacH5Header);
    if (status < 0)
    {
        fprintf(stderr, "%s: Error reading headers\n", __func__);
    }
    H5Sclose(memSpace);
    H5Tclose(dataType);
    H5Sclose(dataSpace);
    H5Dclose(dataSet);
    // Read the data
    dataSet = H5Dopen(groupID, dataName, H5P_DEFAULT);
    dataSpace = H5Dget_space(dataSet);
    rank = H5Sget_simple_extent_ndims(dataSpace);
    if (rank != 2)
    {
        fprintf(stderr, "%s: data must be in rank 2 matrix; rank=%d\n",
                __func__, rank);
        return -1;
    }
    status = H5Sget_simple_extent_dims(dataSpace, dims, NULL);
    if ((size_t) *ntrace != dims[0])
    {
        fprintf(stderr, "%s: Inconsistent trace sizing\n", __func__);
    } 
    nptsMax = (int) dims[1]; 
    data = sacio_malloc64f(nptsMax*(*ntrace));
    memSpace = H5Screate_simple(rank, dims, NULL);
    status = H5Dread(dataSet, H5T_NATIVE_DOUBLE, memSpace, dataSpace,
                     H5P_DEFAULT, data);
    if (status < 0)
    {
        fprintf(stderr, "%s: Error reading data\n", __func__);
        return -1;
    }
    H5Sclose(memSpace);
    H5Sclose(dataSpace);
    H5Dclose(dataSet);
    // Create the SAC traces
    sac = (struct sacData_struct *)
          calloc((size_t) *ntrace, sizeof(struct sacData_struct)); 
    for (i=0; i<*ntrace; i++)
    {
        sach5Header2sacHeader(sacH5Header[i], &sac[i].header);
        sac[i].npts = sac[i].header.npts;
        sac[i].data = sacio_malloc64f(sac[i].npts);
        memcpy(sac[i].data, &data[i*nptsMax],
               (size_t) sac[i].npts*sizeof(double));
    }
    // Free memory
    sacio_free64f(&data);
    free(sacH5Header);
CLEAN_CHAR:;
    free(dataName);
    free(hdrName);
    free(pzName);
    free(fapName);
    *sacOut = sac;
    return 0;
}
//============================================================================//
/*!
 * @brief Writes a chunk of SAC files.  This can be more efficient than 
 *        writing SAC files one-by-one.
 *
 * @param[in] dataSetName   The root name of the dataset to write.  Two items
 *                          will be generated in groupID.  The first will be
 *                          dataSetName_headers and the second will be 
 *                          dataSetName_data.
 * @param[in] ntrace        Number of traces to write.
 * @param[in] compress      Negative or 0 will yield no compression. \n
 *                          1 is the best compression speed but achieves the 
 *                          least compression. \n
 *                          2-8 have decreasing compression speed but 
 *                          increasingly improved compression. \n
 *                          9 is the slowest but achieves the greatest
 *                          compression. 
 * @param[in] groupID       HDF5 group handle to where the SAC traces will be
 *                          written.
 * @param[in] traceChunkSz  Optional tuning parameter.  If not NULL then this
 *                          is the chunk size for the hyperslab in the trace
 *                          (slow) dimension.
 * @param[in] dataChunkSz   Optional tuning parameter.  If not NULL then this
 *                          is the chunk size for the hyperslab in the data
 *                          (fast) dimension. 
 * @param[in] sac           SAC traces to write.  This is an array of dimension
 *                          [ntrace].
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_io
 *
 * @author Ben Baker
 *
 */
int sacioh5_writeTracesToChunk(const char *dataSetName,
                               const int ntrace,
                               const int compress,
                               const hid_t groupID,
                               const size_t *traceChunkSz,
                               const size_t *dataChunkSz,
                               const struct sacData_struct *sac)
{
    char *dataName, *fapName, *hdrName, *pzName;
    double *data;
    size_t nbytes;
    hsize_t dims[2], cdims[2];
    hid_t dataSet, dataSpace, dataType, plistID;
    int i, ierr, nptsMax, ntraceMax;
    bool lhaveFAP, lhavePZ;
    struct sacH5Header_struct *sacH5Header = NULL;
    struct sacH5PoleZero_struct *sacH5PZ = NULL;
    struct sacH5FAP_struct *sacH5FAP = NULL;
    const hsize_t chunkTrace = 16;
    const hsize_t chunkData  = 256;
    if (ntrace < 1 || sac == NULL)
    {
        if (ntrace < 1)
        {
            fprintf(stderr, "%s: Error ntrace must be positive\n", __func__);
        }
        if (sac == NULL)
        {
            fprintf(stderr, "%s: Error sac is NULL\n", __func__);
        }
        return -1;
    }
    // Get the max number of data points and do some data checks
    lhavePZ = false;
    lhaveFAP = false;
    nptsMax = sac[0].npts;
    for (i=0; i<ntrace; i++)
    {
        nptsMax = MAX(nptsMax, sac[i].npts);
        if (sac[i].npts != sac[i].header.npts)
        {
            fprintf(stderr, "%s: Trace %d has inconsistent npts\n",
                     __func__, i+1);
            return -1;
        }
        if (sac[i].header.delta <= 0.0)
        {
            fprintf(stderr, "%s: Trace %d has invalid sampling period\n",
                    __func__, i+1);
            return -1;
        }
        if (sac[i].data == NULL)
        {
            fprintf(stderr, "%s: Trace %d's data is NULL\n", __func__, i+1);
            return -1;
        }
        if (sac[i].pz.lhavePZ){lhavePZ = true;}
        if (sac[i].fap.lhaveFAP){lhaveFAP = true;}
    }
    if (nptsMax < 1)
    {
        fprintf(stderr, "%s: There is no data to write\n", __func__);
        return -1;
    }
    // Make sure the data types exist to write
    ierr = sacioh5_createHeaderType(groupID);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Error creating header type\n", __func__);
        return -1;
    }
    if (lhavePZ)
    {
        ierr = sacio_createPoleZeroType(groupID);
        if (ierr != 0)
        {
            fprintf(stderr, "%s: Error creating PZ type\n", __func__);
            return -1;
        }
    }
    if (lhaveFAP)
    {
        ierr = sacio_createFAPType(groupID);
        if (ierr != 0)
        {
            fprintf(stderr, "%s: Error creating FAP type\n", __func__);
            return -1;
        }
    }
    // Set the data set names
    dataName = (char *) calloc(strlen(dataSetName) + 64, sizeof(char));
    hdrName  = (char *) calloc(strlen(dataSetName) + 64, sizeof(char));
    pzName   = (char *) calloc(strlen(dataSetName) + 64, sizeof(char));
    fapName  = (char *) calloc(strlen(dataSetName) + 64, sizeof(char));
    strcpy(dataName, dataSetName);
    strcat(dataName, "_data\0");
    strcpy(hdrName,  dataSetName);
    strcat(hdrName,  "_header\0");
    strcpy(pzName,   dataSetName);
    strcat(pzName,   "_pz\0");
    strcpy(fapName,  dataSetName);
    strcat(fapName,  "_fap\0");
    if (H5Lexists(groupID, dataName, H5P_DEFAULT) > 0) 
    {    
        printf("%s: Dataset %s exists, skipping\n", __func__, dataName);
        goto CLEAN_CHAR;
    }
    if (H5Lexists(groupID, hdrName, H5P_DEFAULT) > 0)
    {    
        printf("%s: Dataset %s exists, skipping\n", __func__, hdrName);
        goto CLEAN_CHAR;
    }
    if (H5Lexists(groupID, pzName, H5P_DEFAULT) > 0)
    {    
        printf("%s: Dataset %s exists, skipping\n", __func__, pzName);
        goto CLEAN_CHAR;
    }
    if (H5Lexists(groupID, fapName, H5P_DEFAULT) > 0)
    {    
        printf("%s: Dataset %s exists, skipping\n", __func__, fapName);
        goto CLEAN_CHAR;
    }
    // Copy the sac header
    sacH5Header = (struct sacH5Header_struct *)
                  calloc((size_t) ntrace, sizeof(struct sacH5Header_struct));
    for (i=0; i<ntrace; i++)
    {
        sacHeader2sach5Header(sac[i].header, &sacH5Header[i]);
    }
    // Open the requisite structure and create the dataspaces
    dataType  = H5Topen(groupID, SACH5_HEADER_STRUCT_NAME, H5P_DEFAULT);
    dims[0] = (hsize_t) ntrace;
    dataSpace = H5Screate_simple(1, dims, NULL);
    dataSet   = H5Dcreate(groupID, hdrName, dataType,
                          dataSpace,
                          H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataSet, dataType, H5S_ALL, H5S_ALL, H5P_DEFAULT, sacH5Header);
    H5Tclose(dataType);
    H5Sclose(dataSpace);
    H5Dclose(dataSet);
    // Copy and write the pole-zeros
    sacH5PZ = NULL;
    if (lhavePZ)
    {    
        sacH5PZ = (struct sacH5PoleZero_struct *)
                  calloc((size_t) ntrace, sizeof(struct sacH5PoleZero_struct));
        for (i=0; i<ntrace; i++)
        {
            sacPZ2sach5PZ(sac[i].pz, &sacH5PZ[i]);
        }
        dataType  = H5Topen(groupID, SACH5_PZ_STRUCT_NAME, H5P_DEFAULT);
        dims[0] = (hsize_t) ntrace;
        dataSpace = H5Screate_simple(1, dims, NULL);
        dataSet   = H5Dcreate(groupID, pzName, dataType,
                              dataSpace,
                              H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        H5Dwrite(dataSet, dataType, H5S_ALL, H5S_ALL, H5P_DEFAULT, sacH5PZ);
        H5Tclose(dataType);
        H5Sclose(dataSpace);
        H5Dclose(dataSet);
        for (i=0; i<ntrace; i++)
        {
            sacioh5_freePoleZero(&sacH5PZ[i]);
        }
        free(sacH5PZ);
    }    
    // Copy and write the FAP
    sacH5FAP = NULL;
    if (lhaveFAP)
    {    
        sacH5FAP = (struct sacH5FAP_struct *)
                   calloc((size_t) ntrace, sizeof(struct sacH5FAP_struct));
        for (i=0; i<ntrace; i++)
        {
            sacFAP2sach5FAP(sac[i].fap, &sacH5FAP[i]);
        }
        dataType  = H5Topen(groupID, SACH5_FAP_STRUCT_NAME, H5P_DEFAULT);
        dims[0] = (hsize_t) ntrace;
        dataSpace = H5Screate_simple(1, dims, NULL);
        dataSet   = H5Dcreate(groupID, fapName, dataType,
                              dataSpace,
                              H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        H5Dwrite(dataSet, dataType, H5S_ALL, H5S_ALL, H5P_DEFAULT, sacH5FAP);
        H5Tclose(dataType);
        H5Sclose(dataSpace);
        H5Dclose(dataSet);
        for (i=0; i<ntrace; i++)
        {
            sacioh5_freeFAP(&sacH5FAP[i]);
        }
        free(sacH5FAP);
    }
    // Pad nptsMax
    ntraceMax = (int) (PAD_FLOAT64(64, (size_t) ntrace));
    nptsMax   = (int) (PAD_FLOAT64(64, (size_t) nptsMax));
    // Extract the data
    nbytes = (size_t) (nptsMax*ntraceMax)*sizeof(double); 
    data = sacio_malloc64f(nptsMax*ntraceMax);
    memset(data, 0, nbytes);
    // Now copy the data
    for (i=0; i<ntrace; i++)
    {
        nbytes = (size_t) sac[i].npts*sizeof(double); 
        memcpy(&data[i*nptsMax], sac[i].data, nbytes);
    }
    // Create the dataset
    plistID  = H5Pcreate(H5P_DATASET_CREATE);
    dims[0] = (hsize_t) ntrace;
    dims[1] = (hsize_t) nptsMax;
    dataSpace = H5Screate_simple(2, dims, NULL);
    cdims[0] = MIN(chunkTrace, (size_t) ntrace);
    cdims[1] = MIN(chunkData,  (size_t) nptsMax);
    if (traceChunkSz != NULL)
    {
        cdims[0] = MIN((size_t) ntrace,  MAX(1, *traceChunkSz));
    }
    if (dataChunkSz  != NULL)
    {
        cdims[1] = MIN((size_t) nptsMax, MAX(1, *dataChunkSz));
    }
    H5Pset_chunk(plistID, 2, cdims);
    if (compress > 0)
    {
        H5Pset_deflate(plistID, MIN(9, (unsigned int) compress));
    }
    dataSet = H5Dcreate2(groupID, dataName, H5T_NATIVE_DOUBLE,
                         dataSpace, H5P_DEFAULT, plistID, H5P_DEFAULT);
    // Write the data
    H5Dwrite(dataSet, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
    // Free HDF5 resources
    H5Dclose(dataSet);
    H5Sclose(dataSpace);
    H5Pclose(plistID);
    // Clean up
    sacio_free64f(&data);
CLEAN_CHAR:;
    free(sacH5Header);
    free(dataName);
    free(hdrName);
    free(pzName);
    free(fapName);
    return 0;
}
//============================================================================//
/*!
 * @brief Writes a SAC structure to HDF5.
 *
 * @param[in] dataSetName  Name of dataset to write.
 * @param[in] groupID      HDF5 group handle where dataSetName will be
 *                         written.
 * @param[in] sac          SAC data structure to write.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_io
 *
 * @author Ben Baker
 *
 */
int sacioh5_writeTimeSeries2(const char *dataSetName,
                             const hid_t groupID,
                             const struct sacData_struct sac)
{
    struct sacH5Header_struct *sacH5Header;
    struct sacH5PoleZero_struct *sacH5PZ;
    struct sacH5FAP_struct *sacH5FAP;
    struct sacH5Data_struct sach5;
    double *x;
    hid_t dataSet, dataSpace, dataType;
    herr_t status;
    //size_t nalloc;
    const hsize_t dimInfo[1] = {1};
    int ierr;
    //------------------------------------------------------------------------//
    //
    // Initialize
    ierr = 0;
    memset(&sach5, 0, sizeof(struct sacH5Data_struct));
    // Some fidelity checks
    if (sac.header.npts != sac.npts || sac.npts < 1 || sac.data == NULL ||
        sac.header.delta <= 0.0)
    {
        if (sac.header.npts != sac.npts)
        {
            printf("%s: Header and npts inconsistent\n", __func__);
        }
        if (sac.npts < 1){printf("%s: Error no data points\n", __func__);}
        if (sac.data == NULL){printf("%s: Error data is NULL\n", __func__);}
        if (sac.header.delta <= 0.0)
        {
            printf("%s: sac dt is invalid\n", __func__);
        }
        return -1;
    }
    // Make sure the dataset doesn't already exist
    if (H5Lexists(groupID, dataSetName, H5P_DEFAULT) > 0)
    {
        printf("%s: Dataset %s exists, skipping\n", __func__, dataSetName);
        return 0;
    }
    // Make sure the structures exists to write to
    ierr = sacioh5_createDataType(groupID);
    if (ierr != 0)
    {
        printf("%s: Failed to create structures\n", __func__);
        H5Gclose(groupID);
        return -1;
    }
    // Copy the sac header
    sacH5Header = (struct sacH5Header_struct *)
                  calloc(1, sizeof(struct sacH5Header_struct));
    sacHeader2sach5Header(sac.header, sacH5Header);
    sach5.header.p   = (void *) sacH5Header;
    sach5.header.len = 1;
    // Copy the pole-zeros
    sacH5PZ = NULL;
    if (sac.pz.lhavePZ)
    {
        sacH5PZ = (struct sacH5PoleZero_struct *)
                  calloc(1, sizeof(struct sacH5PoleZero_struct));
        sacPZ2sach5PZ(sac.pz, sacH5PZ);
        sach5.pz.p = (void *) sacH5PZ;
        sach5.pz.len = 1;
    }
    // Copy the FAP
    sacH5FAP = NULL;
    if (sac.fap.lhaveFAP)
    {
        sacH5FAP = (struct sacH5FAP_struct *)
                   calloc(1, sizeof(struct sacH5FAP_struct));
        sacFAP2sach5FAP(sac.fap, sacH5FAP);
        sach5.fap.p = (void *) sacH5FAP;
        sach5.pz.len = 1;
    } 
    // Copy the data
    x = sacio_malloc64f(sac.npts); // This is freed by sacioh5_freeData
    memcpy(x, sac.data, (size_t) sac.npts*sizeof(double));
    //ierr = array_copy64f_work(sac.npts, sac.data, x); //sach5.data.p);
    sach5.data.p = (void *) x;
    sach5.data.len = (hsize_t) sac.npts;
    // Open the requisite structure and create the dataspaces
    dataType  = H5Topen(groupID, SACH5_DATA_STRUCT_NAME, H5P_DEFAULT); 
    dataSpace = H5Screate_simple(1, dimInfo, NULL);
    dataSet   = H5Dcreate(groupID, dataSetName, dataType,
                          dataSpace,
                          H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    // Write it
    status = H5Dwrite(dataSet, dataType, H5S_ALL, H5S_ALL, H5P_DEFAULT, &sach5);
    if (status < 0)
    {
        printf("%s: Failed to write dataset %s\n", __func__, dataSetName);
        ierr = 1;
    }
    // Release H5 resources
    status  = H5Dclose(dataSet);
    status += H5Sclose(dataSpace);
    status += H5Tclose(dataType);
    // Free the structure
    sacioh5_freeData(&sach5);
    if (status != 0 || ierr != 0)
    {
        if (status != 0)
        {
            printf("%s; Failed to release h5 memory\n", __func__);
        }
        return -1;
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Writes a SAC structure to HDF5.
 *
 * @param[in] dataSetName  Name of dataset to write.
 * @param[in] groupName    HDF5 group name where dataSetName will be
 *                         written.
 * @param[in] fileID       HDF5 file handle.
 * @param[in] sac          SAC data structure to write.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_io
 *
 * @author Ben Baker
 *
 */
int sacioh5_writeTimeSeries1(const char *dataSetName,
                             const char *groupName, const hid_t fileID,
                             const struct sacData_struct sac)
{
    int ierr;
    hid_t groupID;
    htri_t lexists;
    herr_t status;
    // Check if the group exists
    lexists = H5Lexists(fileID, groupName, H5P_DEFAULT);
    // Open the group
    if (lexists > 0)
    {
        groupID = H5Gopen2(fileID, groupName, H5P_DEFAULT);
    }
    // Create it
    else
    {
        groupID = H5Gcreate2(fileID, groupName, H5P_DEFAULT,
                             H5P_DEFAULT, H5P_DEFAULT);
    }
    // Write it
    ierr = sacioh5_writeTimeSeries2(dataSetName, groupID, sac);
    // Close it up
    status = H5Gclose(groupID);
    if (status != 0 || ierr != 0)
    {
        if (status != 0)
        {
            printf("%s: Failed to close group %s\n", __func__, groupName);
        }
        if (ierr != 0){printf("%s: Failed to write sac structure\n", __func__);}
        return -1;
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Utility for getting the names of the SAC files in a group.
 *
 * @param[in] groupID   HDF5 group handle.
 *
 * @param[out] nfiles   Number of files in group.
 * @param[out] ierr     0 indicates success.
 *
 * @result char ** null terminated list of file names in group with dimension
 *         nfiles.
 *
 * @ingroup sac_h5_io
 *
 * @author Ben Baker
 *
 */
char **sacioh5_getFilesInGroup(const hid_t groupID,
                               int *nfiles, int *ierr)
{
    char **files;
    struct fileList_struct flist;
    int i;
    herr_t status;
    hsize_t nobj;

    *ierr = 0; 
    *nfiles = 0;
    files = NULL;
    memset(&flist, 0, sizeof(struct fileList_struct));
    // Get the number of objects in the group
    status = H5Gget_num_objs(groupID, &nobj);
    if (status < 0)
    {
        printf("%s: Could not get number of objects group\n", __func__);
        *ierr = 1;
        return files; 
    }
    // Group is empty
    if (nobj < 1)
    {
        *ierr = 0;
        return files;
    }
    // Group
    flist.files = (char **) calloc((size_t) nobj, sizeof(char *));
    flist.maxSpace = (int) nobj;
    status = H5Literate(groupID, H5_INDEX_NAME, H5_ITER_NATIVE,
                        NULL, fileOpFunc, (void *) &flist); 
    if (status < 0)
    {
        printf("%s: Error getting file list\n", __func__);
        if (flist.files != NULL)
        {
            for (i=0; i<flist.nfiles; i++)
            {
                if (flist.files[i] != NULL){free(flist.files[i]);}
            }    
            free(flist.files);
        }
    }
    else
    {
        *nfiles = flist.nfiles;
        files = (char **) calloc((size_t) flist.nfiles, sizeof(char *));
        for (i=0; i<flist.nfiles; i++)
        {
            files[i] = strdup_private(flist.files[i]);
            free(flist.files[i]);
        }
        free(flist.files);
    }
    memset(&flist, 0, sizeof(struct fileList_struct));
    return files;
} 
//============================================================================//
static herr_t fileOpFunc(hid_t locID, const char *name, const H5L_info_t *info,
                  void *operatorData)
{
    struct fileList_struct *flist;
    herr_t status;
    H5O_info_t infoBuf;
    status = H5Oget_info_by_name(locID, name, &infoBuf, H5P_DEFAULT);
    if (status < 0)
    {
        printf("%s: Warning - status < 0; breaking\n", __func__);
        return status;
    }
    switch (infoBuf.type)
    {
        case H5O_TYPE_DATASET:
            flist = (struct fileList_struct *) operatorData;
            if (flist->nfiles >= flist->maxSpace)
            {
                printf("%s: Critical error - out of segfault\n", __func__);
                return -1;
            }
            flist->files[flist->nfiles] = strdup_private(name);
            flist->nfiles = flist->nfiles + 1;
            break;
        case H5O_TYPE_UNKNOWN:
            printf("%s: Unknown type\n", __func__);
            break;
        case H5O_TYPE_GROUP:
            break;
        case H5O_TYPE_NAMED_DATATYPE:
            break;
        case H5O_TYPE_NTYPES:
            break;
         
    };
    return 0;
}
//============================================================================//
/*!
 * @brief Reads a list of SAC files in a group.
 *
 * @param[in] nnames       Number of names in SAC file list.
 * @param[in] dataNames    Names of SAC files in group to read.
 * @param[in] groupID      HDF5 group handle.
 *
 * @param[out] nfilesRead  Number of SAC files successfully read.
 * @param[out] ierr        0 indicates success. 
 *
 * @result Array of SAC structures of length nnames.  However, only the
 *         first nfilesRead structures contain data.
 *         This can be freed with free().
 *
 * @ingroup sac_h5_io
 *
 * @author Ben Baker, ISTI
 *
 */
struct sacData_struct *
   sacioh5_readTimeSeriesList(const int nnames, const char **dataNames,
                              const hid_t groupID,
                              int *nfilesRead, int *ierr)
{
    struct sacData_struct *sacData;
    int i, ierr1;
    *ierr = 0;
    *nfilesRead = 0;
    sacData = NULL;
    if (nnames < 1 || dataNames == NULL)
    {
        if (nnames < 1){printf("%s: No files in list\n", __func__);}
        if (dataNames == NULL)
        {
            printf("%s: Error dataNames is NULL\n", __func__);
        }
        *ierr = 1;
        return sacData;
    }
    *nfilesRead = 0;
    sacData = (struct sacData_struct *)
              calloc((size_t) nnames, sizeof(struct sacData_struct));
    for (i=0; i<nnames; i++)
    {
        ierr1 = sacioh5_readTimeSeries2(dataNames[i], groupID, 
                                        &sacData[*nfilesRead]);
        if (ierr1 != 0)
        {
            printf("%s: Failed to read: %s\n", __func__, dataNames[i]);
            *ierr = *ierr + 1;
        }
        else
        {
            *nfilesRead = *nfilesRead + 1;
        } 
    }
    return sacData;
}
//============================================================================//
/*!
 * @brief Reads the HDF5 time series.
 *
 * @param[in] dataSetName  Name of dataset.
 * @param[in] groupID      Group where datset resides.
 *
 * @param[out] sac         SAC data structure.
 *
 * @result 0 indicates success
 *
 * @ingroup sac_h5_io
 *
 * @author Ben Baker
 *
 * @TODO make a function where the type and memspace is passed to save time.
 *
 */
int sacioh5_readTimeSeries2(const char *dataSetName,
                            const hid_t groupID,
                            struct sacData_struct *sac)
{
    struct sacH5Header_struct *sacH5Header; 
    struct sacH5PoleZero_struct *sacH5PZ;
    struct sacH5FAP_struct *sacH5FAP;
    struct sacH5Data_struct sach5;
    char *citem;
    double *x;
    hid_t dataSet, dataSpace, dataType, memSpace;
    hsize_t dims[1];
    herr_t status;
    size_t lenos; //, nalloc;
    int rank;
    //------------------------------------------------------------------------//
    //
    // Initialize and make sure group exists
    memset(sac, 0, sizeof(struct sacData_struct));
    memset(&sach5, 0, sizeof(struct sacH5Data_struct));
    if (H5Lexists(groupID, dataSetName, H5P_DEFAULT) < 0)
    {
        printf("%s: Dataset %s does not exist\n", __func__, dataSetName);
        return -1;
    }
    // Copy variable name
    lenos = strlen(dataSetName);
    citem = (char *) calloc(lenos+1, sizeof(char));
    strcpy(citem, dataSetName);
    // Open the dataset and dataspace
    dataSet = H5Dopen(groupID, citem, H5P_DEFAULT);
    dataSpace = H5Dget_space(dataSet);
    free(citem);
    // Open the datatype
    dataType  = H5Topen(groupID, SACH5_DATA_STRUCT_NAME, H5P_DEFAULT);
    // Verify size
    rank = H5Sget_simple_extent_ndims(dataSpace);
    if (rank != 1)
    {
        printf("%s: Error data can only be 1d\n", __func__);
        return -1;
    }
    status = H5Sget_simple_extent_dims(dataSpace, dims, NULL);
    if (status < 0 || dims[0] != 1)
    {
        if (status < 0)
        {
            printf("%s: Error getting dims\n", __func__);
        }
        if (dims[0] != 1)
        {
            printf("%s: Can only load 1 item for now\n", __func__);
        }
        return -1;
    }
    // Read it
    memSpace = H5Screate_simple(rank, dims, NULL);
    status = H5Dread(dataSet, dataType, memSpace, dataSpace,
                     H5P_DEFAULT, &sach5);
    //status = H5Dread(dataSet, dataType, H5S_ALL, H5S_ALL, H5P_DEFAULT, &sach5);
    if (status < 0)
    {
        printf("%s: Failed to read data\n", __func__);
        return -1;
    }
    // Copy the header
    sacH5Header = (struct sacH5Header_struct *) sach5.header.p;
    sach5Header2sacHeader(sacH5Header[0], &sac->header);
    // Copy poles and zeros
    if (sach5.pz.len > 0)
    {
        sacH5PZ = (struct sacH5PoleZero_struct *) sach5.pz.p;
        sach5PZ2sacPZ(sacH5PZ[0], &sac->pz);
    }
    // Copy the FAP structure
    if (sach5.fap.len > 0)
    {
        sacH5FAP = (struct sacH5FAP_struct *) sach5.fap.p;
        sach5FAP2sacFAP(sacH5FAP[0], &sac->fap);
    }
    // Copy the data
    if (sach5.data.len > 0)
    {
        x = (double *) sach5.data.p;
        sac->npts = (int) sach5.data.len;
        sac->data = sacio_malloc64f(sac->npts);
/*
        nalloc = (size_t) sac->npts*sizeof(double); 
#ifdef USE_POSIX
        posix_memalign((void **) &sac->data, 64, nalloc);
#else
        sac->data = (double *) aligned_alloc(64, nalloc);
#endif
*/
        memcpy(sac->data, x, (size_t) sac->npts*sizeof(double));
/*
        ierr = array_copy64f_work(sac->npts, x, sac->data); 
        if (ierr != 0)
        {
            printf("%s: Failed to copy data\n", __func__);
            return -1;
        }
*/
        x = NULL;
    }
    // Release H5 resources
    status  = H5Dvlen_reclaim(dataType, dataSpace, H5P_DEFAULT, &sach5);
    status += H5Sclose(memSpace);
    status += H5Tclose(dataType);
    status += H5Sclose(dataSpace);
    status += H5Dclose(dataSet);
    // check for errors
    if (status != 0 || status != 0)
    {
        if (status != 0)
        {
            printf("%s; Failed to release h5 memory\n", __func__);
        }
        return -1;
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Reads the HDF5 time series.
 *
 * @param[in] dataName   name of HDF5 time series to read
 * @param[in] fileID     HDF5 file or group handle where dataName resides.
 * @param[in] nwork      if -1 then this is a space query and npts will
 *                       contains the number of points but x will not be
 *                       accessed.
 *                       otherwise this is the number of elements that x
 *                       can hold.
 *
 * @param[out] npts      number of points in time series.
 * @param[out] x         when nwork > 0 then this contains the npts length
 *                       time series at all points [nwork]
 *
 * @result 0 indicates success
 *
 * @author Ben Baker
 *
 * @copyright ISTI distributed under Apache 2
 *
 */
/*
int sacioh5_readTimeSeries1(const char *dataName, const hid_t fileID,
                            struct sacData_struct *sac)
{
    char *citem;
    int ierr;
    hid_t dataSet;
    herr_t status;
    // Copy the filename and open the dataset
    citem = (char *) calloc(strlen(dataName)+1, sizeof(char));
    strcpy(citem, dataName); 
    dataSet = H5Dopen(fileID, citem, H5P_DEFAULT); 
    free(citem);
    // Do the appropriate read
    ierr = sacioh5_readTimeSeries2(dataSet, sac);
    // Close it up and free memory
    status = H5Dclose(dataSet);
    if (status != 0 || ierr != 0)
    {
        if (status != 0){printf("%s: Error closing dataset\n", __func__);}
        if (ierr != 0){printf("%s: Error reading dataset\n", __func__);}
        return -1;
    }
    return 0;
}
*/
//============================================================================//
/*!
 * @brief If not already present this function makes the data structure
 *        and places it in the given HDF5 group.
 *
 * @param[in] groupID    HDF5 group handle to which the data structure will
 *                       be written.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_data_type
 *
 * @author Ben Baker
 *
 */
int sacioh5_createDataType(const hid_t groupID)
{
    hid_t dataType, headerType, fapType, pzType, vlenData,
          vlenFAP, vlenHeader, vlenPZ;
    herr_t status;
    int ierr;
    //------------------------------------------------------------------------//
    //
    // Nothing to do
    if (H5Lexists(groupID, SACH5_DATA_STRUCT_NAME, H5P_DEFAULT) > 0)
    {
        return 0;
    }
    // Make the header
    status = 0;
    ierr = sacioh5_createHeaderType(groupID);
    if (ierr != 0)
    {
        printf("%s: Failed to create header\n", __func__);
        return -1;
    }
    // Make the pole-zeros structure
    ierr = sacio_createPoleZeroType(groupID);
    if (ierr != 0)
    {
        printf("%s: Failed to create pz struct\n", __func__);
        return -1;
    }
    // Make the FAP structure
    ierr = sacio_createFAPType(groupID);
    if (ierr != 0)
    {
        printf("%s: Failed to create FAP struct\n", __func__);
        return -1;
    }
    // Make/load primitives
    vlenData = H5Tvlen_create(H5T_NATIVE_DOUBLE);
    headerType = H5Topen(groupID, SACH5_HEADER_STRUCT_NAME, H5P_DEFAULT);
    pzType = H5Topen(groupID, SACH5_PZ_STRUCT_NAME, H5P_DEFAULT); 
    fapType = H5Topen(groupID, SACH5_FAP_STRUCT_NAME, H5P_DEFAULT);
    vlenHeader = H5Tvlen_create(headerType);
    vlenPZ = H5Tvlen_create(pzType);
    vlenFAP = H5Tvlen_create(fapType);
    // Build the structure
    dataType = H5Tcreate(H5T_COMPOUND, sizeof(struct sacH5Data_struct)); 
    status += H5Tinsert(dataType, "header\0",
                        HOFFSET(struct sacH5Data_struct, header),
                        vlenHeader);
    status += H5Tinsert(dataType, "pz\0",
                        HOFFSET(struct sacH5Data_struct, pz),
                        vlenPZ);
    status += H5Tinsert(dataType, "fap\0",
                        HOFFSET(struct sacH5Data_struct, fap),
                        vlenFAP);
    status += H5Tinsert(dataType, "data\0",
                        HOFFSET(struct sacH5Data_struct, data),
                        vlenData);
    // commit it
    status += H5Tcommit2(groupID, SACH5_DATA_STRUCT_NAME, dataType,
                          H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    if (status < 0)
    {
        printf("%s: Failed to make structure %s\n",
               __func__, SACH5_DATA_STRUCT_NAME);
        return -1;
    }
    // close it up 
    status  = H5Tclose(dataType);
    status += H5Tclose(vlenHeader);
    status += H5Tclose(headerType);
    status += H5Tclose(pzType);
    status += H5Tclose(fapType);
    status += H5Tclose(vlenData);
    status += H5Tclose(vlenPZ);
    status += H5Tclose(vlenFAP);
    return 0;
}
//============================================================================//
/*!
 * @brief Creates the FAP structure data type.
 *
 * @param[in] groupID    HDF5 group to which the data type will be written.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_data_type
 *
 */
int sacio_createFAPType(const hid_t groupID)
{
    hid_t dataType, vlenData;
    herr_t status;
    // Already exists - nothing to make
    status = 0; 
    if (H5Lexists(groupID, SACH5_FAP_STRUCT_NAME, H5P_DEFAULT) > 0){return 0;} 
    // Create the primitives
    vlenData = H5Tvlen_create(H5T_NATIVE_DOUBLE);
    // Build the structure
    dataType = H5Tcreate(H5T_COMPOUND, sizeof(struct sacH5FAP_struct));
    status += H5Tinsert(dataType, "Frequencies\0",
                        HOFFSET(struct sacH5FAP_struct, freqs),
                        vlenData);
    status += H5Tinsert(dataType, "Amplitude\0",
                        HOFFSET(struct sacH5FAP_struct, amp),
                        vlenData);
    status += H5Tinsert(dataType, "Phase\0",
                        HOFFSET(struct sacH5FAP_struct, phase),
                        vlenData);
    status += H5Tinsert(dataType, "NumberOfFrequencies\0",
                        HOFFSET(struct sacH5FAP_struct, nf),
                        H5T_NATIVE_INT);
    status += H5Tinsert(dataType, "haveFAP\0",
                        HOFFSET(struct sacH5FAP_struct, lhaveFAP),
                        H5T_NATIVE_INT);
    // Commit it
    status += H5Tcommit2(groupID, SACH5_FAP_STRUCT_NAME, dataType,
                          H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    if (status < 0)
    {
        printf("%s: Failed to make structure %s\n",
               __func__, SACH5_FAP_STRUCT_NAME);
        return -1;
    }
    // Close it up 
    status  = H5Tclose(dataType);
    status += H5Tclose(vlenData);
    return 0;
}
//============================================================================//
/*!
 * @brief Creates the pole-zero structure data type.
 *
 * @param[in] groupID    HDF5 group to which the data type will be written.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_data_type
 *
 */
int sacio_createPoleZeroType(const hid_t groupID)
{
    hid_t dataType, string64Type, vlenC64Data, vlenData;
    herr_t status;
    //------------------------------------------------------------------------//
    //
    // Already exists - nothing to make
    status = 0;
    if (H5Lexists(groupID, SACH5_PZ_STRUCT_NAME, H5P_DEFAULT) > 0){return 0;}
    // Create the primitives
    string64Type = H5Tcopy(H5T_C_S1);
    H5Tset_size(string64Type, 64 + PAD);
    vlenC64Data = H5Tvlen_create(string64Type);
    vlenData = H5Tvlen_create(H5T_NATIVE_DOUBLE);
    // Build the structure
    dataType = H5Tcreate(H5T_COMPOUND, sizeof(struct sacH5PoleZero_struct));
    status += H5Tinsert(dataType, "Network\0",
                        HOFFSET(struct sacH5PoleZero_struct, network),
                        vlenC64Data);
    status += H5Tinsert(dataType, "Station\0",
                        HOFFSET(struct sacH5PoleZero_struct, station),
                        vlenC64Data);
    status += H5Tinsert(dataType, "Location\0",
                        HOFFSET(struct sacH5PoleZero_struct, location),
                        vlenC64Data);
    status += H5Tinsert(dataType, "Channel\0",
                        HOFFSET(struct sacH5PoleZero_struct, channel),
                        vlenC64Data);
    status += H5Tinsert(dataType, "Comment\0",
                        HOFFSET(struct sacH5PoleZero_struct, comment),
                        vlenC64Data);
    status += H5Tinsert(dataType, "Description\0",
                        HOFFSET(struct sacH5PoleZero_struct, description),
                        vlenC64Data);
    status += H5Tinsert(dataType, "instrumentType\0",
                        HOFFSET(struct sacH5PoleZero_struct, instrumentType),
                        vlenC64Data);
    status += H5Tinsert(dataType, "polesRe\0",
                        HOFFSET(struct sacH5PoleZero_struct, polesRe),
                        vlenData);
    status += H5Tinsert(dataType, "polesIm\0",
                        HOFFSET(struct sacH5PoleZero_struct, polesIm),
                        vlenData);
    status += H5Tinsert(dataType, "zerosRe\0",
                        HOFFSET(struct sacH5PoleZero_struct, zerosRe),
                        vlenData);
    status += H5Tinsert(dataType, "zerosIm\0",
                        HOFFSET(struct sacH5PoleZero_struct, zerosIm),
                        vlenData);
    status += H5Tinsert(dataType, "constant\0",
                        HOFFSET(struct sacH5PoleZero_struct, constant),
                        H5T_NATIVE_DOUBLE);
    status += H5Tinsert(dataType, "created\0",
                        HOFFSET(struct sacH5PoleZero_struct, created),
                        H5T_NATIVE_DOUBLE);
    status += H5Tinsert(dataType, "start\0",
                        HOFFSET(struct sacH5PoleZero_struct, start),
                        H5T_NATIVE_DOUBLE);
    status += H5Tinsert(dataType, "end\0",
                        HOFFSET(struct sacH5PoleZero_struct, end),
                        H5T_NATIVE_DOUBLE);
    status += H5Tinsert(dataType, "latitude\0",
                        HOFFSET(struct sacH5PoleZero_struct, latitude),
                        H5T_NATIVE_DOUBLE);
    status += H5Tinsert(dataType, "longitude\0",
                        HOFFSET(struct sacH5PoleZero_struct, longitude),
                        H5T_NATIVE_DOUBLE);
    status += H5Tinsert(dataType, "elevation\0",
                        HOFFSET(struct sacH5PoleZero_struct, elevation),
                        H5T_NATIVE_DOUBLE);
    status += H5Tinsert(dataType, "depth\0",
                        HOFFSET(struct sacH5PoleZero_struct, depth),
                        H5T_NATIVE_DOUBLE);
    status += H5Tinsert(dataType, "dip\0",
                        HOFFSET(struct sacH5PoleZero_struct, dip),
                        H5T_NATIVE_DOUBLE);
    status += H5Tinsert(dataType, "azimuth\0",
                        HOFFSET(struct sacH5PoleZero_struct, azimuth),
                        H5T_NATIVE_DOUBLE);
    status += H5Tinsert(dataType, "sampleRate\0",
                        HOFFSET(struct sacH5PoleZero_struct, sampleRate),
                        H5T_NATIVE_DOUBLE);
    status += H5Tinsert(dataType, "instgain\0",
                        HOFFSET(struct sacH5PoleZero_struct, instgain),
                        H5T_NATIVE_DOUBLE);
    status += H5Tinsert(dataType, "sensitivity\0",
                        HOFFSET(struct sacH5PoleZero_struct, sensitivity),
                        H5T_NATIVE_DOUBLE);
    status += H5Tinsert(dataType, "a0\0",
                        HOFFSET(struct sacH5PoleZero_struct, a0),
                        H5T_NATIVE_DOUBLE);
    status += H5Tinsert(dataType, "npoles\0",
                        HOFFSET(struct sacH5PoleZero_struct, npoles),
                        H5T_NATIVE_INT);
    status += H5Tinsert(dataType, "nzeros\0",
                        HOFFSET(struct sacH5PoleZero_struct, nzeros),
                        H5T_NATIVE_INT);
    status += H5Tinsert(dataType, "inputUnits\0",
                        HOFFSET(struct sacH5PoleZero_struct, inputUnits),
                        H5T_NATIVE_INT);
    status += H5Tinsert(dataType, "instgainUnits\0",
                        HOFFSET(struct sacH5PoleZero_struct, instgainUnits),
                        H5T_NATIVE_INT);
    status += H5Tinsert(dataType, "sensitivityUnits\0",
                        HOFFSET(struct sacH5PoleZero_struct, sensitivityUnits),
                        H5T_NATIVE_INT);
    status += H5Tinsert(dataType, "havePoleZeros\0",
                        HOFFSET(struct sacH5PoleZero_struct, lhavePZ),
                        H5T_NATIVE_INT);
    // Commit it
    status += H5Tcommit2(groupID, SACH5_PZ_STRUCT_NAME, dataType,
                          H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    if (status < 0) 
    {
        printf("%s: Failed to make structure %s\n",
               __func__, SACH5_PZ_STRUCT_NAME);
        return -1;
    }
    // Close it up 
    status  = H5Tclose(dataType);
    status += H5Tclose(string64Type);
    status += H5Tclose(vlenC64Data);
    status += H5Tclose(vlenData);
    return 0;
}
//============================================================================//
/*!
 * @brief If not already present this function creates the header structure
 *        and places it in the given HDF5 group.
 *
 * @param[in] groupID    HDF5 group handle to which the header structure will
 *                       be written
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_data_type
 *
 * @author Ben Baker
 *
 */ 
int sacioh5_createHeaderType(const hid_t groupID)
{
    hid_t dataType, string16Type, string8Type, vlenC8Data, vlenC16Data;
    herr_t ierr;
    //------------------------------------------------------------------------//
    //
    // Already exists - nothing to make
    ierr = 0;
    if (H5Lexists(groupID, SACH5_HEADER_STRUCT_NAME, H5P_DEFAULT) > 0)
    {
        return 0;
    }
    // Set the 8 and 16 character types
    string8Type  = H5Tcopy(H5T_C_S1);
    string16Type = H5Tcopy(H5T_C_S1);
    H5Tset_size(string8Type,  8 + PAD);
    H5Tset_size(string16Type, 16 + PAD);
    vlenC8Data  = H5Tvlen_create(string8Type);
    vlenC16Data = H5Tvlen_create(string16Type);
    // Build the data structure
    dataType = H5Tcreate(H5T_COMPOUND,
                         sizeof(struct sacH5Header_struct));
    // doubles
    ierr += H5Tinsert(dataType, "delta\0",
                      HOFFSET(struct sacH5Header_struct, delta),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "depmin\0",
                      HOFFSET(struct sacH5Header_struct, depmin),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "depmax\0",
                      HOFFSET(struct sacH5Header_struct, depmax),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "scale\0",
                      HOFFSET(struct sacH5Header_struct, scale),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "odelta\0",
                      HOFFSET(struct sacH5Header_struct, odelta),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "b\0",
                      HOFFSET(struct sacH5Header_struct, b),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "e\0",
                      HOFFSET(struct sacH5Header_struct, e),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "o\0",
                      HOFFSET(struct sacH5Header_struct, o),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "a\0",
                      HOFFSET(struct sacH5Header_struct, a),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "internal1\0",
                      HOFFSET(struct sacH5Header_struct, internal1),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "t0\0",
                      HOFFSET(struct sacH5Header_struct, t0),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "t1\0",
                      HOFFSET(struct sacH5Header_struct, t1),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "t2\0",
                      HOFFSET(struct sacH5Header_struct, t2),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "t3\0",
                      HOFFSET(struct sacH5Header_struct, t3),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "t4\0",
                      HOFFSET(struct sacH5Header_struct, t4),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "t5\0",
                      HOFFSET(struct sacH5Header_struct, t5),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "t6\0",
                      HOFFSET(struct sacH5Header_struct, t6),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "t7\0",
                      HOFFSET(struct sacH5Header_struct, t7),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "t8\0",
                      HOFFSET(struct sacH5Header_struct, t8),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "t9\0",
                      HOFFSET(struct sacH5Header_struct, t9),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "f\0",
                      HOFFSET(struct sacH5Header_struct, f),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "resp0\0",
                      HOFFSET(struct sacH5Header_struct, resp0),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "resp1\0",
                      HOFFSET(struct sacH5Header_struct, resp1),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "resp2\0",
                      HOFFSET(struct sacH5Header_struct, resp2),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "resp3\0",
                      HOFFSET(struct sacH5Header_struct, resp3),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "resp4\0",
                      HOFFSET(struct sacH5Header_struct, resp4),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "resp5\0",
                      HOFFSET(struct sacH5Header_struct, resp5),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "resp6\0",
                      HOFFSET(struct sacH5Header_struct, resp6),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "resp7\0",
                      HOFFSET(struct sacH5Header_struct, resp7),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "resp8\0",
                      HOFFSET(struct sacH5Header_struct, resp8),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "resp9\0",
                      HOFFSET(struct sacH5Header_struct, resp9),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "stla\0",
                      HOFFSET(struct sacH5Header_struct, stla),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "stlo\0",
                      HOFFSET(struct sacH5Header_struct, stlo),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "stel\0",
                      HOFFSET(struct sacH5Header_struct, stel),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "stdp\0",
                      HOFFSET(struct sacH5Header_struct, stdp),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "evla\0",
                      HOFFSET(struct sacH5Header_struct, evla),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "evlo\0",
                      HOFFSET(struct sacH5Header_struct, evlo),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "evel\0",
                      HOFFSET(struct sacH5Header_struct, evel),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "evdp\0",
                      HOFFSET(struct sacH5Header_struct, evdp),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "mag\0",
                      HOFFSET(struct sacH5Header_struct, mag),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "user0\0",
                      HOFFSET(struct sacH5Header_struct, user0),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "user1\0",
                      HOFFSET(struct sacH5Header_struct, user1),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "user2\0",
                      HOFFSET(struct sacH5Header_struct, user2),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "user3\0",
                      HOFFSET(struct sacH5Header_struct, user3),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "user4\0",
                      HOFFSET(struct sacH5Header_struct, user4),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "user5\0",
                      HOFFSET(struct sacH5Header_struct, user5),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "user6\0",
                      HOFFSET(struct sacH5Header_struct, user6),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "user7\0",
                      HOFFSET(struct sacH5Header_struct, user7),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "user8\0",
                      HOFFSET(struct sacH5Header_struct, user8),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "user9\0",
                      HOFFSET(struct sacH5Header_struct, user9),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "dist\0",
                      HOFFSET(struct sacH5Header_struct, dist),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "az\0",
                      HOFFSET(struct sacH5Header_struct, az),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "baz\0",
                      HOFFSET(struct sacH5Header_struct, baz),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "gcarc\0",
                      HOFFSET(struct sacH5Header_struct, gcarc),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "internal2\0",
                      HOFFSET(struct sacH5Header_struct, internal2),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "depmen\0",
                      HOFFSET(struct sacH5Header_struct, depmen),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "cmpaz\0",
                      HOFFSET(struct sacH5Header_struct, cmpaz),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "cmpinc\0",
                      HOFFSET(struct sacH5Header_struct, cmpinc),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "xminimum\0",
                      HOFFSET(struct sacH5Header_struct, xminimum),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "xmaximum\0",
                      HOFFSET(struct sacH5Header_struct, xmaximum),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "yminimum\0",
                      HOFFSET(struct sacH5Header_struct, yminimum),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "ymaximum\0",
                      HOFFSET(struct sacH5Header_struct, ymaximum),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "unused0\0",
                      HOFFSET(struct sacH5Header_struct, unused0),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "unused1\0",
                      HOFFSET(struct sacH5Header_struct, unused1),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "unused2\0",
                      HOFFSET(struct sacH5Header_struct, unused2),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "unused3\0",
                      HOFFSET(struct sacH5Header_struct, unused3),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "unused4\0",
                      HOFFSET(struct sacH5Header_struct, unused4),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "unused5\0",
                      HOFFSET(struct sacH5Header_struct, unused5),
                      H5T_NATIVE_DOUBLE);
    ierr += H5Tinsert(dataType, "unused6\0",
                      HOFFSET(struct sacH5Header_struct, unused6),
                      H5T_NATIVE_DOUBLE);
    // integers
    ierr += H5Tinsert(dataType, "nzyear\0",
                      HOFFSET(struct sacH5Header_struct, nzyear),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nzjday\0",
                      HOFFSET(struct sacH5Header_struct, nzjday),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nzhour\0",
                      HOFFSET(struct sacH5Header_struct, nzhour),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nzmin\0",
                      HOFFSET(struct sacH5Header_struct, nzmin),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nzsec\0",
                      HOFFSET(struct sacH5Header_struct, nzsec),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nzmsec\0",
                      HOFFSET(struct sacH5Header_struct, nzmsec),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nvhdr\0",
                      HOFFSET(struct sacH5Header_struct, nvhdr),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "norid\0",
                      HOFFSET(struct sacH5Header_struct, norid),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nevid\0",
                      HOFFSET(struct sacH5Header_struct, nevid),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "npts\0",
                      HOFFSET(struct sacH5Header_struct, npts),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "ninternal1\0",
                      HOFFSET(struct sacH5Header_struct, ninternal1),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nwfid\0",
                      HOFFSET(struct sacH5Header_struct, nwfid),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nxsize\0",
                      HOFFSET(struct sacH5Header_struct, nxsize),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nysize\0",
                      HOFFSET(struct sacH5Header_struct, nysize),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nunused0\0",
                      HOFFSET(struct sacH5Header_struct, nunused0),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "iftype\0",
                      HOFFSET(struct sacH5Header_struct, iftype),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "idep\0",
                      HOFFSET(struct sacH5Header_struct, idep),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "iztype\0",
                      HOFFSET(struct sacH5Header_struct, iztype),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nunused1\0",
                      HOFFSET(struct sacH5Header_struct, nunused1),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "iinst\0",
                      HOFFSET(struct sacH5Header_struct, iinst),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "istreg\0",
                      HOFFSET(struct sacH5Header_struct, istreg),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "ievreg\0",
                      HOFFSET(struct sacH5Header_struct, ievreg),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "ievtyp\0",
                      HOFFSET(struct sacH5Header_struct, ievtyp),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "iqual\0",
                      HOFFSET(struct sacH5Header_struct, iqual),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "isynth\0",
                      HOFFSET(struct sacH5Header_struct, isynth),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "imagtyp\0",
                      HOFFSET(struct sacH5Header_struct, imagtyp),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "imagsrc\0",
                      HOFFSET(struct sacH5Header_struct, imagsrc),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nunused2\0",
                      HOFFSET(struct sacH5Header_struct, nunused2),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nunused3\0",
                      HOFFSET(struct sacH5Header_struct, nunused3),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nunused4\0",
                      HOFFSET(struct sacH5Header_struct, nunused4),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nunused5\0",
                      HOFFSET(struct sacH5Header_struct, nunused5),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nunused6\0",
                      HOFFSET(struct sacH5Header_struct, nunused6),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nunused7\0",
                      HOFFSET(struct sacH5Header_struct, nunused7),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nunused8\0",
                      HOFFSET(struct sacH5Header_struct, nunused8),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "nunused9\0",
                      HOFFSET(struct sacH5Header_struct, nunused9),
                      H5T_NATIVE_INT);
    // logicals
    ierr += H5Tinsert(dataType, "leven\0",
                      HOFFSET(struct sacH5Header_struct, leven),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "lpspol\0",
                      HOFFSET(struct sacH5Header_struct, lpspol),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "lovrok\0",
                      HOFFSET(struct sacH5Header_struct, lovrok),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "lcalda\0",
                      HOFFSET(struct sacH5Header_struct, lcalda),
                      H5T_NATIVE_INT);
    ierr += H5Tinsert(dataType, "lunused\0",
                      HOFFSET(struct sacH5Header_struct, lunused),
                      H5T_NATIVE_INT);
    // characters
    ierr += H5Tinsert(dataType, "kstnm\0",
                      HOFFSET(struct sacH5Header_struct, kstnm),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kevnm\0",
                      HOFFSET(struct sacH5Header_struct, kevnm),
                      vlenC16Data);
    ierr += H5Tinsert(dataType, "khole\0",
                      HOFFSET(struct sacH5Header_struct, khole),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "ko\0",
                      HOFFSET(struct sacH5Header_struct, ko),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "ka\0",
                      HOFFSET(struct sacH5Header_struct, ka),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kt0\0",
                      HOFFSET(struct sacH5Header_struct, kt0),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kt1\0",
                      HOFFSET(struct sacH5Header_struct, kt1),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kt2\0",
                      HOFFSET(struct sacH5Header_struct, kt2),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kt3\0",
                      HOFFSET(struct sacH5Header_struct, kt3),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kt4\0",
                      HOFFSET(struct sacH5Header_struct, kt4),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kt5\0",
                      HOFFSET(struct sacH5Header_struct, kt5),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kt6\0",
                      HOFFSET(struct sacH5Header_struct, kt6),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kt7\0",
                      HOFFSET(struct sacH5Header_struct, kt7),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kt8\0",
                      HOFFSET(struct sacH5Header_struct, kt8),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kt9\0",
                      HOFFSET(struct sacH5Header_struct, kt9),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kf\0",
                      HOFFSET(struct sacH5Header_struct, kf),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kuser0\0",
                      HOFFSET(struct sacH5Header_struct, kuser0),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kuser1\0",
                      HOFFSET(struct sacH5Header_struct, kuser1),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kuser2\0",
                      HOFFSET(struct sacH5Header_struct, kuser2),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kcmpnm\0",
                      HOFFSET(struct sacH5Header_struct, kcmpnm),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "knetwk\0",
                      HOFFSET(struct sacH5Header_struct, knetwk),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kdatrd\0",
                      HOFFSET(struct sacH5Header_struct, kdatrd),
                      vlenC8Data);
    ierr += H5Tinsert(dataType, "kinst\0",
                      HOFFSET(struct sacH5Header_struct, kinst),
                      vlenC8Data);
    if (ierr != 0)
    {
        printf("%s: Failed to create %s type\n",
               __func__, SACH5_HEADER_STRUCT_NAME);
        return -1;
    }
    // Commit it
    ierr = H5Tcommit2(groupID, SACH5_HEADER_STRUCT_NAME, dataType,
                      H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    if (ierr != 0)
    {
        printf("%s: Failed to commit structure %s\n", __func__,
               SACH5_HEADER_STRUCT_NAME);
        return -1;
    }
    // Close up shop
    ierr += H5Tclose(dataType);
    ierr += H5Tclose(vlenC8Data);
    ierr += H5Tclose(vlenC16Data);
    ierr += H5Tclose(string8Type);
    ierr += H5Tclose(string16Type);
    if (ierr != 0)
    {
        printf("%s: Failed to close data types\n", __func__);
        return -1;
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Gets a float64 variable on the SAC header.
 *
 * @param[in] type     SAC header variable name to get.
 * @param[in] header   SAC header from which to get variable type.
 *
 * @param[out] var     Value of variable to get from the SAC header.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_header_utils
 *
 * @author Ben Baker
 *
 */
int sacioh5_getFloatHeader(const enum sacHeader_enum type,
                           const struct sacH5Header_struct header,
                           double *var)
{
    if (type == SAC_FLOAT_DELTA)
    {
        *var = header.delta;
    }
    else if (type == SAC_FLOAT_DEPMIN)
    {
        *var = header.depmin;
    }
    else if (type == SAC_FLOAT_DEPMAX)
    {
        *var = header.depmax;
    }
    else if (type == SAC_FLOAT_SCALE)
    {
        *var = header.scale;
    }
    else if (type == SAC_FLOAT_ODELTA)
    {
        *var = header.odelta;
    }
    else if (type == SAC_FLOAT_B)
    {
        *var = header.b;
    }
    else if (type == SAC_FLOAT_E)
    {
        *var = header.e;
    }
    else if (type == SAC_FLOAT_O)
    {
        *var = header.o;
    }
    else if (type == SAC_FLOAT_A)
    {
        *var = header.a;
    }
    else if (type == SAC_FLOAT_INTERNAL1)
    {
        *var = header.internal1;
    }
    else if (type == SAC_FLOAT_T0)
    {
        *var = header.t0;
    }
    else if (type == SAC_FLOAT_T1)
    {
        *var = header.t1;
    }
    else if (type == SAC_FLOAT_T2)
    {
        *var = header.t2;
    }
    else if (type == SAC_FLOAT_T3)
    {
        *var = header.t3;
    }
    else if (type == SAC_FLOAT_T4)
    {
        *var = header.t4;
    }
    else if (type == SAC_FLOAT_T5)
    {
        *var = header.t5;
    }
    else if (type == SAC_FLOAT_T6)
    {
        *var = header.t6;
    }
    else if (type == SAC_FLOAT_T7)
    {
        *var = header.t7;
    }
    else if (type == SAC_FLOAT_T8)
    {
        *var = header.t8;
    }
    else if (type == SAC_FLOAT_T9)
    {
        *var = header.t9;
    }
    else if (type == SAC_FLOAT_F)
    {
        *var = header.f;
    }
    else if (type == SAC_FLOAT_RESP0)
    {
        *var = header.resp0;
    }
    else if (type == SAC_FLOAT_RESP1)
    {
        *var = header.resp1;
    }
    else if (type == SAC_FLOAT_RESP2)
    {
        *var = header.resp2;
    }
    else if (type == SAC_FLOAT_RESP3)
    {
        *var = header.resp3;
    }
    else if (type == SAC_FLOAT_RESP4)
    {
        *var = header.resp4;
    }
    else if (type == SAC_FLOAT_RESP5)
    {
        *var = header.resp5;
    }
    else if (type == SAC_FLOAT_RESP6)
    {
        *var = header.resp6;
    }
    else if (type == SAC_FLOAT_RESP7)
    {
        *var = header.resp7;
    }
    else if (type == SAC_FLOAT_RESP8)
    {
        *var = header.resp8;
    }
    else if (type == SAC_FLOAT_RESP9)
    {
        *var = header.resp9;
    }
    else if (type == SAC_FLOAT_STLA)
    {
        *var = header.stla;
    }
    else if (type == SAC_FLOAT_STLO)
    {
        *var = header.stlo;
    }
    else if (type == SAC_FLOAT_STEL)
    {
        *var = header.stel;
    }
    else if (type == SAC_FLOAT_STDP)
    {
        *var = header.stdp;
    }
    else if (type == SAC_FLOAT_EVLA)
    {
        *var = header.evla;
    }
    else if (type == SAC_FLOAT_EVLO)
    {
        *var = header.evlo;
    }
    else if (type == SAC_FLOAT_EVEL)
    {
        *var = header.evel;
    }
    else if (type == SAC_FLOAT_EVDP)
    {
        *var = header.evdp;
    }
    else if (type == SAC_FLOAT_MAG)
    {
        *var = header.mag;
    }
    else if (type == SAC_FLOAT_USER0)
    {
        *var = header.user0;
    }
    else if (type == SAC_FLOAT_USER1)
    {
        *var = header.user1;
    }
    else if (type == SAC_FLOAT_USER2)
    {
        *var = header.user2;
    }
    else if (type == SAC_FLOAT_USER3)
    {
        *var = header.user3;
    }
    else if (type == SAC_FLOAT_USER4)
    {
        *var = header.user4;
    }
    else if (type == SAC_FLOAT_USER5)
    {
        *var = header.user5;
    }
    else if (type == SAC_FLOAT_USER6)
    {
        *var = header.user6;
    }
    else if (type == SAC_FLOAT_USER7)
    {
        *var = header.user7;
    }
    else if (type == SAC_FLOAT_USER8)
    {
        *var = header.user8;
    }
    else if (type == SAC_FLOAT_USER9)
    {
        *var = header.user9;
    }
    else if (type == SAC_FLOAT_DIST)
    {
        *var = header.dist;
    }
    else if (type == SAC_FLOAT_AZ)
    {
        *var = header.az;
    }
    else if (type == SAC_FLOAT_BAZ)
    {
        *var = header.baz;
    }
    else if (type == SAC_FLOAT_GCARC)
    {
        *var = header.gcarc;
    }
    else if (type == SAC_FLOAT_INTERNAL2)
    {
        *var = header.internal2;
    }
    else if (type == SAC_FLOAT_DEPMEN)
    {
        *var = header.depmen;
    }
    else if (type == SAC_FLOAT_CMPAZ)
    {
        *var = header.cmpaz;
    }
    else if (type == SAC_FLOAT_CMPINC)
    {
        *var = header.cmpinc;
    }
    else if (type == SAC_FLOAT_XMINIMUM)
    {
        *var = header.xminimum;
    }
    else if (type == SAC_FLOAT_XMAXIMUM)
    {
        *var = header.xmaximum;
    }
    else if (type == SAC_FLOAT_YMINIMUM)
    {
        *var = header.yminimum;
    }
    else if (type == SAC_FLOAT_YMAXIMUM)
    {
        *var = header.ymaximum;
    }
    else if (type == SAC_FLOAT_UNUSED0)
    {
        *var = header.unused0;
    }
    else if (type == SAC_FLOAT_UNUSED1)
    {
        *var = header.unused1;
    }
    else if (type == SAC_FLOAT_UNUSED2)
    {
        *var = header.unused2;
    }
    else if (type == SAC_FLOAT_UNUSED3)
    {
        *var = header.unused3;
    }
    else if (type == SAC_FLOAT_UNUSED4)
    {
        *var = header.unused4;
    }
    else if (type == SAC_FLOAT_UNUSED5)
    {
        *var = header.unused5;
    }
    else if (type == SAC_FLOAT_UNUSED6)
    {
        *var = header.unused6;
    }
    else
    {
        printf("%s: Invalid header type %d\n", __func__, (int) type);
        return -1;
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Gets an integer variable from the SAC header.
 *
 * @param[in] type     SAC header variable name to get.
 * @param[in] header   Header from which to get integer SAC variable.
 *
 * @param[out] ivar    Value of variable to set in SAC header.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_header_utils
 *
 * @author Ben Baker
 *
 */
int sacioh5_getIntegerHeader(const enum sacHeader_enum type,
                             const struct sacH5Header_struct header,
                             int *ivar)
{
    *ivar = 0;
    if (type == SAC_INT_NZYEAR)
    {
        *ivar = header.nzyear;
    }
    else if (type == SAC_INT_NZJDAY)
    {
        *ivar = header.nzjday;
    }
    else if (type == SAC_INT_NZHOUR)
    {
        *ivar = header.nzhour;
    }
    else if (type == SAC_INT_NZMIN)
    {
        *ivar = header.nzmin;
    }
    else if (type == SAC_INT_NZSEC)
    {
        *ivar = header.nzsec;
    }
    else if (type == SAC_INT_NZMSEC)
    {
        *ivar = header.nzmsec;
    }
    else if (type == SAC_INT_NVHDR)
    {
        *ivar = header.nvhdr;
    }
    else if (type == SAC_INT_NORID)
    {
        *ivar = header.norid;
    }
    else if (type == SAC_INT_NEVID)
    {
        *ivar = header.nevid;
    }
    else if (type == SAC_INT_NPTS)
    {
        *ivar = header.npts;
    }
    else if (type == SAC_INT_INTERNAL1)
    {
        *ivar = header.ninternal1;
    }
    else if (type == SAC_INT_NWFID)
    {
        *ivar = header.nwfid;
    }
    else if (type == SAC_INT_NXSIZE)
    {
        *ivar = header.nxsize;
    }
    else if (type == SAC_INT_NYSIZE)
    {
        *ivar = header.nysize;
    }
    else if (type == SAC_INT_UNUSED0)
    {
        *ivar = header.nunused0;
    }
    else if (type == SAC_INT_IFTYPE)
    {
        *ivar = header.iftype;
    }
    else if (type == SAC_INT_IDEP)
    {
        *ivar = header.idep;
    }
    else if (type == SAC_INT_IZTYPE)
    {
        *ivar = header.iztype;
    }
    else if (type == SAC_INT_UNUSED1)
    {
        *ivar = header.nunused1;
    }
    else if (type == SAC_INT_IINST)
    {
        *ivar = header.iinst;
    }
    else if (type == SAC_INT_ISTREG)
    {
        *ivar = header.istreg;
    }
    else if (type == SAC_INT_IEVREG)
    {
        *ivar = header.ievreg;
    }
    else if (type == SAC_INT_IEVTYP)
    {
        *ivar = header.ievtyp;
    }
    else if (type == SAC_INT_IQUAL)
    {
        *ivar = header.iqual;
    }
    else if (type == SAC_INT_ISYNTH)
    {
        *ivar = header.isynth;
    }
    else if (type == SAC_INT_IMAGTYP)
    {
        *ivar = header.imagtyp;
    }
    else if (type == SAC_INT_IMAGSRC)
    {
        *ivar = header.imagsrc;
    }
    else if (type == SAC_INT_UNUSED2)
    {
        *ivar = header.nunused2;
    }
    else if (type == SAC_INT_UNUSED3)
    {
        *ivar = header.nunused3;
    }
    else if (type == SAC_INT_UNUSED4)
    {
        *ivar = header.nunused4;
    }
    else if (type == SAC_INT_UNUSED5)
    {
        *ivar = header.nunused5;
    }
    else if (type == SAC_INT_UNUSED6)
    {
        *ivar = header.nunused6;
    }
    else if (type == SAC_INT_UNUSED7)
    {
        *ivar = header.nunused7;
    }
    else if (type == SAC_INT_UNUSED8)
    {
        *ivar = header.nunused8;
    }
    else if (type == SAC_INT_UNUSED9)
    {
        *ivar = header.nunused9;
    }
    else 
    {
        printf("%s: Invalid header type %d\n", __func__, (int) type);
        return -1;
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Gets a boolean variable on the SAC header.
 *
 * @param[in] type     SAC header variable name to get.
 * @param[in] header   SAC header from which to get boolean variable.
 *
 * @param[out] lvar    Value of variable to set in SAC header.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_header_utils
 *
 * @author Ben Baker
 *
 */
int sacioh5_getBooleanHeader(const enum sacHeader_enum type,
                             const struct sacH5Header_struct header,
                             int *lvar)
{
    int ivar;
    *lvar = false;
    ivar = 0;
    if (type == SAC_BOOL_LEVEN)
    {
        ivar = header.leven;
    }
    else if (type == SAC_BOOL_LPSPOL)
    {
        ivar = header.lpspol;
    }
    else if (type == SAC_BOOL_LOVROK)
    {
        ivar = header.lovrok;
    }
    else if (type == SAC_BOOL_LCALDA)
    {
        ivar = header.lcalda;
    }
    else if (type == SAC_BOOL_LUNUSED)
    {
        ivar = header.lunused;
    }
    else
    {
        printf("%s: Invalid header type\n", __func__);
        return -1;
    }
    // check if it wasn't defined
    if (ivar ==-12345)
    {
        *lvar = false;
        return -1;
    }
    *lvar = (bool) ivar;
    return 0;
}
//============================================================================//
/*!
 * @brief Gets a character variable from the SAC header.
 *
 * @param[in] type     SAC header variable name to get.
 * @param[in] header   SAC header from which to extract character type.
 *
 * @param[out] kvar    Character variable corresponding to type.
 *                     This should be length of 8 unless setting KEVNM
 *                     in which case this should be length 16.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_header_utils
 *
 * @author Ben Baker
 *
 */
int sacioh5_getCharacterHeader(const enum sacHeader_enum type,
                               const struct sacH5Header_struct header,
                               char *kvar)
{
    if (type == SAC_CHAR_KSTNM)
    {
        readChar8(header.kstnm, kvar);
    }
    else if (type == SAC_CHAR_KEVNM)
    {
        readChar16(header.kevnm, kvar);
    }
    else if (type == SAC_CHAR_KHOLE)
    {
        readChar8(header.khole, kvar);
    }
    else if (type == SAC_CHAR_KO)
    {
        readChar8(header.ko, kvar);
    }
    else if (type == SAC_CHAR_KA)
    {
        readChar8(header.ka, kvar);
    }
    else if (type == SAC_CHAR_KT0)
    {
        readChar8(header.kt0, kvar);
    }
    else if (type == SAC_CHAR_KT1)
    {
        readChar8(header.kt1, kvar);
    }
    else if (type == SAC_CHAR_KT2)
    {
        readChar8(header.kt2, kvar);
    }
    else if (type == SAC_CHAR_KT3)
    {
        readChar8(header.kt3, kvar);
    }
    else if (type == SAC_CHAR_KT4)
    {
        readChar8(header.kt4, kvar);
    }
    else if (type == SAC_CHAR_KT5)
    {
        readChar8(header.kt5, kvar);
    }
    else if (type == SAC_CHAR_KT6)
    {
        readChar8(header.kt6, kvar);
    }
    else if (type == SAC_CHAR_KT7)
    {
        readChar8(header.kt7, kvar);
    }
    else if (type == SAC_CHAR_KT8)
    {
        readChar8(header.kt8, kvar);
    }
    else if (type == SAC_CHAR_KT9)
    {
        readChar8(header.kt9, kvar);
    }
    else if (type == SAC_CHAR_KF)
    {
        readChar8(header.kf, kvar);
    }
    else if (type == SAC_CHAR_KUSER0)
    {
        readChar8(header.kuser0, kvar);
    }
    else if (type == SAC_CHAR_KUSER1)
    {
        readChar8(header.kuser1, kvar);
    }
    else if (type == SAC_CHAR_KUSER2)
    {
        readChar8(header.kuser2, kvar);
    }
    else if (type == SAC_CHAR_KCMPNM)
    {
        readChar8(header.kcmpnm, kvar);
    }
    else if (type == SAC_CHAR_KNETWK)
    {
        readChar8(header.knetwk, kvar);
    }
    else if (type == SAC_CHAR_KDATRD)
    {
        readChar8(header.kdatrd, kvar);
    }
    else if (type == SAC_CHAR_KINST)
    {
        readChar8(header.kinst, kvar);
    }
    else
    {
        printf("%s: Invalid header type\n", __func__); 
        return 1;
    }
    if (strcasecmp(kvar, "-12345\0") == 0){return -1;}
    return 0;
}
//============================================================================//
/*!
 * @brief Sets a float64 variable on the SAC header.
 *
 * @param[in] type     SAC header variable name to set.
 * @param[in] var      Value of variable to set in SAC header.
 *
 * @param[out] header  SAC header updated with new real value.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_header_utils
 *
 * @author Ben Baker
 *
 */
int sacioh5_setFloatHeader(const enum sacHeader_enum type,
                           const double var,
                           struct sacH5Header_struct *header)
{
    if (type == SAC_FLOAT_DELTA)
    {
        header->delta = var; 
    }
    else if (type == SAC_FLOAT_DEPMIN)
    {
        header->depmin = var;
    }
    else if (type == SAC_FLOAT_DEPMAX)
    {
        header->depmax = var;
    }
    else if (type == SAC_FLOAT_SCALE)
    {
        header->scale = var;
    }
    else if (type == SAC_FLOAT_ODELTA)
    {
        header->odelta = var;
    }
    else if (type == SAC_FLOAT_B)
    {
        header->b = var;
    }
    else if (type == SAC_FLOAT_E)
    {
        header->e = var;
    }
    else if (type == SAC_FLOAT_O)
    {
        header->o = var;
    }
    else if (type == SAC_FLOAT_A)
    {
        header->a = var;
    }
    else if (type == SAC_FLOAT_INTERNAL1)
    {
        header->internal1 = var;
    }
    else if (type == SAC_FLOAT_T0)
    {
        header->t0 = var;
    }
    else if (type == SAC_FLOAT_T1)
    {
        header->t1 = var;
    }
    else if (type == SAC_FLOAT_T2)
    {
        header->t2 = var;
    }
    else if (type == SAC_FLOAT_T3)
    {
        header->t3 = var;
    }
    else if (type == SAC_FLOAT_T4)
    {
        header->t4 = var;
    }
    else if (type == SAC_FLOAT_T5)
    {
        header->t5 = var;
    }
    else if (type == SAC_FLOAT_T6)
    {
        header->t6 = var;
    }
    else if (type == SAC_FLOAT_T7)
    {
        header->t7 = var;
    }
    else if (type == SAC_FLOAT_T8)
    {
        header->t8 = var;
    }
    else if (type == SAC_FLOAT_T9)
    {
        header->t9 = var;
    }
    else if (type == SAC_FLOAT_F)
    {
        header->f = var;
    }
    else if (type == SAC_FLOAT_RESP0)
    {
        header->resp0 = var;
    }
    else if (type == SAC_FLOAT_RESP1)
    {
        header->resp1 = var;
    }
    else if (type == SAC_FLOAT_RESP2)
    {
        header->resp2 = var;
    }
    else if (type == SAC_FLOAT_RESP3)
    {
        header->resp3 = var;
    }
    else if (type == SAC_FLOAT_RESP4)
    {
        header->resp4 = var;
    }
    else if (type == SAC_FLOAT_RESP5)
    {
        header->resp5 = var;
    }
    else if (type == SAC_FLOAT_RESP6)
    {
        header->resp6 = var;
    }
    else if (type == SAC_FLOAT_RESP7)
    {
        header->resp7 = var;
    }
    else if (type == SAC_FLOAT_RESP8)
    {
        header->resp8 = var;
    }
    else if (type == SAC_FLOAT_RESP9)
    {
        header->resp9 = var;
    }
    else if (type == SAC_FLOAT_STLA)
    {
        header->stla = var;
    }
    else if (type == SAC_FLOAT_STLO)
    {
        header->stlo = var;
    }
    else if (type == SAC_FLOAT_STEL)
    {
        header->stel = var;
    }
    else if (type == SAC_FLOAT_STDP)
    {
        header->stdp = var;
    }
    else if (type == SAC_FLOAT_EVLA)
    {
        header->evla = var;
    }
    else if (type == SAC_FLOAT_EVLO)
    {
        header->evlo = var;
    }
    else if (type == SAC_FLOAT_EVEL)
    {
        header->evel = var;
    }
    else if (type == SAC_FLOAT_EVDP)
    {
        header->evdp = var;
    }
    else if (type == SAC_FLOAT_MAG)
    {
        header->mag = var;
    }
    else if (type == SAC_FLOAT_USER0)
    {
        header->user0 = var;
    }
    else if (type == SAC_FLOAT_USER1)
    {
        header->user1 = var;
    }
    else if (type == SAC_FLOAT_USER2)
    {
        header->user2 = var;
    }
    else if (type == SAC_FLOAT_USER3)
    {
        header->user3 = var;
    }
    else if (type == SAC_FLOAT_USER4)
    {
        header->user4 = var;
    }
    else if (type == SAC_FLOAT_USER5)
    {
        header->user5 = var;
    }
    else if (type == SAC_FLOAT_USER6)
    {
        header->user6 = var;
    }
    else if (type == SAC_FLOAT_USER7)
    {
        header->user7 = var;
    }
    else if (type == SAC_FLOAT_USER8)
    {
        header->user8 = var;
    }
    else if (type == SAC_FLOAT_USER9)
    {
        header->user9 = var;
    }
    else if (type == SAC_FLOAT_DIST)
    {
        header->dist = var;
    }
    else if (type == SAC_FLOAT_AZ)
    {
        header->az = var;
    }
    else if (type == SAC_FLOAT_BAZ)
    {
        header->baz = var;
    }
    else if (type == SAC_FLOAT_GCARC)
    {
        header->gcarc = var;
    }
    else if (type == SAC_FLOAT_INTERNAL2)
    {
        header->internal2 = var;
    }
    else if (type == SAC_FLOAT_DEPMEN)
    {
        header->depmen = var;
    }
    else if (type == SAC_FLOAT_CMPAZ)
    {
        header->cmpaz = var;
    }
    else if (type == SAC_FLOAT_CMPINC)
    {
        header->cmpinc = var;
    }
    else if (type == SAC_FLOAT_XMINIMUM)
    {
        header->xminimum = var;
    }
    else if (type == SAC_FLOAT_XMAXIMUM)
    {
        header->xmaximum = var;
    }
    else if (type == SAC_FLOAT_YMINIMUM)
    {
        header->yminimum = var;
    }
    else if (type == SAC_FLOAT_YMAXIMUM)
    {
        header->ymaximum = var;
    }
    else if (type == SAC_FLOAT_UNUSED0)
    {
        header->unused0 = var;
    }
    else if (type == SAC_FLOAT_UNUSED1)
    {
        header->unused1 = var;
    }
    else if (type == SAC_FLOAT_UNUSED2)
    {
        header->unused2 = var;
    }
    else if (type == SAC_FLOAT_UNUSED3)
    {
        header->unused3 = var;
    }
    else if (type == SAC_FLOAT_UNUSED4)
    {
        header->unused4 = var;
    }
    else if (type == SAC_FLOAT_UNUSED5)
    {
        header->unused5 = var;
    }
    else if (type == SAC_FLOAT_UNUSED6)
    {
        header->unused6 = var;
    }
    else
    {
        printf("%s: Invalid header type %d\n", __func__, (int) type);
        return -1;
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Sets an integer variable on the SAC header.
 *
 * @param[in] type     SAC header variable name to set.
 * @param[in] ivar     Value of variable to set in SAC header.
 *
 * @param[out] header  SAC header updated with new integer value .
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_header_utils
 *
 * @author Ben Baker
 *
 */
int sacioh5_setIntegerHeader(const enum sacHeader_enum type,
                             const int ivar,
                             struct sacH5Header_struct *header)
{
    if (type == SAC_INT_NZYEAR)
    {
        header->nzyear = ivar;
    }
    else if (type == SAC_INT_NZJDAY)
    {
        header->nzjday = ivar;
    }
    else if (type == SAC_INT_NZHOUR)
    {
        header->nzhour = ivar;
    }
    else if (type == SAC_INT_NZMIN)
    {
        header->nzmin = ivar;
    }
    else if (type == SAC_INT_NZSEC)
    {
        header->nzsec = ivar;
    }
    else if (type == SAC_INT_NZMSEC)
    {
        header->nzmsec = ivar;
    }
    else if (type == SAC_INT_NVHDR)
    {
        header->nvhdr = ivar;
    }
    else if (type == SAC_INT_NORID)
    {
        header->norid = ivar;
    }
    else if (type == SAC_INT_NEVID)
    {
        header->nevid = ivar;
    }
    else if (type == SAC_INT_NPTS)
    {
        header->npts = ivar;
    }
    else if (type == SAC_INT_INTERNAL1)
    {
        header->ninternal1 = ivar;
    }
    else if (type == SAC_INT_NWFID)
    {
        header->nwfid = ivar;
    }
    else if (type == SAC_INT_NXSIZE)
    {
        header->nxsize = ivar;
    }
    else if (type == SAC_INT_NYSIZE)
    {
        header->nysize = ivar;
    }
    else if (type == SAC_INT_UNUSED0)
    {
        header->nunused0 = ivar;
    }
    else if (type == SAC_INT_IFTYPE)
    {
        header->iftype = ivar;
    }
    else if (type == SAC_INT_IDEP)
    {
        header->idep = ivar;
    }
    else if (type == SAC_INT_IZTYPE)
    {
        header->iztype = ivar;
    }
    else if (type == SAC_INT_UNUSED1)
    {
        header->nunused1 = ivar;
    }
    else if (type == SAC_INT_IINST)
    {
        header->iinst = ivar;
    }
    else if (type == SAC_INT_ISTREG)
    {
        header->istreg = ivar;
    }
    else if (type == SAC_INT_IEVREG)
    {
        header->ievreg = ivar;
    }
    else if (type == SAC_INT_IEVTYP)
    {
        header->ievtyp = ivar;
    }
    else if (type == SAC_INT_IQUAL)
    {
        header->iqual = ivar;
    }
    else if (type == SAC_INT_ISYNTH)
    {
        header->isynth = ivar;
    }
    else if (type == SAC_INT_IMAGTYP)
    {
        header->imagtyp = ivar;
    }
    else if (type == SAC_INT_IMAGSRC)
    {
        header->imagsrc = ivar;
    }
    else if (type == SAC_INT_UNUSED2)
    {
        header->nunused2 = ivar;
    }
    else if (type == SAC_INT_UNUSED3)
    {
        header->nunused3 = ivar;
    }
    else if (type == SAC_INT_UNUSED4)
    {
        header->nunused4 = ivar;
    }
    else if (type == SAC_INT_UNUSED5)
    {
        header->nunused5 = ivar;
    }
    else if (type == SAC_INT_UNUSED6)
    {
        header->nunused6 = ivar;
    }
    else if (type == SAC_INT_UNUSED7)
    {
        header->nunused7 = ivar;
    }
    else if (type == SAC_INT_UNUSED8)
    {
        header->nunused8 = ivar;
    }
    else if (type == SAC_INT_UNUSED9)
    {
        header->nunused9 = ivar;
    }
    else 
    {
        printf("%s: Invalid header type %d\n", __func__, (int) type);
        return -1;
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Sets a boolean variable on the SAC header.
 *
 * @param[in] type     SAC header variable name to set.
 * @param[in] lvar     Value of variable to set in SAC header.
 *
 * @param[out] header  SAC header updated with new boolean value.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_header_utils
 *
 * @author Ben Baker
 *
 */
int sacioh5_setBooleanHeader(const enum sacHeader_enum type,
                             const bool lvar,
                             struct sacH5Header_struct *header)
{
    int ivar;
    ivar = (int) lvar;
    if (type == SAC_BOOL_LEVEN)
    {
        header->leven = ivar;
    }
    else if (type == SAC_BOOL_LPSPOL)
    {
        header->lpspol = ivar;
    }
    else if (type == SAC_BOOL_LOVROK)
    {
        header->lovrok = ivar;
    }
    else if (type == SAC_BOOL_LCALDA)
    {
        header->lcalda = ivar;
    }
    else if (type == SAC_BOOL_LUNUSED)
    {
        header->lunused = ivar;
    }
    else
    {
        printf("%s: Invalid header type\n", __func__);
        return -1;
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Sets a charater variable on the SAC header.
 *
 * @param[in] type     SAC header variable name to set.
 * @param[in] kvar     NULL terminated character variable to set. 
 *                     this should be length of 8 unless setting KEVNM
 *                     in which case this should be length 16.
 *
 * @param[out] header  SAC header updated with new character value .
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_h5_header_utils
 *
 * @author Ben Baker
 *
 */
int sacioh5_setCharacterHeader(const enum sacHeader_enum type,
                               const char *kvar,
                               struct sacH5Header_struct *header)
{
    if (type == SAC_CHAR_KSTNM)
    {
        writeChar8(kvar, &header->kstnm);
    }
    else if (type == SAC_CHAR_KEVNM)
    {
        writeChar16(kvar, &header->kevnm);
    }
    else if (type == SAC_CHAR_KHOLE)
    {
        writeChar8(kvar, &header->khole);
    }
    else if (type == SAC_CHAR_KO)
    {
        writeChar8(kvar, &header->ko);
    }
    else if (type == SAC_CHAR_KA)
    {
        writeChar8(kvar, &header->ka);
    }
    else if (type == SAC_CHAR_KT0)
    {
        writeChar8(kvar, &header->kt0);
    }
    else if (type == SAC_CHAR_KT1)
    {
        writeChar8(kvar, &header->kt1);
    }
    else if (type == SAC_CHAR_KT2)
    {
        writeChar8(kvar, &header->kt2);
    }
    else if (type == SAC_CHAR_KT3)
    {
        writeChar8(kvar, &header->kt3);
    }
    else if (type == SAC_CHAR_KT4)
    {
        writeChar8(kvar, &header->kt4);
    }
    else if (type == SAC_CHAR_KT5)
    {
        writeChar8(kvar, &header->kt5);
    }
    else if (type == SAC_CHAR_KT6)
    {
        writeChar8(kvar, &header->kt6);
    }
    else if (type == SAC_CHAR_KT7)
    {
        writeChar8(kvar, &header->kt7);
    }
    else if (type == SAC_CHAR_KT8)
    {
        writeChar8(kvar, &header->kt8);
    }
    else if (type == SAC_CHAR_KT9)
    {
        writeChar8(kvar, &header->kt9);
    }
    else if (type == SAC_CHAR_KF)
    {
        writeChar8(kvar, &header->kf);
    }
    else if (type == SAC_CHAR_KUSER0)
    {
        writeChar8(kvar, &header->kuser0);
    }
    else if (type == SAC_CHAR_KUSER1)
    {
        writeChar8(kvar, &header->kuser1);
    }
    else if (type == SAC_CHAR_KUSER2)
    {
        writeChar8(kvar, &header->kuser2);
    }
    else if (type == SAC_CHAR_KCMPNM)
    {
        writeChar8(kvar, &header->kcmpnm);
    }
    else if (type == SAC_CHAR_KNETWK)
    {
        writeChar8(kvar, &header->knetwk);
    }
    else if (type == SAC_CHAR_KDATRD)
    {
        writeChar8(kvar, &header->kdatrd);
    }
    else if (type == SAC_CHAR_KINST)
    {
        writeChar8(kvar, &header->kinst);
    }
    else
    {
        printf("%s: Invalid header type\n", __func__); 
        return -1;
    }
    return 0;
}

static void sacPZ2sach5PZ(const struct sacPoleZero_struct pz,
                          struct sacH5PoleZero_struct *pzh5)
{
    double *zre, *zim, *pre, *pim;
    int i;
    memset(pzh5, 0, sizeof(struct sacH5PoleZero_struct));
    if (!pz.lhavePZ){return;}
    pzh5->constant = pz.constant;
    pzh5->created = pz.created;
    pzh5->start = pz.start;
    pzh5->end = pz.end;
    pzh5->latitude = pz.latitude;
    pzh5->longitude = pz.longitude;
    pzh5->elevation = pz.elevation;
    pzh5->depth = pz.depth;
    pzh5->dip = pz.dip;
    pzh5->azimuth = pz.azimuth;
    pzh5->sampleRate = pz.sampleRate; 
    pzh5->instgain = pz.instgain;
    pzh5->sensitivity = pz.sensitivity;
    pzh5->a0 = pz.a0;
    pzh5->npoles = pz.npoles;
    pzh5->nzeros = pz.nzeros;

    writeChar64(pz.network, &pzh5->network);
    writeChar64(pz.station, &pzh5->station);
    writeChar64(pz.location, &pzh5->location);
    writeChar64(pz.channel, &pzh5->channel);
    writeChar64(pz.comment, &pzh5->comment);
    writeChar64(pz.description, &pzh5->description);
    writeChar64(pz.instrumentType, &pzh5->instrumentType);

    if (pz.nzeros > 0 && pz.zeros != NULL)
    {
        zre = (double *) calloc((size_t) pz.nzeros, sizeof(double));
        zim = (double *) calloc((size_t) pz.nzeros, sizeof(double));
        for (i=0; i<pz.nzeros; i++)
        {
            zre[i] = creal(pz.zeros[i]);
            zim[i] = cimag(pz.zeros[i]);
        }
        pzh5->zerosRe.p = zre;
        pzh5->zerosRe.len = (size_t) pz.nzeros;
        pzh5->zerosIm.p = zim;
        pzh5->zerosIm.len = (size_t) pz.nzeros;
    }
    if (pz.npoles > 0 && pz.poles != NULL)
    {
        pre = (double *) calloc((size_t) pz.npoles, sizeof(double));
        pim = (double *) calloc((size_t) pz.npoles, sizeof(double));
        for (i=0; i<pz.npoles; i++) 
        {
            pre[i] = creal(pz.poles[i]);
            pim[i] = cimag(pz.poles[i]);
        }

        pzh5->polesRe.p = pre;
        pzh5->polesRe.len = (size_t) pz.npoles;
        pzh5->polesIm.p = pim;
        pzh5->polesIm.len = (size_t) pz.npoles; 
    }
    pzh5->inputUnits = (int) pz.inputUnits;
    pzh5->instgainUnits = (int) pz.instgainUnits;
    pzh5->sensitivityUnits = (int) pz.instgainUnits; 
    pzh5->lhavePZ = (int) pz.lhavePZ;
    return;
}

static void sacFAP2sach5FAP(const struct sacFAP_struct fap,
                            struct sacH5FAP_struct *faph5)
{
    double *amp, *freqs, *phase;
    memset(faph5, 0, sizeof(struct sacH5FAP_struct));
    if (!fap.lhaveFAP){return;}
    faph5->nf = fap.nf;
    faph5->lhaveFAP = (int) fap.lhaveFAP;
    if (fap.nf > 0)
    {
        freqs = (double *) calloc((size_t) fap.nf, sizeof(double));
        amp   = (double *) calloc((size_t) fap.nf, sizeof(double));
        phase = (double *) calloc((size_t) fap.nf, sizeof(double));
        //array_copy64f_work(fap.nf, fap.freqs, freqs);
        //array_copy64f_work(fap.nf, fap.amp,   amp);
        //array_copy64f_work(fap.nf, fap.phase, phase);
        memcpy(freqs, fap.freqs, (size_t) fap.nf*sizeof(double));
        memcpy(amp,   fap.amp,   (size_t) fap.nf*sizeof(double));
        memcpy(phase, fap.phase, (size_t) fap.nf*sizeof(double));
        faph5->freqs.p   = (void *) freqs; 
        faph5->freqs.len = (size_t) fap.nf;
        faph5->amp.p     = (void *) amp;
        faph5->amp.len   = (size_t) fap.nf;
        faph5->phase.p   = (void *) phase;
        faph5->phase.len = (size_t) fap.nf;
    }
    return;
}

static void sacHeader2sach5Header(const struct sacHeader_struct sac,
                                  struct sacH5Header_struct *sach5)
{
    memset(sach5, 0, sizeof(struct sacH5Header_struct));
    if (!sac.lhaveHeader)
    {
        printf("%s: default h5 sac header maker function not yet done\n",
               __func__);
    }
    // floats 
    sacioh5_setFloatHeader(SAC_FLOAT_DELTA,     sac.delta,     sach5); 
    sacioh5_setFloatHeader(SAC_FLOAT_DEPMIN,    sac.depmin,    sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_DEPMAX,    sac.depmax,    sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_SCALE,     sac.scale,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_ODELTA,    sac.odelta,    sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_B,         sac.b,         sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_E,         sac.e,         sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_O,         sac.o,         sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_A,         sac.a,         sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_INTERNAL1, sac.internal1, sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_T0,        sac.t0,        sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_T1,        sac.t1,        sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_T2,        sac.t2,        sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_T3,        sac.t3,        sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_T4,        sac.t4,        sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_T5,        sac.t5,        sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_T6,        sac.t6,        sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_T7,        sac.t7,        sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_T8,        sac.t8,        sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_T9,        sac.t9,        sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_F,         sac.f,         sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_RESP0,     sac.resp0,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_RESP1,     sac.resp1,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_RESP2,     sac.resp2,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_RESP3,     sac.resp3,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_RESP4,     sac.resp4,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_RESP5,     sac.resp5,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_RESP6,     sac.resp6,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_RESP7,     sac.resp7,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_RESP8,     sac.resp8,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_RESP9,     sac.resp9,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_STLA,      sac.stla,      sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_STLO,      sac.stlo,      sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_STEL,      sac.stel,      sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_STDP,      sac.stdp,      sach5); 
    sacioh5_setFloatHeader(SAC_FLOAT_EVLA,      sac.evla,      sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_EVLO,      sac.evlo,      sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_EVDP,      sac.evdp,      sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_MAG,       sac.mag,       sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_USER0,     sac.user0,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_USER1,     sac.user1,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_USER2,     sac.user2,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_USER3,     sac.user3,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_USER4,     sac.user4,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_USER5,     sac.user5,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_USER6,     sac.user6,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_USER7,     sac.user7,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_USER8,     sac.user8,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_USER9,     sac.user9,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_DIST,      sac.dist,      sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_AZ,        sac.az,        sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_BAZ,       sac.baz,       sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_GCARC,     sac.gcarc,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_INTERNAL2, sac.internal2, sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_DEPMEN,    sac.depmen,    sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_CMPAZ,     sac.cmpaz,     sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_CMPINC,    sac.cmpinc,    sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_XMINIMUM,  sac.xminimum,  sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_XMAXIMUM,  sac.xmaximum,  sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_YMINIMUM,  sac.yminimum,  sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_YMAXIMUM,  sac.ymaximum,  sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_UNUSED0,   sac.unused0,   sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_UNUSED1,   sac.unused1,   sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_UNUSED2,   sac.unused2,   sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_UNUSED3,   sac.unused3,   sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_UNUSED4,   sac.unused4,   sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_UNUSED5,   sac.unused5,   sach5);
    sacioh5_setFloatHeader(SAC_FLOAT_UNUSED6,   sac.unused6,   sach5);
    // integers
    sacioh5_setIntegerHeader(SAC_INT_NZYEAR,    sac.nzyear,     sach5);
    sacioh5_setIntegerHeader(SAC_INT_NZJDAY,    sac.nzjday,     sach5);
    sacioh5_setIntegerHeader(SAC_INT_NZHOUR,    sac.nzhour,     sach5);
    sacioh5_setIntegerHeader(SAC_INT_NZMIN,     sac.nzmin,      sach5);
    sacioh5_setIntegerHeader(SAC_INT_NZSEC,     sac.nzsec,      sach5);
    sacioh5_setIntegerHeader(SAC_INT_NZMSEC,    sac.nzmsec,     sach5);
    sacioh5_setIntegerHeader(SAC_INT_NVHDR,     sac.nvhdr,      sach5);
    sacioh5_setIntegerHeader(SAC_INT_NORID,     sac.norid,      sach5);
    sacioh5_setIntegerHeader(SAC_INT_NEVID,     sac.nevid,      sach5);
    sacioh5_setIntegerHeader(SAC_INT_NPTS,      sac.npts,       sach5);
    sacioh5_setIntegerHeader(SAC_INT_INTERNAL1, sac.ninternal1, sach5);
    sacioh5_setIntegerHeader(SAC_INT_NWFID,     sac.nwfid,      sach5);
    sacioh5_setIntegerHeader(SAC_INT_NXSIZE,    sac.nxsize,     sach5);
    sacioh5_setIntegerHeader(SAC_INT_NYSIZE,    sac.nysize,     sach5);
    sacioh5_setIntegerHeader(SAC_INT_UNUSED0,   sac.nunused0,   sach5);
    sacioh5_setIntegerHeader(SAC_INT_IFTYPE,    sac.iftype,     sach5);
    sacioh5_setIntegerHeader(SAC_INT_IDEP,      sac.idep,       sach5);
    sacioh5_setIntegerHeader(SAC_INT_IZTYPE,    sac.iztype,     sach5);
    sacioh5_setIntegerHeader(SAC_INT_UNUSED1,   sac.nunused1,   sach5);
    sacioh5_setIntegerHeader(SAC_INT_IINST,     sac.iinst,      sach5);
    sacioh5_setIntegerHeader(SAC_INT_ISTREG,    sac.istreg,     sach5);
    sacioh5_setIntegerHeader(SAC_INT_IEVREG,    sac.ievreg,     sach5);
    sacioh5_setIntegerHeader(SAC_INT_IEVTYP,    sac.ievtyp,     sach5);
    sacioh5_setIntegerHeader(SAC_INT_IQUAL,     sac.iqual,      sach5);
    sacioh5_setIntegerHeader(SAC_INT_ISYNTH,    sac.isynth,     sach5);
    sacioh5_setIntegerHeader(SAC_INT_IMAGTYP,   sac.imagtyp,    sach5);
    sacioh5_setIntegerHeader(SAC_INT_IMAGSRC,   sac.imagsrc,    sach5);
    sacioh5_setIntegerHeader(SAC_INT_UNUSED2,   sac.nunused2,   sach5);
    sacioh5_setIntegerHeader(SAC_INT_UNUSED3,   sac.nunused3,   sach5);
    sacioh5_setIntegerHeader(SAC_INT_UNUSED4,   sac.nunused4,   sach5);
    sacioh5_setIntegerHeader(SAC_INT_UNUSED5,   sac.nunused5,   sach5);
    sacioh5_setIntegerHeader(SAC_INT_UNUSED6,   sac.nunused6,   sach5);
    sacioh5_setIntegerHeader(SAC_INT_UNUSED7,   sac.nunused7,   sach5);
    sacioh5_setIntegerHeader(SAC_INT_UNUSED8,   sac.nunused8,   sach5);
    sacioh5_setIntegerHeader(SAC_INT_UNUSED9,   sac.nunused9,   sach5);
    // booleans
    sacioh5_setBooleanHeader(SAC_BOOL_LEVEN,   sac.leven, sach5);
    sacioh5_setBooleanHeader(SAC_BOOL_LPSPOL,  sac.lpspol, sach5);
    sacioh5_setBooleanHeader(SAC_BOOL_LOVROK,  sac.lovrok, sach5);
    sacioh5_setBooleanHeader(SAC_BOOL_LCALDA,  sac.lcalda, sach5);
    sacioh5_setBooleanHeader(SAC_BOOL_LUNUSED, sac.lunused, sach5);
    // strings
    sacioh5_setCharacterHeader(SAC_CHAR_KSTNM,  sac.kstnm,  sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KEVNM,  sac.kevnm,  sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KHOLE,  sac.khole,  sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KO,     sac.ko,     sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KA,     sac.ka,     sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KT0,    sac.kt0,    sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KT1,    sac.kt1,    sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KT2,    sac.kt2,    sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KT3,    sac.kt3,    sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KT4,    sac.kt4,    sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KT5,    sac.kt5,    sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KT6,    sac.kt6,    sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KT7,    sac.kt7,    sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KT8,    sac.kt8,    sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KT9,    sac.kt9,    sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KF,     sac.kf,     sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KUSER0, sac.kuser0, sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KUSER1, sac.kuser1, sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KUSER2, sac.kuser2, sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KCMPNM, sac.kcmpnm, sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KNETWK, sac.knetwk, sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KDATRD, sac.kdatrd, sach5);
    sacioh5_setCharacterHeader(SAC_CHAR_KINST,  sac.kinst,  sach5);
    return;
}

static void sach5PZ2sacPZ(const struct sacH5PoleZero_struct sach5PZ,
                          struct sacPoleZero_struct *pz)
{
    double *pre, *pim, *zre, *zim;
    //size_t nalloc;
    int i;
    memset(pz, 0, sizeof(struct sacPoleZero_struct));
    readChar64(sach5PZ.network, pz->network);
    readChar64(sach5PZ.station, pz->station);
    readChar64(sach5PZ.location, pz->location);
    readChar64(sach5PZ.channel, pz->channel);
    readChar64(sach5PZ.comment, pz->comment); 
    readChar64(sach5PZ.description, pz->description);
    readChar64(sach5PZ.instrumentType, pz->instrumentType);
    pz->constant = sach5PZ.constant;
    pz->created = sach5PZ.created;
    pz->start = sach5PZ.start;
    pz->end = sach5PZ.end;
    pz->latitude = sach5PZ.latitude;
    pz->longitude = sach5PZ.longitude;
    pz->elevation = sach5PZ.elevation;
    pz->depth = sach5PZ.depth;
    pz->dip = sach5PZ.dip;
    pz->azimuth = sach5PZ.azimuth;
    pz->sampleRate = sach5PZ.sampleRate;
    pz->instgain = sach5PZ.instgain;
    pz->sensitivity = sach5PZ.sensitivity;
    pz->a0 = sach5PZ.a0;
    pz->inputUnits = (enum sacUnits_enum) sach5PZ.inputUnits;
    pz->instgainUnits = (enum sacUnits_enum) sach5PZ.instgainUnits;
    pz->sensitivityUnits = (enum sacUnits_enum) sach5PZ.sensitivityUnits;
    pz->npoles = (int) sach5PZ.polesRe.len;
    pz->nzeros = (int) sach5PZ.zerosRe.len;
    if (pz->npoles > 0)
    {
        pz->poles = sacio_malloc64z(pz->npoles);
/*
        nalloc = (size_t) pz->npoles*sizeof(double complex);
#ifdef USE_POSIX
        posix_memalign((void **) &pz->poles, 64, nalloc);
#else
        pz->poles = (double complex *) aligned_alloc(64, nalloc);
#endif
*/
        pre = (double *) sach5PZ.polesRe.p;
        pim = (double *) sach5PZ.polesIm.p;
        for (i=0; i<pz->npoles; i++)
        {
            pz->poles[i] = DCMPLX(pre[i], pim[i]);
        }
        pre = NULL;
        pim = NULL;
     }
    if (pz->nzeros > 0)
    {
        pz->zeros = sacio_malloc64z(pz->nzeros);
/*
        nalloc = (size_t) pz->nzeros*sizeof(double complex);
#ifdef USE_POSIX
        posix_memalign((void **) &pz->zeros, 64, nalloc);
#else
        pz->zeros = (double complex *) aligned_alloc(64, nalloc);
#endif
*/
        zre = (double *) sach5PZ.zerosRe.p;
        zim = (double *) sach5PZ.zerosIm.p;
        for (i=0; i<pz->nzeros; i++) 
        {
            pz->zeros[i] = DCMPLX(zre[i], zim[i]);
        }    
        zre = NULL;
        zim = NULL;
    }
    pz->lhavePZ = (bool) sach5PZ.lhavePZ;
    return;
}

static void sach5FAP2sacFAP(const struct sacH5FAP_struct sach5FAP,
                            struct sacFAP_struct *fap) 
{
    double *amp, *freqs, *phase; 
    memset(fap, 0, sizeof(struct sacFAP_struct));
    fap->nf = sach5FAP.nf;
    if (fap->nf > 0)
    {
        fap->freqs = sacio_malloc64f(fap->nf);
        fap->amp = sacio_malloc64f(fap->nf);
        fap->phase = sacio_malloc64f(fap->nf);
        freqs = (double *) sach5FAP.freqs.p;
        amp   = (double *) sach5FAP.amp.p;
        phase = (double *) sach5FAP.phase.p;
        //array_copy64f_work(fap->nf, freqs, fap->freqs);
        //array_copy64f_work(fap->nf, amp,   fap->amp);
        //array_copy64f_work(fap->nf, phase, fap->phase);
        memcpy(fap->freqs, freqs, (size_t) fap->nf*sizeof(double));
        memcpy(fap->amp,   amp,   (size_t) fap->nf*sizeof(double));
        memcpy(fap->phase, phase, (size_t) fap->nf*sizeof(double));
        freqs = NULL;
        amp = NULL;
        phase = NULL;
    }
    fap->lhaveFAP = (bool) sach5FAP.lhaveFAP;
    return;
}

static void sach5Header2sacHeader(const struct sacH5Header_struct sach5,
                                  struct sacHeader_struct *sac)
{
    memset(sac, 0, sizeof(struct sacHeader_struct));
    // floats 
    sacioh5_getFloatHeader(SAC_FLOAT_DELTA,     sach5, &sac->delta);
    sacioh5_getFloatHeader(SAC_FLOAT_DEPMIN,    sach5, &sac->depmen);
    sacioh5_getFloatHeader(SAC_FLOAT_DEPMAX,    sach5, &sac->depmax);
    sacioh5_getFloatHeader(SAC_FLOAT_SCALE,     sach5, &sac->scale);
    sacioh5_getFloatHeader(SAC_FLOAT_ODELTA,    sach5, &sac->odelta);
    sacioh5_getFloatHeader(SAC_FLOAT_B,         sach5, &sac->b);
    sacioh5_getFloatHeader(SAC_FLOAT_E,         sach5, &sac->e);
    sacioh5_getFloatHeader(SAC_FLOAT_O,         sach5, &sac->o);
    sacioh5_getFloatHeader(SAC_FLOAT_A,         sach5, &sac->a);
    sacioh5_getFloatHeader(SAC_FLOAT_INTERNAL1, sach5, &sac->internal1);
    sacioh5_getFloatHeader(SAC_FLOAT_T0,        sach5, &sac->t0);
    sacioh5_getFloatHeader(SAC_FLOAT_T1,        sach5, &sac->t1);
    sacioh5_getFloatHeader(SAC_FLOAT_T2,        sach5, &sac->t2);
    sacioh5_getFloatHeader(SAC_FLOAT_T3,        sach5, &sac->t3);
    sacioh5_getFloatHeader(SAC_FLOAT_T4,        sach5, &sac->t4);
    sacioh5_getFloatHeader(SAC_FLOAT_T5,        sach5, &sac->t5);
    sacioh5_getFloatHeader(SAC_FLOAT_T6,        sach5, &sac->t6);
    sacioh5_getFloatHeader(SAC_FLOAT_T7,        sach5, &sac->t7);
    sacioh5_getFloatHeader(SAC_FLOAT_T8,        sach5, &sac->t8);
    sacioh5_getFloatHeader(SAC_FLOAT_T9,        sach5, &sac->t9);
    sacioh5_getFloatHeader(SAC_FLOAT_F,         sach5, &sac->f);
    sacioh5_getFloatHeader(SAC_FLOAT_RESP0,     sach5, &sac->resp0);
    sacioh5_getFloatHeader(SAC_FLOAT_RESP1,     sach5, &sac->resp1);
    sacioh5_getFloatHeader(SAC_FLOAT_RESP2,     sach5, &sac->resp2);
    sacioh5_getFloatHeader(SAC_FLOAT_RESP3,     sach5, &sac->resp3);
    sacioh5_getFloatHeader(SAC_FLOAT_RESP4,     sach5, &sac->resp4);
    sacioh5_getFloatHeader(SAC_FLOAT_RESP5,     sach5, &sac->resp5);
    sacioh5_getFloatHeader(SAC_FLOAT_RESP6,     sach5, &sac->resp6);
    sacioh5_getFloatHeader(SAC_FLOAT_RESP7,     sach5, &sac->resp7);
    sacioh5_getFloatHeader(SAC_FLOAT_RESP8,     sach5, &sac->resp8);
    sacioh5_getFloatHeader(SAC_FLOAT_RESP9,     sach5, &sac->resp9);
    sacioh5_getFloatHeader(SAC_FLOAT_STLA,      sach5, &sac->stla);
    sacioh5_getFloatHeader(SAC_FLOAT_STLO,      sach5, &sac->stlo);
    sacioh5_getFloatHeader(SAC_FLOAT_STEL,      sach5, &sac->stel);
    sacioh5_getFloatHeader(SAC_FLOAT_STDP,      sach5, &sac->stdp);
    sacioh5_getFloatHeader(SAC_FLOAT_EVLA,      sach5, &sac->evla);
    sacioh5_getFloatHeader(SAC_FLOAT_EVLO,      sach5, &sac->evlo);
    sacioh5_getFloatHeader(SAC_FLOAT_EVDP,      sach5, &sac->evdp);
    sacioh5_getFloatHeader(SAC_FLOAT_MAG,       sach5, &sac->mag);
    sacioh5_getFloatHeader(SAC_FLOAT_USER0,     sach5, &sac->user0);
    sacioh5_getFloatHeader(SAC_FLOAT_USER1,     sach5, &sac->user1);
    sacioh5_getFloatHeader(SAC_FLOAT_USER2,     sach5, &sac->user2);
    sacioh5_getFloatHeader(SAC_FLOAT_USER3,     sach5, &sac->user3);
    sacioh5_getFloatHeader(SAC_FLOAT_USER4,     sach5, &sac->user4);
    sacioh5_getFloatHeader(SAC_FLOAT_USER5,     sach5, &sac->user5);
    sacioh5_getFloatHeader(SAC_FLOAT_USER6,     sach5, &sac->user6);
    sacioh5_getFloatHeader(SAC_FLOAT_USER7,     sach5, &sac->user7);
    sacioh5_getFloatHeader(SAC_FLOAT_USER8,     sach5, &sac->user8);
    sacioh5_getFloatHeader(SAC_FLOAT_USER9,     sach5, &sac->user9);
    sacioh5_getFloatHeader(SAC_FLOAT_DIST,      sach5, &sac->dist);
    sacioh5_getFloatHeader(SAC_FLOAT_AZ,        sach5, &sac->az);
    sacioh5_getFloatHeader(SAC_FLOAT_BAZ,       sach5, &sac->baz);
    sacioh5_getFloatHeader(SAC_FLOAT_GCARC,     sach5, &sac->gcarc);
    sacioh5_getFloatHeader(SAC_FLOAT_INTERNAL2, sach5, &sac->internal2);
    sacioh5_getFloatHeader(SAC_FLOAT_DEPMEN,    sach5, &sac->depmen);
    sacioh5_getFloatHeader(SAC_FLOAT_CMPAZ,     sach5, &sac->cmpaz);
    sacioh5_getFloatHeader(SAC_FLOAT_CMPINC,    sach5, &sac->cmpinc);
    sacioh5_getFloatHeader(SAC_FLOAT_XMINIMUM,  sach5, &sac->xminimum);
    sacioh5_getFloatHeader(SAC_FLOAT_XMAXIMUM,  sach5, &sac->xmaximum);
    sacioh5_getFloatHeader(SAC_FLOAT_YMINIMUM,  sach5, &sac->yminimum);
    sacioh5_getFloatHeader(SAC_FLOAT_YMAXIMUM,  sach5, &sac->ymaximum);
    sacioh5_getFloatHeader(SAC_FLOAT_UNUSED0,   sach5, &sac->unused0);
    sacioh5_getFloatHeader(SAC_FLOAT_UNUSED1,   sach5, &sac->unused1);
    sacioh5_getFloatHeader(SAC_FLOAT_UNUSED2,   sach5, &sac->unused2);
    sacioh5_getFloatHeader(SAC_FLOAT_UNUSED3,   sach5, &sac->unused3);
    sacioh5_getFloatHeader(SAC_FLOAT_UNUSED4,   sach5, &sac->unused4);
    sacioh5_getFloatHeader(SAC_FLOAT_UNUSED5,   sach5, &sac->unused5);
    sacioh5_getFloatHeader(SAC_FLOAT_UNUSED6,   sach5, &sac->unused6);
    // integers
    sacioh5_getIntegerHeader(SAC_INT_NZYEAR,    sach5, &sac->nzyear);
    sacioh5_getIntegerHeader(SAC_INT_NZJDAY,    sach5, &sac->nzjday);
    sacioh5_getIntegerHeader(SAC_INT_NZHOUR,    sach5, &sac->nzhour);
    sacioh5_getIntegerHeader(SAC_INT_NZMIN,     sach5, &sac->nzmin);
    sacioh5_getIntegerHeader(SAC_INT_NZSEC,     sach5, &sac->nzsec);
    sacioh5_getIntegerHeader(SAC_INT_NZMSEC,    sach5, &sac->nzmsec);
    sacioh5_getIntegerHeader(SAC_INT_NVHDR,     sach5, &sac->nvhdr);
    sacioh5_getIntegerHeader(SAC_INT_NORID,     sach5, &sac->norid);
    sacioh5_getIntegerHeader(SAC_INT_NEVID,     sach5, &sac->nevid);
    sacioh5_getIntegerHeader(SAC_INT_NPTS,      sach5, &sac->npts);
    sacioh5_getIntegerHeader(SAC_INT_INTERNAL1, sach5, &sac->ninternal1);
    sacioh5_getIntegerHeader(SAC_INT_NWFID,     sach5, &sac->nwfid);
    sacioh5_getIntegerHeader(SAC_INT_NXSIZE,    sach5, &sac->nxsize);
    sacioh5_getIntegerHeader(SAC_INT_NYSIZE,    sach5, &sac->nysize);
    sacioh5_getIntegerHeader(SAC_INT_UNUSED0,   sach5, &sac->nunused0);
    sacioh5_getIntegerHeader(SAC_INT_IFTYPE,    sach5, &sac->iftype);
    sacioh5_getIntegerHeader(SAC_INT_IDEP,      sach5, &sac->idep);
    sacioh5_getIntegerHeader(SAC_INT_IZTYPE,    sach5, &sac->iztype);
    sacioh5_getIntegerHeader(SAC_INT_UNUSED1,   sach5, &sac->nunused1);
    sacioh5_getIntegerHeader(SAC_INT_IINST,     sach5, &sac->iinst);
    sacioh5_getIntegerHeader(SAC_INT_ISTREG,    sach5, &sac->istreg);
    sacioh5_getIntegerHeader(SAC_INT_IEVREG,    sach5, &sac->ievreg);
    sacioh5_getIntegerHeader(SAC_INT_IEVTYP,    sach5, &sac->ievtyp);
    sacioh5_getIntegerHeader(SAC_INT_IQUAL,     sach5, &sac->iqual);
    sacioh5_getIntegerHeader(SAC_INT_ISYNTH,    sach5, &sac->isynth);
    sacioh5_getIntegerHeader(SAC_INT_IMAGTYP,   sach5, &sac->imagtyp);
    sacioh5_getIntegerHeader(SAC_INT_IMAGSRC,   sach5, &sac->imagsrc);
    sacioh5_getIntegerHeader(SAC_INT_UNUSED2,   sach5, &sac->nunused2);
    sacioh5_getIntegerHeader(SAC_INT_UNUSED3,   sach5, &sac->nunused3);
    sacioh5_getIntegerHeader(SAC_INT_UNUSED4,   sach5, &sac->nunused4);
    sacioh5_getIntegerHeader(SAC_INT_UNUSED5,   sach5, &sac->nunused5);
    sacioh5_getIntegerHeader(SAC_INT_UNUSED6,   sach5, &sac->nunused6);
    sacioh5_getIntegerHeader(SAC_INT_UNUSED7,   sach5, &sac->nunused7);
    sacioh5_getIntegerHeader(SAC_INT_UNUSED8,   sach5, &sac->nunused8);
    sacioh5_getIntegerHeader(SAC_INT_UNUSED9,   sach5, &sac->nunused9);
    // booleans
    sacioh5_getBooleanHeader(SAC_BOOL_LEVEN,   sach5, &sac->leven);
    sacioh5_getBooleanHeader(SAC_BOOL_LPSPOL,  sach5, &sac->lpspol);
    sacioh5_getBooleanHeader(SAC_BOOL_LOVROK,  sach5, &sac->lovrok);
    sacioh5_getBooleanHeader(SAC_BOOL_LCALDA,  sach5, &sac->lcalda);
    sacioh5_getBooleanHeader(SAC_BOOL_LUNUSED, sach5, &sac->lunused);
    // strings
    sacioh5_getCharacterHeader(SAC_CHAR_KSTNM,  sach5, sac->kstnm);
    sacioh5_getCharacterHeader(SAC_CHAR_KEVNM,  sach5, sac->kevnm);
    sacioh5_getCharacterHeader(SAC_CHAR_KHOLE,  sach5, sac->khole);
    sacioh5_getCharacterHeader(SAC_CHAR_KO,     sach5, sac->ko);
    sacioh5_getCharacterHeader(SAC_CHAR_KA,     sach5, sac->ka);
    sacioh5_getCharacterHeader(SAC_CHAR_KT0,    sach5, sac->kt0);
    sacioh5_getCharacterHeader(SAC_CHAR_KT1,    sach5, sac->kt1);
    sacioh5_getCharacterHeader(SAC_CHAR_KT2,    sach5, sac->kt2);
    sacioh5_getCharacterHeader(SAC_CHAR_KT3,    sach5, sac->kt3);
    sacioh5_getCharacterHeader(SAC_CHAR_KT4,    sach5, sac->kt4);
    sacioh5_getCharacterHeader(SAC_CHAR_KT5,    sach5, sac->kt5);
    sacioh5_getCharacterHeader(SAC_CHAR_KT6,    sach5, sac->kt6);
    sacioh5_getCharacterHeader(SAC_CHAR_KT7,    sach5, sac->kt7);
    sacioh5_getCharacterHeader(SAC_CHAR_KT8,    sach5, sac->kt8);
    sacioh5_getCharacterHeader(SAC_CHAR_KT9,    sach5, sac->kt9);
    sacioh5_getCharacterHeader(SAC_CHAR_KF,     sach5, sac->kf);
    sacioh5_getCharacterHeader(SAC_CHAR_KUSER0, sach5, sac->kuser0);
    sacioh5_getCharacterHeader(SAC_CHAR_KUSER1, sach5, sac->kuser1);
    sacioh5_getCharacterHeader(SAC_CHAR_KUSER2, sach5, sac->kuser2);
    sacioh5_getCharacterHeader(SAC_CHAR_KCMPNM, sach5, sac->kcmpnm);
    sacioh5_getCharacterHeader(SAC_CHAR_KNETWK, sach5, sac->knetwk);
    sacioh5_getCharacterHeader(SAC_CHAR_KDATRD, sach5, sac->kdatrd);
    sacioh5_getCharacterHeader(SAC_CHAR_KINST,  sach5, sac->kinst);
    sac->lhaveHeader = true;
    return;
}

static void readChar8(const hvl_t t, char *__restrict__ kvar)
{
    char *k8;
    size_t lenos;
    memset(kvar, 0, 8*sizeof(char));
    k8 = t.p;
    lenos = t.len;
    strncpy(kvar, k8, MIN(lenos, 8));
    k8 = NULL;
    return;
}

static void readChar16(const hvl_t t, char *__restrict__ kvar)
{
    char *k16; 
    size_t lenos;
    memset(kvar, 0, 16*sizeof(char));
    k16 = t.p; 
    lenos = t.len;
    strncpy(kvar, k16, MIN(lenos, 16));
    k16 = NULL;
    return;
}

static void readChar64(const hvl_t t, char *__restrict__ kvar)
{
    char *k64; 
    size_t lenos;
    memset(kvar, 0, 64*sizeof(char));
    k64 = t.p; 
    lenos = t.len;
    strncpy(kvar, k64, MIN(lenos, 64));
    k64 = NULL;
    return;
}

static void writeChar8(const char *__restrict__ kvar, hvl_t *t)
{
    char *k8;
    size_t lenos;
    lenos = MIN(8, strlen(kvar));
    k8 = (char *) calloc(8 + PAD, sizeof(char));
    strncpy(k8, kvar, lenos);
    t->p = (void *) k8; 
    t->len = 8 + PAD;
    return;
}

static void writeChar16(const char *__restrict__ kvar, hvl_t *t) 
{
    char *k16;
    size_t lenos;
    lenos = MIN(16, strlen(kvar));
    k16 = (char *) calloc(16 + PAD, sizeof(char));
    strncpy(k16, kvar, lenos);
    t->p = (void *) k16;
    t->len = 16 + PAD; //lenos; 
    return;
}

static void writeChar64(const char *__restrict__ kvar, hvl_t *t)  
{
    char *k64;
    size_t lenos;
    lenos = MIN(64, strlen(kvar));
    k64 = (char *) calloc(64 + PAD, sizeof(char));
    strncpy(k64, kvar, lenos);
    t->p = (void *) k64; 
    t->len = 64 + PAD; //lenos; 
    return;
}

static void freeHVLt(hvl_t *t)
{
    if (t->p != NULL)
    {
        free(t->p);
        t->p = NULL;
    }
    t->len = 0;
    return;
}
