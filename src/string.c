#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdbool.h>
#include <ctype.h>
#include "sacio_string.h"

#ifndef MAX
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#endif
#ifndef MIN
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#endif

/*!
 * @defgroup sac_io_string String
 * @brief String handling utilities.  These files have beeen copied from
 *        ISCL, their names appropriately modified to conform with the
 *        sacio namespacing, all items are allocatable/deallocatable
 *        with the standard library, and return codes are now integers.
 * @ingroup sac
 * @copyright ISTI distributed under the Apache 2 license.
 */

/*!
 * @brief Removes all leading and trailing whitespaces, or optionally
 *        the given pattern, from a NULL terminated string
 * @param[in] pattern    Pattern to search for when stripping string.
 *                       If NULL then all leading and trailing whitespaces
 *                       will be removed.
 * @param[in] string     NULL terminated string from which to remove
 *                       trailing blank space.
 *
 * @result NULL terminated string with leading and trailing blank spaces
 *         removed.  The size can be determined from strlen and this
 *         should be freed with stdlib's free().
 * @ingroup sac_io_string
 * @author Ben Baker
 */
char *sacio_string_strip(const char *pattern, const char *string)
{
    char *lstrip, *strip;
    // remove leading blanks
    lstrip = sacio_string_lstrip(pattern, string);
    // did anything come back?
    if (lstrip == NULL){return lstrip;}
    if (strlen(lstrip) == 0){return lstrip;}
    // remove trailing blanks
    strip = sacio_string_rstrip(pattern, lstrip);
    free(lstrip);
    return strip;
}
/*!
 * @brief Removes all leading whitespaces, or optionally the given
 *        pattern, from a NULL terminated string.
 *
 * @param[in] pattern    Pattern to search for when stripping string.
 *                       If NULL then all leading whitespaces will
 *                       be stripped.
 * @param[in] string     NULL terminated string from which to remove
 *                       leading blank space.
 *
 * @result Null terminated string with leading blank spaces removed.
 *         It's length can be deduced from strlen().  This should
 *         be freed with stdlib's free();
 * @ingroup sac_io_string
 * @author Ben Baker
 */
char *sacio_string_lstrip(const char *pattern, const char *string)
{
    char *lstrip;
    int i, i1, job, l, len, lenp, lenos;
    bool lfound;
    lstrip = NULL;
    if (string == NULL)
    {
        lstrip = (char *) calloc(8, sizeof(char));
        return lstrip;
    }
    // Classify the job - i expect user will want to remove blank space
    job = 2;
    lenp = 0;
    if (pattern == NULL)
    {
        job = 1;
    }
    else
    {
        lenp = (int) strlen(pattern);
        if (lenp == 0)
        {
            job = 1;
        }
        if (lenp == 1 && pattern[0] == ' ')
        {
            job = 1;
        }
    }
    lenos = (int) strlen(string);
    // Removing blanks
    if (job == 1)
    {
        // Search string until we encounter something that isn't a `space'
        i1 = 0;
        for (i=0; i<lenos; i++)
        {
            if (!isspace(string[i]))
            {
                i1 = i;
                break;
            }
        }
    }
    // More generally removing any desired character 
    else
    {
        i1 = 0;
        for (i=0; i<lenos; i++)
        {
            // Does the i'th index match any of these characters?
            lfound = false;
            for (l=0; l<lenp; l++)
            {
                // Blank space option
                if (pattern[l] == ' ')
                {
                    if (isspace(string[i]))
                    {
                        lfound = true;
                        break;
                    }
                }
                // More general desired character
                else
                {
                    if (string[i] == pattern[l])
                    {
                        lfound = true;
                        break;
                    }
                }
            } // Loop on pattern length
            // It doesn't match any character - we're done
            if (!lfound)
            {
                i1 = i;
                break;
            }
        } // Loop on string length
    }
    len = lenos - i1;
    lstrip = (char *) calloc((size_t) (MAX((int) len+1, 8)), sizeof(char));
    strncpy(lstrip, &string[i1], (size_t) len);
    return lstrip;
}
/*!
 * @brief Removes all trailing whitespaces, or optionally the given
 *        pattern, from a NULL terminated string.
 *
 * @param[in] pattern    Pattern to search for when stripping string.
 *                       If NULL then all trailing whitespaces will
 *                       be removed..
 * @param[in] string     NULL terminated string from which to remove
 *                       trailing blank space.
 *
 * @result NULL terminated string with trailing blank spaces removed.
 *         The length can be deduced with strlen.  This should be
 *         freed with stdlib's free().
 * @ingroup sac_io_string
 * @author Ben Baker (ISTI)
 */
char *sacio_string_rstrip(const char *pattern, const char *string)
{
    char *rstrip;
    int i, i2, job, l, len, lenp, lenos;
    bool lfound;
    rstrip = NULL;
    if (string == NULL)
    {
        rstrip = (char *) calloc(8, sizeof(char));
        return rstrip;
    }
    // Classify the job - i expect user will want to remove blank space
    job = 2;
    lenp = 0;
    if (pattern == NULL)
    {
        job = 1;
    }
    else
    {
        lenp = (int) strlen(pattern);
        if (lenp == 0)
        {
            job = 1;
        }
        if (lenp == 1 && pattern[0] == ' ')
        {
            job = 1;
        }
    }
    lenos = (int) strlen(string);
    // Removing blanks
    if (job == 1)
    {
        // Search string until we encounter something that isn't a `space'
        i2 = 0;
        for (i=lenos-1; i>=0; i--)
        {
            if (!isspace(string[i]))
            {
                i2 = MIN(lenos, i+1);
                break;
            }
        }
    }
    // More generally removing any desired character 
    else
    {
        i2 = 0;
        for (i=lenos-1; i>=0; i--)
        {
            // Does the i'th index match any of these characters?
            lfound = false;
            for (l=0; l<lenp; l++)
            {
                // Blank space option
                if (pattern[l] == ' ')
                {
                    if (isspace(string[i]))
                    {
                        lfound = true;
                        break;
                    }
                }
                // More general desired character
                else
                {
                    if (string[i] == pattern[l])
                    {
                        lfound = true;
                        break;
                    }
                }
            } // Loop on pattern length
            // It doesn't match any character - we're done
            if (!lfound)
            {
                i2 = MIN(lenos, i + 1);
                break;
            }
        } // Loop on string length
    }
    len = MAX(0, i2);
    rstrip = (char *) calloc((size_t) (MAX(len+1, 8)), sizeof(char));
    strncpy(rstrip, string, (size_t) len);
    return rstrip;
}
static bool lmatched(const size_t lpattern, const char item,
                     const char *pattern);

static bool lmatched(const size_t lpattern, const char item,
                     const char *pattern)
{
    size_t l;
    bool lfound;
    lfound = false;
    for (l=0; l<lpattern; l++)
    {
        if (pattern[l] == item)
        {
            lfound = true;
            break;
        }
    }
    return lfound;
}
/*!
 * @brief Splits a string based on a given pattern starting from the right.
 *
 * @param[in] pattern   Pattern on which to split the string.  If NULL
 *                      then this will split on blank space.
 * @param[in] stringIn  NULL terminated string to split.
 *
 * @param[out] nitems   Number of items in the result.
 *
 * @result This is a jagged matrix with nitems rows containing the split
 *         string.  At the moment this should be freed by applying free
 *         to each row then the matrix.
 * @ingroup sac_io_string
 * @author Ben Baker
 */
char **sacio_string_rsplit(const char *pattern, const char *stringIn,
                           int *nitems)
{
    char **items, *string, *ccopy;
    size_t lens;
    int *ptr, i, i1, i2, j, k, nc;
    size_t lpattern, nalloc;
    //bool loff;
    //------------------------------------------------------------------------//
    *nitems = 0;
    items = NULL;
    ptr = NULL;
    if (stringIn == NULL){return items;}
    lens = strlen(stringIn);
    if (lens == 0){return items;}
    // chop up the string
    string = sacio_string_strip(pattern, stringIn); 
    lens = strlen(string);
    ptr = (int *) calloc(MAX(8, 2*lens + 2), sizeof(int));
    //loff = true;
    *nitems = 0;
    ptr[0] = 0;
    if (pattern == NULL)
    {
        // get the start index
        ptr[0] = (int) lens;
        for (i=0; i<(int) lens; i++)
        {
            if (!isblank(string[i]))
            {
                ptr[0] = i;
                break;
            }
        }
        if (ptr[0] == (int) lens){goto END;} // blank line
        // count number of non-blank spaces
        nc = 0;
        for (i=0; i<(int) lens; i++)
        {
            if (!isblank(string[i])){nc = nc + 1;}
        }
        // just one word
        if (nc == (int) lens)
        {
            *nitems = 1;
            ptr[0] = 0;
            ptr[1] = nc;
        } 
        else
        {
            // now parse the string
            for (i=ptr[0]; i<(int) lens; i++)
            {
                // search until next word starts
                for (j=i; j<(int) lens; j++)
                {
                    if (!isblank(string[j]))
                    {
                        if (*nitems*2 >= (int) (2*lens+2))
                        {
                            fprintf(stderr, "%s: Internal error 1a\n",
                                    __func__);
                        }
                        ptr[*nitems*2] = j;
                        i = j;
                        break;
                    }
                }
                // search until this word ends
                //loff = false;
                for (j=i; j<(int) lens; j++)
                {
                    if (*nitems*2+1 >= (int) (2*lens+2))
                    {
                        fprintf(stderr, "%s: Internal error 2a\n", __func__);
                    }
                    ptr[*nitems*2+1] =-1;
                    if (isblank(string[j]))
                    {
                        ptr[*nitems*2+1] = j;
                        i = j;
                        *nitems = *nitems + 1;
                        //loff = true;
                        break;
                    }
                }
                // is the rest blank?
                // did i get the last thing?
                if (i == (int) lens - 1)
                {
                    if (*nitems*2 + 1 >= (int) (2*lens+2) ||
                       (*nitems-1)*2+1 < 0)
                    {
                        fprintf(stderr, "%s: Internal error 3a\n", __func__);
                    }
                    ptr[*nitems*2] = ptr[(*nitems-1)*2+1];
                    ptr[*nitems*2+1] = (int) lens;
                    *nitems = *nitems + 1;
                }
            }
        }
    }
    // Looking for a pattern
    else
    {
        //isclPrintError("%s", "General splitting not yet programmed");
        lpattern = strlen(pattern);
        // get the start index
        ptr[0] = (int) lens;
        for (i=0; i<(int) lens; i++)
        {
            if (!lmatched(lpattern, string[i], pattern)) //!isblank(string[i]))
            {
                ptr[0] = i;
                break;
            }
        }
        if (ptr[0] == (int) lens){goto END;} // blank line
        // count number of non-blank spaces
        nc = 0;
        for (i=0; i<(int) lens; i++)
        {
            //if (!isblank(string[i])){nc = nc + 1;} 
            if (!lmatched(lpattern, string[i], pattern))
            {
                nc = nc + 1;
            }
        }
        // just one word
        if (nc == (int) lens)
        {
            *nitems = 1;
            ptr[0] = 0;
            ptr[1] = nc; 
        }
        else
        {
            // now parse the string
            for (i=ptr[0]; i<(int) lens; i++)
            {
                // search until next word starts
                for (j=i; j<(int) lens; j++)
                {
                    if (*nitems*2 >= (int) (2*lens+2))
                    {
                        fprintf(stderr, "%s: Internal error 1\n", __func__);
                    }
                    //if (!isblank(string[j]))
                    if (!lmatched(lpattern, string[j], pattern))
                    {
                        ptr[*nitems*2] = j;
                        i = j;
                        break;
                    }
                }
                // search until this word ends
                //loff = false;
                for (j=i; j<(int) lens; j++)
                {
                    if (*nitems*2+1 >= (int) (2*lens+2))
                    {
                        fprintf(stderr, "%s: Internal error 2\n", __func__);
                    }
                    ptr[*nitems*2+1] =-1;
                    //if (isblank(string[j]))
                    if (lmatched(lpattern, string[j], pattern))
                    {
                        ptr[*nitems*2+1] = j;
                        i = j;
                        *nitems = *nitems + 1;
                        //loff = true;
                        break;
                    }
                }
                // is the rest blank?
                // did i get the last thing?
                if (i == (int) lens - 1)
                {
                    if (*nitems*2 + 1 >= (int) (2*lens+2) ||
                        (*nitems-1)*2+1 < 0)
                    {
                        fprintf(stderr, "%s: Internal error 3\n", __func__);
                    }
                    ptr[*nitems*2] = ptr[(*nitems-1)*2+1];
                    ptr[*nitems*2+1] = (int) lens;
                    *nitems = *nitems + 1;
                }
            }
        }
    }
    // i didn't find anything 
    if (*nitems == 0){goto END;}
    // make sure i get the last thing
    //ptr[*nitems*2+1] = (int) lens;
    // now split the string
    //printf("%s\n", string);
    //printf("\n");
    items = (char **) calloc((size_t) *nitems, sizeof(char *));
    for (i=0; i<*nitems; i++)
    {
        i1 = ptr[2*i];
        i2 = ptr[2*i+1];
        //printf("%d %d\n", i1, i2);
        nalloc = MAX(16, (size_t) (i2 - i1 + 1)); 
        ccopy = (char *) calloc(nalloc, sizeof(char));
        //items[i] = (char *) calloc(MAX(2, (size_t) (i2-i1+1)), sizeof(char));
        k = 0;
        for (j=i1; j<i2; j++)
        {
            if (isblank(string[j])){continue;} // TODO FIX Me
            //items[i][k] = string[j];
            ccopy[k] = string[j];
            k = k + 1;
        }
        items[i] = ccopy;
    }
    // free space
END:;
    free(ptr);
    free(string);
    return items;
}

/*!
 * @brief Splits a string based on the given pattern.
 *
 * @param[in] np             Length of pattern.  This can be 0.
 * @param[in] pattern        Pattern on which to split string in which case
 *                           this is an array of dimension [np].
 * @param[in] pattern        Otherwise,  if pattern is NULL then
 *                           the algorithm will use isspace to split the
 *                           string.
 * @param[in] string         NULL terminated string to split based on the
 *                           given pattern.
 * @param[in] lenp           This is the max size of ptr.
 * @param[out] ptr           This points to the start index of the is'th
 *                           sub-string.  This is an array of dimension
 *                           [lenp] however only the first ns elements are
 *                           accssed.
 * @param[out] lens          The max number of elements in stringSplit.
 * @param[out] ns            The number of sub-strings string generated
 *                           by parsing string.
 * @param[out] stringSplit   The string split based on the given pattern.
 *                           It has dimension [lens] however only the 
 *                           first ptr[ns] elements are accessed.
 *
 * @result 0 indicates success.
 * @ingroup sac_io_string
 * @author Ben Baker
 */
int sacio_string_split_work(const int np, const char *pattern,
                            const char *__restrict__ string,
                            const int lenp, int *__restrict__ ptr,
                            const int lens,
                            int *ns, char *__restrict__ stringSplit)
{
    bool *mask;
    int i, i1, i2, id, j, lenc, nwork;
    int ierr;
    size_t lenos;
    *ns = 0;
    ierr = 0;
    if (lenp < 1)
    {
        fprintf(stderr, "%s: lenp must be at least 2\n", __func__);
        return -1;
    }
    if (lens < 1)
    {
        fprintf(stderr, "%s: lens must be at least 1\n", __func__);
        return -1;
    }
    if (string == NULL)
    {
        fprintf(stderr, "%s: String is NULL\n", __func__);
        return -1;
    }
    if (ptr == NULL)
    {
        fprintf(stderr, "%s: ptr is NULL\n", __func__);
        return -1;
    }
    memset(stringSplit, 0, (size_t) lens*sizeof(char));
    memset(ptr, 0, (size_t) lens*sizeof(int));
    lenos = strlen(string);
    // Nothing to do
    if (lenos < 1)
    {
        stringSplit[0] = '\0';
        ptr[0] = 0;
        ptr[1] = 0;
        return 0;
    }
    // Determine which elements in the string will be retained
    mask = (bool *) calloc(MAX(lenos+1, 8), sizeof(bool));
    if (pattern == NULL || np == 0)
    {
        for (i=0; i<(int) lenos; i++)
        {
            if (isspace(string[i])){mask[i] = true;}
        }
    }
    else
    {
        for (id=0; id<np; id++)
        {
            //#pragma omp simd
            for (i=0; i<(int) lenos; i++)
            {
                if (string[i] == pattern[id]){mask[i] = true;}
            }
        }
    }
    nwork = 0;
    for (i=0; i<(int) lenos; i++)
    {
        if (mask[i]){nwork = nwork + 1;}
    }
    // Nothing to be purged 
    if (nwork == 0)
    {
        *ns = 1;
        ptr[0] = 0;
        ptr[1] = (int) lenos;
        strcpy(stringSplit, string);
        free(mask);
        return 0;
    } 
    // Entire string matches
    else if (nwork == (int) lenos)
    {
        stringSplit[0] = '\0';
        ptr[0] = 0;
        ptr[1] = 0;
        free(mask);
        return 0;
    }
    // Somewhere in between
    else
    {
        if (nwork > lenp)
        {
            fprintf(stderr, "%s: Error lenp=%d must be at least %d",
                    __func__, lenp, nwork);
            free(mask);
            return -1;
        }
    } 
    // Generate the sparse pointer and copy the string
    mask[lenos] = true; // will force the last entity to evaluate
    ptr[0] = 0;
    *ns = 0;
    for (i=0; i<(int) lenos; i++)
    {
        // Turn the string copy on
        if (!mask[i])
        {
            i1 = i;
            i2 = i; 
            for (j=i; j<(int) lenos; j++)
            {
                // Turn the string copy off 
                if (mask[j+1])
                {
                    i2 = j;
                    i = j + 1;
                    break;
                }
            }
            // Add a sub-string to the list
            lenc = i2 - i1 + 1;
            if (lenc > 0)
            {
                ptr[*ns+1] = ptr[*ns] + lenc + 1; //+1 will be \0
                //printf("%d %d\n", ptr[*ns], ptr[*ns+1]);
                if (ptr[*ns+1] > lens)
                {
                    fprintf(stderr, "%s: Insufficient space in stringsplit",
                            __func__);
                    free(mask);
                    return -1;
                }
                strncpy(&stringSplit[ptr[*ns]], &string[i1], (size_t) lenc);
                //printf("%s\n", &stringSplit[ptr[*ns]]);
                *ns = *ns + 1; 
            }
        }
    }
    free(mask);
    return ierr;
}
