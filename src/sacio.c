#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdbool.h>
#include <float.h>
#include <math.h>
#include <ctype.h>
#include <sys/stat.h>
#include "sacio.h"
#include "sacio_os.h"
#include "sacio_string.h"
#include "sacio_time.h"

#ifndef MIN
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#endif
#ifndef DOUBLEI
#define DOUBLEI (__extension__ 1.0i)
#endif
#ifndef DCMPLX
#define DCMPLX(r,i) ((double) (r) + (double) (i)*DOUBLEI)
#endif


/*!
 * @defgroup sac SAC
 * @brief Handles the core SAC data structure manipulations and file handling.
 * @copyright ISTI distributed under the Apache 2 license.
 */
/*!
 * @defgroup sac_header_utils Header Access
 * @brief Accesses and modifies header variables.
 * @ingroup sac
 * @copyright ISTI distributed under the Apache 2 license.
 */
/*!
 * @defgroup sac_memory Memory
 * @brief Utilties for allocating and deallocating memory.
 * @ingroup sac
 * @copyright ISTI distributed under the Apache 2 license.
 */
/*!
 * @defgroup sac_io Input/Output
 * @brief Utilities for reading/writing SAC files.
 * @ingroup sac
 * @copyright ISTI distributed under the Apache 2 license.
 */

/*
#ifndef DI
#define DI(__extension__ 1.0i)
#endif
#ifndef DCMPLX
#define DCMPLX(r,i) ((double) (r) + (double) (i)*DI)
#endif
*/

#define ITIME 1

static void readFloatHeader(const bool lswap, const char *__restrict__ cfloat,
                            double *val);
static void readIntHeader(const bool lswap, const char *__restrict__ cint,
                          int *val);
static void readBoolHeader(const bool lswap, const char *__restrict__ cbool,
                           int *val);
static void readChar8(const char *__restrict__ cin8, char *__restrict__ cout8);
static void readChar16(const char *__restrict__ cin16,
                       char *__restrict__ cout16);
static void writeFloatHeader(const bool lswap, const double val,
                             char *__restrict__ cflt);
static void writeIntHeader(const bool lswap, const int val,
                           char *__restrict__ cint);
static void writeBoolHeader(const bool lswap, const int val,
                            char *__restrict__ cbool);
static void writeChar8(const char *__restrict__ cout8,
                       char *__restrict__ cin8);
static void writeChar16(const char *__restrict__ cout16,
                        char *__restrict__ cin16);
static double makeTime(const char *time);

/*
int main()
{
    struct sacHeader_struct hdr;
    struct sacData_struct sac;
sacio_readHeader("data/2016.247.12.00.00.0499.IU.YSS.00.BH1.M.SAC\0", &hdr);
printf("%d\n", hdr.npts);
sacio_readTimeSeriesFile("data/2016.247.12.00.00.0499.IU.YSS.00.BH1.M.SAC\0", &sac);
sacio_writeTimeSeriesFile("test.sac\0", sac);
sacio_free(&sac);
    return 0;
}
*/

/*!
 * @brief Reads the frequency, phase, amplitude phase file.
 *
 * @param[in] fname   Name of file to read.
 *
 * @param[out] fap    The SAC FAP data structure. 
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_io
 *
 * @author Ben Baker
 *
 */
int sacio_readFAPFile(const char *fname,
                      struct sacFAP_struct *fap)
{
    FILE *fapfl;
    char cline[256], *line;
    double *a, *f, *p;
    int i, ierr, nitems, nf, nlines;
    ierr = 0;
    memset(fap, 0, sizeof(struct sacFAP_struct));
    if (!sacio_os_path_isfile(fname))
    {
        printf("%s: fap file %s does not exist\n", __func__, fname);
        return -1;
    }
    fapfl = fopen(fname, "r");
    // Get the number of lines in the file 
    nlines = 0; 
    while (fgets(cline, 256, fapfl) != NULL){nlines = nlines + 1;}
    rewind(fapfl);
    // Set workspace and read file 
    nf = 0;
    line = NULL;
    f = (double *) calloc((size_t) nlines, sizeof(double));
    a = (double *) calloc((size_t) nlines, sizeof(double));
    p = (double *) calloc((size_t) nlines, sizeof(double));
    for (i=0; i<nlines; i++)
    {
        memset(cline, 0, 256);
        if (fgets(cline, 256, fapfl))
        {
            // Apply isspace to start and end of string
            line = sacio_string_strip(NULL, cline);
            if (line == NULL){goto NEXT_LINE;} // Nothing
            if (line[0] == '#' || line[0] == '*'){goto NEXT_LINE;} // Comment
            nitems = sscanf(line, "%lf %lf %lf", &f[nf], &a[nf], &p[nf]);
            if (nitems != 3)
            {
                printf("%s: Invalid line %s\n", __func__, cline);
                goto NEXT_LINE;
            }
            nf = nf + 1;
NEXT_LINE:;
            free(line);
        }
    }
    fclose(fapfl); 
    // Copy 
    if (nf > 0)
    {
        fap->freqs = sacio_malloc64f(nf);
        fap->amp = sacio_malloc64f(nf);
        fap->phase = sacio_malloc64f(nf);
        //array_copy64f_work(nf, f, fap->freqs);
        //array_copy64f_work(nf, a, fap->amp);
        //array_copy64f_work(nf, p, fap->phase);
        memcpy(fap->freqs, f, (size_t) nf*sizeof(double));
        memcpy(fap->amp,   a, (size_t) nf*sizeof(double));  
        memcpy(fap->phase, p, (size_t) nf*sizeof(double));
        fap->nf = nf;
        fap->lhaveFAP = true;
    }
    // No data in file
    else
    {
        printf("%s: No frequencies read from fap file %s\n", __func__, fname);
        ierr = 1;
    }
    // Release workspace 
    free(f);
    free(a);
    free(p);
    return ierr;
}
//============================================================================//
/*!
 * @brief Reads the SAC pole-zero file.
 *
 * @param[in] fname      Name of SAC file to read.
 *
 * @param[out] pz        Pole-zero structure.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_io
 *
 * @author Ben Baker
 *
 */
int sacio_readPoleZeroFile(const char *fname,
                           struct sacPoleZero_struct *pz)
{
    FILE *pzfl;
    char cline[256] __attribute__ ((aligned(64)));
    char **csplit = NULL;
    size_t lens, lenCat, lenCsplit;
    int i, ierr, indx, j, k, nitems, nlines, npoles, nzeros;
    bool lcomment, lpoles, lzeros;
    ierr = 0;
    memset(pz, 0, sizeof(struct sacPoleZero_struct));
    if (!sacio_os_path_isfile(fname))
    {
        printf("%s: pole-zero file %s does not exist\n", __func__, fname);
        return -1;
    }
    pzfl = fopen(fname, "r"); 
    // get the number of lines in the file 
    nlines = 0;
    while (fgets(cline, 256, pzfl) != NULL){nlines = nlines + 1;}
    rewind(pzfl);
    lpoles = false; 
    lzeros = false;
    npoles = 0;
    nzeros = 0;
    // read and parse
    for (i=0; i<nlines; i++)
    {
        memset(cline, 0, 256*sizeof(char));
        if (fgets(cline, 256, pzfl) == NULL)
        {
            printf("%s: Premature end of file\n", __func__);
            ierr = 1;
            break;
        }
        // try to fix some issues - rsplit should be improved
        for (j=0; j<256; j++)
        {
            if (cline[j] == '\n'){cline[j] = '\0';}
            if (cline[j] == '\t'){cline[j] = ' ';}
        }
        // split the string
        csplit = sacio_string_rsplit(NULL, cline, &nitems);
        // classify the job
        if (nitems > 0)
        {
            lcomment = false;
            if (strcasecmp(csplit[0], "*\0") == 0){lcomment = true;}
            if (lcomment)
            {
                indx = 0;
                for (j=0; j<nitems; j++)
                {
                    lens = strlen(csplit[j]);
                    if (lens > 0)
                    {
                        // get the index of the split
                        if (csplit[j][lens-1] == ':')
                        {
                            indx = j;
                            break;
                        }
                    }
                }
                if (indx + 1 >= nitems)
                {
                    if (strcasecmp(csplit[1], "LOCATION\0") == 0)
                    {
                        strcpy(pz->location, "--\0"); //printf("yes\n");
                    }
                    else
                    {
                        printf("%s: Invalid split index %s in file %s\n",
                               __func__, csplit[1], fname);
                    }
                    goto FREE;
                }
                if (strcasecmp(csplit[1], "NETWORK\0") == 0)
                {
                    strcpy(pz->network, csplit[indx+1]);
                }
                else if (strcasecmp(csplit[1], "STATION\0") == 0)
                {
                    strcpy(pz->station, csplit[indx+1]);
                }
                else if (strcasecmp(csplit[1], "LOCATION\0") == 0)
                {
                    strcpy(pz->location, csplit[indx+1]);
                }
                else if (strcasecmp(csplit[1], "CHANNEL\0") == 0)
                {
                    strcpy(pz->channel, csplit[indx+1]);
                }
                else if (strcasecmp(csplit[1], "CREATED\0") == 0)
                {
                    pz->created = makeTime(csplit[indx+1]);
                }
                else if (strcasecmp(csplit[1], "START\0") == 0)
                {
                    pz->start = makeTime(csplit[indx+1]);
                }
                else if (strcasecmp(csplit[1], "END\0") == 0)
                {
                    pz->end = makeTime(csplit[indx+1]);
                }
                else if (strcasecmp(csplit[1], "DESCRIPTION\0") == 0)
                {
                    strncpy(pz->description, csplit[indx+1],
                            MIN(64, strlen(csplit[indx+1])));
                    for (k=indx+2; k<nitems; k++)
                    {
                        lenCsplit = strlen(csplit[k]);
                        if (lenCsplit + 1 >= 64){break;}
                        if (k < nitems - 1){strcat(pz->description, " ");}
                        lenCat = lenCsplit;
                        if (strlen(pz->description) + lenCsplit > 64 - 1)
                        {
                            lenCat = 64 - 1
                                   - strlen(pz->description) - lenCsplit;
                        }
                        strncat(pz->description, csplit[k], lenCat);
                    }
                }
                else if (strcasecmp(csplit[1], "LATITUDE\0") == 0)
                {
                    pz->latitude = atof(csplit[indx+1]);
                }
                else if (strcasecmp(csplit[1], "LONGITUDE\0") == 0)
                {
                    pz->longitude = atof(csplit[indx+1]);
                }
                else if (strcasecmp(csplit[1], "ELEVATION\0") == 0)
                {
                    pz->elevation = atof(csplit[indx+1]);
                }
                else if (strcasecmp(csplit[1], "DEPTH\0") == 0)
                {
                    pz->depth = atof(csplit[indx+1]);
                }
                else if (strcasecmp(csplit[1], "DIP\0") == 0)
                {
                    pz->dip = atof(csplit[indx+1]);
                }
                else if (strcasecmp(csplit[1], "AZIMUTH\0") == 0)
                {
                    pz->azimuth = atof(csplit[indx+1]);
                }
                else if (strcasecmp(csplit[1], "SAMPLE\0") == 0)
                {
                    pz->sampleRate = atof(csplit[indx+1]);
                }
                else if (strcasecmp(csplit[1], "INPUT\0") == 0)
                {
                    if (strcasecmp(csplit[indx+1], "M/S\0") == 0)
                    {
                        pz->inputUnits = SAC_METERS_SECOND;
                    }
                    else if (strcasecmp(csplit[indx+1], "M\0") == 0)
                    {
                        pz->inputUnits = SAC_METERS;
                    }
                    else if (strcasecmp(csplit[indx+1], "M/S**2\0") == 0 ||
                             strcasecmp(csplit[indx+1], "M/S^2\0") == 0)
                    {
                        pz->inputUnits = SAC_METERS_SECOND_SECOND;
                    }
                    else
                    {
                        printf("%s: Unknown input unit: %s %d\n",
                               __func__,
                               csplit[indx+1], (int) strlen(csplit[indx+1]));
                    }
                }
                else if (strcasecmp(csplit[1], "INSTTYPE\0") == 0)
                {
                    strncpy(pz->instrumentType, csplit[indx+1],
                            MIN(64, strlen(csplit[indx+1])));
                    for (k=indx+2; k<nitems; k++) 
                    {
                        lenCsplit = strlen(csplit[k]);
                        if (lenCsplit + 1 >= 64){break;}
                        if (k < nitems - 1){strcat(pz->instrumentType, " ");}
                        lenCat = lenCsplit;
                        if (strlen(pz->instrumentType) + lenCsplit > 64 - 1)
                        {
                            lenCat = 64 - 1
                                   - strlen(pz->instrumentType) - lenCsplit; 
                        }
                        strncat(pz->instrumentType, csplit[k], lenCat);
                    }
                }
                else if (strcasecmp(csplit[1], "INSTGAIN\0") == 0)
                {
                    pz->instgain = atof(csplit[indx+1]);
                    // TODO get units
                    if (indx+2 < nitems)
                    {
                        if (strcasecmp(csplit[indx+2], "(M/S)") == 0)
                        {
                            pz->instgainUnits = SAC_METERS_SECOND;
                        }
                        else if (strcasecmp(csplit[indx+2], "(M)") == 0)
                        {
                            pz->instgainUnits = SAC_METERS;
                        }
                        else if (strcasecmp(csplit[indx+2], "(M/S**2)\0")== 0 ||
                                 strcasecmp(csplit[indx+2], "(M/S^2)\0") == 0)
                        {
                            pz->instgainUnits = SAC_METERS_SECOND_SECOND;
                        } 
                        else
                        {
                            printf("%s: Can't get instgain units\n", __func__);
                        }
                    }
                }
                else if (strcasecmp(csplit[1], "COMMENT\0") == 0)
                {
                    strcpy(pz->comment, csplit[indx+1]);
                    for (k=indx+2; k<nitems; k++) 
                    {
                        if (strlen(csplit[k]) + 1 >= 64){break;}
                        if (k < nitems - 1){strcat(pz->comment, " ");}
                        strcat(pz->comment, csplit[k]);
     
                    }
                }
                else if (strcasecmp(csplit[1], "SENSITIVITY\0") == 0)
                {
                    pz->sensitivity = atof(csplit[indx+1]);
                    // TODO get units
                    if (indx+2 < nitems)
                    {
                        if (strcasecmp(csplit[indx+2], "(M/S)") == 0)
                        {
                            pz->sensitivityUnits = SAC_METERS_SECOND;
                        }
                        else if (strcasecmp(csplit[indx+2], "(M)") == 0)
                        {
                            pz->sensitivityUnits = SAC_METERS;
                        }
                        else if (strcasecmp(csplit[indx+2], "(M/S**2)\0")== 0 ||
                                 strcasecmp(csplit[indx+2], "(M/S^2)\0") == 0)
                        {
                            pz->sensitivityUnits = SAC_METERS_SECOND_SECOND;
                        }
                        else
                        {
                            printf("%s: Can't get sensitivity units %s\n",
                                   __func__, csplit[indx+2]);
                        }
                    }
                }
                else if (strcasecmp(csplit[1], "A0\0") == 0)
                {
                    pz->a0 = atof(csplit[indx+1]);
                }
            }
            else
            {
                if (strcasecmp(csplit[0], "ZEROS\0") == 0)
                {
                    lpoles = false;
                    lzeros = true;
                    if (nitems != 2)
                    {
                        printf("%s: Can't determine number of zeros\n",
                               __func__);
                        return -1;
                    }
                    pz->nzeros = atoi(csplit[nitems-1]);
                    pz->zeros = sacio_malloc64z(pz->nzeros);
/*
                    nalloc = (size_t) pz->nzeros*sizeof(double complex);
#ifdef USE_POSIX
                    posix_memalign((void **) &pz->zeros, 64, nalloc);
#else
                    pz->zeros = (double complex *)
                                aligned_alloc(64, nalloc);
#endif
*/
                }
                else if (strcasecmp(csplit[0], "POLES\0") == 0)
                {
                    lpoles = true;
                    lzeros = false;
                    if (nitems != 2)
                    {
                        printf("%s: Can't determine number of poles\n",
                               __func__);
                        return -1;
                    }
                    pz->npoles = atoi(csplit[nitems-1]);
                    pz->poles = sacio_malloc64z(pz->npoles);
/*
                    nalloc = (size_t) pz->npoles*sizeof(double complex);
#ifdef USE_POSIX
                    posix_memalign((void **) &pz->poles, 64, nalloc);
#else
                    pz->poles = (double complex *)
                                aligned_alloc(64, nalloc);
#endif
*/
                }
                else if (strcasecmp(csplit[0], "CONSTANT\0") == 0)
                {
                    lpoles = true;
                    lzeros = false;
                    if (nitems != 2)
                    {
                        printf("%s: Can't determine constant\n", __func__);
                        return -1;
                    }
                    pz->constant = atof(csplit[1]);
                }
                else if (lpoles)
                {
                    if (nitems != 2)
                    {
                        printf("%s: Can't determine pole\n", __func__);
                        return -1;
                    }
                    pz->poles[npoles] = DCMPLX(atof(csplit[0]),
                                               atof(csplit[1]));
                    npoles = npoles + 1;
                }
                else if (lzeros)
                {
                    if (nitems != 2)
                    {
                        printf("%s: Can't determine zero\n", __func__);
                        return -1;
                    }
                    pz->zeros[nzeros] = DCMPLX(atof(csplit[0]),
                                               atof(csplit[1]));
                    nzeros = nzeros + 1;
                }
            }
        }
FREE:;
        for (j=0; j<nitems; j++){if (csplit[j] != NULL){free(csplit[j]);}}
        if (csplit != NULL){free(csplit);}
    }
    if (ierr == 0){pz->lhavePZ = true;}
    fclose(pzfl);
    return ierr;
}
//============================================================================//
/*!
 * @brief Writes a SAC time series.
 *
 * @param[in] fname    Name of SAC file to write.
 * @param[in] sac      SAC data structure with data and header to write.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_io
 *
 * @author Ben Baker
 *
 */
int sacio_writeTimeSeriesFile(const char *fname,
                              const struct sacData_struct sac)
{
    FILE *sacfl;
    struct sacHeader_struct hdr;
    char dirName[PATH_MAX];
    char *cdat;
    char cheader[632] __attribute__ ((aligned(64))); 
    int ierr, npout;
    size_t nalloc;
    const bool lswap = false;
    if (fname == NULL)
    {
        fprintf(stderr, "%s: Output file name is NULL\n", __func__);
        return -1;
    }
    if (strlen(fname) == 0)
    {
        fprintf(stderr, "%s: Output file name is blank\n", __func__);
        return -1;
    }
    memcpy(&hdr, &sac.header, sizeof(struct sacHeader_struct));
    npout = sac.npts;
    if (npout <= 0 || sac.data == NULL){hdr.npts = 0;}
    sacio_setIntegerHeader(SAC_INT_IFTYPE, ITIME, &hdr); // time series
    sacio_setIntegerHeader(SAC_INT_NVHDR,  6, &hdr);     // header version 6
    sacio_setBooleanHeader(SAC_BOOL_LEVEN, true, &hdr);  // data evenly spaced
    if (sac.npts != hdr.npts)
    {
        printf("%s: Inconsistent output sizes\n", __func__);
        return -1;
    }
    // ensure the path exists
    sacio_os_dirname_work(fname, dirName);
    if (!sacio_os_path_isdir(dirName))
    {
        ierr = sacio_os_makedirs(dirName);
        if (ierr != 0)
        {
            fprintf(stderr, "%s: Failed to make directory %s\n",
                     __func__, dirName);
            return -1;
        }
    }
    // pack and write the header
    sacio_packHeader(lswap, hdr, cheader);
    sacfl = fopen(fname, "wb+");
    fwrite(cheader, 632, sizeof(char), sacfl);
    // pack and write the data
    if (npout > 0 && sac.data != NULL)
    {
        nalloc = (size_t) npout*sizeof(float);
#ifdef USE_POSIX 
        cdat = NULL;
        posix_memalign((void **) &cdat, 64, nalloc);
#else
        cdat = (char *) aligned_alloc(64, nalloc);
#endif
        memset(cdat, 0, (size_t) npout*sizeof(float));
        sacio_packData(lswap, npout, sac.data, cdat);
        fwrite(cdat, (size_t) npout, sizeof(float), sacfl);
        free(cdat);
    }
    fclose(sacfl);
    return 0;
}
//============================================================================//
/*!
 * @brief Frees the pole-zero structure.
 * @param[out] pz     Memory has been released from pz structure and 
 *                    and all variables set to 0.
 * @result 0 indicates success.
 * @ingroup sac_memory
 */
int sacio_freePoleZeros(struct sacPoleZero_struct *pz)
{
    sacio_free64z(&pz->poles);
    sacio_free64z(&pz->zeros);
    memset(pz, 0, sizeof(struct sacPoleZero_struct));
    return 0;
}
//============================================================================//
/*!
 * @brief Frees the FAP structure.
 * @param[out] fap     Memory has been released from the FAP structure and
 *                     all variables have been set to 0.
 * @result 0 indicates success.
 * @ingroup sac_memory
 */
int sacio_freeFAP(struct sacFAP_struct *fap)
{
    sacio_free64f(&fap->freqs);
    sacio_free64f(&fap->amp);
    sacio_free64f(&fap->phase);
    memset(fap, 0, sizeof(struct sacFAP_struct));
    return 0;
}
//============================================================================//
/*!
 * @brief Releases memory on double array p and sets it to NULL.
 *
 * @param[out] p   Pointer to memory to be released.
 *
 * @ingroup sac_memory
 *
 * @author Ben Baker
 *
 */
void sacio_free64f(double **p)
{
    if (*p != NULL)
    {   
        free(*p);
        *p = NULL;
    }
    return;
}
//============================================================================//
/*!
 * @brief Releases memory on double complex p and sets it to NULL.
 *
 * @param[out] p   Pointer to memory to be released.
 *
 * @ingroup sac_memory
 * 
 * @author Ben Baker
 *
 */
void sacio_free64z(double complex **p)
{
    if (*p != NULL)
    {    
        free(*p);
        *p = NULL;
    }    
    return;
}
//============================================================================//
/*!
 * @brief Releases the data and sets the number of points to 0 on the 
 *        SAC data structure.
 *
 * @param[out] sac    On exit data has been freed and npts set to 0.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_memory
 *
 * @author Ben Baker
 *
 */
int sacio_freeData(struct sacData_struct *sac)
{
    sacio_free64f(&sac->data); //if (sac->data != NULL){free(sac->data);}
    sac->npts = 0;
    return 0; 
}
//============================================================================//
/*!
 * @brief Sets a header to all SAC NaN values which are -12345.
 *
 * @param[out] header   On exit all header values have been set to -12345.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_memory
 *
 * @author Ben Baker
 *
 */
int sacio_freeHeader(struct sacHeader_struct *header)
{
    sacio_setDefaultHeader(header);
    return 0;
}
//============================================================================//
/*!
 * @brief Extends a SAC dataset by appending the array xadd to the current
 *        data array.
 *
 * @param[in] nptsAdd   Number of points to add to sac data structure.
 * @param[in] xadd      Array to append to sac data structure.  This has
 *                      dimension [nptsAdd].
 *
 * @param[in,out] sac   On input contains an initialized SAC data structure
 *                      with data.
 * @param[in,out] sac   On exit the array xadd has been appended to the SAC
 *                      data structure.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_memory
 *
 * @author Ben Baker
 *
 * @copyright ISTI distributed under the Apache 2 license.
 *
 */
int sacio_extendData(const int nptsAdd, 
                     const double *__restrict__ xadd,
                     struct sacData_struct *sac)
{
    double *temp;
    int nptsNew;
    int ierr, npts;
    if (nptsAdd < 1 || xadd == NULL)
    {
        if (nptsAdd < 1)
        {
            fprintf(stderr, "%s: No points by which to extend array\n",
                    __func__);
        }
        if (xadd == NULL)
        {
            fprintf(stderr, "%s: xadd is NULL\n", __func__);
        }
        return -1;
    }
    ierr = sacio_getIntegerHeader(SAC_INT_NPTS, sac->header, &npts); 
    // Could be an initial allocation
    if (ierr != 0)
    { 
        if (npts < 0)
        {
            sacio_freeData(sac);
            sac->npts = nptsAdd;
            sac->data = sacio_malloc64f(nptsAdd);
            memcpy(sac->data, xadd, (size_t) nptsAdd*sizeof(double));
            sacio_setIntegerHeader(SAC_INT_NPTS, nptsAdd, &sac->header);
            return 0;
        }
        fprintf(stderr, "%s: Error getting npts from header\n", __func__);
        return -1;
    }
    // Extend
    nptsNew = nptsAdd + npts;
    if ((int64_t) nptsNew != (int64_t) nptsAdd + (int64_t) npts)
    {
        fprintf(stderr, "%s: Integer overflow in extending array\n", __func__);
        return -1;
    }
    temp = sacio_malloc64f(nptsNew);
    if (temp == NULL)
    {
        fprintf(stderr, "%s: Error allocating space\n", __func__);
        return -1;
    }
    // Copy sac->data to temporary array
    memcpy(temp,        sac->data, (size_t) npts*sizeof(double));
    // Append xadd to temp
    memcpy(&temp[npts], xadd,      (size_t) nptsAdd*sizeof(double));
    sacio_free64f(&sac->data);
    // Update the header with the new number of points
    sacio_setIntegerHeader(SAC_INT_NPTS, nptsNew, &sac->header);
    // Update the pointer and set the number of points
    sac->data = temp;
    sac->npts = nptsNew; 
    return 0;
}
//============================================================================//
/*!
 * @brief Safely allocates a double array for SAC data structures.
 *
 * @param[in] npts   Number of points to allocate (> 0).
 *
 * @retval Allocated array to hold SAC data.
 * @retval If NULL then this function was unsuccessful.
 *
 * @ingroup sac_memory
 *
 * @author Ben Baker
 *
 */
double *sacio_malloc64f(const int npts)
{
    double *y;
    size_t nalloc;
    y = NULL;
    if (npts < 1)
    {
        printf("%s: Error no points to allocate\n", __func__);
    }
    nalloc = (size_t) npts*sizeof(double);
#ifdef USE_POSIX
    posix_memalign((void **) &y, 64, nalloc);
#else
    y = (double *) aligned_alloc(64, nalloc);
#endif
    return y;
}
//============================================================================//
/*!
 * @brief Safely allocates a double complex array for SAC data structures.
 *
 * @param[in] npts   Number of points to allocate (> 0).
 *
 * @result Allocated array to hold SAC data.
 *         If NULL then this function was unsuccessful.
 *
 * @ingroup sac_memory
 *
 * @author Ben Baker
 *
 */
double complex *sacio_malloc64z(const int npts)
{
    double complex *y;
    size_t nalloc;
    y = NULL;
    if (npts < 1) 
    {
        printf("%s: Error no points to allocate\n", __func__);
    }    
    nalloc = (size_t) npts*sizeof(double complex);
#ifdef USE_POSIX
    posix_memalign((void **) &y, 64, nalloc);
#else
    y = (double complex *) aligned_alloc(64, nalloc);
#endif
    return y;
}
//============================================================================//
/*!
 * @brief Convenience routine that releases the memory on the SAC data
 *        structure.
 *
 * @param[out] sac    The memory is released on the structure and the 
 *                    values are set to 0.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_memory
 *
 */
int sacio_free(struct sacData_struct *sac)
{
    sacio_freePoleZeros(&sac->pz);
    sacio_freeHeader(&sac->header);
    sacio_freeFAP(&sac->fap);
    sacio_freeData(sac);
    memset(sac, 0, sizeof(struct sacData_struct));
    return 0;
}
//============================================================================//
/*!
 * @brief Copies a SAC FAP structure.
 *
 * @param[in] fapIn     FAP structure to copy to fapOut.
 *
 * @param[out] fapOut   Copy of input FAP structure fapIn.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_memory
 *
 * @author Ben Baker
 *
 */
int sacio_copyFAP(const struct sacFAP_struct fapIn,
                  struct sacFAP_struct *fapOut)
{
    memset(fapOut, 0, sizeof(struct sacFAP_struct));
    fapOut->nf = fapIn.nf;
    fapOut->lhaveFAP = fapIn.lhaveFAP;
    if (fapIn.nf > 0)
    {
        fapOut->freqs = sacio_malloc64f(fapIn.nf);
        fapOut->amp   = sacio_malloc64f(fapIn.nf);
        fapOut->phase = sacio_malloc64f(fapIn.nf);
        //array_copy64f_work(fapIn.nf, fapIn.freqs, fapOut->freqs);
        //array_copy64f_work(fapIn.nf, fapIn.amp,   fapOut->amp);
        //array_copy64f_work(fapIn.nf, fapIn.phase, fapOut->phase);
        memcpy(fapOut->freqs, fapIn.freqs, (size_t) fapIn.nf*sizeof(double));
        memcpy(fapOut->amp,   fapIn.amp,   (size_t) fapIn.nf*sizeof(double));
        memcpy(fapOut->phase, fapIn.phase, (size_t) fapIn.nf*sizeof(double));
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Copies a SAC pole-zero structure.
 *
 * @param[in] pzIn    Pole-zero structure to copy to pzOut.
 *
 * @param[out] pzOut  Copy of input pole-zero structure pzIn.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_memory
 *
 * @author Ben Baker
 *
 */
int sacio_copyPolesAndZeros(const struct sacPoleZero_struct pzIn,
                            struct sacPoleZero_struct *pzOut)
{
    //size_t nalloc;
    //int ierr;
    memset(pzOut, 0, sizeof(struct sacPoleZero_struct));
    // chars
    strcpy(pzOut->network,        pzIn.network);
    strcpy(pzOut->station,        pzIn.station);
    strcpy(pzOut->location,       pzIn.location);
    strcpy(pzOut->channel,        pzIn.channel);
    strcpy(pzOut->comment,        pzIn.comment);
    strcpy(pzOut->description,    pzIn.description);
    strcpy(pzOut->instrumentType, pzIn.instrumentType);
    // doubles
    pzOut->constant    = pzIn.constant;
    pzOut->created     = pzIn.created;
    pzOut->start       = pzIn.start;
    pzOut->end         = pzIn.end;
    pzOut->latitude    = pzIn.latitude;
    pzOut->longitude   = pzIn.longitude;
    pzOut->elevation   = pzIn.elevation;
    pzOut->depth       = pzIn.depth;
    pzOut->dip         = pzIn.dip;
    pzOut->azimuth     = pzIn.azimuth;
    pzOut->sampleRate  = pzIn.sampleRate;
    pzOut->instgain    = pzIn.instgain;
    pzOut->sensitivity = pzIn.sensitivity;
    pzOut->a0          = pzIn.a0;
    // enums
    pzOut->inputUnits       = pzIn.inputUnits;
    pzOut->instgainUnits    = pzIn.instgainUnits;
    pzOut->sensitivityUnits = pzIn.sensitivityUnits;
    // bools
    pzOut->lhavePZ = pzIn.lhavePZ;
    // ints
    pzOut->npoles = pzIn.npoles;
    pzOut->nzeros = pzIn.nzeros;
    // complex arrays
    if (pzIn.npoles > 0)
    {
        pzOut->poles = sacio_malloc64z(pzIn.npoles);
/*
        nalloc = (size_t) pzIn.npoles*sizeof(double complex);
#ifdef USE_POSIX
        posix_memalign((void **) &pzOut->poles, 64, nalloc);
#else
        pzOut->poles = (double complex *) aligned_alloc(64, nalloc);
#endif
*/
        memcpy(pzOut->poles, pzIn.poles, 
               (size_t) pzIn.npoles*sizeof(double complex));
/*
        ierr = array_copy64z_work(pzIn.npoles, pzIn.poles, pzOut->poles);
        if (ierr != 0)
        {
            printf("%s: Error copying poles\n", __func__);
            return -1;
        }
*/
    }
    if (pzIn.nzeros > 0) 
    {
        pzOut->zeros = sacio_malloc64z(pzIn.nzeros);
/*
        nalloc = (size_t) pzIn.nzeros*sizeof(double complex);
#ifdef USE_POSIX
        posix_memalign((void **) &pzOut->zeros, 64, nalloc);
#else
        pzOut->zeros = (double complex *) aligned_alloc(64, nalloc);
#endif
*/
        memcpy(pzOut->zeros, pzIn.zeros,
               (size_t) pzIn.nzeros*sizeof(double complex));
/*
        ierr = array_copy64z_work(pzIn.nzeros, pzIn.zeros, pzOut->zeros);
        if (ierr != 0)
        {
            printf("%s: Error copying zeros\n", __func__);
            return -1;
        }
*/
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Copies a SAC header structure.
 *
 * @param[in] hdrIn     Header struct to copy of hdrOut.
 *
 * @param[out] hdrOut   Copy of hdrIn.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_memory
 *
 * @author Ben Baker
 *
 */
int sacio_copyHeader(const struct sacHeader_struct hdrIn,
                     struct sacHeader_struct *hdrOut)
{
    memcpy(hdrOut, &hdrIn, sizeof(struct sacHeader_struct)); 
    return 0;
}
//============================================================================//
/*!
 * @brief Copies a SAC data structure.
 *
 * @param[in] sacIn    Input structure to copy to sacOut.
 *
 * @param[out] sacOut  On successful exit this is a copy of the sacIn structure.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_memory
 *
 * @author Ben Baker
 *
 */
int sacio_copy(const struct sacData_struct sacIn,
               struct sacData_struct *sacOut)
{
    int ierr;
    //size_t nalloc;
    memset(sacOut, 0, sizeof(struct sacData_struct));
    ierr = sacio_copyHeader(sacIn.header, &sacOut->header); 
    if (ierr != 0)
    {
        printf("%s: Error copying header\n", __func__);
        return -1;
    }
    ierr = sacio_copyPolesAndZeros(sacIn.pz, &sacOut->pz);
    if (ierr != 0)
    {
        printf("%s: Error copying poles and zeros\n", __func__);
        return -1;
    }
    ierr = sacio_copyFAP(sacIn.fap, &sacOut->fap);
    if (ierr != 0)
    {
        printf("%s: Error copying FAP\n", __func__);
        return -1;
    }
    sacOut->npts = sacIn.npts;
    if (sacIn.npts > 0 && sacIn.data != NULL)
    {
        sacOut->data = sacio_malloc64f(sacIn.npts);
/*
        nalloc = (size_t) sacIn.npts*sizeof(double);
#ifdef USE_POSIX
        posix_memalign((void **) &sacOut->data, 64, nalloc);
#else
        sacOut->data = (double *) aligned_alloc(64, nalloc);
#endif
*/
        memcpy(sacOut->data, sacIn.data, (size_t) sacIn.npts*sizeof(double)); 
/*
        ierr = array_copy64f_work(sacIn.npts, sacIn.data, sacOut->data);
        if (ierr != 0)
        {
            printf("%s: Error copying data\n", __func__);
            return -1;
        }
*/
    }
    return ierr;
}
//============================================================================//
/*!
 * @brief Reads a SAC time series file.  This includes the data and the
 *        header.
 *
 * @param[in] fname    Name of sac file to read.
 *
 * @param[out] sac     On successful exit contains the SAC header and data.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_io
 *
 * @author Ben Baker
 *
 */
int sacio_readTimeSeriesFile(const char *fname, struct sacData_struct *sac)
{
    FILE *sacfl;
    char *cdat;
    size_t nalloc, nread;
    long sz;
    double delta; 
    int ierr;
    bool lswap;
    //------------------------------------------------------------------------//
    ierr = 0;
    sacio_freeData(sac);
    sacio_freeHeader(&sac->header);
    // check the file exists 
    if (!sacio_os_path_isfile(fname))
    {
        printf("%s: Error %s does not exist\n", __func__, fname);
        return -1;
    }
    // read the file
    sacfl = fopen(fname, "rb");
    fseek(sacfl, 0L, SEEK_END);
    sz = ftell(sacfl);
    rewind(sacfl);
    if (sz < 632)
    {
        printf("%s: Cannot load header - file too small\n", __func__);
        return -1;
    }
    nalloc = (size_t) (sz + 8)*sizeof(char);
#ifdef USE_POSIX
    cdat = NULL;
    posix_memalign((void **) &cdat, 64, nalloc);
#else
    cdat = (char *) aligned_alloc(64, nalloc);
#endif
    nread = fread(cdat, 632, sizeof(char), sacfl); 
    if (nread == 0)
    {
         printf("%s: Faild to read header\n", __func__);
         return -1;
    }
    nread = fread(&cdat[640], (size_t) (sz - 632), sizeof(char), sacfl);
    if (nread == 0)
    {
        printf("%s: Failed to read data\n", __func__);
        return -1;
    }
    fclose(sacfl);
    // figure out the endianess
    lswap = false;
    readIntHeader(lswap, &cdat[316], &sac->npts);
    if (632 + 4*(long) sac->npts != sz)
    {
        lswap = true;
        readIntHeader(lswap, &cdat[316], &sac->npts);
        if (632 + 4*(long) sac->npts != sz)
        {
            printf("%s: Can't determine endiannesss\n", __func__);
            return -2;
        }
    }
    // unpack the header
    sacio_unpackHeader(lswap, cdat, &sac->header);
    // sampling period is tricky - it is read as a float which can be
    // very fidgety so ensure it is good to 6 digits.
    delta = (double) ((int64_t) (sac->header.delta*1.e6 + 0.5))*1.e-6;
    if (fabs(sac->header.delta - delta) > (double) FLT_EPSILON*10.0) //1.e-6)
    {
        fprintf(stdout, "Changing sampling period from %.8e to %.8e\n", 
                sac->header.delta, delta);
    }
    sac->header.delta = delta;
    // unpack the data
    if (sac->npts > 0)
    {
        sac->data = sacio_malloc64f(sac->npts);
/*
        nalloc = (size_t) sac->npts*sizeof(double);
#ifdef USE_POSIX
        posix_memalign((void **) &sac->data, 64, nalloc);
#else
        sac->data = (double *) aligned_alloc(64, nalloc);
#endif
*/
        ierr = sacio_unpackData(lswap, &cdat[640], sac->npts, sac->data); 
        if (ierr != 0)
        {
            printf("%s: Error unpacking data\n", __func__);
            return -1; 
        }   
    }
    free(cdat);
    return ierr;
}
//============================================================================//
/*!
 * @brief Reads the data points from SAC file.
 *
 * @param[in] fname   SAC file name.
 * @param[in] nwork   Max number of points x can hold.  If this is negative
 *                    then this is a space query and x will not be set.
 *
 * @param[out] npts   Number of data points in x.
 * @param[out] x      Data points \f$ x[i] \, i=1,2,\cdots n_{pts} \f$.  This
 *                    is an array of dimension [nwork] however only the first
 *                    npts are accessed..
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_io
 *
 * @author Ben Baker
 *
 */
int sacio_readData(const char *fname,
                   const int nwork, int *npts, double *__restrict__ x)
{
    FILE *sacfl;
    char *cdat;
    long sz;
    size_t nalloc, nread;
    int ierr;
    bool lswap;
    //------------------------------------------------------------------------//
    *npts = 0;
    // read the data 
    if (!sacio_os_path_isfile(fname))
    {
        printf("%s: Error %s does not exist\n", __func__, fname);
        return -1;
    }
    sacfl = fopen(fname, "rb");
    fseek(sacfl, 0L, SEEK_END);
    sz = ftell(sacfl);
    rewind(sacfl); //fseek(sacfl, 0L, SEEK_SET);
    if (sz < 632)
    {
        printf("%s: Cannot load header - file too small\n", __func__);
        return -1;
    }
    nalloc = (size_t) (sz + 8)*sizeof(char);
#ifdef USE_POSIX
    cdat = NULL;
    posix_memalign((void **) cdat, 64, nalloc);
#else
    cdat = (char *) aligned_alloc(64, nalloc);
#endif
    nread = fread(cdat, 632, sizeof(char), sacfl);
    if (nread == 0)
    {
         printf("%s: Faild to read header\n", __func__);
         return -1;
    }
    nread = fread(&cdat[640], (size_t) (sz - 632), sizeof(char), sacfl);
    if (nread == 0)
    {
        printf("%s: Failed to read data\n", __func__);
        return -1;
    }
    fclose(sacfl);
    // figure out the endianess
    lswap = false;
    readIntHeader(lswap, &cdat[316], npts);
    if (632 + 4*(long) *npts != sz) 
    {
        lswap = true;
        readIntHeader(lswap, &cdat[316], npts);
        if (632 + 4*(long) *npts != sz) 
        {
            printf("%s: Can't determine endiannesss\n", __func__);
            return -2; 
        }   
    }
    // space query
    if (nwork < 0){return 0;}
    // is there enough space for the result?
    if (x == NULL || *npts > nwork)
    {
        if (x == NULL){printf("%s: Error x is NULL\n", __func__);}
        if (*npts > nwork)
        {
            printf("%s: Insufficient workspace for x\n", __func__);
        }
        return -1;
    }
    // unpack the data
    ierr = sacio_unpackData(lswap, &cdat[640], *npts, x); 
    if (ierr != 0)
    {
        printf("%s: Error unpacking data\n", __func__);
        return -1;
    }
    free(cdat);
    return ierr;
} 
//============================================================================//
/*!
 * @brief Unpacks the data in cdat.
 *
 * @param[in] lswap    If true then byte swap the data. 
 * @param[in] npts     Number of data points to unpack.
 * @param[in] cdat     Character array holding the binary data.  This has
 *                     dimension [4*npts].
 *
 * @param[out] x       Data points.  This is an array of dimension [npts].
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_io
 *
 * @author Ben Baker
 *
 */
int sacio_unpackData(const bool lswap, const char *__restrict__ cdat,
                     const int npts, double *__restrict__ x)
{
    char c4[64] __attribute__ ((aligned(64)));
    float f;
    int i;
    if (!lswap)
    {
        for (i=0; i<npts; i++)
        {
            c4[0] = cdat[4*i];
            c4[1] = cdat[4*i+1];
            c4[2] = cdat[4*i+2];
            c4[3] = cdat[4*i+3];
            memcpy(&f, c4, 4*sizeof(char));
            x[i] = (double) f;
        }
    } 
    else
    {
        for (i=0; i<npts; i++)
        {
            c4[0] = cdat[4*i+3];
            c4[1] = cdat[4*i+2];
            c4[2] = cdat[4*i+1];
            c4[3] = cdat[4*i];
            memcpy(&f, c4, 4*sizeof(char));
            x[i] = (double) f;
        }
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Packs the time series data into a char * for writing.
 *
 * @param[in] lswap     If true then swap bytes.
 * @param[in] npts      Number of points in time series.
 * @param[in] x         Time series to pack.  This is an array of dimension
 *                      [npts].
 *
 * @param[out] cdat     Time series to write to disk.  This is an array of
 *                      dimension [4*npts].
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_io
 *
 * @author Ben Baker
 *
 */
int sacio_packData(const bool lswap, const int npts, 
                   const double *__restrict__ x, char *__restrict__ cdat)
{
    char c4[64] __attribute__ ((aligned(64)));
    float f;
    int i;
    if (!lswap)
    {
        for (i=0; i<npts; i++)
        {
            f = (float) x[i];
            memcpy(&cdat[4*i], &f, 4*sizeof(char));
        }
    }
    else
    {
        for (i=0; i<npts; i++)
        {
            f = (float) x[i];
            memcpy(c4, &f, 4*sizeof(char));
            cdat[4*i]   = c4[3];
            cdat[4*i+1] = c4[2];
            cdat[4*i+2] = c4[1];
            cdat[4*i+3] = c4[0];
        }
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Reads the SAC header from a file.
 *
 * @param[in] fname    Name of SAC file.
 *
 * @param[out] header  SAC header values from file.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_io
 *
 * @author Ben Baker
 *
 */
int sacio_readHeader(const char *fname, struct sacHeader_struct *header)
{
    FILE *sacfl;
    struct stat info;
    long sz;
    size_t nread;
    double delta;
    int ierr, npts;
    bool lswap;
    char cdat[632] __attribute__ ((aligned(64)));
    ierr = 0;
    memset(header, 0, sizeof(struct sacHeader_struct));
    memset(cdat, 0, 632*sizeof(char));
    if (stat(fname, &info) == -1)
    {
        printf("%s: Error %s does not exist\n", __func__, fname);
        return -1;
    }
    if (!S_ISREG(info.st_mode))
    {
        printf("%s: Error %s isn't a file\n", __func__, fname);
        return -1;
    }
    sacfl = fopen(fname, "rb");
    fseek(sacfl, 0L, SEEK_END);
    sz = ftell(sacfl);
    fseek(sacfl, 0L, SEEK_SET);
    if (sz < 632)
    {
        printf("%s: Cannot load header - file too small\n", __func__);
        return -1;
    }
    nread = fread(cdat, 632, sizeof(char), sacfl);
    if (nread == 0)
    {
        printf("%s: Failed to read header\n", __func__);
        return -1;
    }
    fclose(sacfl);
    // figure out the endianess
    lswap = false;
    readIntHeader(lswap, &cdat[316], &npts);
    if (632 + 4*(long) npts != sz)
    {
        lswap = true;
        readIntHeader(lswap, &cdat[316], &npts);
        if (632 + 4*(long) npts != sz)
        {
            printf("%s: Can't determine endiannesss\n", __func__);
            return -2;
        }
    }
    // unpack the header
    sacio_unpackHeader(lswap, cdat, header);
    // sampling period is tricky - it is read as a float which can be
    // very fidgety so ensure it is good to 6 digits.
    delta = (double) ((int64_t) (header->delta*1.e6 + 0.5))*1.e-6;
    if (fabs(header->delta - delta) > (double) FLT_EPSILON*10.0) //1.e-6)
    {
        fprintf(stdout, "Changing sampling period from %.8e to %.8e\n",
                header->delta, delta);
    }
    header->delta = delta;
    return ierr;
}
//============================================================================//
/*!
 * @brief Sets a float64 variable on the SAC header.
 *
 * @param[in] type     SAC header variable name to set.
 * @param[in] var      Value of variable to set in SAC header.
 *
 * @param[out] header  SAC header updated with new real value.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_header_utils
 *
 * @author Ben Baker
 *
 */
int sacio_setFloatHeader(const enum sacHeader_enum type,
                         const double var,
                         struct sacHeader_struct *header)
{
    if (type == SAC_FLOAT_DELTA)
    {
        header->delta = var; 
    }
    else if (type == SAC_FLOAT_DEPMIN)
    {
        header->depmin = var;
    }
    else if (type == SAC_FLOAT_DEPMAX)
    {
        header->depmax = var;
    }
    else if (type == SAC_FLOAT_SCALE)
    {
        header->scale = var;
    }
    else if (type == SAC_FLOAT_ODELTA)
    {
        header->odelta = var;
    }
    else if (type == SAC_FLOAT_B)
    {
        header->b = var;
    }
    else if (type == SAC_FLOAT_E)
    {
        header->e = var;
    }
    else if (type == SAC_FLOAT_O)
    {
        header->o = var;
    }
    else if (type == SAC_FLOAT_A)
    {
        header->a = var;
    }
    else if (type == SAC_FLOAT_INTERNAL1)
    {
        header->internal1 = var;
    }
    else if (type == SAC_FLOAT_T0)
    {
        header->t0 = var;
    }
    else if (type == SAC_FLOAT_T1)
    {
        header->t1 = var;
    }
    else if (type == SAC_FLOAT_T2)
    {
        header->t2 = var;
    }
    else if (type == SAC_FLOAT_T3)
    {
        header->t3 = var;
    }
    else if (type == SAC_FLOAT_T4)
    {
        header->t4 = var;
    }
    else if (type == SAC_FLOAT_T5)
    {
        header->t5 = var;
    }
    else if (type == SAC_FLOAT_T6)
    {
        header->t6 = var;
    }
    else if (type == SAC_FLOAT_T7)
    {
        header->t7 = var;
    }
    else if (type == SAC_FLOAT_T8)
    {
        header->t8 = var;
    }
    else if (type == SAC_FLOAT_T9)
    {
        header->t9 = var;
    }
    else if (type == SAC_FLOAT_F)
    {
        header->f = var;
    }
    else if (type == SAC_FLOAT_RESP0)
    {
        header->resp0 = var;
    }
    else if (type == SAC_FLOAT_RESP1)
    {
        header->resp1 = var;
    }
    else if (type == SAC_FLOAT_RESP2)
    {
        header->resp2 = var;
    }
    else if (type == SAC_FLOAT_RESP3)
    {
        header->resp3 = var;
    }
    else if (type == SAC_FLOAT_RESP4)
    {
        header->resp4 = var;
    }
    else if (type == SAC_FLOAT_RESP5)
    {
        header->resp5 = var;
    }
    else if (type == SAC_FLOAT_RESP6)
    {
        header->resp6 = var;
    }
    else if (type == SAC_FLOAT_RESP7)
    {
        header->resp7 = var;
    }
    else if (type == SAC_FLOAT_RESP8)
    {
        header->resp8 = var;
    }
    else if (type == SAC_FLOAT_RESP9)
    {
        header->resp9 = var;
    }
    else if (type == SAC_FLOAT_STLA)
    {
        header->stla = var;
    }
    else if (type == SAC_FLOAT_STLO)
    {
        header->stlo = var;
    }
    else if (type == SAC_FLOAT_STEL)
    {
        header->stel = var;
    }
    else if (type == SAC_FLOAT_STDP)
    {
        header->stdp = var;
    }
    else if (type == SAC_FLOAT_EVLA)
    {
        header->evla = var;
    }
    else if (type == SAC_FLOAT_EVLO)
    {
        header->evlo = var;
    }
    else if (type == SAC_FLOAT_EVEL)
    {
        header->evel = var;
    }
    else if (type == SAC_FLOAT_EVDP)
    {
        header->evdp = var;
    }
    else if (type == SAC_FLOAT_MAG)
    {
        header->mag = var;
    }
    else if (type == SAC_FLOAT_USER0)
    {
        header->user0 = var;
    }
    else if (type == SAC_FLOAT_USER1)
    {
        header->user1 = var;
    }
    else if (type == SAC_FLOAT_USER2)
    {
        header->user2 = var;
    }
    else if (type == SAC_FLOAT_USER3)
    {
        header->user3 = var;
    }
    else if (type == SAC_FLOAT_USER4)
    {
        header->user4 = var;
    }
    else if (type == SAC_FLOAT_USER5)
    {
        header->user5 = var;
    }
    else if (type == SAC_FLOAT_USER6)
    {
        header->user6 = var;
    }
    else if (type == SAC_FLOAT_USER7)
    {
        header->user7 = var;
    }
    else if (type == SAC_FLOAT_USER8)
    {
        header->user8 = var;
    }
    else if (type == SAC_FLOAT_USER9)
    {
        header->user9 = var;
    }
    else if (type == SAC_FLOAT_DIST)
    {
        header->dist = var;
    }
    else if (type == SAC_FLOAT_AZ)
    {
        header->az = var;
    }
    else if (type == SAC_FLOAT_BAZ)
    {
        header->baz = var;
    }
    else if (type == SAC_FLOAT_GCARC)
    {
        header->gcarc = var;
    }
    else if (type == SAC_FLOAT_INTERNAL2)
    {
        header->internal2 = var;
    }
    else if (type == SAC_FLOAT_DEPMEN)
    {
        header->depmen = var;
    }
    else if (type == SAC_FLOAT_CMPAZ)
    {
        header->cmpaz = var;
    }
    else if (type == SAC_FLOAT_CMPINC)
    {
        header->cmpinc = var;
    }
    else if (type == SAC_FLOAT_XMINIMUM)
    {
        header->xminimum = var;
    }
    else if (type == SAC_FLOAT_XMAXIMUM)
    {
        header->xmaximum = var;
    }
    else if (type == SAC_FLOAT_YMINIMUM)
    {
        header->yminimum = var;
    }
    else if (type == SAC_FLOAT_YMAXIMUM)
    {
        header->ymaximum = var;
    }
    else if (type == SAC_FLOAT_UNUSED0)
    {
        header->unused0 = var;
    }
    else if (type == SAC_FLOAT_UNUSED1)
    {
        header->unused1 = var;
    }
    else if (type == SAC_FLOAT_UNUSED2)
    {
        header->unused2 = var;
    }
    else if (type == SAC_FLOAT_UNUSED3)
    {
        header->unused3 = var;
    }
    else if (type == SAC_FLOAT_UNUSED4)
    {
        header->unused4 = var;
    }
    else if (type == SAC_FLOAT_UNUSED5)
    {
        header->unused5 = var;
    }
    else if (type == SAC_FLOAT_UNUSED6)
    {
        header->unused6 = var;
    }
    else
    {
        printf("%s: Invalid header type\n", __func__);
        return -1;
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Sets an integer variable on the SAC header.
 *
 * @param[in] type     SAC header variable name to set.
 * @param[in] ivar     Value of variable to set in SAC header.
 *
 * @param[out] header  SAC header updated with new integer value .
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_header_utils
 *
 * @author Ben Baker
 *
 */
int sacio_setIntegerHeader(const enum sacHeader_enum type,
                           const int ivar,
                           struct sacHeader_struct *header)
{
    if (type == SAC_INT_NZYEAR)
    {
        header->nzyear = ivar;
    }
    else if (type == SAC_INT_NZJDAY)
    {
        header->nzjday = ivar;
    }
    else if (type == SAC_INT_NZHOUR)
    {
        header->nzhour = ivar;
    }
    else if (type == SAC_INT_NZMIN)
    {
        header->nzmin = ivar;
    }
    else if (type == SAC_INT_NZSEC)
    {
        header->nzsec = ivar;
    }
    else if (type == SAC_INT_NZMSEC)
    {
        header->nzmsec = ivar;
    }
    else if (type == SAC_INT_NVHDR)
    {
        header->nvhdr = ivar;
    }
    else if (type == SAC_INT_NORID)
    {
        header->norid = ivar;
    }
    else if (type == SAC_INT_NEVID)
    {
        header->nevid = ivar;
    }
    else if (type == SAC_INT_NPTS)
    {
        header->npts = ivar;
    }
    else if (type == SAC_INT_INTERNAL1)
    {
        header->ninternal1 = ivar;
    }
    else if (type == SAC_INT_NWFID)
    {
        header->nwfid = ivar;
    }
    else if (type == SAC_INT_NXSIZE)
    {
        header->nxsize = ivar;
    }
    else if (type == SAC_INT_NYSIZE)
    {
        header->nysize = ivar;
    }
    else if (type == SAC_INT_UNUSED0)
    {
        header->nunused0 = ivar;
    }
    else if (type == SAC_INT_IFTYPE)
    {
        header->iftype = ivar;
    }
    else if (type == SAC_INT_IDEP)
    {
        header->idep = ivar;
    }
    else if (type == SAC_INT_IZTYPE)
    {
        header->iztype = ivar;
    }
    else if (type == SAC_INT_UNUSED1)
    {
        header->nunused1 = ivar;
    }
    else if (type == SAC_INT_IINST)
    {
        header->iinst = ivar;
    }
    else if (type == SAC_INT_ISTREG)
    {
        header->istreg = ivar;
    }
    else if (type == SAC_INT_IEVREG)
    {
        header->ievreg = ivar;
    }
    else if (type == SAC_INT_IEVTYP)
    {
        header->ievtyp = ivar;
    }
    else if (type == SAC_INT_IQUAL)
    {
        header->iqual = ivar;
    }
    else if (type == SAC_INT_ISYNTH)
    {
        header->isynth = ivar;
    }
    else if (type == SAC_INT_IMAGTYP)
    {
        header->imagtyp = ivar;
    }
    else if (type == SAC_INT_IMAGSRC)
    {
        header->imagsrc = ivar;
    }
    else if (type == SAC_INT_UNUSED2)
    {
        header->nunused2 = ivar;
    }
    else if (type == SAC_INT_UNUSED3)
    {
        header->nunused3 = ivar;
    }
    else if (type == SAC_INT_UNUSED4)
    {
        header->nunused4 = ivar;
    }
    else if (type == SAC_INT_UNUSED5)
    {
        header->nunused5 = ivar;
    }
    else if (type == SAC_INT_UNUSED6)
    {
        header->nunused6 = ivar;
    }
    else if (type == SAC_INT_UNUSED7)
    {
        header->nunused7 = ivar;
    }
    else if (type == SAC_INT_UNUSED8)
    {
        header->nunused8 = ivar;
    }
    else if (type == SAC_INT_UNUSED9)
    {
        header->nunused9 = ivar;
    }
    else 
    {
        printf("%s: Invalid header type\n", __func__);
        return -1;
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Sets a boolean variable on the SAC header.
 *
 * @param[in] type     SAC header variable name to set.
 * @param[in] lvar     Value of variable to set in SAC header.
 *
 * @param[out] header  SAC header updated with new boolean value .
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_header_utils
 *
 * @author Ben Baker
 *
 */
int sacio_setBooleanHeader(const enum sacHeader_enum type,
                           const bool lvar,
                           struct sacHeader_struct *header) 
{
    int ivar;
    ivar = (int) lvar;
    if (type == SAC_BOOL_LEVEN)
    {
        header->leven = ivar;
    }
    else if (type == SAC_BOOL_LPSPOL)
    {
        header->lpspol = ivar;
    }
    else if (type == SAC_BOOL_LOVROK)
    {
        header->lovrok = ivar;
    }
    else if (type == SAC_BOOL_LCALDA)
    {
        header->lcalda = ivar;
    }
    else if (type == SAC_BOOL_LUNUSED)
    {
        header->lunused = ivar;
    }
    else
    {
        printf("%s: Invalid header type\n", __func__);
        return -1;
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Sets a variable on the SAC header.
 *
 * @param[in] type     SAC header variable name to set.
 * @param[in] kvar     NULL terminated character variable to set. 
 *                     This should be length of 8 unless setting KEVNM
 *                     in which case this should be length 16.
 *
 * @param[out] header  SAC header updated with new character value.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_header_utils
 *
 * @author Ben Baker
 *
 */
int sacio_setCharacterHeader(const enum sacHeader_enum type,
                             const char *kvar,
                             struct sacHeader_struct *header)
{
    if (type == SAC_CHAR_KSTNM)
    {
        writeChar8(kvar, header->kstnm);
    }
    else if (type == SAC_CHAR_KEVNM)
    {
        writeChar16(kvar, header->kevnm);
    }
    else if (type == SAC_CHAR_KHOLE)
    {
        writeChar8(kvar, header->khole);
    }
    else if (type == SAC_CHAR_KO)
    {
        writeChar8(kvar, header->ko);
    }
    else if (type == SAC_CHAR_KA)
    {
        writeChar8(kvar, header->ka);
    }
    else if (type == SAC_CHAR_KT0)
    {
        writeChar8(kvar, header->kt0);
    }
    else if (type == SAC_CHAR_KT1)
    {
        writeChar8(kvar, header->kt1);
    }
    else if (type == SAC_CHAR_KT2)
    {
        writeChar8(kvar, header->kt2);
    }
    else if (type == SAC_CHAR_KT3)
    {
        writeChar8(kvar, header->kt3);
    }
    else if (type == SAC_CHAR_KT4)
    {
        writeChar8(kvar, header->kt4);
    }
    else if (type == SAC_CHAR_KT5)
    {
        writeChar8(kvar, header->kt5);
    }
    else if (type == SAC_CHAR_KT6)
    {
        writeChar8(kvar, header->kt6);
    }
    else if (type == SAC_CHAR_KT7)
    {
        writeChar8(kvar, header->kt7);
    }
    else if (type == SAC_CHAR_KT8)
    {
        writeChar8(kvar, header->kt8);
    }
    else if (type == SAC_CHAR_KT9)
    {
        writeChar8(kvar, header->kt9);
    }
    else if (type == SAC_CHAR_KF)
    {
        writeChar8(kvar, header->kf);
    }
    else if (type == SAC_CHAR_KUSER0)
    {
        writeChar8(kvar, header->kuser0);
    }
    else if (type == SAC_CHAR_KUSER1)
    {
        writeChar8(kvar, header->kuser1);
    }
    else if (type == SAC_CHAR_KUSER2)
    {
        writeChar8(kvar, header->kuser2);
    }
    else if (type == SAC_CHAR_KCMPNM)
    {
        writeChar8(kvar, header->kcmpnm);
    }
    else if (type == SAC_CHAR_KNETWK)
    {
        writeChar8(kvar, header->knetwk);
    }
    else if (type == SAC_CHAR_KDATRD)
    {
        writeChar8(kvar, header->kdatrd);
    }
    else if (type == SAC_CHAR_KINST)
    {
        writeChar8(kvar, header->kinst);
    }
    else
    {
        printf("%s: Invalid header type\n", __func__);
        return -1;
    }
    return 0;
}
//============================================================================//
/*!
 * @brief Sets the epochal pick time on the SAC header.
 *
 * @param[in] pickTime        Epochal time of phase arrival (UTC-seconds).
 * @param[in] phaseName       Name of phase.  This should be no longer than
 *                            8 characters.
 * @param[in] pickHeaderTime  Header variable identifier to hold the pick
 *                            time.  While any valid SAC_FLOAT variable will
 *                            work this will likely be SAC_FLOAT_A for a
 *                            primary arrival or SAC_FLOAT_T0-SAC_FLOAT_T9
 *                            for other phases.
 * @param[in] pickHeaderName  Header variable identifer to hold the phase name.
 *                            While any valid SAC_CHAR variable will work this
 *                            will likely be SAC_CHAR_KA for a primary arrival
 *                            or SAC_CHAR_KT0-SAC_CHAR_KT9 for other phases.
 * 
 * @param[in,out] header      On input contains the sampling period, number of 
 *                            points in the trace, and trace start time.
 *                            On output the pick time has been written to
 *                            pickHeaderTime and the phase name has been written
 *                            to pickHeaderName.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_header_utils
 *
 * @author Ben Baker, ISTI
 */
int sacio_setEpochalPickOnHeader(const double pickTime,
                                 const char *phaseName,
                                 const enum sacHeader_enum pickHeaderTime,
                                 const enum sacHeader_enum pickHeaderName,
                                 struct sacHeader_struct *header)
{
    double dt, epoch, relativePickTime;
    int ierr, npts;
    // Get the trace epochal start time 
    ierr = sacio_getEpochalStartTime(*header, &epoch);
    if (ierr != 0)
    {
        printf("%s: Failed to get start time\n", __func__);
        return -1;
    }
    sacio_getIntegerHeader(SAC_INT_NPTS, *header, &npts);
    sacio_getFloatHeader(SAC_FLOAT_DELTA, *header, &dt);
    if (pickTime < epoch)
    {
        printf("%s: Pick time is before trace start time\n", __func__);
    }
    if (pickTime > epoch + (double) (npts - 1)*dt)
    {
        printf("%s: Pick time comes in after trace end time\n", __func__);
    }
    // Make it a relative time
    relativePickTime = pickTime - epoch;
    sacio_setFloatHeader(pickHeaderTime, relativePickTime, header);
    sacio_setCharacterHeader(pickHeaderName, phaseName, header);
    return 0;
}
//============================================================================//
/*!
 * @brief Convenience utility for getting the epochal time of the first
 *        sample in a SAC structure.  Note the beginning offset, b, has been
 *        accounted for.
 *
 * @param[in] header    SAC header from which to extract start time.
 * @param[out] t0       Epochal time of first sample in trace in UTC seconds.
 * @result 0 indicates success
 * @ingroup sac_header_utils
 */
int sacio_getEpochalStartTime(const struct sacHeader_struct header, double *t0)
{
    double b;
    int ierr, nzyear, nzjday, nzhour, nzmin, nzsec, nzmsec, nzmusec;
    *t0 = 0.0;
    ierr = 0;
    ierr  = sacio_getFloatHeader(SAC_FLOAT_B, header, &b);
    ierr += sacio_getIntegerHeader(SAC_INT_NZYEAR, header, &nzyear); 
    ierr += sacio_getIntegerHeader(SAC_INT_NZJDAY, header, &nzjday);
    ierr += sacio_getIntegerHeader(SAC_INT_NZHOUR, header, &nzhour);
    ierr += sacio_getIntegerHeader(SAC_INT_NZMIN,  header, &nzmin);
    ierr += sacio_getIntegerHeader(SAC_INT_NZSEC,  header, &nzsec);
    ierr += sacio_getIntegerHeader(SAC_INT_NZMSEC, header, &nzmsec);
    if (ierr != 0)
    {
        printf("%s: Timing information in header is invalid\n", __func__);
        return -1;
    }
    nzmusec = nzmsec*1000; // milli to microseconds
    *t0 = sacio_time_calendar2epoch(nzyear, nzjday, nzhour, nzmin,
                                    nzsec, nzmusec);
    *t0 = *t0 + b;
    return 0;
}
/*!
 * @brief Convenience utility for getting the end epochal time of the
 *        last sample in the SAC strutcure.  Note the beginning offset, b,
 *        has been accounted for.
 * @param[in] header  SAC header from which to extract end time.
 * @param[out] t1     Epochal time of last time in UTC seconds.
 * @ingroup sac_header_utils
 */
int sacio_getEpochalEndTime(const struct sacHeader_struct header, double *t1)
{
    int ierr, npts;
    double dt, t0;
    *t1 = 0;
    ierr = sacio_getEpochalStartTime(header, &t0); 
    if (ierr != 0)
    {
        printf("%s: Error getting start time\n", __func__);
        return -1;
    }
    ierr  = sacio_getIntegerHeader(SAC_INT_NPTS, header, &npts);
    if (ierr != 0)
    {
        printf("%s: Error getting npts\n", __func__);
        return -1;
    }
    ierr = sacio_getFloatHeader(SAC_FLOAT_DELTA, header, &dt);
    if (ierr != 0)
    {
        printf("%s: Failed to get sampling period\n", __func__);
        return -1;
    }
    *t1 = t0 + (double) (npts - 1)*dt;
    return 0;
}
//============================================================================//
/*!
 * @brief Sets the epochal start time (UTC seconds) on the SAC header.
 *
 * @param[in] t0       Epochal start time in UTC seconds of trace.
 *
 * @param[out] header  On exit the epochal start time has been set.
 *
 * @result 0 indicates success.
 *
 * @ingroup sac_header_utils
 *
 * @author Ben Baker
 */
int sacio_setEpochalStartTime(const double t0, struct sacHeader_struct *header)
{
    double b;
    int ierr, mday, month, nzyear, nzjday,  nzhour,
        nzmin, nzsec, nzmusec, nzmsec;
    ierr = sacio_time_epoch2calendar(t0, &nzyear, &nzjday, &month, &mday,
                                     &nzhour, &nzmin, &nzsec, &nzmusec);
    if (ierr != 0)
    {
        printf("%s: Error computing calendrical date\n", __func__);
        return -1;
    }
    nzmsec = (int) ((double) nzmusec*1.e-3 + 0.5);
    // avoid precision in micro to milliseconds by using b 
    b = ((double) nzmusec*1.e-3 - (double) nzmsec)*1.e-3;
    sacio_setFloatHeader(SAC_FLOAT_B, b, header);
    sacio_setIntegerHeader(SAC_INT_NZYEAR, nzyear, header);
    sacio_setIntegerHeader(SAC_INT_NZJDAY, nzjday, header);
    sacio_setIntegerHeader(SAC_INT_NZHOUR, nzhour, header);
    sacio_setIntegerHeader(SAC_INT_NZMIN,  nzmin,  header);
    sacio_setIntegerHeader(SAC_INT_NZSEC,  nzsec,  header);
    sacio_setIntegerHeader(SAC_INT_NZMSEC, nzmsec, header);
    return 0;
}
//============================================================================//
/*!
 * @brief Gets an double value from the SAC header.
 *
 * @param[in] type       SAC header variable to get.
 * @param[in] header     Header from which to extract desired variable.
 *
 * @param[out] var       Value of desired header variable.
 *
 * @retval  0 indicates success.
 * @retval -1 indicates the variable was not defined.
 * @retval  1 indicates an invalid enumerated type.
 *
 * @ingroup sac_header_utils
 *
 * @author Ben Baker
 *
 */
int sacio_getFloatHeader(const enum sacHeader_enum type,
                         const struct sacHeader_struct header,
                         double *var)
{
    *var = 0.0;
    if (type == SAC_FLOAT_DELTA)
    {
        *var = header.delta;
    }
    else if (type == SAC_FLOAT_DEPMIN)
    {
        *var = header.depmin;
    }
    else if (type == SAC_FLOAT_DEPMAX)
    {
        *var = header.depmax;
    }
    else if (type == SAC_FLOAT_SCALE)
    {
        *var = header.scale;
    }
    else if (type == SAC_FLOAT_ODELTA)
    {
        *var = header.odelta;
    }
    else if (type == SAC_FLOAT_B)
    {
        *var = header.b;
    }
    else if (type == SAC_FLOAT_E)
    {
        *var = header.e;
    }
    else if (type == SAC_FLOAT_O)
    {
        *var = header.o;
    }
    else if (type == SAC_FLOAT_A)
    {
        *var = header.a;
    }
    else if (type == SAC_FLOAT_INTERNAL1)
    {
        *var = header.internal1;
    }
    else if (type == SAC_FLOAT_T0)
    {
        *var = header.t0;
    }
    else if (type == SAC_FLOAT_T1)
    {
        *var = header.t1;
    }
    else if (type == SAC_FLOAT_T2)
    {
        *var = header.t2;
    }
    else if (type == SAC_FLOAT_T3)
    {
        *var = header.t3;
    }
    else if (type == SAC_FLOAT_T4)
    {
        *var = header.t4;
    }
    else if (type == SAC_FLOAT_T5)
    {
        *var = header.t5;
    }
    else if (type == SAC_FLOAT_T6)
    {
        *var = header.t6;
    }
    else if (type == SAC_FLOAT_T7)
    {
        *var = header.t7;
    }
    else if (type == SAC_FLOAT_T8)
    {
        *var = header.t8;
    }
    else if (type == SAC_FLOAT_T9)
    {
        *var = header.t9;
    }
    else if (type == SAC_FLOAT_F)
    {
        *var = header.f;
    }
    else if (type == SAC_FLOAT_RESP0)
    {
        *var = header.resp0;
    }
    else if (type == SAC_FLOAT_RESP1)
    {
        *var = header.resp1;
    }
    else if (type == SAC_FLOAT_RESP2)
    {
        *var = header.resp2;
    }
    else if (type == SAC_FLOAT_RESP3)
    {
        *var = header.resp3;
    }
    else if (type == SAC_FLOAT_RESP4)
    {
        *var = header.resp4;
    }
    else if (type == SAC_FLOAT_RESP5)
    {
        *var = header.resp5;
    }
    else if (type == SAC_FLOAT_RESP6)
    {
        *var = header.resp6;
    }
    else if (type == SAC_FLOAT_RESP7)
    {
        *var = header.resp7;
    }
    else if (type == SAC_FLOAT_RESP8)
    {
        *var = header.resp8;
    }
    else if (type == SAC_FLOAT_RESP9)
    {
        *var = header.resp9;
    }
    else if (type == SAC_FLOAT_STLA)
    {
        *var = header.stla;
    }
    else if (type == SAC_FLOAT_STLO)
    {
        *var = header.stlo;
    }
    else if (type == SAC_FLOAT_STEL)
    {
        *var = header.stel;
    }
    else if (type == SAC_FLOAT_STDP)
    {
        *var = header.stdp;
    }
    else if (type == SAC_FLOAT_EVLA)
    {
        *var = header.evla;
    }
    else if (type == SAC_FLOAT_EVLO)
    {
        *var = header.evlo;
    }
    else if (type == SAC_FLOAT_EVEL)
    {
        *var = header.evel;
    }
    else if (type == SAC_FLOAT_EVDP)
    {
        *var = header.evdp;
    }
    else if (type == SAC_FLOAT_MAG)
    {
        *var = header.mag;
    }
    else if (type == SAC_FLOAT_USER0)
    {
        *var = header.user0;
    }
    else if (type == SAC_FLOAT_USER1)
    {
        *var = header.user1;
    }
    else if (type == SAC_FLOAT_USER2)
    {
        *var = header.user2;
    }
    else if (type == SAC_FLOAT_USER3)
    {
        *var = header.user3;
    }
    else if (type == SAC_FLOAT_USER4)
    {
        *var = header.user4;
    }
    else if (type == SAC_FLOAT_USER5)
    {
        *var = header.user5;
    }
    else if (type == SAC_FLOAT_USER6)
    {
        *var = header.user6;
    }
    else if (type == SAC_FLOAT_USER7)
    {
        *var = header.user7;
    }
    else if (type == SAC_FLOAT_USER8)
    {
        *var = header.user8;
    }
    else if (type == SAC_FLOAT_USER9)
    {
        *var = header.user9;
    }
    else if (type == SAC_FLOAT_DIST)
    {
        *var = header.dist;
    }
    else if (type == SAC_FLOAT_AZ)
    {
        *var = header.az;
    }
    else if (type == SAC_FLOAT_BAZ)
    {
        *var = header.baz;
    }
    else if (type == SAC_FLOAT_GCARC)
    {
        *var = header.gcarc;
    }
    else if (type == SAC_FLOAT_INTERNAL2)
    {
        *var = header.internal2;
    }
    else if (type == SAC_FLOAT_DEPMEN)
    {
        *var = header.depmen;
    }
    else if (type == SAC_FLOAT_CMPAZ)
    {
        *var = header.cmpaz;
    }
    else if (type == SAC_FLOAT_CMPINC)
    {
        *var = header.cmpinc;
    }
    else if (type == SAC_FLOAT_XMINIMUM)
    {
        *var = header.xminimum;
    }
    else if (type == SAC_FLOAT_XMAXIMUM)
    {
        *var = header.xmaximum;
    }
    else if (type == SAC_FLOAT_YMINIMUM)
    {
        *var = header.yminimum;
    }
    else if (type == SAC_FLOAT_YMAXIMUM)
    {
        *var = header.ymaximum;
    }
    else if (type == SAC_FLOAT_UNUSED0)
    {
        *var = header.unused0;
    }
    else if (type == SAC_FLOAT_UNUSED1)
    {
        *var = header.unused1;
    }
    else if (type == SAC_FLOAT_UNUSED2)
    {
        *var = header.unused2;
    }
    else if (type == SAC_FLOAT_UNUSED3)
    {
        *var = header.unused3;
    }
    else if (type == SAC_FLOAT_UNUSED4)
    {
        *var = header.unused4;
    }
    else if (type == SAC_FLOAT_UNUSED5)
    {
        *var = header.unused5;
    }
    else if (type == SAC_FLOAT_UNUSED6)
    {
        *var = header.unused6;
    }
    else
    {
        printf("%s: Invalid header type\n", __func__);
        return -1;
    }
    // check if it wasn't defined
    if (fabs(*var + 12345) < 1.e-10){return -1;}
    return 0;
}
//============================================================================//
/*!
 * @brief Gets an integer value from the SAC header.
 *
 * @param[in] type       SAC header variable to get.
 * @param[in] header     Header from which to extract desired variable.
 *
 * @param[out] ivar      Value of desired header variable.
 *
 * @retval  0 indicates success.
 * @retval -1 indicates the variable was not defined.
 * @retval  1 indicates an invalid enumerated type.
 *
 * @ingroup sac_header_utils
 *
 * @author Ben Baker
 *
 */
int sacio_getIntegerHeader(const enum sacHeader_enum type,
                           const struct sacHeader_struct header,
                           int *ivar)
{
    *ivar = 0;
    if (type == SAC_INT_NZYEAR)
    {
        *ivar = header.nzyear;
    }
    else if (type == SAC_INT_NZJDAY)
    {
        *ivar = header.nzjday;
    }
    else if (type == SAC_INT_NZHOUR)
    {
        *ivar = header.nzhour;
    }
    else if (type == SAC_INT_NZMIN)
    {
        *ivar = header.nzmin;
    }
    else if (type == SAC_INT_NZSEC)
    {
        *ivar = header.nzsec;
    }
    else if (type == SAC_INT_NZMSEC)
    {
        *ivar = header.nzmsec;
    }
    else if (type == SAC_INT_NVHDR)
    {
        *ivar = header.nvhdr;
    }
    else if (type == SAC_INT_NORID)
    {
        *ivar = header.norid;
    }
    else if (type == SAC_INT_NEVID)
    {
        *ivar = header.nevid;
    }
    else if (type == SAC_INT_NPTS)
    {
        *ivar = header.npts;
    }
    else if (type == SAC_INT_INTERNAL1)
    {
        *ivar = header.ninternal1;
    }
    else if (type == SAC_INT_NWFID)
    {
        *ivar = header.nwfid;
    }
    else if (type == SAC_INT_NXSIZE)
    {
        *ivar = header.nxsize;
    }
    else if (type == SAC_INT_NYSIZE)
    {
        *ivar = header.nysize;
    }
    else if (type == SAC_INT_UNUSED0)
    {
        *ivar = header.nunused0;
    }
    else if (type == SAC_INT_IFTYPE)
    {
        *ivar = header.iftype;
    }
    else if (type == SAC_INT_IDEP)
    {
        *ivar = header.idep;
    }
    else if (type == SAC_INT_IZTYPE)
    {
        *ivar = header.iztype;
    }
    else if (type == SAC_INT_UNUSED1)
    {
        *ivar = header.nunused1;
    }
    else if (type == SAC_INT_IINST)
    {
        *ivar = header.iinst;
    }
    else if (type == SAC_INT_ISTREG)
    {
        *ivar = header.istreg;
    }
    else if (type == SAC_INT_IEVREG)
    {
        *ivar = header.ievreg;
    }
    else if (type == SAC_INT_IEVTYP)
    {
        *ivar = header.ievtyp;
    }
    else if (type == SAC_INT_IQUAL)
    {
        *ivar = header.iqual;
    }
    else if (type == SAC_INT_ISYNTH)
    {
        *ivar = header.isynth;
    }
    else if (type == SAC_INT_IMAGTYP)
    {
        *ivar = header.imagtyp;
    }
    else if (type == SAC_INT_IMAGSRC)
    {
        *ivar = header.imagsrc;
    }
    else if (type == SAC_INT_UNUSED2)
    {
        *ivar = header.nunused2;
    }
    else if (type == SAC_INT_UNUSED3)
    {
        *ivar = header.nunused3;
    }
    else if (type == SAC_INT_UNUSED4)
    {
        *ivar = header.nunused4;
    }
    else if (type == SAC_INT_UNUSED5)
    {
        *ivar = header.nunused5;
    }
    else if (type == SAC_INT_UNUSED6)
    {
        *ivar = header.nunused6;
    }
    else if (type == SAC_INT_UNUSED7)
    {
        *ivar = header.nunused7;
    }
    else if (type == SAC_INT_UNUSED8)
    {
        *ivar = header.nunused8;
    }
    else if (type == SAC_INT_UNUSED9)
    {
        *ivar = header.nunused9;
    }
    else 
    {
        printf("%s: Invalid header type\n", __func__);
        return -1;
    }
    // check if it wasn't defined
    if (*ivar ==-12345){return -1;} 
    return 0;
}
//============================================================================//
/*!
 * @brief Gets a logical value from the SAC header.
 *
 * @param[in] type       SAC header variable to get.
 * @param[in] header     Header from which to extract desired variable.
 *
 * @param[out] lvar      Value of desired header variable.
 *
 * @retval  0 indicates success.
 * @retval -1 indicates the variable was not defined.
 * @retval  1 indicates an invalid enumerated type.
 *
 * @ingroup sac_header_utils
 *
 * @author Ben Baker
 *
 */
int sacio_getBooleanHeader(const enum sacHeader_enum type,
                           const struct sacHeader_struct header,
                           bool *lvar)
{
    int ivar;
    *lvar = false;
    ivar = 0;
    if (type == SAC_BOOL_LEVEN)
    {
        ivar = header.leven;
    }
    else if (type == SAC_BOOL_LPSPOL)
    {
        ivar = header.lpspol;
    }
    else if (type == SAC_BOOL_LOVROK)
    {
        ivar = header.lovrok;
    }
    else if (type == SAC_BOOL_LCALDA)
    {
        ivar = header.lcalda;
    }
    else if (type == SAC_BOOL_LUNUSED)
    {
        ivar = header.lunused;
    }
    else
    {
        printf("%s: Invalid header type\n", __func__);
        return 1;
    }
    // check if it wasn't defined
    if (ivar ==-12345)
    {
        *lvar = false;
        return -1;
    }
    *lvar = (bool) ivar;
    return 0;
}
//============================================================================//
/*!
 * @brief Gets a character variable from the SAC header.
 *
 * @param[in] type     SAC header variable name to set.
 * @param[in] header   Header from which to get character variable.
 *
 * @param[out] kvar    Character variable corresponding to type.
 *                     This should be length of 8 unless setting KEVNM
 *                     in which case this should be length 16.
 *
 * @retval  0 indicates success.
 * @retval -1 indicates the variable was not defined.
 * @retval  1 indicates an invalid enumerated type.
 *
 * @ingroup sac_header_utils
 *
 * @author Ben Baker
 *
 */
int sacio_getCharacterHeader(const enum sacHeader_enum type,
                             const struct sacHeader_struct header,
                             char *kvar)
{
    memset(kvar, 0, 8*sizeof(char));
    if (type == SAC_CHAR_KSTNM)
    {
        readChar8(header.kstnm, kvar);
    }
    else if (type == SAC_CHAR_KEVNM)
    {
        readChar16(header.kevnm, kvar);
    }
    else if (type == SAC_CHAR_KHOLE)
    {
        readChar8(header.khole, kvar);
    }
    else if (type == SAC_CHAR_KO)
    {
        readChar8(header.ko, kvar);
    }
    else if (type == SAC_CHAR_KA)
    {
        readChar8(header.ka, kvar);
    }
    else if (type == SAC_CHAR_KT0)
    {
        readChar8(header.kt0, kvar);
    }
    else if (type == SAC_CHAR_KT1)
    {
        readChar8(header.kt1, kvar);
    }
    else if (type == SAC_CHAR_KT2)
    {
        readChar8(header.kt2, kvar);
    }
    else if (type == SAC_CHAR_KT3)
    {
        readChar8(header.kt3, kvar);
    }
    else if (type == SAC_CHAR_KT4)
    {
        readChar8(header.kt4, kvar);
    }
    else if (type == SAC_CHAR_KT5)
    {
        readChar8(header.kt5, kvar);
    }
    else if (type == SAC_CHAR_KT6)
    {
        readChar8(header.kt6, kvar);
    }
    else if (type == SAC_CHAR_KT7)
    {
        readChar8(header.kt7, kvar);
    }
    else if (type == SAC_CHAR_KT8)
    {
        readChar8(header.kt8, kvar);
    }
    else if (type == SAC_CHAR_KT9)
    {
        readChar8(header.kt9, kvar);
    }
    else if (type == SAC_CHAR_KF)
    {
        readChar8(header.kf, kvar);
    }
    else if (type == SAC_CHAR_KUSER0)
    {
        readChar8(header.kuser0, kvar);
    }
    else if (type == SAC_CHAR_KUSER1)
    {
        readChar8(header.kuser1, kvar);
    }
    else if (type == SAC_CHAR_KUSER2)
    {
        readChar8(header.kuser2, kvar);
    }
    else if (type == SAC_CHAR_KCMPNM)
    {
        readChar8(header.kcmpnm, kvar);
    }
    else if (type == SAC_CHAR_KNETWK)
    {
        readChar8(header.knetwk, kvar);
    }
    else if (type == SAC_CHAR_KDATRD)
    {
        readChar8(header.kdatrd, kvar);
    }
    else if (type == SAC_CHAR_KINST)
    {
        readChar8(header.kinst, kvar);
    }
    else
    {
        printf("%s: Invalid header type\n", __func__);
        return 1;
    }
    if (strncasecmp(kvar, "-12345\0", 6) == 0){return -1;}
    return 0;
}
/*!
 * @brief Determines if two headers, h1 and h2, are equal.
 * @param[in] h1   First header in comparision.
 * @param[in] h2   Second header in comparison.
 * @param[in] tol  The tolerance to which the difference of the
 *                 double precision header variables must fall in
 *                 to be considered equal.  If negative, then 
 *                 machine epsilon will be used.
 * @retval True indicates that the headers are equal.
 * @retval False indicates that the headers are different. 
 */
bool sacio_isHeaderEqual(const struct sacHeader_struct h1,
                         const struct sacHeader_struct h2,
                         const double tolIn)
{
    double tol = DBL_EPSILON;
    if (tolIn > 0){tol = tolIn;}
    // double
    if (fabs(h1.delta - h2.delta) > tol){return false;}
    if (fabs(h1.depmin - h2.depmin) > tol){return false;}
    if (fabs(h1.depmax - h2.depmax) > tol){return false;}
    if (fabs(h1.scale  - h2.scale) > tol){return false;}
    if (fabs(h1.odelta - h2.odelta) > tol){return false;}
    if (fabs(h1.b - h2.b) > tol){return false;}
    if (fabs(h1.e - h2.e) > tol){return false;}
    if (fabs(h1.o - h2.o) > tol){return false;}
    if (fabs(h1.a - h2.a) > tol){return false;}
    if (fabs(h1.internal1 - h2.internal1) > tol){return false;}
    if (fabs(h1.t0 - h2.t0) > tol){return false;}
    if (fabs(h1.t1 - h2.t1) > tol){return false;}
    if (fabs(h1.t2 - h2.t2) > tol){return false;}
    if (fabs(h1.t3 - h2.t3) > tol){return false;}
    if (fabs(h1.t4 - h2.t4) > tol){return false;}
    if (fabs(h1.t5 - h2.t5) > tol){return false;}
    if (fabs(h1.t6 - h2.t6) > tol){return false;}
    if (fabs(h1.t7 - h2.t7) > tol){return false;}
    if (fabs(h1.t8 - h2.t8) > tol){return false;}
    if (fabs(h1.t9 - h2.t9) > tol){return false;}
    if (fabs(h1.f - h2.f) > tol){return false;}
    if (fabs(h1.resp0 - h2.resp0) > tol){return false;}
    if (fabs(h1.resp1 - h2.resp1) > tol){return false;}
    if (fabs(h1.resp2 - h2.resp2) > tol){return false;}
    if (fabs(h1.resp3 - h2.resp3) > tol){return false;}
    if (fabs(h1.resp4 - h2.resp4) > tol){return false;}
    if (fabs(h1.resp5 - h2.resp5) > tol){return false;} 
    if (fabs(h1.resp6 - h2.resp6) > tol){return false;}
    if (fabs(h1.resp7 - h2.resp7) > tol){return false;}
    if (fabs(h1.resp8 - h2.resp8) > tol){return false;}
    if (fabs(h1.resp9 - h2.resp9) > tol){return false;}
    if (fabs(h1.stla - h2.stla) > tol){return false;}
    if (fabs(h1.stlo - h2.stlo) > tol){return false;}
    if (fabs(h1.stel - h2.stel) > tol){return false;}
    if (fabs(h1.stdp - h2.stdp) > tol){return false;}
    if (fabs(h1.evla - h2.evla) > tol){return false;}
    if (fabs(h1.evlo - h2.evlo) > tol){return false;}
    if (fabs(h1.evel - h2.evel) > tol){return false;}
    if (fabs(h1.evdp - h2.evdp) > tol){return false;}
    if (fabs(h1.mag - h2.mag) > tol){return false;}
    if (fabs(h1.user0 - h2.user0) > tol){return false;}
    if (fabs(h1.user1 - h2.user1) > tol){return false;}
    if (fabs(h1.user2 - h2.user2) > tol){return false;}
    if (fabs(h1.user3 - h2.user3) > tol){return false;}
    if (fabs(h1.user4 - h2.user4) > tol){return false;}
    if (fabs(h1.user5 - h2.user5) > tol){return false;} 
    if (fabs(h1.user6 - h2.user6) > tol){return false;}
    if (fabs(h1.user7 - h2.user7) > tol){return false;}
    if (fabs(h1.user8 - h2.user8) > tol){return false;}
    if (fabs(h1.user9 - h2.user9) > tol){return false;}
    if (fabs(h1.dist - h2.dist) > tol){return false;}
    if (fabs(h1.az - h2.az) > tol){return false;}
    if (fabs(h1.gcarc - h2.gcarc) > tol){return false;}
    if (fabs(h1.internal2 - h2.internal2) > tol){return false;}
    if (fabs(h1.depmen - h2.depmen) > tol){return false;}
    if (fabs(h1.cmpaz - h2.cmpaz) > tol){return false;}
    if (fabs(h1.cmpinc - h2.cmpinc) > tol){return false;}
    if (fabs(h1.xminimum - h2.xminimum) > tol){return false;}
    if (fabs(h1.xmaximum - h2.xmaximum) > tol){return false;}
    if (fabs(h1.yminimum - h2.yminimum) > tol){return false;}
    if (fabs(h1.ymaximum - h2.ymaximum) > tol){return false;}
    if (fabs(h1.unused0 - h2.unused0) > tol){return false;}
    if (fabs(h1.unused1 - h2.unused1) > tol){return false;}
    if (fabs(h1.unused2 - h2.unused2) > tol){return false;}
    if (fabs(h1.unused3 - h2.unused3) > tol){return false;}
    if (fabs(h1.unused4 - h2.unused4) > tol){return false;}
    if (fabs(h1.unused5 - h2.unused5) > tol){return false;} 
    if (fabs(h1.unused6 - h2.unused6) > tol){return false;}
    // int
    if (h1.nzyear != h2.nzyear){return false;}
    if (h1.nzjday != h2.nzjday){return false;}
    if (h1.nzhour != h2.nzhour){return false;}
    if (h1.nzmin  != h2.nzmin){return false;}
    if (h1.nzsec  != h2.nzsec){return false;}
    if (h1.nzmsec != h2.nzmsec){return false;}
    if (h1.nvhdr  != h2.nvhdr){return false;}
    if (h1.norid  != h2.norid){return false;}
    if (h1.nevid  != h2.nevid){return false;}
    if (h1.npts   != h2.npts){return false;}
    if (h1.ninternal1 != h2.ninternal1){return false;} 
    if (h1.nwfid != h2.nwfid){return false;}
    if (h1.nxsize != h2.nxsize){return false;}
    if (h1.nunused0 != h2.nunused0){return false;}
    if (h1.iftype != h2.iftype){return false;}
    if (h1.idep != h2.idep){return false;}
    if (h1.iztype != h2.iztype){return false;}
    if (h1.nunused1 != h2.nunused1){return false;}
    if (h1.iinst != h2.iinst){return false;}
    if (h1.istreg != h2.istreg){return false;}
    if (h1.ievreg != h2.ievreg){return false;}
    if (h1.ievtyp != h2.ievtyp){return false;}
    if (h1.iqual != h2.iqual){return false;}
    if (h1.isynth != h2.isynth){return false;}
    if (h1.imagtyp != h2.imagtyp){return false;}
    if (h1.imagsrc != h2.imagsrc){return false;}
    if (h1.nunused2 != h2.nunused2){return false;}
    if (h1.nunused3 != h2.nunused3){return false;}
    if (h1.nunused4 != h2.nunused4){return false;}
    if (h1.nunused5 != h2.nunused5){return false;}
    if (h1.nunused6 != h2.nunused6){return false;}
    if (h1.nunused7 != h2.nunused7){return false;}
    if (h1.nunused8 != h2.nunused8){return false;}
    if (h1.nunused9 != h2.nunused9){return false;}
    // bool
    if (h1.leven != h2.leven){return false;}
    if (h1.lpspol != h2.lpspol){return false;}
    if (h1.lovrok != h2.lovrok){return false;}
    if (h1.lcalda != h2.lcalda){return false;}
    if (h1.lunused != h2.lunused){return false;}
    // character
    if (strncasecmp(h1.kstnm, h2.kstnm, 8) != 0){return false;}
    if (strncasecmp(h1.kevnm, h2.kevnm, 16) != 0){return false;}
    if (strncasecmp(h1.khole, h2.khole, 8) != 0){return false;}
    if (strncasecmp(h1.ko, h2.ko, 8) != 0){return false;} 
    if (strncasecmp(h1.ka, h2.ka, 8) != 0){return false;}
    if (strncasecmp(h1.kt0, h2.kt0, 8) != 0){return false;}
    if (strncasecmp(h1.kt1, h2.kt1, 8) != 0){return false;}
    if (strncasecmp(h1.kt2, h2.kt2, 8) != 0){return false;}
    if (strncasecmp(h1.kt3, h2.kt3, 8) != 0){return false;}
    if (strncasecmp(h1.kt4, h2.kt4, 8) != 0){return false;}
    if (strncasecmp(h1.kt5, h2.kt5, 8) != 0){return false;}
    if (strncasecmp(h1.kt6, h2.kt6, 8) != 0){return false;}
    if (strncasecmp(h1.kt7, h2.kt7, 8) != 0){return false;}
    if (strncasecmp(h1.kt8, h2.kt8, 8) != 0){return false;}
    if (strncasecmp(h1.kt9, h2.kt9, 8) != 0){return false;}
    if (strncasecmp(h1.kf, h2.kf, 8) != 0){return false;}
    if (strncasecmp(h1.kuser0, h2.kuser0, 8) != 0){return false;}
    if (strncasecmp(h1.kuser1, h2.kuser1, 8) != 0){return false;} 
    if (strncasecmp(h1.kuser2, h2.kuser2, 8) != 0){return false;}
    if (strncasecmp(h1.kcmpnm, h2.kcmpnm, 8) != 0){return false;}
    if (strncasecmp(h1.knetwk, h2.knetwk, 8) != 0){return false;}
    if (strncasecmp(h1.kdatrd, h2.kdatrd, 8) != 0){return false;}
    if (strncasecmp(h1.kinst,  h2.kinst, 8) != 0){return false;}
    if (h1.lhaveHeader != h2.lhaveHeader){return false;}
    return true;
}
//============================================================================//
/*!
 * @brief Unpacks a SAC header.
 *
 * @param[in] lswap      If true then byte swap the SAC header values.
 * @param[in] cheader    SAC header read from disk.
 *
 * @param[out] header    SAC header values.
 *
 * @ingroup sac_header_utils
 *
 * @author Ben Baker
 *
 */
void sacio_unpackHeader(const bool lswap, const char *__restrict__ cheader,
                        struct sacHeader_struct *header)
{
    memset(header, 0, sizeof(struct sacHeader_struct));
    // floats
    readFloatHeader(lswap, &cheader[0],   &header->delta); 
    readFloatHeader(lswap, &cheader[4],   &header->depmin);
    readFloatHeader(lswap, &cheader[8],   &header->depmax);
    readFloatHeader(lswap, &cheader[12],  &header->scale);
    readFloatHeader(lswap, &cheader[16],  &header->odelta);
    readFloatHeader(lswap, &cheader[20],  &header->b);
    readFloatHeader(lswap, &cheader[24],  &header->e);
    readFloatHeader(lswap, &cheader[28],  &header->o);
    readFloatHeader(lswap, &cheader[32],  &header->a);
    readFloatHeader(lswap, &cheader[36],  &header->internal1);
    readFloatHeader(lswap, &cheader[40],  &header->t0);
    readFloatHeader(lswap, &cheader[44],  &header->t1);
    readFloatHeader(lswap, &cheader[48],  &header->t2);
    readFloatHeader(lswap, &cheader[52],  &header->t3);
    readFloatHeader(lswap, &cheader[56],  &header->t4);
    readFloatHeader(lswap, &cheader[60],  &header->t5);
    readFloatHeader(lswap, &cheader[64],  &header->t6);
    readFloatHeader(lswap, &cheader[68],  &header->t7);
    readFloatHeader(lswap, &cheader[72],  &header->t8);
    readFloatHeader(lswap, &cheader[76],  &header->t9);
    readFloatHeader(lswap, &cheader[80],  &header->f);
    readFloatHeader(lswap, &cheader[84],  &header->resp0);
    readFloatHeader(lswap, &cheader[88],  &header->resp1);
    readFloatHeader(lswap, &cheader[92],  &header->resp2);
    readFloatHeader(lswap, &cheader[96],  &header->resp3);
    readFloatHeader(lswap, &cheader[100], &header->resp4);
    readFloatHeader(lswap, &cheader[104], &header->resp5);
    readFloatHeader(lswap, &cheader[108], &header->resp6);
    readFloatHeader(lswap, &cheader[112], &header->resp7);
    readFloatHeader(lswap, &cheader[116], &header->resp8);
    readFloatHeader(lswap, &cheader[120], &header->resp9);
    readFloatHeader(lswap, &cheader[124], &header->stla);
    readFloatHeader(lswap, &cheader[128], &header->stlo);
    readFloatHeader(lswap, &cheader[132], &header->stel);
    readFloatHeader(lswap, &cheader[136], &header->stdp);
    readFloatHeader(lswap, &cheader[140], &header->evla);
    readFloatHeader(lswap, &cheader[144], &header->evlo);
    readFloatHeader(lswap, &cheader[148], &header->evel);
    readFloatHeader(lswap, &cheader[152], &header->evdp);
    readFloatHeader(lswap, &cheader[156], &header->mag);
    readFloatHeader(lswap, &cheader[160], &header->user0);
    readFloatHeader(lswap, &cheader[164], &header->user1);
    readFloatHeader(lswap, &cheader[168], &header->user2);
    readFloatHeader(lswap, &cheader[172], &header->user3);
    readFloatHeader(lswap, &cheader[176], &header->user4);
    readFloatHeader(lswap, &cheader[180], &header->user5);
    readFloatHeader(lswap, &cheader[184], &header->user6);
    readFloatHeader(lswap, &cheader[188], &header->user7);
    readFloatHeader(lswap, &cheader[192], &header->user8);
    readFloatHeader(lswap, &cheader[196], &header->user9);
    readFloatHeader(lswap, &cheader[200], &header->dist);
    readFloatHeader(lswap, &cheader[204], &header->az);
    readFloatHeader(lswap, &cheader[208], &header->baz);
    readFloatHeader(lswap, &cheader[212], &header->gcarc);
    readFloatHeader(lswap, &cheader[216], &header->internal1);
    readFloatHeader(lswap, &cheader[220], &header->internal2);
    readFloatHeader(lswap, &cheader[224], &header->depmen);
    readFloatHeader(lswap, &cheader[228], &header->cmpaz);
    readFloatHeader(lswap, &cheader[232], &header->cmpinc);
    readFloatHeader(lswap, &cheader[236], &header->xminimum);
    readFloatHeader(lswap, &cheader[240], &header->xmaximum);
    readFloatHeader(lswap, &cheader[244], &header->yminimum);
    readFloatHeader(lswap, &cheader[248], &header->ymaximum);
    readFloatHeader(lswap, &cheader[252], &header->unused0);
    readFloatHeader(lswap, &cheader[256], &header->unused1);
    readFloatHeader(lswap, &cheader[260], &header->unused2);
    readFloatHeader(lswap, &cheader[264], &header->unused3);
    readFloatHeader(lswap, &cheader[268], &header->unused4);
    readFloatHeader(lswap, &cheader[272], &header->unused5);
    readFloatHeader(lswap, &cheader[276], &header->unused6);
    // ints
    readIntHeader(lswap, &cheader[280], &header->nzyear);
    readIntHeader(lswap, &cheader[284], &header->nzjday);
    readIntHeader(lswap, &cheader[288], &header->nzhour);
    readIntHeader(lswap, &cheader[292], &header->nzmin);
    readIntHeader(lswap, &cheader[296], &header->nzsec);
    readIntHeader(lswap, &cheader[300], &header->nzmsec);
    readIntHeader(lswap, &cheader[304], &header->nvhdr);
    readIntHeader(lswap, &cheader[308], &header->norid);
    readIntHeader(lswap, &cheader[312], &header->nevid);
    readIntHeader(lswap, &cheader[316], &header->npts);
    readIntHeader(lswap, &cheader[320], &header->ninternal1);
    readIntHeader(lswap, &cheader[324], &header->nwfid);
    readIntHeader(lswap, &cheader[328], &header->nxsize);
    readIntHeader(lswap, &cheader[332], &header->nysize);
    readIntHeader(lswap, &cheader[336], &header->nunused0);
    readIntHeader(lswap, &cheader[340], &header->iftype);
    readIntHeader(lswap, &cheader[344], &header->idep);
    readIntHeader(lswap, &cheader[348], &header->iztype);
    readIntHeader(lswap, &cheader[352], &header->nunused1);
    readIntHeader(lswap, &cheader[356], &header->iinst);
    readIntHeader(lswap, &cheader[360], &header->istreg);
    readIntHeader(lswap, &cheader[364], &header->ievreg);
    readIntHeader(lswap, &cheader[368], &header->ievtyp);
    readIntHeader(lswap, &cheader[372], &header->iqual);
    readIntHeader(lswap, &cheader[376], &header->isynth);
    readIntHeader(lswap, &cheader[380], &header->imagtyp);
    readIntHeader(lswap, &cheader[384], &header->imagsrc);
    readIntHeader(lswap, &cheader[388], &header->nunused2);
    readIntHeader(lswap, &cheader[392], &header->nunused3);
    readIntHeader(lswap, &cheader[396], &header->nunused4); 
    readIntHeader(lswap, &cheader[400], &header->nunused5);
    readIntHeader(lswap, &cheader[404], &header->nunused6);
    readIntHeader(lswap, &cheader[408], &header->nunused7);
    readIntHeader(lswap, &cheader[412], &header->nunused8);
    readIntHeader(lswap, &cheader[416], &header->nunused9);
    // bools
    readBoolHeader(lswap, &cheader[420], &header->leven);
    readBoolHeader(lswap, &cheader[424], &header->lpspol);
    readBoolHeader(lswap, &cheader[428], &header->lovrok);
    readBoolHeader(lswap, &cheader[432], &header->lcalda);
    readBoolHeader(lswap, &cheader[436], &header->lunused);
    // chars
    readChar8( &cheader[440], header->kstnm);
    readChar16(&cheader[448], header->kevnm);
    readChar8( &cheader[464], header->khole);
    readChar8( &cheader[472], header->ko);
    readChar8( &cheader[480], header->ka);
    readChar8( &cheader[488], header->kt0);
    readChar8( &cheader[496], header->kt1);
    readChar8( &cheader[504], header->kt2);
    readChar8( &cheader[512], header->kt3);
    readChar8( &cheader[520], header->kt4);
    readChar8( &cheader[528], header->kt5);
    readChar8( &cheader[536], header->kt6);
    readChar8( &cheader[544], header->kt7);
    readChar8( &cheader[552], header->kt8);
    readChar8( &cheader[560], header->kt9);
    readChar8( &cheader[568], header->kf);
    readChar8( &cheader[576], header->kuser0);
    readChar8( &cheader[584], header->kuser1);
    readChar8( &cheader[592], header->kuser2);
    readChar8( &cheader[600], header->kcmpnm); 
    readChar8( &cheader[608], header->knetwk);
    readChar8( &cheader[616], header->kdatrd);
    readChar8( &cheader[624], header->kinst);
    // i have the header
    header->lhaveHeader = true;
    return;
}
//============================================================================//
/*!
 * @brief Packs SAC header defined in struct.
 *
 * @param[in] lswap     If true then byte swap the output data.
 * @param[in] header    SAC header.
 *
 * @param[out] cheader  Packed header to write to disk.
 *
 * @ingroup sac_header_utils
 *
 * @author Ben Baker
 *
 */
void sacio_packHeader(const bool lswap,
                      const struct sacHeader_struct header,
                      char *__restrict__ cheader)
{
    // floats
    memset(cheader, 0, 632*sizeof(char));
    // floats
    writeFloatHeader(lswap, header.delta,     &cheader[0]);
    writeFloatHeader(lswap, header.depmin,    &cheader[4]);
    writeFloatHeader(lswap, header.depmax,    &cheader[8]);
    writeFloatHeader(lswap, header.scale,     &cheader[12]);
    writeFloatHeader(lswap, header.odelta,    &cheader[16]);
    writeFloatHeader(lswap, header.b,         &cheader[20]);
    writeFloatHeader(lswap, header.e,         &cheader[24]);
    writeFloatHeader(lswap, header.o,         &cheader[28]);
    writeFloatHeader(lswap, header.a,         &cheader[32]);
    writeFloatHeader(lswap, header.internal1, &cheader[36]);
    writeFloatHeader(lswap, header.t0,        &cheader[40]);
    writeFloatHeader(lswap, header.t1,        &cheader[44]);
    writeFloatHeader(lswap, header.t2,        &cheader[48]);
    writeFloatHeader(lswap, header.t3,        &cheader[52]);
    writeFloatHeader(lswap, header.t4,        &cheader[56]);
    writeFloatHeader(lswap, header.t5,        &cheader[60]);
    writeFloatHeader(lswap, header.t6,        &cheader[64]);
    writeFloatHeader(lswap, header.t7,        &cheader[68]);
    writeFloatHeader(lswap, header.t8,        &cheader[72]);
    writeFloatHeader(lswap, header.t9,        &cheader[76]);
    writeFloatHeader(lswap, header.f,         &cheader[80]);
    writeFloatHeader(lswap, header.resp0,     &cheader[84]);
    writeFloatHeader(lswap, header.resp1,     &cheader[88]);
    writeFloatHeader(lswap, header.resp2,     &cheader[92]);
    writeFloatHeader(lswap, header.resp3,     &cheader[96]);
    writeFloatHeader(lswap, header.resp4,     &cheader[100]);
    writeFloatHeader(lswap, header.resp5,     &cheader[104]);
    writeFloatHeader(lswap, header.resp6,     &cheader[108]);
    writeFloatHeader(lswap, header.resp7,     &cheader[112]);
    writeFloatHeader(lswap, header.resp8,     &cheader[116]);
    writeFloatHeader(lswap, header.resp9,     &cheader[120]);
    writeFloatHeader(lswap, header.stla,      &cheader[124]);
    writeFloatHeader(lswap, header.stlo,      &cheader[128]);
    writeFloatHeader(lswap, header.stel,      &cheader[132]);
    writeFloatHeader(lswap, header.stdp,      &cheader[136]);
    writeFloatHeader(lswap, header.evla,      &cheader[140]);
    writeFloatHeader(lswap, header.evlo,      &cheader[144]);
    writeFloatHeader(lswap, header.evel,      &cheader[148]);
    writeFloatHeader(lswap, header.evdp,      &cheader[152]);
    writeFloatHeader(lswap, header.mag,       &cheader[156]);
    writeFloatHeader(lswap, header.user0,     &cheader[160]);
    writeFloatHeader(lswap, header.user1,     &cheader[164]);
    writeFloatHeader(lswap, header.user2,     &cheader[168]);
    writeFloatHeader(lswap, header.user3,     &cheader[172]);
    writeFloatHeader(lswap, header.user4,     &cheader[176]);
    writeFloatHeader(lswap, header.user5,     &cheader[180]);
    writeFloatHeader(lswap, header.user6,     &cheader[184]);
    writeFloatHeader(lswap, header.user7,     &cheader[188]);
    writeFloatHeader(lswap, header.user8,     &cheader[192]);
    writeFloatHeader(lswap, header.user9,     &cheader[196]);
    writeFloatHeader(lswap, header.dist,      &cheader[200]);
    writeFloatHeader(lswap, header.az,        &cheader[204]);
    writeFloatHeader(lswap, header.baz,       &cheader[208]);
    writeFloatHeader(lswap, header.gcarc,     &cheader[212]);
    writeFloatHeader(lswap, header.internal1, &cheader[216]);
    writeFloatHeader(lswap, header.internal2, &cheader[220]);
    writeFloatHeader(lswap, header.depmen,    &cheader[224]);
    writeFloatHeader(lswap, header.cmpaz,     &cheader[228]);
    writeFloatHeader(lswap, header.cmpinc,    &cheader[232]);
    writeFloatHeader(lswap, header.xminimum,  &cheader[236]);
    writeFloatHeader(lswap, header.xmaximum,  &cheader[240]);
    writeFloatHeader(lswap, header.yminimum,  &cheader[244]);
    writeFloatHeader(lswap, header.ymaximum,  &cheader[248]);
    writeFloatHeader(lswap, header.unused0,   &cheader[252]);
    writeFloatHeader(lswap, header.unused1,   &cheader[256]);
    writeFloatHeader(lswap, header.unused2,   &cheader[260]);
    writeFloatHeader(lswap, header.unused3,   &cheader[264]);
    writeFloatHeader(lswap, header.unused4,   &cheader[268]);
    writeFloatHeader(lswap, header.unused5,   &cheader[272]);
    writeFloatHeader(lswap, header.unused6,   &cheader[276]);
    // ints
    writeIntHeader(lswap, header.nzyear,     &cheader[280]);
    writeIntHeader(lswap, header.nzjday,     &cheader[284]);
    writeIntHeader(lswap, header.nzhour,     &cheader[288]);
    writeIntHeader(lswap, header.nzmin,      &cheader[292]);
    writeIntHeader(lswap, header.nzsec,      &cheader[296]);
    writeIntHeader(lswap, header.nzmsec,     &cheader[300]);
    writeIntHeader(lswap, header.nvhdr,      &cheader[304]);
    writeIntHeader(lswap, header.norid,      &cheader[308]);
    writeIntHeader(lswap, header.nevid,      &cheader[312]);
    writeIntHeader(lswap, header.npts,       &cheader[316]);
    writeIntHeader(lswap, header.ninternal1, &cheader[320]);
    writeIntHeader(lswap, header.nwfid,      &cheader[324]);
    writeIntHeader(lswap, header.nxsize,     &cheader[328]);
    writeIntHeader(lswap, header.nysize,     &cheader[332]);
    writeIntHeader(lswap, header.nunused0,   &cheader[336]);
    writeIntHeader(lswap, header.iftype,     &cheader[340]);
    writeIntHeader(lswap, header.idep,       &cheader[344]);
    writeIntHeader(lswap, header.iztype,     &cheader[348]);
    writeIntHeader(lswap, header.nunused1,   &cheader[352]);
    writeIntHeader(lswap, header.iinst,      &cheader[356]);
    writeIntHeader(lswap, header.istreg,     &cheader[360]);
    writeIntHeader(lswap, header.ievreg,     &cheader[364]);
    writeIntHeader(lswap, header.ievtyp,     &cheader[368]);
    writeIntHeader(lswap, header.iqual,      &cheader[372]);
    writeIntHeader(lswap, header.isynth,     &cheader[376]);
    writeIntHeader(lswap, header.imagtyp,    &cheader[380]);
    writeIntHeader(lswap, header.imagsrc,    &cheader[384]);
    writeIntHeader(lswap, header.nunused2,   &cheader[388]);
    writeIntHeader(lswap, header.nunused3,   &cheader[392]);
    writeIntHeader(lswap, header.nunused4,   &cheader[396]);
    writeIntHeader(lswap, header.nunused5,   &cheader[400]);
    writeIntHeader(lswap, header.nunused6,   &cheader[404]);
    writeIntHeader(lswap, header.nunused7,   &cheader[408]);
    writeIntHeader(lswap, header.nunused8,   &cheader[412]);
    writeIntHeader(lswap, header.nunused9,   &cheader[416]);
    // bools
    writeBoolHeader(lswap, header.leven,    &cheader[420]);
    writeBoolHeader(lswap, header.lpspol,   &cheader[424]);
    writeBoolHeader(lswap, header.lovrok,   &cheader[428]);
    writeBoolHeader(lswap, header.lcalda,   &cheader[432]);
    writeBoolHeader(lswap, header.lunused,  &cheader[436]);
    // chars
    writeChar8( header.kstnm, &cheader[440]);
    writeChar16(header.kevnm, &cheader[448]);
    writeChar8( header.khole, &cheader[464]);
    writeChar8( header.ko,    &cheader[472]);
    writeChar8( header.ka,    &cheader[480]);
    writeChar8( header.kt0,   &cheader[488]);
    writeChar8( header.kt1,   &cheader[496]);
    writeChar8( header.kt2,   &cheader[504]);
    writeChar8( header.kt3,   &cheader[512]);
    writeChar8( header.kt4,   &cheader[520]);
    writeChar8( header.kt5,   &cheader[528]);
    writeChar8( header.kt6,   &cheader[536]);
    writeChar8( header.kt7,   &cheader[544]);
    writeChar8( header.kt8,   &cheader[552]);
    writeChar8( header.kt9,   &cheader[560]);
    writeChar8( header.kf,    &cheader[568]);
    writeChar8( header.kuser0, &cheader[576]);
    writeChar8( header.kuser1, &cheader[584]);
    writeChar8( header.kuser2, &cheader[592]);
    writeChar8( header.kcmpnm, &cheader[600]);
    writeChar8( header.knetwk, &cheader[608]);
    writeChar8( header.kdatrd, &cheader[616]);
    writeChar8( header.kinst,  &cheader[624]);
    return;
}
//============================================================================//
/*!
 * @brief Sets the default SAC header which is all SAC NaN's which is -12345.
 *
 * @param[out] header    Default SAC header.
 *
 * @ingroup sac_header_utils
 *
 * @author Ben Baker
 *
 */
void sacio_setDefaultHeader(struct sacHeader_struct *header)
{
    const double dnan =-12345.0;
    const int inan =-12345;
    const int lnan =-12345;
    const char *cnan = "-12345\0";
    memset(header, 0, sizeof(struct sacHeader_struct));
    // doubles
    header->delta = dnan;
    header->depmin = dnan;
    header->depmax = dnan;
    header->scale = dnan;
    header->odelta = dnan;
    header->b = dnan;
    header->e = dnan;
    header->o = dnan;
    header->a = dnan;
    header->internal1 = dnan;
    header->t0 = dnan;
    header->t1 = dnan;
    header->t2 = dnan;
    header->t3 = dnan;
    header->t4 = dnan;
    header->t5 = dnan;
    header->t6 = dnan;
    header->t7 = dnan;
    header->t8 = dnan;
    header->t9 = dnan;
    header->f = dnan;
    header->resp0 = dnan;
    header->resp1 = dnan;
    header->resp2 = dnan;
    header->resp3 = dnan;
    header->resp4 = dnan;
    header->resp5 = dnan;
    header->resp6 = dnan;
    header->resp7 = dnan;
    header->resp8 = dnan;
    header->resp9 = dnan;
    header->stla = dnan;
    header->stlo = dnan;
    header->stel = dnan;
    header->stdp = dnan;
    header->evla = dnan;
    header->evlo = dnan;
    header->evel = dnan;
    header->evdp = dnan;
    header->mag = dnan;
    header->user0 = dnan;
    header->user1 = dnan;
    header->user2 = dnan;
    header->user3 = dnan;
    header->user4 = dnan;
    header->user5 = dnan;
    header->user6 = dnan;
    header->user7 = dnan;
    header->user8 = dnan;
    header->user9 = dnan;
    header->dist = dnan;
    header->az = dnan;
    header->baz = dnan;
    header->gcarc = dnan;
    header->internal1 = dnan;
    header->internal2 = dnan;
    header->depmen = dnan;
    header->cmpaz = dnan;
    header->cmpinc = dnan;
    header->xminimum = dnan;
    header->xmaximum = dnan;
    header->yminimum = dnan;
    header->ymaximum = dnan;
    header->unused0 = dnan;
    header->unused1 = dnan;
    header->unused2 = dnan;
    header->unused3 = dnan;
    header->unused4 = dnan;
    header->unused5 = dnan;
    header->unused6 = dnan;
    // ints
    header->nzyear = inan;
    header->nzjday = inan;
    header->nzhour = inan;
    header->nzmin = inan;
    header->nzsec = inan;
    header->nzmsec = inan;
    header->nvhdr = inan;
    header->norid = inan;
    header->nevid = inan;
    header->npts = inan;
    header->ninternal1 = inan;
    header->nwfid = inan;
    header->nxsize = inan;
    header->nysize = inan;
    header->nunused0 = inan;
    header->iftype = inan;
    header->idep = inan;
    header->iztype = inan;
    header->nunused1 = inan;
    header->iinst = inan;
    header->istreg = inan;
    header->ievreg = inan;
    header->ievtyp = inan;
    header->iqual = inan;
    header->isynth = inan;
    header->imagtyp = inan;
    header->imagsrc = inan;
    header->nunused2 = inan;
    header->nunused3 = inan;
    header->nunused4 = inan;
    header->nunused5 = inan;
    header->nunused6 = inan;
    header->nunused7 = inan;
    header->nunused8 = inan;
    header->nunused9 = inan;
    // bools
    header->leven = lnan;
    header->lpspol = lnan;
    header->lovrok = lnan;
    header->lcalda = lnan;
    header->lunused = lnan;
    // characters
    strcpy(header->kstnm, cnan);
    strcpy(header->kevnm, cnan);
    strcpy(header->khole, cnan);
    strcpy(header->ko, cnan);
    strcpy(header->ka, cnan);
    strcpy(header->kt0, cnan);
    strcpy(header->kt1, cnan);
    strcpy(header->kt2, cnan);
    strcpy(header->kt3, cnan);
    strcpy(header->kt4, cnan);
    strcpy(header->kt5, cnan);
    strcpy(header->kt6, cnan);
    strcpy(header->kt7, cnan);
    strcpy(header->kt8, cnan);
    strcpy(header->kt9, cnan);
    strcpy(header->kf, cnan);
    strcpy(header->kuser0, cnan);
    strcpy(header->kuser1, cnan);
    strcpy(header->kuser2, cnan);
    strcpy(header->kcmpnm, cnan);
    strcpy(header->knetwk, cnan);
    strcpy(header->kdatrd, cnan);
    strcpy(header->kinst, cnan);
    return;
}
//============================================================================//
/*!
 * @brief Debugging utility to print the header to a file.
 * @param[in] file    File handle to which the header will be printed.
 *                    If NULL then the header is printed to standard out.
 * @param[in] header  Header with information to print.
 * @result 0 indicates success.
 * @ingroup sac_io
 */
int sacio_printHeader(FILE *file, const struct sacHeader_struct header)
{
    FILE *fout = stdout;
    double dvar;
    char svar[16];
    int ivar;
    bool lvar;
    if (file != NULL){fout = file;}
    // priorities
    memset(svar, 0, 16*sizeof(char));
    if (sacio_getCharacterHeader(SAC_CHAR_KNETWK, header, svar) == 0)
    {
        fprintf(fout, "%16s: %s\n", "KNETWK", svar);
    }
    memset(svar, 0, 16*sizeof(char));
    if (sacio_getCharacterHeader(SAC_CHAR_KSTNM, header, svar) == 0)
    {
        fprintf(fout, "%16s: %s\n", "KSTNM", svar);
    }
    memset(svar, 0, 16*sizeof(char));
    if (sacio_getCharacterHeader(SAC_CHAR_KEVNM, header, svar) == 0)
    {
        fprintf(fout, "%16s: %s\n", "KEVNM", svar);
    }
    memset(svar, 0, 16*sizeof(char));
    if (sacio_getCharacterHeader(SAC_CHAR_KCMPNM, header, svar) == 0)
    {
        fprintf(fout, "%16s: %s\n", "KCMPNM", svar);
    }
    memset(svar, 0, 16*sizeof(char));
    if (sacio_getCharacterHeader(SAC_CHAR_KHOLE, header, svar) == 0)
    {
        fprintf(fout, "%16s: %s\n", "KHOLE", svar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_DELTA, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "DELTA", dvar); 
    }
    if (sacio_getIntegerHeader(SAC_INT_NPTS, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NPTS", ivar);
    }
    // floats
    if (sacio_getFloatHeader(SAC_FLOAT_DEPMIN, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "DEPMIN", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_DEPMAX, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "DEPMAX", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_SCALE, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "SCALE", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_ODELTA, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "ODELTA", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_B, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "B", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_E, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "E", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_O, header, &dvar) == 0)
    {
        memset(svar, 0, 16*sizeof(char));
        if (sacio_getCharacterHeader(SAC_CHAR_KO, header, svar) == 0)
        {
            fprintf(fout, "%16s: %lf (%s)\n", "O", dvar, svar);
        }
        else
        {
            fprintf(fout, "%16s: %lf\n", "O", dvar);
        }
    }
    if (sacio_getFloatHeader(SAC_FLOAT_A, header, &dvar) == 0)
    {
        if (sacio_getCharacterHeader(SAC_CHAR_KA, header, svar) == 0)
        {
            fprintf(fout, "%16s: %lf (%s)\n", "A", dvar, svar);
        }
        else
        {
            fprintf(fout, "%16s: %lf\n", "O", dvar);
        }
    }
    if (sacio_getFloatHeader(SAC_FLOAT_T0, header, &dvar) == 0)
    {
        memset(svar, 0, 16*sizeof(char));
        if (sacio_getCharacterHeader(SAC_CHAR_KT0, header, svar) == 0)
        {
            fprintf(fout, "%16s: %lf (%s)\n", "T0", dvar, svar);
        }
        else
        {
            fprintf(fout, "%16s: %lf\n", "T0", dvar);
        }
    }
    if (sacio_getFloatHeader(SAC_FLOAT_T1, header, &dvar) == 0)
    {
        memset(svar, 0, 16*sizeof(char));
        if (sacio_getCharacterHeader(SAC_CHAR_KT1, header, svar) == 0)
        {
            fprintf(fout, "%16s: %lf (%s)\n", "T1", dvar, svar);
        }
        else
        {
            fprintf(fout, "%16s: %lf\n", "T1", dvar);
        }
    }
    if (sacio_getFloatHeader(SAC_FLOAT_T2, header, &dvar) == 0)
    {
        memset(svar, 0, 16*sizeof(char));
        if (sacio_getCharacterHeader(SAC_CHAR_KT2, header, svar) == 0)
        {
            fprintf(fout, "%16s: %lf (%s)\n", "T2", dvar, svar);
        }
        else
        {
            fprintf(fout, "%16s: %lf\n", "T2", dvar);
        }
    }
    if (sacio_getFloatHeader(SAC_FLOAT_T3, header, &dvar) == 0)
    {
        memset(svar, 0, 16*sizeof(char));
        if (sacio_getCharacterHeader(SAC_CHAR_KT3, header, svar) == 0)
        {
            fprintf(fout, "%16s: %lf (%s)\n", "T3", dvar, svar);
        }
        else
        {
            fprintf(fout, "%16s: %lf\n", "T3", dvar);
        }
    }
    if (sacio_getFloatHeader(SAC_FLOAT_T4, header, &dvar) == 0)
    {
        memset(svar, 0, 16*sizeof(char));
        if (sacio_getCharacterHeader(SAC_CHAR_KT4, header, svar) == 0)
        {
            fprintf(fout, "%16s: %lf (%s)\n", "T4", dvar, svar);
        }
        else
        {
            fprintf(fout, "%16s: %lf\n", "T4", dvar);
        }
    }
    if (sacio_getFloatHeader(SAC_FLOAT_T5, header, &dvar) == 0)
    {
        memset(svar, 0, 16*sizeof(char));
        if (sacio_getCharacterHeader(SAC_CHAR_KT5, header, svar) == 0)
        {
            fprintf(fout, "%16s: %lf (%s)\n", "T5", dvar, svar);
        }
        else
        {
            fprintf(fout, "%16s: %lf\n", "T5", dvar);
        }
    }
    if (sacio_getFloatHeader(SAC_FLOAT_T6, header, &dvar) == 0)
    {
        memset(svar, 0, 16*sizeof(char));
        if (sacio_getCharacterHeader(SAC_CHAR_KT6, header, svar) == 0)
        {
            fprintf(fout, "%16s: %lf (%s)\n", "T6", dvar, svar);
        }
        else
        {
            fprintf(fout, "%16s: %lf\n", "T6", dvar);
        }
    }
    if (sacio_getFloatHeader(SAC_FLOAT_T7, header, &dvar) == 0)
    {
        memset(svar, 0, 16*sizeof(char));
        if (sacio_getCharacterHeader(SAC_CHAR_KT7, header, svar) == 0)
        {
            fprintf(fout, "%16s: %lf (%s)\n", "T7", dvar, svar);
        }
        else
        {
            fprintf(fout, "%16s: %lf\n", "T7", dvar);
        }
    }
    if (sacio_getFloatHeader(SAC_FLOAT_T8, header, &dvar) == 0)
    {
        memset(svar, 0, 16*sizeof(char));
        if (sacio_getCharacterHeader(SAC_CHAR_KT8, header, svar) == 0)
        {
            fprintf(fout, "%16s: %lf (%s)\n", "T8", dvar, svar);
        }
        else
        {
            fprintf(fout, "%16s: %lf\n", "T8", dvar);
        }
    }
    if (sacio_getFloatHeader(SAC_FLOAT_T9, header, &dvar) == 0)
    {
        memset(svar, 0, 16*sizeof(char));
        if (sacio_getCharacterHeader(SAC_CHAR_KT9, header, svar) == 0)
        {
            fprintf(fout, "%16s: %lf (%s)\n", "T9", dvar, svar);
        }
        else
        {
            fprintf(fout, "%16s: %lf\n", "T9", dvar);
        }
    }
    if (sacio_getFloatHeader(SAC_FLOAT_F, header, &dvar) == 0)
    {
        memset(svar, 0, 16*sizeof(char));
        if (sacio_getCharacterHeader(SAC_CHAR_KF, header, svar) == 0)
        {
            fprintf(fout, "F: %lf (%s)\n", dvar, svar);
        }
        else
        {
            fprintf(fout, "F: %lf\n", dvar);
        }
    }
    if (sacio_getFloatHeader(SAC_FLOAT_RESP0, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "RESP0", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_RESP1, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "RESP1", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_RESP2, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "RESP2", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_RESP3, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "RESP3", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_RESP4, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "RESP4", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_RESP5, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "RESP5", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_RESP6, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "RESP6", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_RESP7, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "RESP7", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_RESP8, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "RESP8", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_RESP9, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "RESP9", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_STLA, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "STLA", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_STLO, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "STLO", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_STEL, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "STEL", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_STDP, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "STDP", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_EVLA, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "EVLA", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_EVLO, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "EVLO", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_EVEL, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "EVEL", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_EVDP, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "EVDP", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_MAG, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "MAG", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_USER0, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "USER0", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_USER1, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "USER1", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_USER2, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "USER2", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_USER3, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "USER3", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_USER4, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "USER4", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_USER5, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "USER5", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_USER6, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "USER6", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_USER7, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "USER7", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_USER8, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "USER8", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_USER9, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "USER9", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_DIST, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "DIST", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_AZ, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "AZ", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_BAZ, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "BAZ", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_GCARC, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "GCARC", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_DEPMEN, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "DEPMEN", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_CMPAZ, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "CMPAZ", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_CMPINC, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "CMPINC", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_XMINIMUM, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "XMINIMUM", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_XMAXIMUM, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "XMAXIMUM", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_YMINIMUM, header, &dvar) == 0)
    {    
        fprintf(fout, "%16s: %lf\n", "YMINIMUM", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_YMAXIMUM, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "YMAXIMUM", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_UNUSED0, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "UNUSED0", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_UNUSED1, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "UNUSED1", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_UNUSED2, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "UNUSED2", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_UNUSED3, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "UNUSED3", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_UNUSED4, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "UNUSED4", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_UNUSED5, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "UNUSED5", dvar);
    }
    if (sacio_getFloatHeader(SAC_FLOAT_UNUSED6, header, &dvar) == 0)
    {
        fprintf(fout, "%16s: %lf\n", "UNUSED6", dvar);
    }
    // ints
    if (sacio_getIntegerHeader(SAC_INT_NZYEAR, header, &ivar) == 0)
    {    
        fprintf(fout, "%16s: %d\n", "NZYEAR", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_NZJDAY, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NZJDAY", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_NZHOUR, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NZHOUR", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_NZMIN, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NZMIN", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_NZSEC, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NZSEC", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_NZMSEC, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NZMSEC", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_NVHDR, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NVHDR", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_NORID, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NORID", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_NEVID, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NEVID", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_NWFID, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NWFID", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_NXSIZE, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NXSIZE", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_NYSIZE, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NYSIZE", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_UNUSED0, header, &ivar) == 0)
    {    
        fprintf(fout, "%16s: %d\n", "NUNUSED0", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_IFTYPE, header, &ivar) == 0)
    {    
        if (ivar == 1)
        {
            fprintf(fout, "%16s: %d (time series)\n", "IFTYPE", ivar);
        }
        else if (ivar == 2)
        {
            fprintf(fout, "%16s: %d (spectral file: real/imaginary)\n", "IFTYPE", ivar);
        }
        else if (ivar == 3)
        {
            fprintf(fout, "%16s: %d (spectral file: amplitude/phase)\n", "IFTYPE", ivar);
        }
        else if (ivar == 4)
        {
            fprintf(fout, "%16s: %d (x vs. y)\n", "IFTYPE", ivar);
        }
        else if (ivar == 5)
        {
            fprintf(fout, "%16s: %d (xyz)\n", "IFTYPE", ivar);
        }
        else
        {
            fprintf(fout, "%16s: %d\n", "IFTYPE", ivar);
        }
    }
    if (sacio_getIntegerHeader(SAC_INT_IDEP, header, &ivar) == 0)
    {
        if (ivar == 71)
        {
            fprintf(fout, "%16s: %d (unknown)\n", "IDEP", ivar);
        }
        else if (ivar == 6)
        {
            fprintf(fout, "%16s: %d (displacement in nm)\n", "IDEP", ivar);
        }
        else if (ivar == 7)
        {
            fprintf(fout, "%16s: %d (velocity in nm/sec)\n", "IDEP", ivar);
        }
        else if (ivar == 8)
        {
            fprintf(fout, "%16s: %d (acceleration in nm/sec/sec)\n", "IDEP", ivar);
        } 
        else if (ivar == 50)
        {
            fprintf(fout, "%16s: %d (volts)\n", "IDEP", ivar);
        }
        else
        {
            fprintf(fout, "%16s: %d\n", "IDEP", ivar);
        }
    }
    if (sacio_getIntegerHeader(SAC_INT_IZTYPE, header, &ivar) == 0)
    {
        if (ivar == 71)
        {
            fprintf(fout, "%16s: %d (unknown)\n", "IZTYPE", ivar);
        }
        else if (ivar == 9)
        {
            fprintf(fout, "%16s: %d (begin time)\n", "IZTYPE", ivar);
        }
        else if (ivar == 10)
        {
            fprintf(fout, "%16s: %d (midnight of reference GMT day)\n", "IZTYPE", ivar);
        }
        else if (ivar == 11)
        {
            fprintf(fout, "%16s: %d (event origin time)\n", "IZTYPE", ivar);
        }
        else if (ivar == 12)
        {
            fprintf(fout, "%16s: %d (first arrival time)\n", "IZTYPE",  ivar);
        }
        else if (ivar >= 13 && ivar <= 22)
        {
            fprintf(fout, "%16s: %d (Defined pick time IT%d)\n", "IZTYPE",
                    ivar, ivar - 13);
        }
        else
        {
            fprintf(fout, "%16s: %d\n", "IZTYPE", ivar);
        }
    }
    if (sacio_getIntegerHeader(SAC_INT_UNUSED1, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NUNUSED1", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_IINST, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "IINST", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_ISTREG, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "ISTREG", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_IEVREG, header, &ivar) == 0)
    {    
        fprintf(fout, "%16s: %d\n", "IEVREG", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_IEVTYP, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "IEVTYP", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_IQUAL, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "IQUAL", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_ISYNTH, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "ISYNTH", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_IMAGTYP, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "IMAGTYP", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_IMAGSRC, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "IMAGSRC", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_UNUSED2, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NUNUSED2", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_UNUSED3, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NUNUSED3", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_UNUSED4, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NUNUSED4", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_UNUSED5, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NUNUSED5", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_UNUSED6, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NUNUSED6", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_UNUSED7, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NUNUSED7", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_UNUSED8, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NUNUSED8", ivar);
    }
    if (sacio_getIntegerHeader(SAC_INT_UNUSED9, header, &ivar) == 0)
    {
        fprintf(fout, "%16s: %d\n", "NUNUSED9", ivar);
    }
    // bools
    if (sacio_getBooleanHeader(SAC_BOOL_LEVEN, header, &lvar) == 0)
    {
        fprintf(fout, "%16s: %s\n", "LEVEN", lvar ? "TRUE" : "FALSE");
    }
    if (sacio_getBooleanHeader(SAC_BOOL_LPSPOL, header, &lvar) == 0)
    {
        fprintf(fout, "%16s: %s\n", "LPSPOL", lvar ? "TRUE" : "FALSE");
    }
    if (sacio_getBooleanHeader(SAC_BOOL_LOVROK, header, &lvar) == 0)
    {
        fprintf(fout, "%16s: %s\n", "LOVROK", lvar ? "TRUE" : "FALSE");
    }
    if (sacio_getBooleanHeader(SAC_BOOL_LCALDA, header, &lvar) == 0)
    {
        fprintf(fout, "%16s: %s\n", "LCALDA", lvar ? "TRUE" : "FALSE");
    }
    if (sacio_getBooleanHeader(SAC_BOOL_LUNUSED, header, &lvar) == 0)
    {
        fprintf(fout, "%16s: %s\n", "LUNUSED", lvar ? "TRUE" : "FALSE");
    }
    // characters
    memset(svar, 0, 16*sizeof(char));
    if (sacio_getCharacterHeader(SAC_CHAR_KUSER0, header, svar) == 0)
    {
        fprintf(fout, "%16s: %s\n", "KUSER0", svar);
    }
    memset(svar, 0, 16*sizeof(char));
    if (sacio_getCharacterHeader(SAC_CHAR_KUSER1, header, svar) == 0)
    {
        fprintf(fout, "%16s: %s\n", "KUSER1", svar);
    }
    memset(svar, 0, 16*sizeof(char));
    if (sacio_getCharacterHeader(SAC_CHAR_KUSER2, header, svar) == 0)
    {
        fprintf(fout, "%16s: %s\n", "KUSER2", svar);
    }
    memset(svar, 0, 16*sizeof(char));
    if (sacio_getCharacterHeader(SAC_CHAR_KDATRD, header, svar) == 0)
    {
        fprintf(fout, "%16s: %s\n", "KDATRD", svar);
    }
    memset(svar, 0, 16*sizeof(char));
    if (sacio_getCharacterHeader(SAC_CHAR_KINST, header, svar) == 0)
    {
        fprintf(fout, "%16s: %s\n", "KINST", svar);
    }
    return 0;
}
//============================================================================//
static void readFloatHeader(const bool lswap, const char cfloat[4], double *val)
{
    char c4[4];
    float f;
    *val =-12345.0;
    if (!lswap)
    {
        memcpy(&f, cfloat, 4*sizeof(char)); 
    }
    else
    {
        c4[0] = cfloat[3];
        c4[1] = cfloat[2];
        c4[2] = cfloat[1];
        c4[3] = cfloat[0];
        memcpy(&f, c4, 4*sizeof(char));
    }
    *val = (double) f;
    return;
}

static void readIntHeader(const bool lswap, const char cint[4], int *val)
{
    char c4[4];
    *val =-12345;
    if (!lswap)
    {
        memcpy(val, cint, 4*sizeof(char)); 
    }   
    else
    {
        c4[0] = cint[3];
        c4[1] = cint[2];
        c4[2] = cint[1];
        c4[3] = cint[0];
        memcpy(val, c4, 4*sizeof(char));
    }
    return;
}

static void readBoolHeader(const bool lswap, const char *__restrict__ cbool,
                           int *val)
{
    char c4[4];
    *val =-12345;
    if (!lswap)
    {   
        memcpy(val, cbool, 4*sizeof(char)); 
    }   
    else
    {   
        c4[0] = cbool[3];
        c4[1] = cbool[2];
        c4[2] = cbool[1];
        c4[3] = cbool[0];
        memcpy(val, c4, 4*sizeof(char));
    }   
    return;
}

static void readChar8(const char *__restrict__ cin8,
                      char *__restrict__ cout8)
{
    int i;
    memset(cout8, 0, 8*sizeof(char));
    memcpy(cout8, cin8, 8*sizeof(char));
    for (i=7; i>=0 ; i--)
    {
        if (!isspace(cout8[i])){break;}
        cout8[i] = '\0';
    }
    return;
}

static void readChar16(const char *__restrict__ cin16,
                       char *__restrict__ cout16)
{
    int i;
    memset(cout16, 0, 16*sizeof(char));
    memcpy(cout16, cin16, 16*sizeof(char));
    for (i=15; i>=0 ; i--)
    {   
        if (!isspace(cout16[i])){break;}
        cout16[i] = '\0';
    }
    return;
}

static void writeFloatHeader(const bool lswap, const double val, 
                             char *__restrict__ cflt)
{
    char c4[4];
    float ctemp = (float) val;
    if (!lswap)
    {    
        memcpy(cflt, (void *) &ctemp, 4*sizeof(char));
    }
    else
    {
        memcpy(c4, (void *) &ctemp, 4*sizeof(char));
        cflt[0] = c4[3];
        cflt[1] = c4[2];
        cflt[2] = c4[1];
        cflt[3] = c4[0];
    }
    return;
}

static void writeIntHeader(const bool lswap, const int val,
                           char *__restrict__ cint)
{
    char c4[4];
    int ctemp = val;
    if (!lswap)
    {
        memcpy(cint, (void *) &ctemp, 4*sizeof(char)); 
    }
    else
    {
        memcpy(c4, (void *) &ctemp, 4*sizeof(char));
        cint[0] = c4[3];
        cint[1] = c4[2];
        cint[2] = c4[1];
        cint[3] = c4[0];
    }
    return;
}

static void writeBoolHeader(const bool lswap, const int val,
                            char *__restrict__ cbool)
{
    char c4[4];
    int ctemp = val;
    if (!lswap)
    {
        memcpy(cbool, (void *) &ctemp, 4*sizeof(char));
    }
    else
    {
        memcpy(c4, (void *) &ctemp, 4*sizeof(char));
        cbool[0] = c4[3];
        cbool[1] = c4[2];
        cbool[2] = c4[1];
        cbool[3] = c4[0];
    }
    return;
}

static void writeChar8(const char *__restrict__ cin8,
                       char *__restrict__ cout8)
{
    size_t lenos;
    lenos = MIN(8, strlen(cin8));
    memset(cout8, 0, 8*sizeof(char));
    strncpy(cout8, cin8, lenos);
    return;
}

static void writeChar16(const char *__restrict__ cin16,
                        char *__restrict__ cout16)
{
    size_t lenos;
    lenos = MIN(16, strlen(cin16));
    memset(cout16, 0, 16*sizeof(char));
    strncpy(cout16, cin16, lenos);
    return;
}

static double makeTime(const char *time)
{
    double t;
    int day, hour, minute, month, second, year;
    t = 0.0;
    sscanf(time, "%4d-%2d-%2dT%2d:%2d:%2d",
           &year, &month, &day, &hour, &minute, &second);
    t = sacio_time_calendar2epoch2(year, month, day, hour,
                                   minute, second, 0);
    return t;
}

