#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <cstdint>
#include <cmath>
#include <algorithm>
#include "sacio.hpp"
#include "sacio_mpi.hpp"

inline void swapChar64_(char a[64], char b[64]);
template<class T>

static typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
    almost_equal(T x, T y, int ulp)
{
    // the machine epsilon has to be scaled to the magnitude of the values used
    // and multiplied by the desired precision in ULPs (units in the last place)
    return std::abs(x-y) <= std::numeric_limits<T>::epsilon() * std::abs(x+y) * ulp
    // unless the result is subnormal
           || std::abs(x-y) < std::numeric_limits<T>::min();
}

//============================================================================//
//                                    Header                                  //
//============================================================================//
/*!
 * @defgroup SacHeaderpp SAC C++ Header Manager
 * @brief Manages the SAC header variables.
 * @ingroup Sacpp
 */
/*!
 * @brief Initializes the SAC header.  All variables will be set to the SAC
 *        default NaN which is -12345.
 * @ingroup SacHeaderpp
 */
SacHeader::SacHeader(void)
{
    clear();
    return;
};
/*!
 * @brief Class destructor.
 * @ingroup SacHeaderpp
 */ 
SacHeader::~SacHeader(void)
{
    clear();
    return;
}
/*!
 * @brief Copy constructor.
 * @param[in] sacHeader  The SAC header class to copy to this class.
 * @ingroup SacHeaderpp
 */
SacHeader::SacHeader(const SacHeader &sacHeader)
{
    *this = sacHeader;
    return;
}
/*!
 * @brief Constructs a SAC header class from a C SAC header.
 * @param[in] sacHeader  The SAC header struct to initialize from.
 * @ingroup SacHeaderpp
 */
SacHeader::SacHeader(const struct sacHeader_struct sacHeader)
{
    sacio_copyHeader(sacHeader, &header_);
    return;
}
/*!
 * @brief Copy constructor.
 * @param[in] SacHeader  The SAC header to copy to this class.
 * @result A deep copy of the SAC header.
 * @ingroup SacHeaderpp
 */
SacHeader& SacHeader::operator=(const SacHeader &sacHeader)
{
    if (&sacHeader == this){return *this;}
    sacio_copyHeader(sacHeader.header_, &header_);
    tol_ = sacHeader.tol_;
    return *this;
}
/*!
 * @brief Determines if the given class is equal to this class.
 * @param[in] sacHeader  Header class to compare.
 * @result True if given header class equals this class.
 * @ingroup SacHeaderpp
 */
bool SacHeader::operator==(const SacHeader &sacHeader) const
{
    if (&sacHeader == this){return true;}
    double tol = getEqualityTolerance();
    bool lequal = sacio_isHeaderEqual(header_, sacHeader.header_, tol);
    return lequal;
}
/*!
 * @brief Determines if the given class is not equal to this class.
 * @param[in] sacHeader  Header class to compare.
 * @result True if given header class equals this class.
 * @ingroup SacHeaderpp
 */
bool SacHeader::operator!=(const SacHeader &sacHeader) const
{
    return !(*this == sacHeader);
}
/*!
 * @brief Sets the equality tolerance so that two double variables in the
 *        header are equal when abs(h1.var - h2.var) < tol.
 * @param[in] tol   The tolerance.
 * @ingroup SacHeaderpp
 */
void SacHeader::setEqualityTolerance(const double tol)
{
    tol_ = tol;
    return;
}
/*!
 * @brief Gets the equality tolerance so that two double variables in the
 *        header are equal when abs(h1.var - h2.var) < tol.
 * @result The tolerance.
 * @ingroup SacHeaderpp
 */
double SacHeader::getEqualityTolerance(void) const
{
    return tol_;
} 
/*!
 * @brief Swaps the SAC header class sac1 with sac2.
 * @param[in,out] sac1   On exit this is sac2's header class.
 * @param[in,out] sac2   On exit this is sac1's header class.
 * @ingroup SacHeaderpp
 */
void swap(SacHeader &sac1, SacHeader &sac2)
{
    if (&sac1 == &sac2){return;}
    struct sacHeader_struct temp;
    memset(&temp, 0, sizeof(struct sacHeader_struct));
    sacio_copyHeader(sac1.header_, &temp);
    sacio_copyHeader(sac2.header_, &sac1.header_);
    sacio_copyHeader(temp, &sac2.header_);
    sacio_freeHeader(&temp);
    std::swap(sac1.tol_, sac2.tol_);
    return;
}
/*!
 * @brief Gets a copy of the SAC header for C.
 * @param[out] header  A copy of the header.
 * @ingroup SacHeaderpp
 */
void SacHeader::getSacHeaderStruct(struct sacHeader_struct *header) const
{
    sacio_copyHeader(header_, header);
    return;
}
/*!
 * @brief Copies the header to a SAC header struct.
 * @param[out] sacHeader  SAC header that is a copy of the SAC header in
 *                        the class.
 * @ingroup SacHeaderpp
 */
void SacHeader::makeSacHeaderStruct(struct sacHeader_struct *sacHeader)
{
    sacio_copyHeader(header_, sacHeader);
    return;
}
/*!
 * @brief Prints the header information.
 * @ingroup SacHeaderpp
 */
int SacHeader::print(FILE *file)
{
    int ierr = sacio_printHeader(file, header_);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Errors occurred while printing header\n",
                __func__);
    }
    return 0;
}
/*!
 * @brief Clears and restores the default SAC header.
 * @ingroup SacHeaderpp
 */
void SacHeader::clear(void)
{
    sacio_freeHeader(&header_);
    return;
}
/*!
 * @brief Reads a SAC header.
 * @param[in] fileName  Name of SAC file which contains the header to read.
 * @result 0 indicates success.
 * @ingroup SacHeaderpp
 */
int SacHeader::readHeader(const std::string fileName)
{
    int ierr = readHeader(fileName.c_str());
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to read header\n", __func__);
    }
    return ierr;
}
/*!
 * @brief Reads a SAC header.
 * @param[in] fileName  Name of SAC file which contains the header to read.
 * @result 0 indicates success.
 * @ingroup SacHeaderpp
 */
int SacHeader::readHeader(const char *fileName)
{
    clear();
    int ierr = sacio_readHeader(fileName, &header_);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to read header\n", __func__);
        clear();
        return -1;
    }
    return 0; 
}
/*!
 * @brief Gets an integer header variable.
 * @param[in] hvar  Name of the integer header variable to get.
 *                  This must be from the SacHeader::Integer class.
 * @param[out] hdr  Value of header variable.
 * @retval  0 indicates success.
 * @retval -1 indicates that the result is undefined.
 * @retval  1 indicates an invalid enumerated type.
 * @ingroup SacHeaderpp
 */
int SacHeader::getIntegerHeader(const enum sacIntegerHeader_enum hvar, 
                                int *hdr) const
{
    enum sacHeader_enum hvarPass = static_cast<sacHeader_enum> (hvar);
    int ierr = sacio_getIntegerHeader(hvarPass, header_, hdr);
    return ierr;
}
/*!
 * @brief Sets an integer header variable.
 * @param[in] hvar  Name of the integer header variable to set.
 *                  This must be from the SacHeader::Integer class.
 * @param[in] hdr   Value of header variable to set.
 * @result 0 indicates success.
 * @ingroup SacHeaderpp
 */
int SacHeader::setIntegerHeader(const enum sacIntegerHeader_enum hvar, 
                                const int hdr)
{
    enum sacHeader_enum hvarPass = static_cast<sacHeader_enum> (hvar);
    int ierr = sacio_setIntegerHeader(hvarPass, hdr, &header_);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set header variable\n", __func__);
    }
    return ierr;
}
/*!
 * @brief Gets a boolean header variable.
 * @param[in] hvar  Name of the integer header variable to get.
 *                  This must be from the SacHeader::Boolean class.
 * @param[out] hdr  Value of header variable.
 * @retval  0 indicates success.
 * @retval -1 indicates that the result is undefined.
 * @retval  1 indicates an invalid enumerated type.
 * @ingroup SacHeaderpp
 */
int SacHeader::getBooleanHeader(const enum sacBooleanHeader_enum hvar, 
                                bool *hdr) const
{
    enum sacHeader_enum hvarPass = static_cast<sacHeader_enum> (hvar);
    int ierr = sacio_getBooleanHeader(hvarPass, header_, hdr);
    return ierr;
}
/*!
 * @brief Sets a boolean header variable.
 * @param[in] hvar  Name of the boolean header variable to set.
 *                  This must be from the SacHeader::Boolean class. 
 * @param[in] hdr   Value of header variable to set.
 * @result 0 indicates success.
 * @ingroup SacHeaderpp
 */
 int SacHeader::setBooleanHeader(const enum sacBooleanHeader_enum hvar,
                                 const bool hdr)
{
    enum sacHeader_enum hvarPass = static_cast<sacHeader_enum> (hvar);
    int ierr = sacio_setBooleanHeader(hvarPass, hdr, &header_);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set header variable\n", __func__);
    }
    return ierr;
}
/*!
 * @brief Gets a double precision header variable.
 * @param[in] hvar  Name of the integer header variable to get.
 *                  This must be from the SacHeader::Double class.
 * @param[out] hdr  Value of header variable.
 * @retval  0 indicates success.
 * @retval -1 indicates that the result is undefined.
 * @retval  1 indicates an invalid enumerated type.
 * @ingroup SacHeaderpp
 */
int SacHeader::getDoubleHeader(const enum sacDoubleHeader_enum hvar,
                               double *hdr) const
{
    enum sacHeader_enum hvarPass = static_cast<sacHeader_enum> (hvar);
    int ierr = sacio_getFloatHeader(hvarPass, header_, hdr);
    return ierr;
}
/*!
 * @brief Sets a double precision header variable.
 * @param[in] hvar  Name of the double header variable to set.
 *                  This must be from the SacHeader::Double class.
 * @param[in] hdr   Value of header variable to set.
 * @result 0 indicates success.
 * @ingroup SacHeaderpp
 */
 int SacHeader::setDoubleHeader(const enum sacDoubleHeader_enum hvar,
                                const double hdr)
{
    enum sacHeader_enum hvarPass = static_cast<sacHeader_enum> (hvar);
    int ierr = sacio_setFloatHeader(hvarPass, hdr, &header_);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set header variable\n", __func__);
    }
    return ierr;
}
/*!
 * @brief Gets a single precision header variable.
 * @param[in] hvar  Name of the integer header variable to get.
 *                  This must be from the SacHeader::Float class.
 * @param[out] hdr  Value of header variable.
 * @retval  0 indicates success.
 * @retval -1 indicates that the result is undefined.
 * @retval  1 indicates an invalid enumerated type.
 * @ingroup SacHeaderpp
 */
int SacHeader::getFloatHeader(const enum sacFloatHeader_enum hvar,
                              float *hdr) const
{
    double hdr64;
    enum sacHeader_enum hvarPass = static_cast<sacHeader_enum> (hvar);
    int ierr = sacio_getFloatHeader(hvarPass, header_, &hdr64);
    *hdr = static_cast<float> (hdr64);
    return ierr;
}
/*!
 * @brief Sets a float header variable.  This will convert the float to a 
 *        double and store the variable in double precision.
 * @param[in] hvar Name of the float header variable to set.
 *                  This must be from the SacHeader::Float class.
 * @param[in] hdr  Value of header variable to set.
 * @result 0 indicates success.
 * @ingroup SacHeaderpp
 */
int SacHeader::setFloatHeader(const enum sacFloatHeader_enum hvar,
                              const float hdr)
{
    double hdr64 = static_cast<double> (hdr);
    enum sacDoubleHeader_enum hvarPass
        = static_cast<sacDoubleHeader_enum> (hvar);
    int ierr = setDoubleHeader(hvarPass, hdr64);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set header variable\n", __func__);
    }
    return ierr;
}
/*!
 * @brief Gets a string header variable.
 * @param[in] hvar  Name of the string header variable to get.
 *                  This must be from the SacHeader::String class.
 * @param[out] hdr  Value of the header variable.
 * @retval  0 indicates success.
 * @retval -1 indicates that the result is undefined.
 * @retval  1 indicates an invalid enumerated type.
 * @ingroup SacHeaderpp
 */
int SacHeader::getStringHeader(const enum sacStringHeader_enum hvar,
                               std::string &hdr) const
{
    int ierr = 0;
    enum sacCharacterHeader_enum hvarPass
        = static_cast<sacCharacterHeader_enum> (hvar);
    if (hvar == SAC_STRING_HDR_KEVNM)
    {
        hdr.resize(16);
        char cspace[16];
        memset(cspace, 0, 16*sizeof(char));
        ierr = getCharacterHeader(hvarPass, cspace);
        hdr = cspace;
    }
    else
    {
        char cspace[8];
        memset(cspace, 0, 8*sizeof(char));
        ierr = getCharacterHeader(hvarPass, cspace);
        for (int i=0; i<8; i++){hdr.push_back(cspace[7-i]);}
        hdr = cspace;
    }
    return ierr;
}
/*!
 * @brief Sets a string header variable.  Unless this is an event name the
 *        variable will truncated to the first 8 characters.  If this is
 *        an event name then it will be truncated to the first 16 characters.
 *
 * @param[in] hvar  Name of header variable to set.
 *                  This must be from the SacHeader::String class.
 * @param[in] hdr   Value of header variable to set.
 * @result 0 indicates success.
 * @ingroup SacHeaderpp
 */
 int SacHeader::setStringHeader(const enum sacStringHeader_enum hvar,
                                const std::string hdr)
{
    const size_t sixteen = static_cast<size_t> (16);
    const size_t eight = static_cast<size_t> (8);
    size_t len = hdr.length();
    char chdr[16];
    memset(chdr, 0, 16*sizeof(char));
    if (hvar == SAC_STRING_HDR_KEVNM)
    {
        strncpy(chdr, hdr.c_str(), std::min(len, sixteen)*sizeof(char));
    }
    else
    {
        strncpy(chdr, hdr.c_str(), std::min(len, eight)*sizeof(char));
    }
    enum sacCharacterHeader_enum hvarPass
           = static_cast<sacCharacterHeader_enum> (hvar);
    int ierr = setCharacterHeader(hvarPass, chdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set string hdr variable\n", __func__); 
    }
    return ierr;
}
/*!
 * @brief Gets a character header variable.  If this is an event name then
 *        hdr must be an array of length 16.  Otherwise, it must be an array
 *        of length 8.
 * @param[in] hvar  Name of header variable to get.
 *                  This must be from the SacHeader::Character class.
 * @param[out] hdr  The character header variable.
 * @retval  0 indicates success.
 * @retval -1 indicates that the result is undefined.
 * @retval  1 indicates an invalid enumerated type.
 * @ingroup SacHeaderpp
 */
int SacHeader::getCharacterHeader(const enum sacCharacterHeader_enum hvar,
                                  char hdr[]) const
{
    enum sacHeader_enum hvarPass = static_cast<sacHeader_enum> (hvar);
    int ierr = sacio_getCharacterHeader(hvarPass, header_, hdr);
    return ierr;
}
/*!
 * @brief Sets a character header variable.  Unless this is an event name the
 *        variable will truncated to the first 8 characters.  If this is
 *        an event name then it will be truncated to the first 16 characters.
 * @param[in] hvar  Name of header variable to set.
 *                  This must be from the SacHeader::Character class.
 * @param[in] hdr   The value of the character header variable to set.
 * @result 0 indicates success.
 * @ingroup SacHeaderpp
 */
int SacHeader::setCharacterHeader(const enum sacCharacterHeader_enum hvar,
                                  const char hdr[])
{
    enum sacHeader_enum hvarPass = static_cast<sacHeader_enum> (hvar);
    int ierr = sacio_setCharacterHeader(hvarPass, hdr, &header_);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set char header variable\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::setStringHeader
 * @ingroup SacHeaderpp
 */
int SacHeader::setHeader(const enum sacStringHeader_enum hvar,
                        const std::string hdr) 
{
    int ierr = setStringHeader(hvar, hdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set string header\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::getStringHeader
 * @ingroup SacHeaderpp
 */
int SacHeader::getHeader(const enum sacStringHeader_enum hvar,
                         std::string &hdr) const
{
    int ierr = getStringHeader(hvar, hdr);
    return ierr;
}
/*!
 * @copydoc SacHeader::getCharacterHeader
 * @ingroup SacHeaderpp
 */
int SacHeader::getHeader(const enum sacCharacterHeader_enum hvar,
                         char hdr[]) const
{
    int ierr = getCharacterHeader(hvar, hdr);
    return ierr;
}
/*!
 * @copydoc SacHeader::getDoubleHeader
 * @ingroup SacHeaderpp
 */
int SacHeader::getHeader(const enum sacDoubleHeader_enum hvar,
                         double *hdr) const
{
    int ierr = getDoubleHeader(hvar, hdr);
    return ierr;
}
/*!
 * @copydoc SacHeader::getFloatHeader
 * @ingroup SacHeaderpp
 */
int SacHeader::getHeader(const enum sacFloatHeader_enum hvar,
                         float *hdr) const
{
    int ierr = getFloatHeader(hvar, hdr);
    return ierr;
}
/*!
 * @copydoc SacHeader::getIntegerHeader
 * @ingroup SacHeaderpp
 */
int SacHeader::getHeader(const enum sacIntegerHeader_enum hvar,
                         int *hdr) const
{
    int ierr = getIntegerHeader(hvar, hdr);
    return ierr;
}
/*!
 * @copydoc SacHeader::getBooleanHeader
 * @ingroup SacHeaderpp
 */
int SacHeader::getHeader(const enum sacBooleanHeader_enum hvar,
                         bool *hdr) const
{
    int ierr = getBooleanHeader(hvar, hdr);
    return ierr;
}
/*!
 * @copydoc SacHeader::setCharacterHeader
 * @ingroup SacHeaderpp
 */
int SacHeader::setHeader(const enum sacCharacterHeader_enum hvar,
                         const char hdr[])
{
    int ierr = setCharacterHeader(hvar, hdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set string header\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::setDoubleHeader
 * @ingroup SacHeaderpp
 */
int SacHeader::setHeader(const enum sacDoubleHeader_enum hvar,
                         const double hdr)
{
    int ierr = setDoubleHeader(hvar, hdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set double header\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::setFloatHeader
 * @ingroup SacHeaderpp
 */
int SacHeader::setHeader(const enum sacFloatHeader_enum hvar,
                         const float hdr)
{
    int ierr = setFloatHeader(hvar, hdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set float header\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::setIntegerHeader
 * @ingroup SacHeaderpp
 */
int SacHeader::setHeader(const enum sacIntegerHeader_enum hvar,
                         const int hdr)
{
    int ierr = setIntegerHeader(hvar, hdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set integer header\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::setBooleanHeader
 * @ingroup SacHeaderpp
 */
int SacHeader::setHeader(const enum sacBooleanHeader_enum hvar,
                         const bool hdr)
{
    int ierr = setBooleanHeader(hvar, hdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set bool header\n", __func__);
    }
    return ierr;
}
/*!
 * @brief Sets the epochal time of the first time in the time series.
 * @param[in] t0   The epochal time of the first sample in UTC seconds.
 * @result 0 indicates success.
 * @ingroup SacHeaderpp
 */
int SacHeader::setEpochalStartTime(const double t0)
{
    int ierr = sacio_setEpochalStartTime(t0, &header_);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set start time\n", __func__);
    }
    return ierr;
}
/*!
 * @brief Gets the epochal time of the first sample in the time series.
 * @param[out] t0  The epochal time of the first sample in UTC seconds.
 * @result 0 indicates success.
 * @ingroup SacHeaderpp
 */
int SacHeader::getEpochalStartTime(double *t0) const
{
    int ierr = sacio_getEpochalStartTime(header_, t0);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to get start time\n", __func__);
    }
    return ierr;
}
/*!
 * @brief Gets the epochal time of the last sample in the time series.
 * @param[out] t1   The epochal time of the last sample in UTC seconds.
 * @result 0 indicates success.
 * @ingroup SacHeaderpp
 */
int SacHeader::getEpochalEndTime(double *t1) const
{
    int ierr = sacio_getEpochalEndTime(header_, t1);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to get end time\n", __func__);
    }
    return ierr;
}
//============================================================================//
//                            SAC Data Manager                                //
//============================================================================//
/*! 
 * @defgroup SacDatapp SAC C++ Waveform Data Manager 
 * @brief Manages the waveform data.
 * @ingroup Sacpp
 * @author Ben Baker
 * @copyright ISTI distributed under the Apache 2 license.
 */
/*!
 * @brief Data constructor.
 * @ingroup SacDatapp
 */
SacData::SacData(void) :
    data_(nullptr),
    nbytes_(0),
    npts_(0),
    ulp_(4)
{
    return;
}
/*!
 * @brief Copy constructor.
 * @ingroup SacDatapp
 */
SacData::SacData(const SacData &sacData)
{
    *this = sacData;
    return;
}
/*!
 * @brief Copy constructor.
 * @param[in[ sacData  SAC data from which to initialize the class.
 * @result A deep copy of the SAC data.
 * @ingroup SacDatapp
 */
SacData& SacData::operator=(const SacData &sacData)
{
    // Handle self-copy
    if (&sacData == this){return *this;}
    // Clear the class
    clear();
    // Do the copy
    double *data = nullptr;
    if (sacData.data_ != nullptr && sacData.nbytes_ > 0)
    {
        data = static_cast<double *> (aligned_alloc(64, sacData.nbytes_));
        memcpy(data, sacData.data_, sacData.nbytes_); 
    }
    // Now copy
    data_   = data;
    nbytes_ = sacData.nbytes_;
    npts_   = sacData.npts_;
    ulp_    = sacData.ulp_;
    return *this;
}
/*!
 * @brief SAC data destructor.
 * @ingroup SacDatapp
 */
SacData::~SacData(void)
{
    clear();
    return;
}
/*!
 * @brief Equality operator.  This compares digits to ulp_'s (units in last
 *        place).
 * @param[in] sacData  SAC data class to compare to this class.
 * @brief True indicates that the given class matches this class.
 * @ingroup SacDatapp
 */
bool SacData::operator==(const SacData &sacData) const
{
    if (&sacData == this){return true;}
    if (nbytes_ != sacData.nbytes_){return false;}
    if (npts_ != sacData.npts_){return false;}
    if (data_ != nullptr && sacData.data_ != nullptr)
    {
        for (int i=0; i<npts_; i++)
        {
            if (!almost_equal(data_[i], sacData.data_[i], ulp_)){return false;}
        }
    }
    return true;
}
/*!
 * @brief Inequality operator.  This compares digits to ulp_'s (units in last
 *        place).
 * @param[in] sacData  SAC data class to compare to this class.
 * @result True indicates that the given class does not match this class.
 * @ingroup SacDatapp
 */
bool SacData::operator!=(const SacData &sacData) const
{
    return !(*this == sacData);
}
/*!
 * @brief Deletes all data and sets internal variables to defaults.
 * @ingroup SacDatapp
 */
void SacData::clear(void)
{
    if (data_ != nullptr){free(data_);}
    data_ = nullptr;
    npts_ = 0;
    nbytes_ = 0;
    ulp_ = 4;
    return;
}
/*!
 * @brief Swaps the SAC data class sac1 with sac2.
 * @param[in,out] sac1   On exit this is sac2's data class.
 * @param[in,out] sac2   On exit this is sac1's data class.
 * @ingroup SacDatapp
 */
void swap(SacData &sac1, SacData &sac2)
{
    if (&sac1 == &sac2){return;}
    std::swap(sac1.data_,   sac2.data_);
    std::swap(sac1.nbytes_, sac2.nbytes_);
    std::swap(sac1.npts_,   sac2.npts_);
    std::swap(sac1.ulp_,    sac2.ulp_);
    return;
}
/*!
 * @brief Sets the time series data.
 * @param[in] npts   Number of data points to set.
 * @param[in] data   Data array to copy onto the class.  This is an array of
 *                   dimension [npts].
 * @ingroup SacDatapp
 */
int SacData::setData(const int npts, const double data[])
{
    // Reset the class
    clear();
    // Verify the inputs
    if (npts < 1 || data == nullptr)
    {
        if (npts < 1){fprintf(stderr, "%s: No points in data\n", __func__);}
        if (data == nullptr){fprintf(stderr, "%s: Data is NULL\n", __func__);}
        return -1;
    }
    // Set space 
    npts_ = npts;
    nbytes_ = computePaddedLength64f_(std::max(npts_, 8));
    data_ = static_cast<double *> (aligned_alloc(64, nbytes_));
    memset(data_, 0, nbytes_);
    memcpy(data_, data, static_cast<size_t>(npts)*sizeof(double));
    return 0;
}
/*!
 * @brief Sets the time series data.
 * @param[in] npts   Number of data points to set.
 * @param[in] data   Data array to copy onto the class.  This is an array of
 *                   dimension [npts].
 * @ingroup SacDatapp
 */
int SacData::setData(const int npts, const float data[])
{
    // Reset the class
    clear();
    // Verify the inputs
    if (npts < 1 || data == nullptr)
    {   
        if (npts < 1){fprintf(stderr, "%s: No points in data\n", __func__);}
        if (data == nullptr){fprintf(stderr, "%s: Data is NULL\n", __func__);}
        return -1; 
    }
    double *data64 = new double[npts];
#ifdef _OPENMP
    #pragma omp simd
#endif
    for (int i=0; i<npts; i++)
    {
        data64[i] = static_cast<double> (data[i]);
    }
    int ierr = setData(npts, data64);
    delete []data64;
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set data\n", __func__);
        return -1;
    }
    return 0;
}
/*!
 * @brief Sets the time series data.
 * @param[in] npts   Number of data points to set.
 * @param[in] data   Data array to copy onto the class.  This is an array of
 *                   dimension [npts].
 * @ingroup SacDatapp
 */
int SacData::setData(const int npts, const int data[])
{
    // Reset the class
    clear();
    // Verify the inputs
    if (npts < 1 || data == nullptr)
    {
        if (npts < 1){fprintf(stderr, "%s: No points in data\n", __func__);}
        if (data == nullptr){fprintf(stderr, "%s: Data is NULL\n", __func__);}
        return -1;
    }
    double *data64 = new double[npts];
#ifdef _OPENMP
    #pragma omp simd
#endif
    for (int i=0; i<npts; i++)
    {
        data64[i] = static_cast<double> (data[i]);
    }
    int ierr = setData(npts, data64);
    delete []data64;
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set data\n", __func__);
        return -1;
    }
    return 0;
}
/*!
 * @brief Reads the time series data from a SAC file.
 * @param[in] fileName  Name of SAC file to read.
 * @param[in] npts      An optional parameter.  If positive then this is the
 *                      known number of points to read.
 *                      Otherwise, the file size will be estimated from the
 *                      SAC file.
 * @result 0 indicates success.
 * @ingroup SacDatapp
 */
int SacData::readData(const std::string fileName, const int npts)
{
    int ierr = readData(fileName.c_str(), npts);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to read data\n", __func__);
        return -1;
    }
    return 0;
}
/*!
 * @brief Reads the time series data from a SAC file.
 * @param[in] fileName  Name of SAC file to read.
 * @param[in] npts      An optional parameter.  If positive then this is the
 *                      known number of points to read.
 *                      Otherwise, the file size will be estimated from the
 *                      SAC file.
 * @result 0 indicates success.
 * @ingroup SacDatapp
 */
int SacData::readData(const char *fileName, const int npts)
{
    // Clear the old structure
    clear();
    // Figure out size of data to be read 
    int nwork = -1; 
    int ierr;
    if (npts < 1)
    {
        ierr = sacio_readData(fileName, nwork, &npts_, data_);
        if (ierr != 0)
        {
            fprintf(stderr, "%s: Space query failed\n", __func__);
            npts_ = 0;
            return -1; 
        }
    }
    // Use the space hint
    else
    {
        npts_ = npts;
    }
    // Set space
    nwork = npts_;
    nbytes_ = computePaddedLength64f_(std::max(8, nwork));
    data_ = static_cast<double *> (aligned_alloc(64, nbytes_));
    memset(data_, 0, nbytes_);
    // Load data
    ierr = sacio_readData(fileName, nwork, &npts_, data_);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to load data\n", __func__);
        clear();
        return -1;
    }
    return 0;
}
/*!
 * @brief Gets the number of points in the waveform data.
 * @result The number of points in the waveform.
 * @ingroup SacDatapp
 */
int SacData::getNumberOfPoints(void) const
{
     return npts_;
}
/*!
 * @brief Gets a read-only pointer to the data.
 * @param[out] data  On successful exit this is a pointer to the data. 
 *                   It as an array of dimension of SacData::getNumberOfPoints.
 * @param[out] data  On failure this is a nullptr.
 * @retval  0 indicates success.
 * @retval -1 indicates that the data has not been set and data is a nullptr.
 * @ingroup SacDatapp
 */
int SacData::getConstantDataPtr(const double *data[]) const
{
    *data = nullptr;
    if (npts_ < 1)
    {
        fprintf(stderr, "%s: Data not yet set\n", __func__);
        return -1;
    }
    *data = data_;
    return 0;
}
/*!
 * @brief Gets a pointer to the data that can be modified.
 * @param[out] data  On successful exit this is a pointer to the data. 
 *                   It as an array of dimension of SacData::getNumberOfPoints.
 * @param[out] data  On failure this is a nullptr.
 * @retval  0 indicates success.
 * @retval -1 indicates that the data has not been set and data is a nullptr. 
 * @ingroup SacDatapp
 */
int SacData::getDataPtr(double *data[]) const
{
    *data = nullptr;
    if (npts_ < 1)
    {
        fprintf(stderr, "%s: Data not yet set\n", __func__);
        return -1;
    }
    *data = data_;
    return 0;
}
/*!
 * @brief Gets a copy of the SAC data.
 * @param[in] npts    Number of points allocated to data.
 * @param[out] data   On exit this contains a copy of the SAC data.
 * @ingroup SacDatapp
 */
int SacData::getData(const int npts, double data[]) const
{
    if (npts < npts_)
    {
        fprintf(stderr, "%s: npts=%d must be at least %d\n",
                 __func__, npts, npts_); 
        return -1;
    }
    size_t nzero = static_cast<size_t> (npts)*sizeof(double);
    size_t ncopy = static_cast<size_t> (npts_)*sizeof(double);
    if (nzero != ncopy){memset(data, 0, nzero);}
    memcpy(data, data_, ncopy);
    return 0;
}
//============================================================================//
//                             SAC Pole Zero                                  //
//============================================================================//
/*! 
 * @defgroup SacPZpp SAC C++ Poles and Zero Manager
 * @brief Manages the poles and zeros.
 * @ingroup Sacpp
 * @author Ben Baker
 * @copyright ISTI distributed under the Apache 2 license.
 */
/*!
 * @brief Default constructor.  
 * @ingroup SacPZpp
 */
SacPoleZero::SacPoleZero(void)
{
    memset(&pz_, 0, sizeof(struct sacPoleZero_struct));
    return;
}
/*!
 * @brief Initializes from a SacPoleZero class.
 * @param[in] pz   Pole zero structure from which this class will
 *                 be initialized.
 * @ingroup SacPZpp
 */
SacPoleZero::SacPoleZero(const SacPoleZero &pz)
{
    *this = pz;
    return;
}
/*!
 * @brief Initializes from a pole-zero structure.
 * @param[in] pz   SAC pole-zero structure.
 * @ingroup SacPZpp
 */
SacPoleZero::SacPoleZero(const struct sacPoleZero_struct pz)
{
    memset(&pz_, 0, sizeof(struct sacPoleZero_struct));
    sacio_copyPolesAndZeros(pz, &pz_); 
    return;
}
/*!
 * @brief Copy constructor.
 * @param[in] pz   SAC pole-zero from which to initialize.
 * @retval A deep copy of the SAC pole-zero structure.
 * @ingroup SacPZpp
 */
SacPoleZero& SacPoleZero::operator=(const SacPoleZero &pz)
{
    // Deal with self-copy
    if (&pz == this){return *this;}
    // Free the structure
    clear();
    // Copy the class
    sacio_copyPolesAndZeros(pz.pz_, &pz_);
    return *this;
} 
/*!
 * @brief Destructor.
 * @ingroup SacPZpp
 */
SacPoleZero::~SacPoleZero(void)
{
    clear();
    return;
}
/*!
 * @brief Swaps the SAC pole-zero class sac1 with sac2.
 * @param[in,out] sac1   On exit this is sac2's pole-zero class.
 * @param[in,out] sac2   On exit this is sac1's pole-zero class.
 * @ingroup SacPZpp
 */
void swap(SacPoleZero &sac1, SacPoleZero &sac2)
{
    if (&sac1 == &sac2){return;}
    swapChar64_(sac1.pz_.network,        sac2.pz_.network);
    swapChar64_(sac1.pz_.station,        sac2.pz_.station);
    swapChar64_(sac1.pz_.location,       sac2.pz_.location);
    swapChar64_(sac1.pz_.channel,        sac2.pz_.channel);
    swapChar64_(sac1.pz_.comment,        sac2.pz_.comment);
    swapChar64_(sac1.pz_.description,    sac2.pz_.description);
    swapChar64_(sac1.pz_.instrumentType, sac2.pz_.instrumentType);

    std::swap(sac1.pz_.poles,            sac2.pz_.poles);
    std::swap(sac1.pz_.zeros,            sac2.pz_.zeros);
    std::swap(sac1.pz_.constant,         sac2.pz_.constant);
    std::swap(sac1.pz_.created,          sac2.pz_.created);
    std::swap(sac1.pz_.start,            sac2.pz_.start);
    std::swap(sac1.pz_.end,              sac2.pz_.end);
    std::swap(sac1.pz_.latitude,         sac2.pz_.latitude);
    std::swap(sac1.pz_.longitude,        sac2.pz_.longitude);
    std::swap(sac1.pz_.elevation,        sac2.pz_.elevation);
    std::swap(sac1.pz_.depth,            sac2.pz_.depth);
    std::swap(sac1.pz_.dip,              sac2.pz_.dip);
    std::swap(sac1.pz_.azimuth,          sac2.pz_.azimuth);
    std::swap(sac1.pz_.sampleRate,       sac2.pz_.sampleRate);
    std::swap(sac1.pz_.instgain,         sac2.pz_.instgain);
    std::swap(sac1.pz_.sensitivity,      sac2.pz_.sensitivity);
    std::swap(sac1.pz_.a0,               sac2.pz_.a0);
    std::swap(sac1.pz_.npoles,           sac2.pz_.npoles);
    std::swap(sac1.pz_.nzeros,           sac2.pz_.nzeros);
    std::swap(sac1.pz_.inputUnits,       sac2.pz_.inputUnits);
    std::swap(sac1.pz_.instgainUnits,    sac2.pz_.instgainUnits);
    std::swap(sac1.pz_.sensitivityUnits, sac2.pz_.sensitivityUnits);
    std::swap(sac1.pz_.lhavePZ,          sac2.pz_.lhavePZ); 
    return;
}
/*!
 * @brief Gets a SAC pole-zero structure for use in C.
 * @param[out] pz   Pole-zero structure.
 * @ingroup SacPZpp
 */
void SacPoleZero::getSacPZStruct(struct sacPoleZero_struct *pz) const
{
    memset(pz, 0, sizeof(struct sacPoleZero_struct));
    sacio_copyPolesAndZeros(pz_, pz);
    return;
}
/*!
 * @brief Clears the pole-zeros.
 * @@ingroup SacPZpp
 */
void SacPoleZero::clear(void)
{
    sacio_freePoleZeros(&pz_);
    memset(&pz_, 0, sizeof(struct sacPoleZero_struct));
    return;
}
/*!
 * @brief Gets the number of poles.
 * @param[out] npoles  Number of poles in the instrument response.
 * @result 0 indicates success.
 * @ingroup SacPZpp
 */
int SacPoleZero::getNumberOfPoles(int *npoles) const
{
    *npoles = pz_.npoles;
    return 0;
} 
/*!
 * @brief Gets the number of zeros.
 * @param[out] nzeros  Number of zeros in the instrument response.
 * @result 0 indicates success.
 * @ingroup SacPZpp
 */
int SacPoleZero::getNumberOfZeros(int *nzeros) const
{
    *nzeros = pz_.nzeros;
    return 0;
}
/*!
 * @brief Reads the SAC pole-zero file.
 * @param[in] fileName  Name of SAC pole-zero file.
 * @result 0 indicates success.
 * @ingroup SacPZpp
 */
int SacPoleZero::readPolesAndZeros(const std::string fileName)
{
    int ierr = readPolesAndZeros(fileName.c_str());
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to read pole-zero file\n", __func__);
        return -1;
    }
    return 0;
}
/*!
 * @brief Reads the constant, \f$ c = a_0 \times \f$gain, such that
 *        the transfer function, \f$ H(s) =  c \frac{z(s)}{p(s)} \f$.
 * @param[out] constant  The constant in the system transfer function.
 * @result 0 indicates success.
 * @ingroup SacPZpp
 */
int SacPoleZero::getConstant(double *constant) const
{
    *constant = pz_.constant;
    return 0;
}
/*!
 * @brief Reads the SAC pole-zero file.
 * @param[in] fileName  Name of SAC pole-zero file.
 * @result 0 indicates success.
 * @ingroup SacPZpp
 */
int SacPoleZero::readPolesAndZeros(const char *fileName)
{
    clear();
    int ierr = sacio_readPoleZeroFile(fileName, &pz_);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to read file %s\n", __func__, fileName);
        return -1;
    }
    return 0;
}
/*!
 * @brief Gets the poles.
 * @param[out] poles   The poles of the transfer function.
 * @result 0 indicates success.
 * @ingroup SacPZpp
 */
int SacPoleZero::getPoles(std::vector<std::complex<double>> &poles) const
{
    int ierr, npoles; 
    getNumberOfPoles(&npoles);
    poles.resize(npoles);
    if (npoles < 1){return 0;}
    double *re = new double[std::max(8, npoles)];
    double *im = new double[std::max(8, npoles)];
    ierr = getPoles(npoles, re, im);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to get poles\n", __func__);
        poles.resize(0);
        delete[] re;
        delete[] im; 
    }
    for (int ip=0; ip<npoles; ip++)
    {
          std::complex<double> z(re[ip], im[ip]);
          poles[ip] = z;
    }  
    delete[] re;
    delete[] im;
    return 0;
}
/*!
 * @brief Gets the zeros.
 * @param[out] zeros   The zeros of the transfer function.
 * @result 0 indicates success.
 * @ingroup SacPZpp
 */
int SacPoleZero::getZeros(std::vector<std::complex<double>> &zeros) const
{
    int ierr, nzeros;
    getNumberOfZeros(&nzeros);
    zeros.resize(nzeros);
    if (nzeros < 1){return 0;}
    double *re = new double[std::max(8, nzeros)];
    double *im = new double[std::max(8, nzeros)];
    ierr = getZeros(nzeros, re, im);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to get zeros\n", __func__);
        zeros.resize(0);
        delete[] re;
        delete[] im;
    }
    for (int ip=0; ip<nzeros; ip++)
    {
          std::complex<double> z(re[ip], im[ip]);
          zeros[ip] = z;
    }
    delete[] re;
    delete[] im;
    return 0;
}
/*!
 * @brief Gets the poles.
 * @param[in] npoles  Number of poles.  This should match
 *                    SacPoleZero::getNumberOfPoles.
 * @param[out] re     The real part of the poles.  This has dimension [npoles].
 * @param[out] im     The imaginary part o the poles.  This has dimension
 *                    [npoles].
 * @result 0 indicates success.
 * @ingroup SacPZpp
 */
int SacPoleZero::getPoles(const int npoles, double *re, double *im) const
{
    if (npoles < pz_.npoles)
    {
        fprintf(stderr, "%s: Insufficient space; resizes npoles to %d\n",
                __func__, npoles);
        return -1;
    }
    if (pz_.npoles < 1){return 0;}
    if (re == nullptr || im == nullptr)
    {
        if (re == nullptr){fprintf(stderr, "%s: re is null\n", __func__);}
        if (im == nullptr){fprintf(stderr, "%s: im is null\n", __func__);}
        return -1;
    }
    if (pz_.poles == nullptr || pz_.poles == NULL)
    {
        fprintf(stderr, "%s: Internal error\n", __func__);
        return -1;
    }
    memset(re, 0, static_cast<size_t> (npoles)*sizeof(double));
    memset(im, 0, static_cast<size_t> (npoles)*sizeof(double));
    const double *poles = static_cast<const double *> (pz_.poles);
    for (int i=0; i<npoles; i++)
    {
        re[i] = poles[2*i+0];
        im[i] = poles[2*i+1];
    }
    poles = nullptr;
    return 0; 
}
/*!
 * @brief Gets the zeros.
 * @param[in] nzeros  Number of zeros.  This should match
 *                    SacPoleZero::getNumberOfZeros.
 * @param[out] re     The real part of the zeros.  This has dimension [nzeros].
 * @param[out] im     The imaginary part o the zeros.  This has dimension
 *                    [nzeros].
 * @result 0 indicates success.
 * @ingroup SacPZpp
 */
int SacPoleZero::getZeros(const int nzeros, double *re, double *im) const
{
    if (nzeros < pz_.nzeros)
    {
        fprintf(stderr, "%s: Insufficient space; resizes nzeros to %d\n",
                __func__, nzeros);
        return -1;
    }
    if (pz_.nzeros < 1){return 0;}
    if (re == nullptr || im == nullptr)
    {
        if (re == nullptr){fprintf(stderr, "%s: re is null\n", __func__);}
        if (im == nullptr){fprintf(stderr, "%s: im is null\n", __func__);}
        return -1;
    }
    if (pz_.zeros == nullptr || pz_.zeros == NULL)
    {
        fprintf(stderr, "%s: Internal error\n", __func__);
        return -1;
    }
    memset(re, 0, static_cast<size_t> (nzeros)*sizeof(double));
    memset(im, 0, static_cast<size_t> (nzeros)*sizeof(double));
    const double *zeros = static_cast<const double *> (pz_.zeros);
    for (int i=0; i<nzeros; i++) 
    {
        re[i] = zeros[2*i+0];
        im[i] = zeros[2*i+1];
    }
    zeros = nullptr;
    return 0; 
}
/*!
 * @brief Private function for swapping the character parts of the header.
 * @ingroup SacPZpp
 */
inline void swapChar64_(char a[64], char b[64])
{
    char temp[64];
    memcpy(a, temp, 64*sizeof(char));
    memcpy(b, a,    64*sizeof(char));
    memcpy(temp, b, 64*sizeof(char));
    return;
}
//============================================================================//
//                              SAC Manager                                   //
//============================================================================//
/*!
 * @defgroup Sacpp SAC C++ class for SAC data and header management.
 * @brief The SAC data structure class for C++.
 * @author Ben Baker
 * @copyright ISTI distributed under the Apache 2 license.
 */
/*!
 * @brief Class constructor.
 * @ingroup Sacpp
 */
Sac::Sac(void)
{
    clear();
    return;
}
/*!
 * @brief Copy constructor.
 * @param[in] sac  SAC class from which to initialize.
 * @ingroup Sacpp
 */
Sac::Sac(const Sac &sac)
{
    *this = sac;
    return;
}
/*!
 * @brief SAC class copy constructor.
 * @param[in] sac   SAC class from which to initialize.
 * @result A deep copy of the input SAC class.
 * @ingroup Sacpp
 */
Sac& Sac::operator=(const Sac &sac)
{
    // Handle self copy
    if (&sac == this){return *this;}
    // Clear the original data structure 
    clear();
    // Copy
    header_ = sac.header_;
    data_   = sac.data_;
    pz_     = sac.pz_;
    return *this;
}
/*!
 * @brief Initializes a SAC class from a SAC data structure.
 * @param[in] sac   SAC data structure from which to initialize class.
 * @ingroup Sacpp
 */
Sac::Sac(const struct sacData_struct sac)
{
    // Reset the class
    clear();
    // Ensure the array sizes are consistent
    int npts;
    int ierr = sacio_getIntegerHeader(SAC_INT_NPTS, sac.header, &npts);
    if (ierr != 0 || npts != sac.npts)
    {
        if (ierr != 0){fprintf(stderr, "%s: Failed to read npts\n", __func__);}
        if (npts != sac.npts)
        {
            fprintf(stderr, "%s: Size inconsistency\n", __func__);
        }
        return;
    }
    // Initialize the header
    header_.clear();
    header_ = SacHeader(sac.header);
    // Copy poles and zeros
    pz_ = SacPoleZero(sac.pz);
    // Copy the data
    if (npts > 0)
    {
        ierr = data_.setData(sac.npts, sac.data);
        if (ierr != 0)
        {
            fprintf(stderr, "%s: Failed to set data\n", __func__);
            clear();
        }
    }
    return;
}
/*!
 * @brief Class deconstructor.
 * @ingroup Sacpp
 */
Sac::~Sac(void)
{
    clear();
    return;
}
/*!
 * @brief Equality operator.
 * @param[in] sac   SAC class to compare against.
 * @result True indicates the given SAC class, sac, equals this class. 
 * @ingroup Sacpp
 */
bool Sac::operator==(const Sac &sac) const
{
    if (&sac == this){return true;}
    if (header_ != sac.header_){return false;}
    if (data_ != sac.data_){return false;}
    return true;
}
/*!
 * @brief Inequality operator.
 * @param[in] sac   SAC class to compare against.
 * @result True indicates the given SAC class, sac, does not equal this class. 
 * @ingroup Sacpp
 */
bool Sac::operator!=(const Sac &sac) const
{
    return !(*this == sac);
}
/*!
 * @brief Clears the data and header information.
 * @ingroup Sacpp
 */
void Sac::clear(void)
{
    header_.clear();
    data_.clear();
    pz_.clear();
    return;
}
/*!
 * @brief Swaps two SAC data structures.
 */
void swap(Sac &sac1, Sac &sac2)
{
    if (&sac1 == &sac2){return;}
    swap(sac1.header_, sac2.header_);
    swap(sac1.data_,   sac2.data_); 
    swap(sac1.pz_,     sac2.pz_);
    return;
}
/*!
 * @brief Prints the header.
 * @param[in] file  The file handle to which the header information will be
 *                  printed.  If this is a nullptr then the information 
 *                  will be written to standard out.
 * @result 0 indicates success.
 * @ingroup Sacpp
 */
int Sac::printHeader(FILE *file)
{
    int ierr = header_.print(file);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Errors encountered while printing header\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacData::getConstantDataPtr
 * @ingroup Sacpp
 */
int Sac::getConstantDataPtr(const double *data[]) const
{
    int ierr = data_.getConstantDataPtr(data);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to get data pointer\n", __func__);
    }
    return ierr;
}
/*!
 * @brief Gets the sampling period of the signal.
 * @param[out] dt   Sampling period in seconds.
 * @result 0 indicates success
 * @ingroup Sacpp
 */
int Sac::getSamplingPeriod(double *dt) const
{
    *dt = 0;
    int ierr = header_.getHeader(SacHeader::Double::DELTA, dt);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to get sampling period\n", __func__);
        return -1;
    }
    return 0;
} 
/*!
 * @brief Gets the number of points in the time series.
 * @param[out] npts  Number of points in time series.
 * @result 0 indicates success.
 * @ingroup Sacpp
 */
int Sac::getNumberOfPoints(int *npts) const
{
    *npts = 0;
    int npdata = data_.getNumberOfPoints();
    if (npdata > 0)
    {
        int nphdr;
        int ierr = header_.getIntegerHeader(SacHeader::Integer::NPTS, &nphdr);
        if (ierr != 0)
        {
            fprintf(stderr, "%s: Failed to read npts in header\n", __func__);
            return -1;
        }
        if (npdata != nphdr)
        {
            fprintf(stderr, "%s; Size mismatch\n", __func__);
            return -1;
        }
    }
    *npts = npdata;
    return 0;
}
/*!
 * @copydoc SacData::getDataPtr
 * @ingroup Sacpp
 */
int Sac::getDataPtr(double *data[]) const
{
    int ierr = data_.getDataPtr(data);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to get data pointer\n", __func__);
    }
    return ierr;
}
/*!
 * @brief Sets the epochal time of the first time in the time series.
 * @param[in] t0   The epochal time of the first sample in UTC seconds.
 * @result 0 indicates success.
 * @ingroup Sacpp
 */
int Sac::setEpochalStartTime(const double t0)
{
    int ierr = header_.setEpochalStartTime(t0);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set start time\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::getEpochalStartTime
 * @ingroup Sacpp
 */
int Sac::getEpochalStartTime(double *t0) const
{
    int ierr = header_.getEpochalStartTime(t0);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to get start time\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::getEpochalEndTime
 * @ingroup Sacpp
 */
int Sac::getEpochalEndTime(double *t1) const
{
    int ierr = header_.getEpochalEndTime(t1);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to get end time\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::setIntegerHeader 
 * @ingroup Sacpp
 */
int Sac::setHeader(const enum sacIntegerHeader_enum hvar, const int hdr)
{
    int ierr = header_.setIntegerHeader(hvar, hdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set header\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::getIntegerHeader 
 * @ingroup Sacpp
 */
int Sac::getHeader(const enum sacIntegerHeader_enum hvar, int *hdr) const
{
    return header_.getIntegerHeader(hvar, hdr);
}
/*!
 * @copydoc SacHeader::getIntegerHeader
 * @ingroup Sacpp
 */
int Sac::getIntegerHeader(const enum sacIntegerHeader_enum hvar,
                          int *hdr) const
{
    return header_.getIntegerHeader(hvar, hdr);
}
/*!
 * @copydoc SacHeader::setIntegerHeader 
 * @ingroup Sacpp
 */
int Sac::setIntegerHeader(const enum sacIntegerHeader_enum hvar,
                          const int hdr)
{
    int ierr = header_.setIntegerHeader(hvar, hdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set header\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::setBooleanHeader
 * @ingroup Sacpp
 */
int Sac::setHeader(const enum sacBooleanHeader_enum hvar, const bool hdr)
{
    int ierr = header_.setBooleanHeader(hvar, hdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set header\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::getBooleanHeader
 * @ingroup Sacpp
 */
int Sac::getHeader(const enum sacBooleanHeader_enum hvar, bool *hdr) const
{
    return header_.getBooleanHeader(hvar, hdr);
}
/*!
 * @copydoc SacHeader::getBooleanHeader
 * @ingroup Sacpp
 */
int Sac::getBooleanHeader(const enum sacBooleanHeader_enum hvar,
                          bool *hdr) const
{
    return header_.getBooleanHeader(hvar, hdr);
}
/*!
 * @copydoc SacHeader::setBooleanHeader
 * @ingroup Sacpp
 */
int Sac::setBooleanHeader(const enum sacBooleanHeader_enum hvar,
                          const bool hdr)
{
    int ierr = header_.setBooleanHeader(hvar, hdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set header\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::setDoubleHeader
 * @ingroup Sacpp
 */
int Sac::setHeader(const enum sacDoubleHeader_enum hvar, const double hdr)
{
    return header_.setDoubleHeader(hvar, hdr);
}
/*!
 * @copydoc SacHeader::getDoubleHeader
 * @ingroup Sacpp
 */
int Sac::getHeader(const enum sacDoubleHeader_enum hvar, double *hdr) const
{
    return header_.getDoubleHeader(hvar, hdr);
}
/*!
 * @copydoc SacHeader::getDoubleHeader
 * @ingroup Sacpp
 */
int Sac::getDoubleHeader(const enum sacDoubleHeader_enum hvar,
                         double *hdr) const
{
    return header_.getDoubleHeader(hvar, hdr);
}
/*!
 * @copydoc SacHeader::setDoubleHeader
 * @ingroup Sacpp
 */
int Sac::setDoubleHeader(const enum sacDoubleHeader_enum hvar,
                         const double hdr)
{
    int ierr =  header_.setDoubleHeader(hvar, hdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set header\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::setFloatHeader
 * @ingroup Sacpp
 */
int Sac::setHeader(const enum sacFloatHeader_enum hvar, const float hdr)
{
    int ierr = header_.setFloatHeader(hvar, hdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set header\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::getFloatHeader
 * @ingroup Sacpp
 */
int Sac::getHeader(const enum sacFloatHeader_enum hvar, float *hdr) const
{
    return header_.getFloatHeader(hvar, hdr);
}
/*!
 * @copydoc SacHeader::getFloatHeader
 * @ingroup Sacpp
 */
int Sac::getFloatHeader(const enum sacFloatHeader_enum hvar,
                        float *hdr) const
{
    return header_.getFloatHeader(hvar, hdr);
}
/*!
 * @copydoc SacHeader::setFloatHeader
 * @ingroup Sacpp
 */
int Sac::setFloatHeader(const enum sacFloatHeader_enum hvar,
                        const float hdr)
{
    int ierr = header_.setFloatHeader(hvar, hdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set header\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::setStringHeader
 * @ingroup Sacpp
 */
int Sac::setHeader(const enum sacStringHeader_enum hvar, const std::string hdr)
{
    int ierr = header_.setStringHeader(hvar, hdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set header\n", __func__);
    }
    return ierr;
}
/*!
 * @copydoc SacHeader::setStringHeader
 * @ingroup Sacpp
 */
int Sac::getHeader(const enum sacStringHeader_enum hvar,
                   std::string &hdr) const
{
    return header_.getStringHeader(hvar, hdr);
}
/*!
 * @copydoc SacHeader::getStringHeader
 * @ingroup Sacpp
 */
int Sac::getStringHeader(const enum sacStringHeader_enum hvar,
                         std::string &hdr) const
{
    return header_.getStringHeader(hvar, hdr);
}
/*!
 * @copydoc SacHeader::setStringHeader
 * @ingroup Sacpp
 */
int Sac::setStringHeader(const enum sacStringHeader_enum hvar,
                         const std::string hdr)
{
    int ierr = header_.setStringHeader(hvar, hdr);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to set header\n", __func__);
    }
    return ierr;
}

/*!
 * @brief Reads a SAC data file.
 * @param[in] fileName  Name of file to read.
 * @result 0 indicates success.
 * @ingroup Sacpp
 */
int Sac::readFile(const std::string fileName)
{
   int ierr = readFile(fileName.c_str());
   if (ierr != 0)
   {
       fprintf(stderr, "%s: Failed to read file\n", __func__);
       return -1;
   }
   return 0;
}
/*!
 * @brief Reads a SAC data file.
 * @param[in] fileName  Name of file to read.
 * @result 0 indicates success.
 * @ingroup Sacpp
 */
int Sac::readFile(const char *fileName)
{
    // Require the file exists
    if (!sacio_os_path_isfile(fileName))
    {
         fprintf(stderr, "%s: File %s does not exist\n", __func__, fileName);
         return -1;
    }
    // Reset the data
    clear();
    // Read the header
    int ierr = header_.readHeader(fileName);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to read header\n", __func__);
        clear();
        return -1;
    }
    // Read the time series data
    int npts = 0;
    ierr = header_.getIntegerHeader(SacHeader::Integer::NPTS, &npts);
    if (ierr != 0 || npts < 1)
    {
        fprintf(stderr, "%s: Failed to get npts\n", __func__);
        clear();
        return -1;
    }
    ierr = data_.readData(fileName, npts);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to read data\n", __func__);
        clear();
        return -1;
    }
    return 0;
}
/*!
 * @copydoc SacPoleZero::readPolesAndZeros
 * @ingroup Sacpp
 */
int Sac::readPolesAndZeros(const std::string fileName)
{
    int ierr = pz_.readPolesAndZeros(fileName);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to read pole-zero file\n", __func__);
    }
    return ierr; 
}
/*!
 * @brief Writes a time series for the data in the SAC class.
 * @param[in] fileName  Name of file to write.
 * @result 0 indicates success.
 * @ingroup Sacpp
 */
int Sac::writeFile(const char *fileName)
{
    struct sacData_struct sac;
    // Require the ifle name exists
    if (fileName == nullptr)
    {
        fprintf(stderr, "%s: File name does not exist\n", __func__);
        return -1;
    }
    if (strlen(fileName) == 0)
    {
        fprintf(stderr, "%s: File name is empty\n", __func__);
        return -1;
    }
    memset(&sac, 0, sizeof(struct sacData_struct));
    // Copy the header and get a pointer to the data
    header_.makeSacHeaderStruct(&sac.header);
    sac.npts = data_.getNumberOfPoints();
    if (sac.npts < 1)
    {
        fprintf(stdout, "%s: No data to write\n", __func__);
    }
    data_.getDataPtr(&sac.data);
    // Write the time series file 
    int ierr = sacio_writeTimeSeriesFile(fileName, sac);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to write time series\n", __func__);
    }
    // Derefence pointers 
    memset(&sac, 0, sizeof(struct sacData_struct));
    return ierr;
}
/*!
 * @brief Writes a time series for the data in the SAC class.
 * @param[in] fileName  Name of file to write.
 * @result 0 indicates success.
 * @ingroup Sacpp
 */
int Sac::writeFile(const std::string fileName)
{
     int ierr = writeFile(fileName.c_str());
     if (ierr != 0)
     {
         fprintf(stderr, "%s: Failed to write file\n", __func__);
     }
     return ierr;
}
/*!
 * @brief Gets the poles, zeros, and the constant.
 * @param[out] poles   The poles.
 * @param[out] zeros   The zeros.
 * @param[out] k       The constant.
 * @result 0 indicates success.
 * @ingroup Sacpp
 */
int Sac::getPolesZerosAndConstant(std::vector<std::complex<double>> &zeros,
                                  std::vector<std::complex<double>> &poles,
                                  double *k) const
{
    int ierr;
    ierr = pz_.getPoles(poles);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to get poles\n", __func__);
        return -1; 
    }
    ierr = pz_.getZeros(zeros);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to get zeros\n", __func__);
        return -1;
    }
    ierr = pz_.getConstant(k);
    if (ierr != 0)
    {
        fprintf(stderr, "%s: Failed to get constant\n", __func__);
        return -1;
    }
    return ierr;
}
/*!
 * @copydoc SacData::setData
 * @ingroup Sacpp
 */
int Sac::setData(const int npts, const double data[])
{
    int nptsSet = npts;
    int ierr = data_.setData(nptsSet, data);
    if (ierr != 0)
    {
        nptsSet = 0;
        fprintf(stderr, "%s: Failed to set data\n", __func__);
    }
    header_.setHeader(SacHeader::Integer::NPTS, nptsSet);
    return ierr;
}
/*!
 * @copydoc SacHeader::getSacHeaderStruct
 * @ingroup Sacpp
 */
void Sac::getSacHeaderStruct(struct sacHeader_struct *header) const
{
    header_.getSacHeaderStruct(header);
    return;
}
/*!
 * @copydoc SacPoleZero::getSacPZStruct
 * @ingroup Sacpp
 */
void Sac::getSacPZStruct(struct sacPoleZero_struct *pz) const
{
    pz_.getSacPZStruct(pz);
    return;
}
/*!
 * @brief Gets a SAC data structure from the class.
 * @param[out] sac   C-style SAC data structure corresponding to the class.
 * @ingroup Sacpp
 */
void Sac::getSacDataStruct(struct sacData_struct *sac) const
{
    memset(sac, 0, sizeof(struct sacData_struct));
    header_.getSacHeaderStruct(&sac->header);
    pz_.getSacPZStruct(&sac->pz);
    sac->npts = sac->header.npts;
    if (sac->npts > 0)
    {
        sac->data = sacio_malloc64f(sac->npts);
        data_.getData(sac->npts, sac->data);
    } 
    return;
}
