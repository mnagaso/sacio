# About

[sacio](https://bbaker.pages.isti.com/sacio/index.html) is a utility for reading and writing [SAC](https://ds.iris.edu/files/sac-manual/manual/file_format.html) time series data files.  This library differs from the original sacio (http://ds.iris.edu/ds/nodes/dmc/software/downloads/sac/) in that it does not require the SAC\_AUX environment variable to be set (because this library does not use it), it can naturally be compiled as a shared library, and by by virtue of its Apache 2 license overcomes awkward SAC licensing and redistrubution issues.

The true utility of this library however is that it treats the SAC time series files as data structures.  This means that header variables can be accessed or modified in memory very easily.  This strategy also allows one to keep close the instrument response file to the time series.

Treating the SAC time series file as a data structure is advantageous in two ways.  First, the data structure can be used to extend the useful SAC file format to the self-describing HDF5 high-performance IO library.  The benefit of this is that the generation of many SAC files is avoided which, in turn, benefits file-system performance.  Second, the data structures can be used in defining point-to-point and global communications for the Message Passing Interface.  This is important when working on distributed memory systems.

This library was written by [ISTI](www.isti.com) and is distributed under the Apache 2 license.
