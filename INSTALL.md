# Building the Library

This page reviews the steps for building the library.

## Prerequisites

  - A C11 compliant C compiler.  This may be possible to work around if one enables the -DUSE\_POSIX compiler flag on a POSIX system.

  - A C++11 or greater compliant C++ compiler.
 
  - CMake v 2.8
 
  - Optionally, SAC structures can be saved to [HDF5](https://support.hdfgroup.org/HDF5/).  To enable this functionality compile with SACIO\_USE\_HDF5.

  - Optionally, for primitives on point-to-point and global communication of SAC structures via [MPI](https://www.open-mpi.org/) one can compile with  SACIO\_USE\_MPI.

  - Optionally, [doxygen](https://www.stack.nl/~dimitri/doxygen/) for generation of API documentation.

## Configuring

The most expedient way to configure CMake is by way of build configuration scripts run in the root source directory.

### The GCC Minimal Configuration
sacio can be built with the GCC compilers but without extra features like HDF5 and MPI by specifying something like

    #!/bin/sh
    if [ -f Makefile ]; then
       make clean
    fi
    if [ -f CMakeCache.txt ]; then
       echo "Removing CMakeCache.txt"
       rm CMakeCache.txt
    fi
    if [ -d CMakeFiles ]; then
       echo "Removing CMakeFiles"
       rm -rf CMakeFiles
    fi
    cmake ./ \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=./ \
    -DCMAKE_C_FLAGS="-g -O2 -Wall" \
    -DCMAKE_CXX_FLAGS="-g -O2 -Wall"

### The Intel Full Configuration

Alternatively, sacio can be built with the Intel compilers, HDF5 (SACIO\_USE\_HDF5=TRUE) , and MPI (SACIO\_USE\_MPI=TRUE) by specifying something like

    #!/bin/sh
    export CC=icc
    export CXX=icpc
    export MPI_C_COMPILER=mpiicc
    export MPI_CXX_COMPILER=mpicpc
    if [ -f Makefile ]; then
       make clean
    fi
    if [ -f CMakeCache.txt ]; then
       echo "Removing CMakeCache.txt"
       rm CMakeCache.txt
    fi
    if [ -d CMakeFiles ]; then
       echo "Removing CMakeFiles"
       rm -rf CMakeFiles
    fi
    cmake ./ \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=./ \
    -DCMAKE_C_COMPILER=icc \
    -DCMAKE_CXX_COMPILER=icpc \
    -DMPI_C_INCLUDE_PATH=/path/to/mpi/include \
    -DMPI_C_LIBRARIES=/path/to/mpi/lib/libmpi.so \
    -DCMAKE_C_FLAGS="-g3 -O2 -Wall -Wextra -Wcomment -Wunused -Wcheck" \
    -DCMAKE_CXX_FLAGS="-g3 -O2 -Wall -Wextra -Wcomment -Wunused -Wcheck" \
    -DSACIO_USE_HDF5=TRUE \
    -DSACIO_USE_MPI=TRUE \
    -DH5_C_INCLUDE_DIR=/path/to/HDF5/include \
    -DH5_LIBRARY=/home/path/to/HDF5/lib/libhdf5_shared.so

### GCC Full Configuration With Incomplete C11 Support

In the instance that the system does not have a fully functional C11 and is posix then it may be possible to allocate memory (-DUSE\_POSIX) with something like
 
    #!/bin/sh
    export CC=icc
    export CXX=icpc
    export MPI_C_COMPILER=mpiicc
    export MPI_CXX_COMPILER=mpicpc
    if [ -f Makefile ]; then
       make clean
    fi
    if [ -f CMakeCache.txt ]; then
       echo "Removing CMakeCache.txt"
       rm CMakeCache.txt
    fi
    if [ -d CMakeFiles ]; then
       echo "Removing CMakeFiles"
       rm -rf CMakeFiles
    fi
    cmake ./ \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=./ \
    -DMPI_C_INCLUDE_PATH=/path/to/mpi/include \
    -DMPI_C_LIBRARIES=/path/to/mpi/lib/libmpi.so \
    -DCMAKE_C_FLAGS="-g3 -O2 -Wall -Wextra -Wcomment -fopenmp -DUSE_POSIX" \
    -DSACIO_USE_HDF5=TRUE \
    -DSACIO_USE_MPI=TRUE \
    -DH5_C_INCLUDE_DIR=/path/to/HDF5/include \
    -DH5_LIBRARY=/home/path/to/HDF5/lib/libhdf5_shared.so

## Building

After CMake is correctly configured one would type in the source root directory

    make
    make install

## Generating the Doxygen Documentation

To generate the Doyxgen documentation one would type in the source root directory

    doxygen Doxyfile
