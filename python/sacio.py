#!/usr/bin/env python3
"""
Python interface to the sacio library.

Copyright: ISTI distributed under the Apache 2 license.
"""
import os
from ctypes import cdll
from ctypes import byref
from ctypes import Structure
from ctypes import create_string_buffer
from ctypes import POINTER
from ctypes import c_double
from ctypes import c_void_p
from ctypes import c_int
from ctypes import c_char_p
from ctypes import c_char
from ctypes import c_bool

# Enumerated types
( 
    SAC_UNKNOWN_UNITS,            #/*!< No idea */
    SAC_METERS,                   #/*!< Displacement - Meters */
    SAC_NANOMETERS,               #/*!< Displacement - Nanometers */
    SAC_METERS_SECOND,            #/*!< Velocity - meters/second */
    SAC_NANOMETERS_SECOND,        #/*!< Velocity - nanometers/second */
    SAC_METERS_SECOND_SECOND,     #/*!< Acceleration - meters/second^2 */
    SAC_NANOMETERS_SECOND_SECOND  #/*!< Acceleration - nanometers/second^2 */
) = map(c_int, range(7))

(
    # Floats
    SAC_FLOAT_DELTA,  #/*!< Sampling period. */
    SAC_FLOAT_DEPMIN, #/*!< Minimum value of dependent variable. */
    SAC_FLOAT_DEPMAX, #/*!< Maximum value of dependent variable. */
    SAC_FLOAT_SCALE,  #/*!< Scale factor multiplying dependent variable. */
    SAC_FLOAT_ODELTA, #/*!< Observed increment; if different than delta. */
    SAC_FLOAT_B,      #/*!< Beginning value of dependent variable. */
    SAC_FLOAT_E,      #/*!< Ending value of dependent variable. */
    SAC_FLOAT_O,      #/*!< Origin time (seconds relative to reference time. */
    SAC_FLOAT_A,      #/*!< First arrival time relative to reference time. */
    SAC_FLOAT_INTERNAL1,
    SAC_FLOAT_T0,     #/*!< First user defined pick time or marker.
    SAC_FLOAT_T1,     #/*!< Second user defined pick time or marker. */
    SAC_FLOAT_T2,     #/*!< Third user defined pick time or marker. */
    SAC_FLOAT_T3,     #/*!< Fourth user defined pick time or marker. */
    SAC_FLOAT_T4,     #/*!< Fifth user defined pick time or marker. */
    SAC_FLOAT_T5,     #/*!< Sixth user defined pick time or marker. */
    SAC_FLOAT_T6,     #/*!< Seventh user defined pick time or marker. */
    SAC_FLOAT_T7,     #/*!< Eighth user defined pick time or marker. */
    SAC_FLOAT_T8,     #/*!< Ninth user defined pick time or marker. */
    SAC_FLOAT_T9,     #/*!< Tenth user defined pick time or marker. */
    SAC_FLOAT_F,      #/*!< End time of event. */
    SAC_FLOAT_RESP0,  #/*!< First instrument response parameter. */
    SAC_FLOAT_RESP1,  #/*!< Second instrument response parameter. */
    SAC_FLOAT_RESP2,  #/*!< Third instrument response parameter. */
    SAC_FLOAT_RESP3,  #/*!< Fourth instrument response parameter. */
    SAC_FLOAT_RESP4,  #/*!< Fifth instrument response parameter. */
    SAC_FLOAT_RESP5,  #/*!< Sixth instrument response parameter. */
    SAC_FLOAT_RESP6,  #/*!< Seventh instrument response parameter. */
    SAC_FLOAT_RESP7,  #/*!< Eigth instrument response parameter. */
    SAC_FLOAT_RESP8,  #/*!< Ninth instrument response parameter. */
    SAC_FLOAT_RESP9,  #/*!< Tenth instrument response parameter. */
    SAC_FLOAT_STLA,   #/*!< Station latitude. */
    SAC_FLOAT_STLO,   #/*!< Station longitude. */
    SAC_FLOAT_STEL,   #/*!< Station elevation. */
    SAC_FLOAT_STDP,   #/*!< Station depth. */
    SAC_FLOAT_EVLA,   #/*!< Event latitude. */
    SAC_FLOAT_EVLO,   #/*!< Event longitude. */
    SAC_FLOAT_EVEL,   #/*!< Event elevation. */
    SAC_FLOAT_EVDP,   #/*!< Event depth. */
    SAC_FLOAT_MAG,    #/*!< Event magnitude. */
    SAC_FLOAT_USER0,  #/*!< First user defined variable. */
    SAC_FLOAT_USER1,  #/*!< Second user defined variable. */
    SAC_FLOAT_USER2,  #/*!< Third user defined variable. */
    SAC_FLOAT_USER3,  #/*!< Fourth user defined variable. */
    SAC_FLOAT_USER4,  #/*!< Fifth user defined variable. */
    SAC_FLOAT_USER5,  #/*!< Sixth user defined variable. */
    SAC_FLOAT_USER6,  #/*!< Seventh user defined variable. */
    SAC_FLOAT_USER7,  #/*!< Eighth user defined variable. */
    SAC_FLOAT_USER8,  #/*!< Ninth user defined variable. */
    SAC_FLOAT_USER9,  #/*!< Tenth user defined variable. */
    SAC_FLOAT_DIST,   #/*!< Source-receiver distance. */
    SAC_FLOAT_AZ,     #/*!< Source-receiver azimuth. */
    SAC_FLOAT_BAZ,    #/*!< Source-receiver back-azimuth. */
    SAC_FLOAT_GCARC,  #/*!< Source-receiver great circle distance. */
    SAC_FLOAT_INTERNAL2, #/*!< Internal variable. */
    SAC_FLOAT_DEPMEN,    #/*!< Mean value of dependent variable. */
    SAC_FLOAT_CMPAZ,     #/*!< Component azimuth. */
    SAC_FLOAT_CMPINC,    #/*!< Component incident angle. */
    SAC_FLOAT_XMINIMUM,  #/*!< Minimum value of x (spectral files only). */
    SAC_FLOAT_XMAXIMUM,  #/*!< Maximum value of x (spectral files only). */
    SAC_FLOAT_YMINIMUM,  #/*!< Minimum value of y (spectral files only). */
    SAC_FLOAT_YMAXIMUM,  #/*!< Maximum value of y (spectral files only). */
    SAC_FLOAT_UNUSED0,   #/*!< Extra float variable. */
    SAC_FLOAT_UNUSED1,   #/*!< Extra float variable. */
    SAC_FLOAT_UNUSED2,   #/*!< Extra float variable. */
    SAC_FLOAT_UNUSED3,   #/*!< Extra float variable. */
    SAC_FLOAT_UNUSED4,   #/*!< Extra float variable. */
    SAC_FLOAT_UNUSED5,   #/*!< Extra float variable. */
    SAC_FLOAT_UNUSED6,   #/*!< Extra float variable. */
    # ints
    SAC_INT_NZYEAR,   #/*!< Year */
    SAC_INT_NZJDAY,   #/*!< Julian day */
    SAC_INT_NZHOUR,   #/*!< Hour */
    SAC_INT_NZMIN,    #/*!< Minute */
    SAC_INT_NZSEC,    #/*!< Second */
    SAC_INT_NZMSEC,   #/*!< Millisecond */
    SAC_INT_NVHDR,    #/*!< Header version number. */
    SAC_INT_NORID,    #/*!< CSS 3.0 origin ID. */
    SAC_INT_NEVID,    #/*!< CSS 3.0 event ID. */
    SAC_INT_NPTS,     #/*!< Number of points in time series. */
    SAC_INT_INTERNAL1,#/*!< Internal variable. */
    SAC_INT_NWFID,    #/*!< CSS 3.0 Waveform ID. */
    SAC_INT_NXSIZE,   #/*!< Spectral length - for spectral files only. */
    SAC_INT_NYSIZE,   #/*!< Spectral width - for spectral files only. */
    SAC_INT_UNUSED0,  #/*!< Extra integer variable. */
    SAC_INT_IFTYPE,   #/*!< Type of file.  */
    SAC_INT_IDEP,     #/*!< Type of dependent variable. */
    SAC_INT_IZTYPE,   #/*!< Reference time equivalence. */
    SAC_INT_UNUSED1,  #/*!< Extra integer variable. */
    SAC_INT_IINST,    #/*!< Type of recording instrument. */
    SAC_INT_ISTREG,   #/*!< Station geographic region. */
    SAC_INT_IEVREG,   #/*!< Event geographic region. */
    SAC_INT_IEVTYP,   #/*!< Event type. */
    SAC_INT_IQUAL,    #/*!< Quality of data. */
    SAC_INT_ISYNTH,   #/*!< Synthetic data flag. */
    SAC_INT_IMAGTYP,  #/*!< Source magnitude type. */
    SAC_INT_IMAGSRC,  #/*!< Source magnitude information. */
    SAC_INT_UNUSED2,  #/*!< Extra integer variable */
    SAC_INT_UNUSED3,  #/*!< Extra integer variable */
    SAC_INT_UNUSED4,  #/*!< Extra integer variable */
    SAC_INT_UNUSED5,  #/*!< Extra integer variable */
    SAC_INT_UNUSED6,  #/*!< Extra integer variable */
    SAC_INT_UNUSED7,  #/*!< Extra integer variable */
    SAC_INT_UNUSED8,  #/*!< Extra integer variable */
    SAC_INT_UNUSED9,  #/*!< Extra integer variable */
    # logicals
    SAC_BOOL_LEVEN,   #/*!< If true then data is evenly space. */
    SAC_BOOL_LPSPOL,  #/*!< If true then station conforms to left hand rule. */
    SAC_BOOL_LOVROK,  #/*!< If true then it is okay to overwrite file. */
    SAC_BOOL_LCALDA,  #/*!< If true then SAC wil compute dist, az, and gcarc. */
    SAC_BOOL_LUNUSED, #/*!< Extra logical variable. */
    # characters
    SAC_CHAR_KSTNM,   #/*!< Station name. */
    SAC_CHAR_KEVNM,   #/*!< Event name. */
    SAC_CHAR_KHOLE,   #/*!< Location code. */
    SAC_CHAR_KO,      #/*!< Origin time identifier. */
    SAC_CHAR_KA,      #/*!< First arrival identifier. */
    SAC_CHAR_KT0,     #/*!< Label for first pick time. */
    SAC_CHAR_KT1,     #/*!< Label for second pick time. */ 
    SAC_CHAR_KT2,     #/*!< Label for third pick time. */
    SAC_CHAR_KT3,     #/*!< Label for fourth pick time. */
    SAC_CHAR_KT4,     #/*!< Label for fifth pick time. */
    SAC_CHAR_KT5,     #/*!< Label for sixth pick time. */
    SAC_CHAR_KT6,     #/*!< Label for seventh pick time. */
    SAC_CHAR_KT7,     #/*!< Label for eighth pick time. */
    SAC_CHAR_KT8,     #/*!< Label for ninth pick time. */
    SAC_CHAR_KT9,     #/*!< Label for tenth pick time. */
    SAC_CHAR_KF,      #/*!< End time identifier. */
    SAC_CHAR_KUSER0,  #/*!< Extra character variable for user. */
    SAC_CHAR_KUSER1,  #/*!< Extra character variable for user. */
    SAC_CHAR_KUSER2,  #/*!< Extra character variable for user. */
    SAC_CHAR_KCMPNM,  #/*!< Component name. */
    SAC_CHAR_KNETWK,  #/*!< Network name. */
    SAC_CHAR_KDATRD,  #/*!< Data was read onto computer. */
    SAC_CHAR_KINST,   #*!< Generic name of recording instrument. */
    # unknown header variable
    SAC_UNKNOWN_HDRVAR #/*!< Unknown header variable. */

) = map(c_int, range(133))

# range(7)) 

# Structures
class sacFAP_struct(Structure):
    _fields_ = [ ("freqs",    POINTER(c_double)),
                 ("amp",      POINTER(c_double)),
                 ("phase",    POINTER(c_double)),
                 ("nf",       c_int),
                 ("lhaveFAP", c_bool),
                 ("pad", 3*c_char) ]

class sacPoleZero_struct(Structure):
    _fields_ = [ ("network",        64*c_char),
                 ("station",        64*c_char),
                 ("location",       64*c_char),
                 ("comment",        64*c_char),
                 ("description",    64*c_char),
                 ("instrumentType", 64*c_char),
                 ("poles", POINTER(c_double)),
                 ("zeros", POINTER(c_double)),
                 ("constant", c_double),
                 ("created",  c_double),
                 ("start",    c_double),
                 ("end",      c_double),
                 ("latitude", c_double),
                 ("longitude", c_double),
                 ("elevation", c_double),
                 ("depth",     c_double),
                 ("dip",       c_double),
                 ("azimuth",   c_double),
                 ("sampleRate",  c_double),
                 ("instgain",    c_double),
                 ("sensitivity", c_double),
                 ("a0",          c_double),
                 ("npoles",      c_int),
                 ("nzeros",      c_int),
                 ("inputUnits",  c_int),
                 ("outputUnits", c_int),
                 ("sensitivityUnits", c_int),
                 ("lhavePZ", c_bool),
                 ("pad", 3*c_char)]

class sacHeader_struct(Structure):
    _fields_ = [ ("delta",     c_double),
                 ("depmin",    c_double),
                 ("depmax",    c_double),
                 ("scale",     c_double),
                 ("odelta",    c_double),
                 ("b",         c_double),
                 ("e",         c_double),
                 ("o",         c_double),
                 ("a",         c_double),
                 ("internal1", c_double),
                 ("t0",        c_double),
                 ("t1",        c_double),
                 ("t2",        c_double),
                 ("t3",        c_double),
                 ("t4",        c_double),
                 ("t5",        c_double),
                 ("t6",        c_double),
                 ("t7",        c_double),
                 ("t8",        c_double),
                 ("t9",        c_double),
                 ("f",         c_double),
                 ("resp0",     c_double),
                 ("resp1",     c_double),
                 ("resp2",     c_double),
                 ("resp3",     c_double),
                 ("resp4",     c_double),
                 ("resp5",     c_double),
                 ("resp6",     c_double),
                 ("resp7",     c_double),
                 ("resp8",     c_double),
                 ("resp9",     c_double),
                 ("stla",      c_double),
                 ("stlo",      c_double),
                 ("stel",      c_double),
                 ("stdp",      c_double),
                 ("evla",      c_double),
                 ("evlo",      c_double),
                 ("evel",      c_double),
                 ("evdp",      c_double),
                 ("mag",       c_double),
                 ("user0",     c_double),
                 ("user1",     c_double),
                 ("user2",     c_double),
                 ("user3",     c_double),
                 ("user4",     c_double),
                 ("user5",     c_double),
                 ("user6",     c_double),
                 ("user7",     c_double),
                 ("user8",     c_double),
                 ("user9",     c_double),
                 ("dist",      c_double),
                 ("az",        c_double),
                 ("baz",       c_double),
                 ("gcarc",     c_double),
                 ("internal2", c_double),
                 ("depmen",    c_double),
                 ("cmpaz",     c_double),
                 ("cmpinc",    c_double),
                 ("xminimum",  c_double),
                 ("xmaximum",  c_double),
                 ("yminimum",  c_double),
                 ("ymaximum",  c_double),
                 ("unused0",   c_double),
                 ("unused1",   c_double),
                 ("unused2",   c_double),
                 ("unused3",   c_double),
                 ("unused4",   c_double),
                 ("unused5",   c_double),
                 ("unused6",   c_double),
                 # integers
                 ("nzyear",     c_int),
                 ("nzjday",     c_int),
                 ("nzhour",     c_int),
                 ("nzmin",      c_int),
                 ("nzsec",      c_int),
                 ("nzmsec",     c_int),
                 ("nvhdr",      c_int),
                 ("norid",      c_int),
                 ("nevid",      c_int),
                 ("npts",       c_int),
                 ("ninternal1", c_int),
                 ("nwfid",      c_int),
                 ("nxsize",     c_int),
                 ("nysize",     c_int),
                 ("nunused0",   c_int),
                 ("iftype",     c_int),
                 ("idep",       c_int),
                 ("iztype",     c_int),
                 ("nunused1",   c_int),
                 ("iinst",      c_int),
                 ("istreg",     c_int),
                 ("ievreg",     c_int),
                 ("ievtyp",     c_int),
                 ("iqual",      c_int),
                 ("isynth",     c_int),
                 ("imagtyp",    c_int),
                 ("imagsrc",    c_int),
                 ("nunused2",   c_int),
                 ("nunused3",   c_int),
                 ("nunused4",   c_int),
                 ("nunused5",   c_int),
                 ("nunused6",   c_int),
                 ("nunused7",   c_int),
                 ("nunused8",   c_int),
                 ("nunused9",   c_int),
                 # logicals
                 ("leven",     c_int),
                 ("lpspol",    c_int),
                 ("lovrok",    c_int),
                 ("lcalda",    c_int),
                 ("lunused",   c_int),
                 # characters
                 ("kstnm",   8*c_char),
                 ("kevnm",  16*c_char),
                 ("khole",   8*c_char),
                 ("ko",      8*c_char),
                 ("ka",      8*c_char),
                 ("kt0",     8*c_char),
                 ("kt1",     8*c_char),
                 ("kt2",     8*c_char),
                 ("kt3",     8*c_char),
                 ("kt4",     8*c_char), 
                 ("kt5",     8*c_char),
                 ("kt6",     8*c_char),
                 ("kt7",     8*c_char),
                 ("kt8",     8*c_char),
                 ("kt9",     8*c_char),
                 ("kf",      8*c_char),
                 ("kuser0",  8*c_char),
                 ("kuser1",  8*c_char),
                 ("kuser2",  8*c_char),
                 ("kcmpnm",  8*c_char),
                 ("knetwk",  8*c_char),
                 ("kdatrd",  8*c_char),
                 ("kinst",   8*c_char),
                 ("lhaveHeader", c_bool),
                 ("pad",       7*c_char)
               ]

class sacData_struct(Structure):
    _fields_ = [ ("header", sacHeader_struct),
                 ("pz",     sacPoleZero_struct),
                 ("fap",    sacFAP_struct),
                 ("data",   POINTER(c_double)),
                 ("npts", c_int),
                 ("pad",  4*c_char) ]  

class SacIO:
    def __init__(self,
                 sacio_path=os.environ['LD_LIBRARY_PATH'].split(os.pathsep),
                 sacio_library='libsacio_shared.so'):
        lfound = False
        for path in sacio_path:
            sacPath = os.path.join(path, sacio_library)
            if (os.path.isfile(sacPath)):
                lfound = True
                break
        if (lfound):
            saclib = cdll.LoadLibrary(sacPath) 
        else:
            print("Couldn't find libsacio")
            return
        saclib.sacio_freeData.argtypes = [POINTER(sacData_struct)]
        saclib.sacio_freeData.retval = c_int


        saclib.sacio_setDefaultHeader.argtypes = [POINTER(sacHeader_struct)]
        # character header
        saclib.sacio_getCharacterHeader.argtypes = [c_int, sacHeader_struct,
                                                    POINTER(c_char)]
        saclib.sacio_getCharacterHeader.retval = c_int
        saclib.sacio_setCharacterHeader.argtypes = [c_int, c_char_p,
                                                    POINTER(sacHeader_struct)]
        # integer header
        saclib.sacio_getFloatHeader.retval = c_int
        saclib.sacio_setFloatHeader.argtypes = [c_int, c_double,
                                                POINTER(sacHeader_struct)]
        saclib.sacio_setFloatHeader.retval = c_int
        # Integer header
        saclib.sacio_getIntegerHeader.argtypes = [c_int, sacHeader_struct,
                                                  POINTER(c_int)]
        saclib.sacio_getIntegerHeader.retval = c_int
        saclib.sacio_setIntegerHeader.argtypes = [c_int, c_int,
                                                  POINTER(sacHeader_struct)]
        saclib.sacio_setIntegerHeader.retval = c_int
        #c_void_p]
        self.sac = sacData_struct()
        self.saclib = saclib

        # Initialize the header
        self.setDefaultHeader()
        return

    def __enter__(self):
        return

    def __exit__(self):
        self.saclib.sacio_freeData(self.sac)
        return

    def getCharacterHeader(self, variable):
        """
        Gets a character header variable.
        """
        if (variable == SAC_CHAR_KEVNM):
            var = create_string_buffer(16)
        else:
            var = create_string_buffer(4)
        ierr = self.saclib.sacio_getCharacterHeader(variable, self.sac.header,
                                                    var)
        if (ierr != 0):
            print("getCharacterHeader: Error getting variable")
        return str(var.value.decode('utf-8'))
 
    def setCharacterHeader(self, variable, value='-12345'):
        """
        Sets a character header variable.
        """
        var = c_char_p(value.encode('utf-8')) 
        ierr = self.saclib.sacio_setCharacterHeader(variable, var, 
                                                    self.sac.header)
        return ierr
 
    def getFloatHeader(self, variable):
        """
        Gets a float header variable.

        Input
        variable : sacHeader_struct
            Float header variable to set.

        Returns
        var.value : float
            Value of corresponding float header variable. 
        """
        var = c_double(1)
        ierr = self.saclib.sacio_getFloatHeader(variable, self.sac.header,
                                                byref(var))
        if (ierr != 0): 
            print("getFloatHeader: Error getting variable")
        return var.value

    def setFloatHeader(self, variable, value=-12345.0):
        """
        Sets a float header variable.
        """
        ivar = c_int(1)
        ierr = self.saclib.sacio_setFloatHeader(variable, value,
                                                self.sac.header)
        return ierr

    def setIntegerHeader(self, variable, value=-12345):
        """
        Sets an integer header variable.
        """
        ierr = self.saclib.sacio_setIntegerHeader(variable, value,
                                                  self.sac.header) 
        return ierr
        
    def getIntegerHeader(self, variable):
        """
        Gets the integer header variable.
        """
        ivar = c_int(1)
        ierr = self.saclib.sacio_getIntegerHeader(variable, self.sac.header,
                                                  byref(ivar))
        if (ierr != 0):
            print("getIntegerHeader: Error getting variable")
        return ivar.value

    def setDefaultHeader(self):
        """
        Sets the default header which is all SAC NaN's.
        """
        self.saclib.sacio_setDefaultHeader(self.sac.header)
        return

if __name__ == "__main__":
    sac = SacIO()
    sac.setCharacterHeader(SAC_CHAR_KSTNM, "ANMO")
    sac.setIntegerHeader(SAC_INT_NPTS, 400)
    sac.setFloatHeader(SAC_FLOAT_DELTA, 1.0)
    n = sac.getIntegerHeader(SAC_INT_NPTS)
    dt = sac.getFloatHeader(SAC_FLOAT_DELTA)
    kstnm = sac.getCharacterHeader(SAC_CHAR_KSTNM)
    print(n, dt, kstnm)
    print("okay")
