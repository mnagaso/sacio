#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "sacio_tests.h"
#include "sacio_string.h"

int string_lstrip_test(void);
int string_rstrip_test(void);
int string_rsplit_test(void);

int string_tests(void)
{
    int ierr;
    ierr = string_lstrip_test();
    if (ierr != EXIT_SUCCESS)
    {
        fprintf(stderr, "%s: Failed lstrip test\n", __func__);
        return EXIT_FAILURE;
    }
    fprintf(stdout, "%s: Passed lstrip test\n", __func__);

    ierr = string_rstrip_test();
    if (ierr != EXIT_SUCCESS)
    {
        fprintf(stderr, "%s: Failed rstrip test\n", __func__);
        return EXIT_FAILURE;
    }
    fprintf(stdout, "%s: Passed rstrip test\n", __func__);

    ierr = string_rsplit_test();
    if (ierr != EXIT_SUCCESS)
    {
        fprintf(stderr, "%s: Failed rsplit test\n", __func__);
        return EXIT_FAILURE;
    }
    fprintf(stdout, "%s: Passed rsplit test\n", __func__);

    return EXIT_SUCCESS;
}
//============================================================================//
int string_lstrip_test(void)
{
    char *temp;
    temp = sacio_string_lstrip(NULL, "asdf\0");
    if (strcasecmp("asdf\0", temp) != 0)
    {   
        printf("%s: Failed test 1\n", __func__);
        return EXIT_FAILURE;
    }   
    free(temp);

    temp = sacio_string_lstrip(NULL, "\t\n \f\v \rasdf\0");
    if (strcasecmp("asdf\0", temp) != 0)
    {   
        printf("%s: Failed test 2 %s\n", __func__, temp);
        return EXIT_FAILURE;
    }   
    free(temp);

    temp = sacio_string_lstrip(NULL, "\0");
    if (strcasecmp("\0", temp) != 0)
    {   
        printf("%s: Failed test 3 %s\n", __func__, temp);
        return EXIT_FAILURE;
    }   
    free(temp);

    temp = sacio_string_lstrip(NULL, "123   \0");
    if (strcasecmp("123   \0", temp) != 0)
    {   
        printf("%s: Failed test 4 %s\n", __func__, temp);
        return EXIT_FAILURE;
    }   
    free(temp); 

    temp = sacio_string_lstrip(NULL, "   321   \0");
    if (strcasecmp("321   \0", temp) != 0)
    {   
        printf("%s: Failed test 5 %s\n", __func__, temp);
        return EXIT_FAILURE;
    }   
    free(temp);

    temp = sacio_string_lstrip("abc0", "00000this is a string\0");
    if (strcasecmp("this is a string\0", temp) != 0)
    {   
        printf("%s: Failed test 6 %s\n", __func__, temp);
        return EXIT_FAILURE;
    }
    free(temp);
    return EXIT_SUCCESS;
}
//============================================================================//
int string_rstrip_test(void)
{
    char *temp;
    temp = sacio_string_rstrip(NULL, "asdf\0");
    if (strcasecmp("asdf\0", temp) != 0)
    {
        printf("%s: Failed test 1\n", __func__);
        return EXIT_FAILURE;
    }
    free(temp);

    temp = sacio_string_rstrip(NULL, "asdf\t\n \f\v \r\0");
    if (strcasecmp("asdf\0", temp) != 0)
    {
        printf("%s: Failed test 2 %s\n", __func__, temp);
        return EXIT_FAILURE;
    }
    free(temp);

    temp = sacio_string_rstrip(NULL, "\0");
    if (strcasecmp("\0", temp) != 0)
    {
        printf("%s: Failed test 3 %s\n", __func__, temp);
        return EXIT_FAILURE;
    }
    free(temp);

    temp = sacio_string_rstrip(NULL, "   123\0");
    if (strcasecmp("   123\0", temp) != 0)
    {
        printf("%s: Failed test 4 %s\n", __func__, temp);
        return EXIT_FAILURE;
    }
    free(temp);

    temp = sacio_string_rstrip(NULL, "   321   \0");
    if (strcasecmp("   321\0", temp) != 0)
    {
        printf("%s: Failed test 5 %s\n", __func__, temp);
        return EXIT_FAILURE;
    }
    free(temp);

    temp = sacio_string_rstrip("abc0", "this is a stringabc0000\0");
    if (strcasecmp("this is a string\0", temp) != 0)
    {
        printf("%s: Failed test 6 %s\n", __func__, temp);
        return EXIT_FAILURE;
    }
    free(temp);
    return EXIT_SUCCESS;
}
//============================================================================//
int string_rsplit_test(void)
{
    const char *string1 = " a b c ";
    const char *stringRef1[3] = {"a", "b", "c"};
    char **split;
    int i, nitems;

    split = sacio_string_rsplit(NULL, string1, &nitems);
    if (nitems != 3)
    {
        printf("%s: Failed test 1\n", __func__);
        return EXIT_FAILURE;
    }
    for (i=0; i<nitems; i++)
    {
        if (strcasecmp(stringRef1[i], split[i]) != 0)
        {
            printf("%s: Failed rsplit1\n", __func__);
            return EXIT_FAILURE;
        }
        free(split[i]);
    }
    free(split);

    split = sacio_string_rsplit(" ", string1, &nitems);
    if (nitems != 3)
    {
        printf("%s: Failed test 1b: %d\n", __func__, nitems);
        return EXIT_FAILURE;
    }   
    for (i=0; i<nitems; i++)
    {   
        if (strcasecmp(stringRef1[i], split[i]) != 0)
        {
            printf("%s: Failed rsplit1b\n", __func__);
            return EXIT_FAILURE;
        }
        free(split[i]);
    }   
    free(split);
    
    return EXIT_SUCCESS;
}
