#ifndef SAC_TEST_H
#define SAC_TEST_H 1

#ifdef __cplusplus
extern "C"
{
#endif

int time_tests(void);
int time_epoch2calendar_test(void);
int time_calendar2epoch_test(void);

int string_tests(void);

int cpp_io_tests(void);

#ifdef __cplusplus
}
#endif

#endif
