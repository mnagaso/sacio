#include <stdio.h>
#include <stdlib.h>
#include <sacio.h>
#include <math.h>
#include "sacio_tests.h"
#include "sacio_os.h"

int time_tests(void)
{
    int ierr;
    ierr = time_epoch2calendar_test();
    if (ierr != EXIT_SUCCESS)
    {
        fprintf(stderr, "%s: Failed epoch2calendar\n", __func__);
        return EXIT_FAILURE;
    }
    fprintf(stdout, "%s: Passed epoch2calendar\n", __func__);
    ierr = time_calendar2epoch_test();
    if (ierr != EXIT_SUCCESS)
    {
        fprintf(stderr, "%s: Failed calendar2epoch\n", __func__);
        return EXIT_FAILURE;
    }
    fprintf(stdout, "%s: Passed calendar2epoch\n", __func__);
    return EXIT_SUCCESS;
}
//============================================================================//
int time_epoch2calendar_test(void)
{
    double epoch = 1408117832.844000;
    int nzyear, nzjday, month, mday, nzhour, nzmin, nzsec, nzmusec;
    int ierr;
    ierr = sacio_time_epoch2calendar(epoch,
                                     &nzyear, &nzjday, &month, &mday,
                                     &nzhour, &nzmin, &nzsec, &nzmusec);
    if (ierr != 0)
    { 
        fprintf(stderr, "%s: Error converting epoch to calendar time\n",
                __func__);
        return EXIT_FAILURE;
    }   
    if (nzyear != 2014)
    {
        fprintf(stderr, "%s: Error converting year %d\n", __func__, nzyear);
        return EXIT_FAILURE;
    }   
    if (nzjday != 227)
    {
        fprintf(stderr, "%s: Error converting julian day %d\n",
                __func__, nzjday);
        return EXIT_FAILURE;
    }   
    if (nzhour != 15)
    {
        fprintf(stderr, "%s: Error converting hour %d\n", __func__, nzhour);
        return EXIT_FAILURE;
    }   
    if (nzmin != 50)
    {
        fprintf(stderr, "%s: Error converting minute %d\n", __func__, nzmin);
        return EXIT_FAILURE;
    }   
    if (nzsec != 32)
    {
        fprintf(stderr, "%s: Error converting second %d\n", __func__, nzsec);
        return EXIT_FAILURE;
    }   
    if (nzmusec != 844000)
    {
        fprintf(stderr, "%s: Error converting micro-second %d\n",
                __func__, nzmusec);
        return EXIT_FAILURE;
    }

    epoch = 1452044510.050000;
    ierr = sacio_time_epoch2calendar(epoch,
                                     &nzyear, &nzjday, &month, &mday,
                                     &nzhour, &nzmin, &nzsec, &nzmusec);
    if (ierr != 0)
    { 
        fprintf(stderr, "%s: Error converting epoch to calendar time\n",
                __func__);
        return EXIT_FAILURE;
    }
    if (nzyear != 2016)
    {
        fprintf(stderr, "%s: Error converting year %d 2\n", __func__, nzyear);
        return EXIT_FAILURE;
    }
    if (nzjday != 6)
    {
        fprintf(stderr, "%s: Error converting julian day %d 2\n",
                __func__, nzjday);
        return EXIT_FAILURE;
    }
    if (nzhour != 1)
    {
        fprintf(stderr, "%s: Error converting hour %d\n 2", __func__, nzhour);
        return EXIT_FAILURE;
    }
    if (nzmin != 41)
    {
        fprintf(stderr, "%s: Error converting minute %d\n 2", __func__, nzmin);
        return EXIT_FAILURE;
    }
    if (nzsec != 50)
    {
        fprintf(stderr, "%s: Error converting second %d\n 2", __func__, nzsec);
        return EXIT_FAILURE;
    }
    if (nzmusec != 50000)
    {
        fprintf(stderr, "%s: Error converting micro-second %d\n",
                __func__, nzmusec);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
//============================================================================// 
int time_calendar2epoch_test(void)
{
    double epoch;
    int dom, month, nzyear, nzjday, nzhour, nzmin, nzsec, nzmusec;
    nzyear = 2014;
    nzjday = 227;
    nzhour = 15; 
    nzmin = 50;
    nzsec = 32; 
    nzmusec = 844000;
    epoch = sacio_time_calendar2epoch(nzyear, nzjday, nzhour,
                                      nzmin, nzsec, nzmusec);
    if (fabs(epoch - 1408117832.844000) > 1.e-6)
    {
        fprintf(stderr, "%s: Failed to convert calendar2epoch time\n",
                __func__);
        return EXIT_FAILURE;
    }
    month = 8;
    dom = 15;
    epoch = sacio_time_calendar2epoch2(nzyear, month, dom, nzhour,
                                       nzmin, nzsec, nzmusec); 
    if (fabs(epoch - 1408117832.844000) > 1.e-6)
    {
        fprintf(stderr, "%s: Failed to convert calendar2epoch2 time %lf %lf\n",
                __func__, epoch, epoch - 1408117832.844000);
        return EXIT_FAILURE;
    }

    // Test 2 
    nzyear = 2016;
    nzjday = 6;
    nzhour = 1;
    nzmin = 41;
    nzsec = 50; 
    nzmusec = 50000;
    epoch = sacio_time_calendar2epoch(nzyear, nzjday, nzhour,
                                      nzmin, nzsec, nzmusec);
    if (fabs(epoch - 1452044510.050000) > 1.e-6)
    {
        fprintf(stderr, "%s: Failed to convert calendar2epoch time 2\n",
                __func__);
        return EXIT_FAILURE;
    }
    month = 1;
    dom = 6;
    epoch = sacio_time_calendar2epoch2(nzyear, month, dom, nzhour,
                                       nzmin, nzsec, nzmusec);
    if (fabs(epoch - 1452044510.050000) > 1.e-6)
    {
        fprintf(stderr, "%s: Failed to convert calendar2epoch2 time 2\n",
                __func__);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
