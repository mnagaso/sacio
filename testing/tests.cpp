#include <stdio.h>
#include <stdlib.h>
#include "sacio_tests.h"
#include "sacio.hpp"
#include <complex>
#include <iostream>
#include <cmath>
#include <vector>

int main()
{
    int ierr;
    ierr = time_tests();
    if (ierr != EXIT_SUCCESS)
    {
        fprintf(stderr, "%s: Failed time tests\n", __func__);
        return EXIT_FAILURE;
    }
    fprintf(stdout, "%s: Passed time tests\n", __func__);

    ierr = string_tests();
    if (ierr != EXIT_SUCCESS)
    {
        fprintf(stderr, "%s: Failed string tests\n", __func__);
        return EXIT_FAILURE;
    }
    fprintf(stdout, "%s: Passed string tests\n", __func__);

    ierr = cpp_io_tests();
    if (ierr != EXIT_SUCCESS)
    {
        fprintf(stderr, "%s: Failed cpp io tests\n", __func__);
        return EXIT_FAILURE;
    }
    fprintf(stdout, "%s: Passed cpp tests\n", __func__);
    return EXIT_SUCCESS;
}

int cpp_io_tests(void)
{
    int ierr = EXIT_SUCCESS;
    std::vector<std::complex<double>> zref, pref;
    zref.push_back(std::complex<double> (0, 0));
    zref.push_back(std::complex<double> (0, 0));
    zref.push_back(std::complex<double> (-1.357090e-02, +0.000000e+00));
    zref.push_back(std::complex<double> (-1.357090e-02, +0.000000e+00));
    zref.push_back(std::complex<double> (+0.000000e+00, +0.000000e+00));
    pref.push_back(std::complex<double> (-1.264640e-02, +1.234000e-02));
    pref.push_back(std::complex<double> (-1.264640e-02, -1.234000e-02));
    pref.push_back(std::complex<double> (-7.732870e-03, +0.000000e+00));
    pref.push_back(std::complex<double> (-1.901960e-02, +0.000000e+00));
    pref.push_back(std::complex<double> (-3.918000e+01, +4.912000e+01));
    pref.push_back(std::complex<double> (-3.918000e+01, -4.912000e+01));
    double kref = 2.372346e+13;
    Sac sacCopy1, sacCopy2;
    for (int passes=1; passes<=2; passes++)
    {
        fprintf(stdout, "%s: Pass %d\n", __func__, passes);
        Sac sac;
        std::vector<std::complex<double>> poles, zeros;
        double k;
        std::string inFile = "debug.sac";
        std::string inFilePZ = "debug.sacpz";
        if (passes == 2){inFile = "debug_out.sac";}
        ierr = sac.readFile(inFile);
        if (ierr != 0)
        {
            fprintf(stderr, "%s: Failed to read file\n", __func__);
            return EXIT_FAILURE;
        }
        sac.printHeader();
        int npts;
        ierr = sac.getHeader(SacHeader::Integer::NPTS, &npts);
        if (ierr != 0 || npts != 100)
        {
            fprintf(stderr, "%s: Failed to get npts\n", __func__);
            return EXIT_FAILURE;
        }
        std::string cname;
        ierr = sac.getHeader(SacHeader::String::KNETWK, cname);
        if (ierr != 0 || cname != "FK")
        {
            fprintf(stderr, "%s: Failed to get network\n", __func__);
            return EXIT_FAILURE;
        }
        ierr = sac.getHeader(SacHeader::String::KSTNM, cname);
        if (ierr != 0 || cname != "NEW")
        {
            fprintf(stderr, "%s: Failed to get station\n", __func__);
            return EXIT_FAILURE;
        }
        ierr = sac.getHeader(SacHeader::String::KCMPNM, cname);
        if (ierr != 0 || cname != "HHZ")
        {
            fprintf(stderr, "%s: Failed to get channel\n", __func__);
            return EXIT_FAILURE;
        }
        ierr = sac.getHeader(SacHeader::String::KHOLE, cname);
        if (ierr != 0 || cname != "10")
        {   
            fprintf(stderr, "%s: Failed to get location\n", __func__);
            return EXIT_FAILURE;
        }
        double dt;
        ierr = sac.getHeader(SacHeader::Double::DELTA, &dt);
        if (ierr != 0 || std::abs(dt - 0.005) > 1.e-8)
        {
            fprintf(stderr, "%s: Failed to get dt\n", __func__);
            return EXIT_FAILURE;
        }
        // Read the pole-zero file
        ierr = sac.readPolesAndZeros(inFilePZ);
        if (ierr != 0)
        {
            fprintf(stderr, "%s: Failed to read sac poles and zeros\n",
                    __func__);
            return EXIT_FAILURE;
        }
        sac.getPolesZerosAndConstant(zeros, poles, &k);
        if (zeros.size() != 5)
        {
            fprintf(stderr, "%s: should be 5 zeros\n", __func__);
            return EXIT_FAILURE;
        } 
        for (size_t iz=0; iz<zeros.size(); iz++)
        {
            if (std::abs(zeros[iz] - zref[iz]) > 1.e-5)
            {
                fprintf(stderr, "%s: Failed to read zero %ld\n", __func__, iz);
                return EXIT_FAILURE;
            }
            // std::cout << zeros[iz] <<std::endl;
        }
        if (poles.size() != 6)
        {
            fprintf(stderr, "%s: should be 6 poles\n", __func__);
            return EXIT_FAILURE;
        }
        for (size_t ip=0; ip<poles.size(); ip++)
        {
            if (std::abs(poles[ip] - pref[ip]) > 1.e-5)
            {
                fprintf(stderr, "%s: Failed to read pole %ld\n", __func__, ip);
                return EXIT_FAILURE;
            }
            //std::cout << poles[ip] << std::endl;
        }
        if (std::abs(kref - k) > 1.e-5)
        {
            fprintf(stderr, "%s: k mismatch %lf %lf\n", __func__, k, kref);
            return EXIT_FAILURE;
        } 
        // Check the data
        ierr = sac.getNumberOfPoints(&npts);
        if (ierr != 0 || npts != 100)
        {
            fprintf(stderr, "%s: Failed to get npts\n", __func__);
            return EXIT_FAILURE;
        }
        const double *data = nullptr;
        ierr = sac.getConstantDataPtr(&data);
        if (ierr != 0)
        {
            fprintf(stderr, "%s: Failed to get data ptr\n", __func__);
            return EXIT_FAILURE;
        }
        for (int i=0; i<npts; i++)
        {
            if (std::abs(static_cast<double> (i+1) - data[i]) > 1.e-8)
            {
                fprintf(stderr, "%s: Failed to load time series %lf %lf\n",
                        __func__, static_cast<double> (i+1), data[i]);
                return EXIT_FAILURE;
            }
        }
        // Write debug.sac -> then i'll re-read it, run through the
        // previous tests, and verify the writing works. 
        if (passes == 1)
        {
            ierr = sac.writeFile("debug_out.sac");
            if (ierr != 0)
            {
                fprintf(stderr, "%s: Failed to write SAC file\n", __func__);
                return EXIT_FAILURE;
            }
        }
        if (passes == 1){sacCopy1 = sac;}
        if (passes == 2){sacCopy2 = sac;}
    } // Loop on passes
    if (sacCopy1 != sacCopy2)
    {
        fprintf(stderr, "%s: These classes don't appear equal\n", __func__);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
