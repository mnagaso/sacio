#ifndef SACIO_MPI_HPP
#define SACIO_MPI_HPP 1
#include "sacio_config.h"
#include "sacio.hpp"
#include "sacio_mpi.h"
#ifdef SACIO_USE_MPI
#include <mpi.h>
class SacMPI : public Sac
{
    public:
        SacMPI(void);
        ~SacMPI(void);
        SacMPI(const sacData_struct sac);
        SacMPI &operator=(const SacMPI &sacmpi);
        void clear(void);
        int broadcast(const int root=0, const MPI_Comm comm=MPI_COMM_WORLD);
};

#endif
#endif
