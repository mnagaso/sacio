#ifndef SACIO_OS_H
#define SACIO_OS_H 1
#include <stdbool.h>
#include "sacio_config.h"

#ifdef __cplusplus
extern "C"
{
#endif

/* Determines if a file exists. */
bool sacio_os_path_isfile(const char *filenm);
/* Determines if a directory exists. */
bool sacio_os_path_isdir(const char *dirnm);
/* Gets the dirctory name. */
int sacio_os_dirname_work(const char *path, char dirName[PATH_MAX]);
/* Makes a directory. */
int sacio_os_mkdir(const char *dirnm);
/* Makes a directory tree. */
int sacio_os_makedirs(const char *path);

#ifdef __cplusplus
}
#endif

#endif
