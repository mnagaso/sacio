#ifndef SACIO_STRING_H
#define SACIO_STRING_H 1
#include <stdbool.h>
#include "sacio_config.h"

#ifdef __cplusplus
extern "C"
{
#endif

char *sacio_string_strip(const char *pattern, const char *string);
char *sacio_string_lstrip(const char *pattern, const char *string);
char *sacio_string_rstrip(const char *pattern, const char *string);
char **sacio_string_rsplit(const char *pattern, const char *stringIn,
                           int *nitems);
int sacio_string_split_work(const int np, const char *pattern,
                            const char *__restrict__ string,
                            const int lenp, int *__restrict__ ptr,
                            const int lens,
                            int *ns, char *__restrict__ stringSplit);

#ifdef __cplusplus
}
#endif

#endif
