#ifndef SACIO_TIME_H
#define SACIO_TIME_H 1
#include "sacio_config.h"

#ifdef __cplusplus
extern "C"
{
#endif
double sacio_time_calendar2epoch(
    const int nzyear, const int nzjday, const int nzhour,
    const int nzmin, const int nzsec, const int nzmusec);
double sacio_time_calendar2epoch2(
    const int nzyear, const int month, const int dom,
    const int nzhour, const int nzmin, const int nzsec,
    const int nzmusec);
int sacio_time_epoch2calendar(
    const double epoch,
    int *nzyear, int *nzjday, int *month, int *mday,
    int *nzhour, int *nzmin, int *nzsec, int *nzmusec);

#ifdef __cplusplus
}
#endif

#endif
