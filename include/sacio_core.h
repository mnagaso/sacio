#ifndef SACIO_CORE_H
#define SACIO_CORE_H 1
#include <stdbool.h>
#include <complex.h>
#include "sacio_config.h"
#include "sacio_enum.h"
#include "sacio_struct.h"
#ifdef SACIO_USE_HDF5
#include "sacioh5.h"
#endif

#ifdef __cplusplus
extern "C"
{
#endif

int sacio_copyPolesAndZeros(const struct sacPoleZero_struct pzIn,
                            struct sacPoleZero_struct *pzOut);
int sacio_copyFAP(const struct sacFAP_struct fapIn,
                  struct sacFAP_struct *fapOut);
int sacio_copyHeader(const struct sacHeader_struct hdrIn,
                     struct sacHeader_struct *hdrOut);
int sacio_copy(const struct sacData_struct sacIn,
               struct sacData_struct *sacOut);
int sacio_readHeader(const char *fname, struct sacHeader_struct *header);
int sacio_readPoleZeroFile(const char *fname,
                           struct sacPoleZero_struct *pz);
int sacio_readTimeSeriesFile(const char *fname, struct sacData_struct *sac);
int sacio_writeTimeSeriesFile(const char *fname,
                              const struct sacData_struct sac);
void sacio_unpackHeader(const bool lswap, const char *__restrict__ cheader,
                        struct sacHeader_struct *header);
void sacio_free64f(double **p);
int sacio_printHeader(FILE *file, const struct sacHeader_struct header);
bool sacio_isHeaderEqual(const struct sacHeader_struct h1,
                         const struct sacHeader_struct h2,
                         const double tol);
#ifdef __cplusplus
void sacio_free64z(void **p);
#else
void sacio_free64z(double complex **p);
#endif
int sacio_readFAPFile(const char *fname,
                      struct sacFAP_struct *fap);
int sacio_freeFAP(struct sacFAP_struct *fap);
int sacio_readData(const char *fname,
                   const int nwork, int *npts, double *__restrict__ x);
int sacio_packData(const bool lswap, const int npts, 
                   const double *__restrict__ x, char *__restrict__ cdat);
int sacio_unpackData(const bool lswap, const char *__restrict__ cdat,
                     const int npts, double *__restrict__ x);
void sacio_setDefaultHeader(struct sacHeader_struct *header);
int sacio_setFloatHeader(const enum sacHeader_enum type,
                         const double var, 
                         struct sacHeader_struct *header);
int sacio_setIntegerHeader(const enum sacHeader_enum type,
                           const int ivar,
                           struct sacHeader_struct *header);
int sacio_setBooleanHeader(const enum sacHeader_enum type,
                           const bool lvar,
                           struct sacHeader_struct *header);
int sacio_setCharacterHeader(const enum sacHeader_enum type,
                             const char *kvar,
                             struct sacHeader_struct *header);
int sacio_setEpochalPickOnHeader(const double pickTime,
                                 const char *phaseName,
                                 const enum sacHeader_enum pickHeaderTime,
                                 const enum sacHeader_enum pickHeaderName,
                                 struct sacHeader_struct *header);
int sacio_extendData(const int nptsAdd, 
                     const double *__restrict__ xadd,
                     struct sacData_struct *sac);
int sacio_setEpochalStartTime(const double t0, struct sacHeader_struct *header);
int sacio_getEpochalStartTime(const struct sacHeader_struct header, double *t0);
int sacio_getEpochalEndTime(const struct sacHeader_struct header, double *t1);
int sacio_getFloatHeader(const enum sacHeader_enum type,
                         const struct sacHeader_struct header,
                         double *var);
int sacio_getIntegerHeader(const enum sacHeader_enum type,
                           const struct sacHeader_struct header,
                           int *ivar);
int sacio_getBooleanHeader(const enum sacHeader_enum type,
                           const struct sacHeader_struct header,
                           bool *lvar);
int sacio_getCharacterHeader(const enum sacHeader_enum type,
                             const struct sacHeader_struct header,
                             char *kvar);
double *sacio_malloc64f(const int npts); // __attribute__((aligned(64))) ;
#ifdef __cplusplus
void *sacio_malloc64z(const int npts);
#else
double complex *sacio_malloc64z(const int npts);// __attribute__((aligned(64)));
#endif
void sacio_packHeader(const bool lswap,
                      const struct sacHeader_struct header,
                      char *__restrict__ cheader);
int sacio_freePoleZeros(struct sacPoleZero_struct *pz);
int sacio_free(struct sacData_struct *sac);
int sacio_freeHeader(struct sacHeader_struct *header);
int sacio_freeData(struct sacData_struct *sac);

#ifdef __cplusplus
}
#endif

#endif
