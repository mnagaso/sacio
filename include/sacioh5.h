#ifndef H5SACIO_H
#define H5SACIO_H 1
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wreserved-id-macro"
#pragma clang diagnostic ignored "-Wpadded"
#pragma clang diagnostic ignored "-Wincompatible-pointer-types-discards-qualifiers"
#endif 
#ifdef __INTEL_COMPILER
#pragma warning (push)
#pragma warning (disable:2282)
#endif
#include <hdf5.h>
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __INTEL_COMPILER
#pragma warning (pop)
#endif
#include "sacio.h"


#ifdef SACIO_USE_HDF5

struct sacH5Header_struct
{
    /*!
     * @struct sacH5Header_struct
     * @brief The SAC HDF5 data structure for reading and writing headers.
     * @ingroup sac_h5
     */
    double delta;      /*!< Increment between evenly spaced samples (nominal
                            value) [required] */
    double depmin;     /*!< Minimum value of dependent variable */
    double depmax;     /*!< Maximum value of dependent variable */
    double scale;      /*!< Multiplying scale factor for dependent variable.
                            [currently not used] */
    double odelta;     /*!< Observed increment if different from nominal
                            value */
    double b;          /*!< Beginning value of the independent variable
                            [required] */
    double e;          /*!< Ending value of the independent variable
                            [required] */
    double o;          /*!< Event origin time (seconds relative to reference
                            time) */
    double a;          /*!< First arrival time seconds relative to reference
                            time) */
    double internal1;  /*!< SAC internal variable */
    double t0;         /*!< First user defined pick time or marker (seconds
                            relative to reference time) */
    double t1;         /*!< Second user defined pick time or marker (seconds
                            relative to reference time) */
    double t2;         /*!< Third user defined pick time or marker (seconds
                            relative to reference time) */
    double t3;         /*!< Fourth user defined pick time or marker (seconds
                            relative to reference time) */
    double t4;         /*!< Fifth user defined pick time or marker (seconds
                            relative to reference time) */
    double t5;         /*!< Sixth user defined pick time or marker (seconds
                            relative to reference time) */
    double t6;         /*!< Seventh user defined pick time or marker (seconds
                            relative to reference time) */
    double t7;         /*!< Eighth user defined pick time or marker (seconds
                            relative to reference time) */
    double t8;         /*!< Ninth user defined pick time or marker (seconds
                            relative to reference time) */
    double t9;         /*!< Tenth user defined pick time or marker (seconds
                            relative to reference time) */
    double f;          /*!< End time of event (seconds relative to reference
                            time) */
    double resp0;      /*!< First instrument response parameter [unused] */
    double resp1;      /*!< Second instrument response parameter [unused] */
    double resp2;      /*!< Third instrument response parameter [unused] */
    double resp3;      /*!< Fourth instrument response parameter [unused] */
    double resp4;      /*!< Fifth instrument response parameter [unused] */
    double resp5;      /*!< Sixth instrument response parameter [unused] */
    double resp6;      /*!< Seventh instrument response parameter [unused] */
    double resp7;      /*!< Eigthth instrument response parameter [unused] */
    double resp8;      /*!< Ninth instrument response parameter [unused] */
    double resp9;      /*!< Tenth instrument response parameter [unused] */
    double stla;       /*!< Station latitude (degrees, north positive) */
    double stlo;       /*!< Station longitude (degrees, east positive) */
    double stel;       /*!< Station elevation (meters) */
    double stdp;       /*!< Station depth below surface (meters) */
    double evla;       /*!< Event latitude (degrees, north positive) */
    double evlo;       /*!< Event longitude (degrees, east positive) */
    double evel;       /*!< Event elevation (meters) [unused] */
    double evdp;       /*!< Event depth (kilometers) */
    double mag;        /*!< Event magnitude */
    double user0;      /*!< User defined */
    double user1;      /*!< User defined */
    double user2;      /*!< User defined */
    double user3;      /*!< User defined */
    double user4;      /*!< User defined */
    double user5;      /*!< User defined */
    double user6;      /*!< User defined */
    double user7;      /*!< User defined */
    double user8;      /*!< User defined */
    double user9;      /*!< User defined */
    double dist;       /*!< Station to event distance (km) */
    double az;         /*!< Event to station azimuth (degrees) */
    double baz;        /*!< Station to event azimuth (degrees) */
    double gcarc;      /*!< Station to eventgreat circle arc length (degrees) */
    double internal2;  /*!< SAC internal variable */
    double depmen;     /*!< Mean value of dependent variable */
    double cmpaz;      /*!< Component azimuth (degrees clockwise from north) */
    double cmpinc;     /*!< Component incident angle (degrees from vertical) */
    double xminimum;   /*!< Minimum value of x (spectral files only) */
    double xmaximum;   /*!< Maximum value of x (spectral files only) */
    double yminimum;   /*!< Minimum value of y (spectral files only) */
    double ymaximum;   /*!< Maximum value of y (spectral files only) */
    double unused0;    /*!< Extra space and unused by SAC */
    double unused1;    /*!< Extra space and unused by SAC */
    double unused2;    /*!< Extra space and unused by SAC */
    double unused3;    /*!< Extra space and unused by SAC */
    double unused4;    /*!< Extra space and unused by SAC */
    double unused5;    /*!< Extra space and unused by SAC */
    double unused6;    /*!< Extra space and unused by SAC */
    int nzyear;        /*!< GMT year corresponding to reference (zero)
                            time in file */
    int nzjday;        /*!< GMT julian day */
    int nzhour;        /*!< GMT hour */
    int nzmin;         /*!< GMT minute */
    int nzsec;         /*!< GMT second */
    int nzmsec;        /*!< GMT millisecond */
    int nvhdr;         /*!< Header version number.  Current value is 6. 
                           [required] */
    int norid;         /*!< Origin ID (CSS 3.0) */
    int nevid;         /*!< Event ID (CSS 3.0) */
    int npts;          /*!< Number of points per data component [required] */
    int ninternal1;    /*!< SAC internal variable */
    int nwfid;         /*!< Waveform ID (CSS 3.0) */
    int nxsize;        /*!< Spectral length (spectral files only) */
    int nysize;        /*!< Spectral width (spectral files only) */
    int nunused0;      /*!< Extra space and unused by SAC */
    int iftype;        /*!< Type of file [required].
                             ITIME (01) -> time series file.
                             IRLIM (02) -> spectral file (real and imaginary).
                             IAMPH (03) -> spectral file (amplitude and phase).
                             IXY (04) -> general x versus y data.
                             IXYZ (05) -> general XYZ (3-D) file. */
    int idep;          /*!< Type of dependent variable.
                             IUNKN (unknown).
                             IDISP (displacmenet in nm).
                             IVEL (velocity in nm/sec).
                             IVOLTS (velocity in volts).
                             IACC (acceleration in nm/sec/sec) */
    int iztype;        /*!< Reference time equivalence.
                             IUNWN (unknown).
                             IB (begin time).
                             IDAY (midnight of reference GMT day).
                             IO (event origin time).
                             IA (first arrival time).
                             ITn (user defined pick time n,n=0,9). */
    int nunused1;      /*!< Extra space and unused by SAC */
    int iinst;         /*!< Type of recording instrument [not used] */
    int istreg;        /*!< Station geographic region [not used] */
    int ievreg;        /*!< Event geographic region [not used] */
    int ievtyp;        /*!< Type of event:
                             IUNKN (Unknown).
                             INUCL (Nuclear event).
                             IPREN (Nuclear pre-shot event).
                             IPOSTN (Nuclear post-shot event).
                             IQUAKE (Earthquake).
                             IPREQ (Foreshock).
                             IPOSTQ (Aftershock).
                             ICHEM (Chemical explosion).
                             IQB (Quarry or mine blast confirmed by quarry).
                             IQB1 (Quarry/mine blast with designed shot
                                   info-ripple fired).
                             IQB2 (Quarry/mine blast with observed shot
                                   info-ripple fired).
                             IQBX (Quarry or mine blast - single shot).
                             IQMT (Quarry/mining-induced events:
                                   tremors and rockbursts).
                             IEQ (Earthquake).
                             IEQ1 (Earthquakes in a swarm or aftershock
                                   sequence).
                             IEQ2 (Felt earthquake).
                             IME (Marine explosion).
                             IEX (Other explosion).
                             INU (Nuclear explosion).
                             INC (Nuclear cavity collapse).
                             IO (Other source of known origin).
                             IL (Local event of unknown origin).
                             IR (Regional event of unknown origin).
                             IT (Teleseismic event of unknown origin).
                             IU (Undetermined or conflicting information).
                             IOTHER (Other). */
    int iqual;         /*!< Quality of data [not used]
                             IGOOD (good data).
                             IGLCH (glitches).
                             IDROP (dropouts).
                             ILOWSN (low signal to noise ratio).
                             IOTHER (other) */
    int isynth;        /*!< SYnthetic data flag [not currently used] 
                              IRLDTA (real data).
                              ?????? (flags for various synthetic seismogram
                                      codes) */
    int imagtyp;       /*!< Magnitude type.
                             IMB (body wave magnitude).
                             IMS (surfae wave magnitude).
                             IML (local magnitude).
                             IMW (moment magnitude).
                             IMD (duration magnitude).
                             IMX (user defined magnitude). */
    int imagsrc;       /*!< Source magnitude information:
                             INEIC (National Earthquake Information Center).
                             IPDE (Preliminary Determination of Epicenter).
                             IISC (Internation Seismological Center).
                             IREB (Reviewed Event Bulletin).
                             IUSGS (US Geological Survey).
                             IBRK (UC Berkeley).
                             ICALTECH (California Institute of Technology).
                             ILLNL (Lawrence Livermore National Lab).
                             IEVLOC (Event Location (computer program)).
                             IJSOP (Joint Seismic Observation Program).
                             IUSER (The indivudal using SAC2000).
                             IUNKNOWN (unknown). */
    int nunused2;      /*!< Extra space and unused by SAC */
    int nunused3;      /*!< Extra space and unused by SAC */
    int nunused4;      /*!< Extra space and unused by SAC */
    int nunused5;      /*!< Extra space and unused by SAC */
    int nunused6;      /*!< Extra space and unused by SAC */
    int nunused7;      /*!< Extra space and unused by SAC */
    int nunused8;      /*!< Extra space and unused by SAC */
    int nunused9;      /*!< Extra space and unused by SAC */
    int leven;         /*!< If true then data is evenly spaced [required] */
    int lpspol;        /*!< If true then station components have a positive
                            polarity (left-hand rule). */
    int lovrok;        /*!< If true then it is okay to overwrite this
                            file on disk */
    int lcalda;        /*!< If true then dist, az, baz, and gcard are to 
                            be calculated from the station and event
                            coordinates. */
    int lunused;       /*!< Extra space and unused by SAC */
    hvl_t kstnm;       /*!< Station name */
    hvl_t kevnm;       /*!< Event name */
    hvl_t khole;       /*!< Nuclear: hole identifier.
                            Other: location identifier */
    hvl_t ko;          /*!< Event origin time identification */
    hvl_t ka;          /*!< First arrival identification */
    hvl_t kt0;         /*!< User defined pick time of t0 */
    hvl_t kt1;         /*!< User defined pick time of t1 */
    hvl_t kt2;         /*!< User defined pick time of t2 */
    hvl_t kt3;         /*!< User defined pick time of t3 */
    hvl_t kt4;         /*!< User defined pick time of t4 */
    hvl_t kt5;         /*!< User defined pick time of t5 */
    hvl_t kt6;         /*!< User defined pick time of t6 */
    hvl_t kt7;         /*!< User defined pick time of t7 */
    hvl_t kt8;         /*!< User defined pick time of t8 */
    hvl_t kt9;         /*!< user defined pick time of t9 */
    hvl_t kf;          /*!< End time identification */
    hvl_t kuser0;      /*!< User defined */
    hvl_t kuser1;      /*!< User defined */
    hvl_t kuser2;      /*!< User defined */
    hvl_t kcmpnm;      /*!< Channel name.  SEED volumes use three character
                            names, and the third is the comopnent/orientation.
                            For horizontals, use 1/N and or 2/E. */
    hvl_t knetwk;      /*!< Name of seismic network */
    hvl_t kdatrd;      /*!< Data was read onto computer */
    hvl_t kinst;       /*!< Generic name of recording instrument */
};

struct sacH5PoleZero_struct
{
    /*!
     * @struct sacH5PoleZero_struct
     * @brief The SAC HDF5 data structure for reading and writing poles
     *        and zeros.
     * @ingroup sac_h5
     */
    hvl_t network;        /*!< Network name */
    hvl_t station;        /*!< Station name */
    hvl_t location;       /*!< Location name */
    hvl_t channel;        /*!< Channel name */
    hvl_t comment;        /*!< Comment (e.g. N/A) */
    hvl_t description;    /*!< Description (e.g. Isla Barro, Panama ) */
    hvl_t instrumentType; /*!< instrument type (e.g. STS-2 Standard gain) */
    hvl_t polesRe;        /*!< Real part of analog poles (rad/s) [npoles] */
    hvl_t polesIm;        /*!< Imaginary part of analog poles (rad/s)
                               [npoles] */
    hvl_t zerosRe;        /*!< Real part of analog zeros (rad/s) [nzeros] */
    hvl_t zerosIm;        /*!< Imaginary part of analog zeros (rad/s)
                               [nzeros] */
    double constant;      /*!< System gain (=a0*instgain) */
    double created;       /*!< Time when file was created (seconds since
                               epoch - UTC) */
    double start;         /*!< Earliest time that this metadata is valid
                               (seconds since epoch - UTC) */
    double end;           /*!< Latest time that this metadat is valid
                               (seconds since epoch - UTC) */
    double latitude;      /*!< Station latitude - degrees positive north */
    double longitude;     /*!< Station longitude - degrees positive east */
    double elevation;     /*!< Station elevation above sea level (meters) */
    double depth;         /*!< Station depth */
    double dip;           /*!< Component dip angle (degrees from vertical) */
    double azimuth;       /*!< Component azimuth (degrees from north) */
    double sampleRate;    /*!< Sampling rate (Hz) */
    double instgain;      /*!< Instrument gain */
    double sensitivity;   /*!< Instrument sensititivy */
    double a0;
    int npoles;           /*!< Number of poles */
    int nzeros;           /*!< Number of zeros */
    int inputUnits;       /*!< Input units */
    int instgainUnits;    /*!< Instrument gain units */
    int sensitivityUnits; /*!< Sensitivity units */
    int lhavePZ;          /*!< If 1 then I have the pole-zero structure */
};

struct sacH5FAP_struct
{
    /*!
     * @struct sacH5FAP_struct
     * @brief The SAC HDF5 data structure for reading and writing
     *        frequency, amplitude, and phase responses. 
     * @ingroup sac_h5
     */
    hvl_t freqs;    /*!< Frequencies (Hz).  This is in increasing order and
                         of dimension [nf]. */
    hvl_t amp;      /*!< Amplitude corresponding to frequencies.
                         This is of dimension [nf]. */
    hvl_t phase;    /*!< Phase angle (degrees) corresponding to frequencies.
                         This is of dimension [nf]. */
    int nf;         /*!< Number of frequencies. */
    int lhaveFAP;   /*!< If true then the FAP response is defined. */
};

struct sacH5Data_struct
{
    /*!
     * @struct sacH5Data_struct
     * @brief The SAC HDF5 data structure for reading and writing
     *        SAC data and metadata.
     * @ingroup sac_h5
     */
    hvl_t header;      /*!< Holds the SAC header */
    hvl_t pz;          /*!< Holds the pole-zeros */
    hvl_t fap;         /*!< Holds the FAP response */
    hvl_t data;        /*!< Holds the data */
};

#ifdef __cplusplus
extern "C"
{
#endif

/*! Frees memory on a data structure */
int sacioh5_freeData(struct sacH5Data_struct *data);
/*! Frees memory on header */
int sacioh5_freeHeader(struct sacH5Header_struct *header);
/*! Frees memory on pole-zero structure */
int sacioh5_freePoleZero(struct sacH5PoleZero_struct *pz);
/*! Frees the FAP structure */
int sacioh5_freeFAP(struct sacH5FAP_struct *fap);
/*! Creates the HDF5 header type */
int sacioh5_createHeaderType(const hid_t groupID);
/*! Creates the HDF5 data type */
int sacioh5_createDataType(const hid_t groupID);
/*! Creates the HDF5 FAP type */
int sacio_createFAPType(const hid_t groupID);
/*! Creates the HDF5 pole-zero type */
int sacio_createPoleZeroType(const hid_t groupID);
/*! Gets the float header values. */
int sacioh5_getFloatHeader(const enum sacHeader_enum type,
                           const struct sacH5Header_struct header,
                           double *var);
/*! Gets the boolean header values. */
int sacioh5_getBooleanHeader(const enum sacHeader_enum type,
                             const struct sacH5Header_struct header,
                             int *lvar);
/*! Gets integer header values. */
int sacioh5_getIntegerHeader(const enum sacHeader_enum type,
                             const struct sacH5Header_struct header,
                             int *ivar);
/*! Gets character header values. */
int sacioh5_getCharacterHeader(const enum sacHeader_enum type,
                               const struct sacH5Header_struct header,
                               char *kvar);
/*! Gets list of SAC files in a group */
char **sacioh5_getFilesInGroup(const hid_t groupID,
                               int *nfiles, int *ierr);
/*! Sets boolean values in the HDF5 header */
int sacioh5_setBooleanHeader(const enum sacHeader_enum type,
                             const bool lvar,
                             struct sacH5Header_struct *header);
/*! Gets float values in the HDF5 header */
int sacioh5_setFloatHeader(const enum sacHeader_enum type,
                           const double var,
                           struct sacH5Header_struct *header);
/*! Gets integer values in the HDF5 header */
int sacioh5_setIntegerHeader(const enum sacHeader_enum type,
                             const int ivar,
                             struct sacH5Header_struct *header);
/*! Gets boolean values in the HDF5 header */
int sacioh5_setBooleanHeader(const enum sacHeader_enum type,
                             const bool lvar,
                             struct sacH5Header_struct *header);
/*! Gets character values in the HDF5 header */
int sacioh5_setCharacterHeader(const enum sacHeader_enum type,
                               const char *kvar,
                               struct sacH5Header_struct *header);
/*! Reads a list of time series files */
struct sacData_struct *
   sacioh5_readTimeSeriesList(const int nnames, const char **dataNames,
                              const hid_t groupID,
                              int *nfilesRead, int *ierr);
/*! Reads the time series data */
int sacioh5_readTimeSeries2(const char *dataSetName,
                            const hid_t groupID,
                            struct sacData_struct *sac);
/*! Writes the time series */
int sacioh5_writeTimeSeries1(const char *dataSetName,
                             const char *groupName, const hid_t fileID,
                             const struct sacData_struct sac);
int sacioh5_writeTimeSeries2(const char *dataSetName,
                             const hid_t groupID,
                             const struct sacData_struct sac);
/*! Writes many SAC time series to hyperslabs */
int sacioh5_writeTracesToChunk(const char *dataSetName,
                               const int ntrace,
                               const int compress,
                               const hid_t groupID,
                               const size_t *traceChunk,
                               const size_t *dataChunk,
                               const struct sacData_struct *sac);
/*! Reads many SAC time series from hyperslabs */
int sacioh5_readChunkedTraces(const char *dataSetName,
                              const hid_t groupID,
                              int *ntrace,
                              struct sacData_struct **sacOut);

#ifdef __cplusplus
}
#endif
#endif
#endif
