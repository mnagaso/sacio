#ifndef SACIO_HPP
#define SACIO_HPP 1
#include <cstdio>
#include <string>
#include <vector>
#include "sacio_config.h"
#include "sacio_core.h"
#include "sacio_os.h"
#include <complex>

class SacHeader;
class SacData;
class SacMPI;
class Sac;

class SacHeader 
{
    public:
        SacHeader(void); 
        SacHeader(const SacHeader &sacHeader);
        SacHeader& operator=(const SacHeader &sacHeader);
        bool operator==(const SacHeader &h1) const;
        bool operator!=(const SacHeader &h1) const;
        SacHeader(const struct sacHeader_struct sacHeader);
        ~SacHeader(void);
        void clear(void);
        int readHeader(const std::string fileName);
        int readHeader(const char *fileName);
        void makeSacHeaderStruct(struct sacHeader_struct *sacHeader);
        int getIntegerHeader(const enum sacIntegerHeader_enum hvar,
                            int *hdr) const;
        int setIntegerHeader(const enum sacIntegerHeader_enum hvar,
                             const int hdr);
        int getBooleanHeader(const enum sacBooleanHeader_enum hvar,
                             bool *hdr) const;
        int setBooleanHeader(const enum sacBooleanHeader_enum hvar,
                             const bool hdr);
        int getDoubleHeader(const enum sacDoubleHeader_enum hvar,
                            double *hdr) const;
        int setDoubleHeader(const enum sacDoubleHeader_enum hvar,
                            const double hdr);
        int getFloatHeader(const enum sacFloatHeader_enum hvar,
                           float *hdr) const;
        int setFloatHeader(const enum sacFloatHeader_enum hvar,
                           const float hdr);
        int getStringHeader(const enum sacStringHeader_enum hvar,
                            std::string &hdr) const;
        int setStringHeader(const enum sacStringHeader_enum hvar,
                            const std::string hdr);
        int getCharacterHeader(const enum sacCharacterHeader_enum hvar,
                              char hdr[]) const;
        int setCharacterHeader(const enum sacCharacterHeader_enum hvar,
                               const char hdr[]);
        int getHeader(const enum sacStringHeader_enum hvar,
                      std::string &hdr) const;
        int getHeader(const enum sacCharacterHeader_enum hvar,
                      char hdr[]) const;
        int getHeader(const enum sacDoubleHeader_enum hvar,
                      double *hdr) const;
        int getHeader(const enum sacFloatHeader_enum hvar,
                      float *hdr) const;
        int getHeader(const enum sacIntegerHeader_enum hvar,
                      int *hdr) const;
        int getHeader(const enum sacBooleanHeader_enum hvar,
                      bool *hdr) const;
        int setHeader(const enum sacStringHeader_enum hvar,
                      const std::string hdr);
        int setHeader(const enum sacCharacterHeader_enum hvar,
                      const char hdr[]);
        int setHeader(const enum sacDoubleHeader_enum hvar,
                      const double hdr);
        int setHeader(const enum sacFloatHeader_enum hvar,
                      const float hdr);
        int setHeader(const enum sacIntegerHeader_enum hvar,
                      const int hdr);
        int setHeader(const enum sacBooleanHeader_enum hvar,
                      const bool hdr);
        int setEpochalStartTime(const double t0);
        int getEpochalStartTime(double *t0) const;
        int getEpochalEndTime(double *t1) const;
        int print(FILE *file = nullptr);
        void getSacHeaderStruct(struct sacHeader_struct *header) const;
        void setEqualityTolerance(const double tol);
        double getEqualityTolerance(void) const;
        friend void swap(SacHeader &sac1, SacHeader &sac2);
/*
        template <typename T>
        int setHeader(const enum sacHeader_enum hdrVariable, const T hdr)
        {
            int ierr = 0;
            if (typeid(T).name() == typeid(double).name())
            {
                double dhdr = static_cast<double> (hdr);
                ierr = setDoubleHeader(hdrVariable, dhdr);
            }
            else if (typeid(T).name() == typeid(float).name())
            {
                float fhdr = static_cast<float> (hdr);
                ierr = setFloatHeader(hdrVariable, fhdr);
            }
            else if (typeid(T).name() == typeid(int).name())
            {
                float ihdr = static_cast<int> (hdr);
                ierr = setIntegerHeader(hdrVariable, ihdr);
            }
            else if (typeid(T).name() == typeid(bool).name())
            {
                bool bhdr = static_cast<bool> (hdr);
                ierr = setBooleanHeader(hdrVariable, bhdr);
            }
            else if (typeid(T).name() == typeid(std::string).name())
            {
                //std::string shdr = static_cast<std::string> (hdr);
            }
            else
            {
                fprintf(stderr, "%s: Could not determine type\n", __func__);
                return -1;
            }
            return ierr;
        };
*/
    private:
        /*!< The SAC header. */
        struct sacHeader_struct header_;
        /*!< Comparision tolerance for double variables being equal. */
        double tol_ = 1.e-7;
        friend class SacMPI;
    //=======================================================================//
    //                    Define the header enumerated types.                //
    //=======================================================================//
    public:
        /*!
         * @brief Defines the double header variable names.  These are 
         *        equivalent to the floats.
         * @ingroup SacHeaderpp
         */
        class Double
        {
            public:
                /*!< Sampling period. */
                static constexpr enum sacDoubleHeader_enum DELTA   = SAC_DOUBLE_HDR_DELTA;
                /*!< Minimum value of dependent variable. */
                static constexpr enum sacDoubleHeader_enum DEPMIN  = SAC_DOUBLE_HDR_DEPMIN; 
                /*!< Maximum value of dependent variable. */
                static constexpr enum sacDoubleHeader_enum DEPMAX  = SAC_DOUBLE_HDR_DEPMAX; 
                /*!< Scale factor multiplying dependent variable. */
                static constexpr enum sacDoubleHeader_enum SCALE   = SAC_DOUBLE_HDR_SCALE;
                /*!< Observed increment; if different than delta. */
                static constexpr enum sacDoubleHeader_enum ODELTA  = SAC_DOUBLE_HDR_ODELTA;
                /*!< Beginning value of dependent variable. */
                static constexpr enum sacDoubleHeader_enum B       = SAC_DOUBLE_HDR_B;
                /*!< Ending value of dependent variable. */
                static constexpr enum sacDoubleHeader_enum E       = SAC_DOUBLE_HDR_E;
                /*!< Origin time (seconds relative to reference time. */
                static constexpr enum sacDoubleHeader_enum O       = SAC_DOUBLE_HDR_O;
                /*!< First arrival time relative to reference time. */
                static constexpr enum sacDoubleHeader_enum A       = SAC_DOUBLE_HDR_A;
                /*!< Internal variable. */
                static constexpr enum sacDoubleHeader_enum INTERNAL1 = SAC_DOUBLE_HDR_INTERNAL1;
                /*!< First user defined pick time or marker (seconds relative to reference time). */
                static constexpr enum sacDoubleHeader_enum T0      = SAC_DOUBLE_HDR_T0;
                /*!< Second user defined pick time or marker. */
                static constexpr enum sacDoubleHeader_enum T1      = SAC_DOUBLE_HDR_T1;
                /*!< Third user defined pick time or marker. */
                static constexpr enum sacDoubleHeader_enum T2      = SAC_DOUBLE_HDR_T2;
                /*!< Fourth user defined pick time or marker. */
                static constexpr enum sacDoubleHeader_enum T3      = SAC_DOUBLE_HDR_T3;
                /*!< Fifth user defined pick time or marker. */
                static constexpr enum sacDoubleHeader_enum T4      = SAC_DOUBLE_HDR_T4;
                /*!< Sixth user defined pick time or marker. */
                static constexpr enum sacDoubleHeader_enum T5      = SAC_DOUBLE_HDR_T5;
                /*!< Seventh user defined pick time or marker. */
                static constexpr enum sacDoubleHeader_enum T6      = SAC_DOUBLE_HDR_T6;
                /*!< Eighth user defined pick time or marker. */
                static constexpr enum sacDoubleHeader_enum T7      = SAC_DOUBLE_HDR_T7;
                /*!< Ninth user defined pick time or marker. */
                static constexpr enum sacDoubleHeader_enum T8      = SAC_DOUBLE_HDR_T8;
                /*!< Tenth user defined pick time or marker. */
                static constexpr enum sacDoubleHeader_enum T9      = SAC_DOUBLE_HDR_T9;
                /*!< End time of event. */
                static constexpr enum sacDoubleHeader_enum F       = SAC_DOUBLE_HDR_F;
                /*!< First instrument response parameter. */
                static constexpr enum sacDoubleHeader_enum RESP0   = SAC_DOUBLE_HDR_RESP0;
                /*!< Second instrument response parameter. */
                static constexpr enum sacDoubleHeader_enum RESP1   = SAC_DOUBLE_HDR_RESP1;
                /*!< Third instrument response parameter. */
                static constexpr enum sacDoubleHeader_enum RESP2   = SAC_DOUBLE_HDR_RESP2;
                /*!< Fourth instrument response parameter. */
                static constexpr enum sacDoubleHeader_enum RESP3   = SAC_DOUBLE_HDR_RESP3;
                /*!< Fifth instrument response parameter. */
                static constexpr enum sacDoubleHeader_enum RESP4   = SAC_DOUBLE_HDR_RESP4;
                /*!< Sixth instrument response parameter. */
                static constexpr enum sacDoubleHeader_enum RESP5   = SAC_DOUBLE_HDR_RESP5;
                /*!< Seventh instrument response parameter. */
                static constexpr enum sacDoubleHeader_enum RESP6   = SAC_DOUBLE_HDR_RESP6;
                /*!< Eigth instrument response parameter. */
                static constexpr enum sacDoubleHeader_enum RESP7   = SAC_DOUBLE_HDR_RESP7;
                /*!< Ninth instrument response parameter. */
                static constexpr enum sacDoubleHeader_enum RESP8   = SAC_DOUBLE_HDR_RESP8;
                /*!< Tenth instrument response parameter. */
                static constexpr enum sacDoubleHeader_enum RESP9   = SAC_DOUBLE_HDR_RESP9;
                /*!< Station latitude. */
                static constexpr enum sacDoubleHeader_enum STLA    = SAC_DOUBLE_HDR_STLA;
                /*!< Station longitude. */
                static constexpr enum sacDoubleHeader_enum STLO    = SAC_DOUBLE_HDR_STLO;
                /*!< Station elevation. */
                static constexpr enum sacDoubleHeader_enum STEL    = SAC_DOUBLE_HDR_STEL;
                /*!< Station depth. */
                static constexpr enum sacDoubleHeader_enum STDP    = SAC_DOUBLE_HDR_STDP;
                /*!< Event latitude. */
                static constexpr enum sacDoubleHeader_enum EVLA    = SAC_DOUBLE_HDR_EVLA;
                /*!< Event longitude. */
                static constexpr enum sacDoubleHeader_enum EVLO    = SAC_DOUBLE_HDR_EVLO;
                /*!< Event elevation. */
                static constexpr enum sacDoubleHeader_enum EVEL    = SAC_DOUBLE_HDR_EVEL;
                /*!< Event depth. */
                static constexpr enum sacDoubleHeader_enum EVDP    = SAC_DOUBLE_HDR_EVDP;
                /*!< Event magnitude. */
                static constexpr enum sacDoubleHeader_enum MAG     = SAC_DOUBLE_HDR_MAG;
                /*!< First user defined variable. */
                static constexpr enum sacDoubleHeader_enum USER0   = SAC_DOUBLE_HDR_USER0;
                /*!< Second user defined variable. */
                static constexpr enum sacDoubleHeader_enum USER1   = SAC_DOUBLE_HDR_USER1;
                /*!< Third user defined variable. */
                static constexpr enum sacDoubleHeader_enum USER2   = SAC_DOUBLE_HDR_USER2;
                /*!< Fourth user defined variable. */
                static constexpr enum sacDoubleHeader_enum USER3   = SAC_DOUBLE_HDR_USER3;
                /*!< Fifth user defined variable. */
                static constexpr enum sacDoubleHeader_enum USER4   = SAC_DOUBLE_HDR_USER4;
                /*!< Sixth user defined variable. */
                static constexpr enum sacDoubleHeader_enum USER5   = SAC_DOUBLE_HDR_USER5;
                /*!< Seventh user defined variable. */
                static constexpr enum sacDoubleHeader_enum USER6   = SAC_DOUBLE_HDR_USER6;
                /*!< Eighth user defined variable. */
                static constexpr enum sacDoubleHeader_enum USER7   = SAC_DOUBLE_HDR_USER7;
                /*!< Ninth user defined variable. */
                static constexpr enum sacDoubleHeader_enum USER8   = SAC_DOUBLE_HDR_USER8;
                /*!< Tenth user defined variable. */
                static constexpr enum sacDoubleHeader_enum USER9   = SAC_DOUBLE_HDR_USER9;
                /*!< Source-receiver distance. */
                static constexpr enum sacDoubleHeader_enum DIST    = SAC_DOUBLE_HDR_DIST;
                /*!< Source-receiver azimuth. */
                static constexpr enum sacDoubleHeader_enum AZ      = SAC_DOUBLE_HDR_AZ;
                /*!< Source-receiver back-azimuth. */
                static constexpr enum sacDoubleHeader_enum BAZ     = SAC_DOUBLE_HDR_BAZ;
                /*!< Source-receiver great circle distance. */
                static constexpr enum sacDoubleHeader_enum GCARC   = SAC_DOUBLE_HDR_GCARC;
                /*!< Internal variable. */
                static constexpr enum sacDoubleHeader_enum INTERNAL2 = SAC_DOUBLE_HDR_INTERNAL2;
                /*!< Mean value of dependent variable. */
                static constexpr enum sacDoubleHeader_enum DEPMEN  = SAC_DOUBLE_HDR_DEPMEN;
                /*!< Component azimuth. */
                static constexpr enum sacDoubleHeader_enum CMPAZ   = SAC_DOUBLE_HDR_CMPAZ;
                /*!< Component incident angle. */
                static constexpr enum sacDoubleHeader_enum CMPINC  = SAC_DOUBLE_HDR_CMPINC;
                /*!< Minimum value of x (spectral files only). */
                static constexpr enum sacDoubleHeader_enum XMINIMUM = SAC_DOUBLE_HDR_XMINIMUM;
                /*!< Maximum value of x (spectral files only). */
                static constexpr enum sacDoubleHeader_enum XMAXIMUM = SAC_DOUBLE_HDR_XMAXIMUM;
                /*!< Minimum value of y (spectral files only). */
                static constexpr enum sacDoubleHeader_enum YMINIMUM = SAC_DOUBLE_HDR_YMINIMUM;
                /*!< Maximum value of y (spectral files only). */
                static constexpr enum sacDoubleHeader_enum YMAXIMUM = SAC_DOUBLE_HDR_YMAXIMUM;
                /*!< Extra float variable. */
                static constexpr enum sacDoubleHeader_enum UNUSED0  = SAC_DOUBLE_HDR_UNUSED0;
                /*!< Extra float variable. */
                static constexpr enum sacDoubleHeader_enum UNUSED1  = SAC_DOUBLE_HDR_UNUSED1;
                /*!< Extra float variable. */
                static constexpr enum sacDoubleHeader_enum UNUSED2  = SAC_DOUBLE_HDR_UNUSED2;
                /*!< Extra float variable. */
                static constexpr enum sacDoubleHeader_enum UNUSED3  = SAC_DOUBLE_HDR_UNUSED3;
                /*!< Extra float variable. */
                static constexpr enum sacDoubleHeader_enum UNUSED4  = SAC_DOUBLE_HDR_UNUSED4;
                /*!< Extra float variable. */
                static constexpr enum sacDoubleHeader_enum UNUSED5  = SAC_DOUBLE_HDR_UNUSED5;
                /*!< Extra float variable. */
                static constexpr enum sacDoubleHeader_enum UNUSED6  = SAC_DOUBLE_HDR_UNUSED6;
        };
        /*!
         * @brief Defines the float header variable names.  These are 
         *        equivalent to the doubles.
         * @ingroup SacHeaderpp
         */
        class Float
        {
            public:
                /*!< Sampling period. */
                static constexpr enum sacFloatHeader_enum DELTA   = SAC_FLOAT_HDR_DELTA;
                /*!< Minimum value of dependent variable. */
                static constexpr enum sacFloatHeader_enum DEPMIN  = SAC_FLOAT_HDR_DEPMIN; 
                /*!< Maximum value of dependent variable. */
                static constexpr enum sacFloatHeader_enum DEPMAX  = SAC_FLOAT_HDR_DEPMAX; 
                /*!< Scale factor multiplying dependent variable. */
                static constexpr enum sacFloatHeader_enum SCALE   = SAC_FLOAT_HDR_SCALE;
                /*!< Observed increment; if different than delta. */
                static constexpr enum sacFloatHeader_enum ODELTA  = SAC_FLOAT_HDR_ODELTA;
                /*!< Beginning value of dependent variable. */
                static constexpr enum sacFloatHeader_enum B       = SAC_FLOAT_HDR_B;
                /*!< Ending value of dependent variable. */
                static constexpr enum sacFloatHeader_enum E       = SAC_FLOAT_HDR_E;
                /*!< Origin time (seconds relative to reference time. */
                static constexpr enum sacFloatHeader_enum O       = SAC_FLOAT_HDR_O;
                /*!< First arrival time relative to reference time. */
                static constexpr enum sacFloatHeader_enum A       = SAC_FLOAT_HDR_A;
                /*!< Internal variable. */
                static constexpr enum sacFloatHeader_enum INTERNAL1 = SAC_FLOAT_HDR_INTERNAL1;
                /*!< First user defined pick time or marker (seconds relative to reference time). */
                static constexpr enum sacFloatHeader_enum T0      = SAC_FLOAT_HDR_T0;
                /*!< Second user defined pick time or marker. */
                static constexpr enum sacFloatHeader_enum T1      = SAC_FLOAT_HDR_T1;
                /*!< Third user defined pick time or marker. */
                static constexpr enum sacFloatHeader_enum T2      = SAC_FLOAT_HDR_T2;
                /*!< Fourth user defined pick time or marker. */
                static constexpr enum sacFloatHeader_enum T3      = SAC_FLOAT_HDR_T3;
                /*!< Fifth user defined pick time or marker. */
                static constexpr enum sacFloatHeader_enum T4      = SAC_FLOAT_HDR_T4;
                /*!< Sixth user defined pick time or marker. */
                static constexpr enum sacFloatHeader_enum T5      = SAC_FLOAT_HDR_T5;
                /*!< Seventh user defined pick time or marker. */
                static constexpr enum sacFloatHeader_enum T6      = SAC_FLOAT_HDR_T6;
                /*!< Eighth user defined pick time or marker. */
                static constexpr enum sacFloatHeader_enum T7      = SAC_FLOAT_HDR_T7;
                /*!< Ninth user defined pick time or marker. */
                static constexpr enum sacFloatHeader_enum T8      = SAC_FLOAT_HDR_T8;
                /*!< Tenth user defined pick time or marker. */
                static constexpr enum sacFloatHeader_enum T9      = SAC_FLOAT_HDR_T9;
                /*!< End time of event. */
                static constexpr enum sacFloatHeader_enum F       = SAC_FLOAT_HDR_F;
                /*!< First instrument response parameter. */
                static constexpr enum sacFloatHeader_enum RESP0   = SAC_FLOAT_HDR_RESP0;
                /*!< Second instrument response parameter. */
                static constexpr enum sacFloatHeader_enum RESP1   = SAC_FLOAT_HDR_RESP1;
                /*!< Third instrument response parameter. */
                static constexpr enum sacFloatHeader_enum RESP2   = SAC_FLOAT_HDR_RESP2;
                /*!< Fourth instrument response parameter. */
                static constexpr enum sacFloatHeader_enum RESP3   = SAC_FLOAT_HDR_RESP3;
                /*!< Fifth instrument response parameter. */
                static constexpr enum sacFloatHeader_enum RESP4   = SAC_FLOAT_HDR_RESP4;
                /*!< Sixth instrument response parameter. */
                static constexpr enum sacFloatHeader_enum RESP5   = SAC_FLOAT_HDR_RESP5;
                /*!< Seventh instrument response parameter. */
                static constexpr enum sacFloatHeader_enum RESP6   = SAC_FLOAT_HDR_RESP6;
                /*!< Eigth instrument response parameter. */
                static constexpr enum sacFloatHeader_enum RESP7   = SAC_FLOAT_HDR_RESP7;
                /*!< Ninth instrument response parameter. */
                static constexpr enum sacFloatHeader_enum RESP8   = SAC_FLOAT_HDR_RESP8;
                /*!< Tenth instrument response parameter. */
                static constexpr enum sacFloatHeader_enum RESP9   = SAC_FLOAT_HDR_RESP9;
                /*!< Station latitude. */
                static constexpr enum sacFloatHeader_enum STLA    = SAC_FLOAT_HDR_STLA;
                /*!< Station longitude. */
                static constexpr enum sacFloatHeader_enum STLO    = SAC_FLOAT_HDR_STLO;
                /*!< Station elevation. */
                static constexpr enum sacFloatHeader_enum STEL    = SAC_FLOAT_HDR_STEL;
                /*!< Station depth. */
                static constexpr enum sacFloatHeader_enum STDP    = SAC_FLOAT_HDR_STDP;
                /*!< Event latitude. */
                static constexpr enum sacFloatHeader_enum EVLA    = SAC_FLOAT_HDR_EVLA;
                /*!< Event longitude. */
                static constexpr enum sacFloatHeader_enum EVLO    = SAC_FLOAT_HDR_EVLO;
                /*!< Event elevation. */
                static constexpr enum sacFloatHeader_enum EVEL    = SAC_FLOAT_HDR_EVEL;
                /*!< Event depth. */
                static constexpr enum sacFloatHeader_enum EVDP    = SAC_FLOAT_HDR_EVDP;
                /*!< Event magnitude. */
                static constexpr enum sacFloatHeader_enum MAG     = SAC_FLOAT_HDR_MAG;
                /*!< First user defined variable. */
                static constexpr enum sacFloatHeader_enum USER0   = SAC_FLOAT_HDR_USER0;
                /*!< Second user defined variable. */
                static constexpr enum sacFloatHeader_enum USER1   = SAC_FLOAT_HDR_USER1;
                /*!< Third user defined variable. */
                static constexpr enum sacFloatHeader_enum USER2   = SAC_FLOAT_HDR_USER2;
                /*!< Fourth user defined variable. */
                static constexpr enum sacFloatHeader_enum USER3   = SAC_FLOAT_HDR_USER3;
                /*!< Fifth user defined variable. */
                static constexpr enum sacFloatHeader_enum USER4   = SAC_FLOAT_HDR_USER4;
                /*!< Sixth user defined variable. */
                static constexpr enum sacFloatHeader_enum USER5   = SAC_FLOAT_HDR_USER5;
                /*!< Seventh user defined variable. */
                static constexpr enum sacFloatHeader_enum USER6   = SAC_FLOAT_HDR_USER6;
                /*!< Eighth user defined variable. */
                static constexpr enum sacFloatHeader_enum USER7   = SAC_FLOAT_HDR_USER7;
                /*!< Ninth user defined variable. */
                static constexpr enum sacFloatHeader_enum USER8   = SAC_FLOAT_HDR_USER8;
                /*!< Tenth user defined variable. */
                static constexpr enum sacFloatHeader_enum USER9   = SAC_FLOAT_HDR_USER9;
                /*!< Source-receiver distance. */
                static constexpr enum sacFloatHeader_enum DIST    = SAC_FLOAT_HDR_DIST;
                /*!< Source-receiver azimuth. */
                static constexpr enum sacFloatHeader_enum AZ      = SAC_FLOAT_HDR_AZ;
                /*!< Source-receiver back-azimuth. */
                static constexpr enum sacFloatHeader_enum BAZ     = SAC_FLOAT_HDR_BAZ;
                /*!< Source-receiver great circle distance. */
                static constexpr enum sacFloatHeader_enum GCARC   = SAC_FLOAT_HDR_GCARC;
                /*!< Internal variable. */
                static constexpr enum sacFloatHeader_enum INTERNAL2 = SAC_FLOAT_HDR_INTERNAL2;
                /*!< Mean value of dependent variable. */
                static constexpr enum sacFloatHeader_enum DEPMEN  = SAC_FLOAT_HDR_DEPMEN;
                /*!< Component azimuth. */
                static constexpr enum sacFloatHeader_enum CMPAZ   = SAC_FLOAT_HDR_CMPAZ;
                /*!< Component incident angle. */
                static constexpr enum sacFloatHeader_enum CMPINC  = SAC_FLOAT_HDR_CMPINC;
                /*!< Minimum value of x (spectral files only). */
                static constexpr enum sacFloatHeader_enum XMINIMUM = SAC_FLOAT_HDR_XMINIMUM;
                /*!< Maximum value of x (spectral files only). */
                static constexpr enum sacFloatHeader_enum XMAXIMUM = SAC_FLOAT_HDR_XMAXIMUM;
                /*!< Minimum value of y (spectral files only). */
                static constexpr enum sacFloatHeader_enum YMINIMUM = SAC_FLOAT_HDR_YMINIMUM;
                /*!< Maximum value of y (spectral files only). */
                static constexpr enum sacFloatHeader_enum YMAXIMUM = SAC_FLOAT_HDR_YMAXIMUM;
                /*!< Extra float variable. */
                static constexpr enum sacFloatHeader_enum UNUSED0  = SAC_FLOAT_HDR_UNUSED0;
                /*!< Extra float variable. */
                static constexpr enum sacFloatHeader_enum UNUSED1  = SAC_FLOAT_HDR_UNUSED1;
                /*!< Extra float variable. */
                static constexpr enum sacFloatHeader_enum UNUSED2  = SAC_FLOAT_HDR_UNUSED2;
                /*!< Extra float variable. */
                static constexpr enum sacFloatHeader_enum UNUSED3  = SAC_FLOAT_HDR_UNUSED3;
                /*!< Extra float variable. */
                static constexpr enum sacFloatHeader_enum UNUSED4  = SAC_FLOAT_HDR_UNUSED4;
                /*!< Extra float variable. */
                static constexpr enum sacFloatHeader_enum UNUSED5  = SAC_FLOAT_HDR_UNUSED5;
                /*!< Extra float variable. */
                static constexpr enum sacFloatHeader_enum UNUSED6  = SAC_FLOAT_HDR_UNUSED6;
        };
        /*!
         * @brief Defines the integer variable header names.
         * @ingroup SacHeaderpp
         */
        class Integer
        {
            public:
                /*!< Year of trace start time. */
                static constexpr enum sacIntegerHeader_enum NZYEAR  = SAC_INT_HDR_NZYEAR;
                /*!< Julian day of trace start time. */
                static constexpr enum sacIntegerHeader_enum NZJDAY  = SAC_INT_HDR_NZJDAY;
                /*!< Hour of trace start time. */
                static constexpr enum sacIntegerHeader_enum NZHOUR  = SAC_INT_HDR_NZHOUR;
                /*!< Minute of trace start time. */
                static constexpr enum sacIntegerHeader_enum NZMIN   = SAC_INT_HDR_NZMIN;
                /*!< Second of trace start time. */
                static constexpr enum sacIntegerHeader_enum NZSEC   = SAC_INT_HDR_NZSEC;
                /*!< Millisecond of trace start time. */
                static constexpr enum sacIntegerHeader_enum NZMSEC  = SAC_INT_HDR_NZMSEC;
                /*!< Header version number. */
                static constexpr enum sacIntegerHeader_enum NVHDR   = SAC_INT_HDR_NVHDR;
                /*!< CSS 3.0 origin ID. */
                static constexpr enum sacIntegerHeader_enum NORID   = SAC_INT_HDR_NORID;
                /*!< CSS 3.0 event ID. */
                static constexpr enum sacIntegerHeader_enum NEVID   = SAC_INT_HDR_NEVID;
                /*!< Number of points in time series. */
                static constexpr enum sacIntegerHeader_enum NPTS    = SAC_INT_HDR_NPTS;
                /*!< Internal variable. */
                static constexpr enum sacIntegerHeader_enum INTERNAL1 = SAC_INT_HDR_INTERNAL1;
                /*!< CSS 3.0 Waveform ID. */
                static constexpr enum sacIntegerHeader_enum NWFID   = SAC_INT_HDR_NWFID;
                /*!< Spectral length - for spectral files only. */
                static constexpr enum sacIntegerHeader_enum NXSIZE  = SAC_INT_HDR_NXSIZE;
                /*!< Spectral width - for spectral files only. */
                static constexpr enum sacIntegerHeader_enum NYSIZE  = SAC_INT_HDR_NYSIZE;
                /*!< Extra integer variable. */
                static constexpr enum sacIntegerHeader_enum UNUSED0 = SAC_INT_HDR_UNUSED0;
                /*!< Type of file.  */
                static constexpr enum sacIntegerHeader_enum IFTYPE  = SAC_INT_HDR_IFTYPE;
                /*!< Type of dependent variable. */
                static constexpr enum sacIntegerHeader_enum IDEP    = SAC_INT_HDR_IDEP;
                /*!< Reference time equivalence. */
                static constexpr enum sacIntegerHeader_enum IZTYPE  = SAC_INT_HDR_IZTYPE;
                /*!< Extra integer variable. */
                static constexpr enum sacIntegerHeader_enum UNUSED1 = SAC_INT_HDR_UNUSED1;
                /*!< Type of recording instrument. */
                static constexpr enum sacIntegerHeader_enum IINST   = SAC_INT_HDR_IINST;
                /*!< Station geographic region. */
                static constexpr enum sacIntegerHeader_enum ISTREG  = SAC_INT_HDR_ISTREG;
                /*!< Event geographic region. */
                static constexpr enum sacIntegerHeader_enum IEVREG  = SAC_INT_HDR_IEVREG;
                /*!< Event type. */
                static constexpr enum sacIntegerHeader_enum IEVTYP  = SAC_INT_HDR_IEVTYP;
                /*!< Quality of data. */
                static constexpr enum sacIntegerHeader_enum IQUAL   = SAC_INT_HDR_IQUAL;
                /*!< Synthetic data flag. */
                static constexpr enum sacIntegerHeader_enum ISYNTH  = SAC_INT_HDR_ISYNTH;
                /*!< Source magnitude type. */
                static constexpr enum sacIntegerHeader_enum IMAGTYP = SAC_INT_HDR_IMAGTYP;
                /*!< Source magnitude information. */
                static constexpr enum sacIntegerHeader_enum IMAGSRC = SAC_INT_HDR_IMAGSRC;
                /*!< Extra integer variable. */
                static constexpr enum sacIntegerHeader_enum UNUSED2 = SAC_INT_HDR_UNUSED2;
                /*!< Extra integer variable. */
                static constexpr enum sacIntegerHeader_enum UNUSED3 = SAC_INT_HDR_UNUSED3;
                /*!< Extra integer variable. */
                static constexpr enum sacIntegerHeader_enum UNUSED4 = SAC_INT_HDR_UNUSED4;
                /*!< Extra integer variable. */
                static constexpr enum sacIntegerHeader_enum UNUSED5 = SAC_INT_HDR_UNUSED5;
                /*!< Extra integer variable. */
                static constexpr enum sacIntegerHeader_enum UNUSED6 = SAC_INT_HDR_UNUSED6;
                /*!< Extra integer variable. */
                static constexpr enum sacIntegerHeader_enum UNUSED7 = SAC_INT_HDR_UNUSED7;
                /*!< Extra integer variable. */
                static constexpr enum sacIntegerHeader_enum UNUSED8 = SAC_INT_HDR_UNUSED8;
                /*!< Extra integer variable. */
                static constexpr enum sacIntegerHeader_enum UNUSED9 = SAC_INT_HDR_UNUSED9;
        };
        /*!
         * @brief Defines the boolean header variables.
         * @ingroup SacHeaderpp
         */
        class Boolean
        {
            public:
                /*!< If true then data is evenly spaced. */
                static constexpr enum sacBooleanHeader_enum LEVEN   = SAC_BOOL_HDR_LEVEN;
                /*!< If true then station conforms to left hand rule. */
                static constexpr enum sacBooleanHeader_enum LPSOL   = SAC_BOOL_HDR_LPSPOL;
                /*!< If true then it is okay to overwrite file. */
                static constexpr enum sacBooleanHeader_enum LOVROK  = SAC_BOOL_HDR_LOVROK;
                /*!< If true then SAC wil compute dist, az, and gcarc. */
                static constexpr enum sacBooleanHeader_enum LCALDA  = SAC_BOOL_HDR_LCALDA;
                /*!< Extra logical variable. */
                static constexpr enum sacBooleanHeader_enum LUNUSED = SAC_BOOL_HDR_LUNUSED;
        };
        /*!
         * @brief Defines the string header variables.  These are equivalent to
         *        the character header variables.
         * @ingroup SacHeaderpp
         */
        class String
        {
            public:
                /*!< Station name. */
                static constexpr enum sacStringHeader_enum KSTNM  = SAC_STRING_HDR_KSTNM;
                /*!< Event name. */
                static constexpr enum sacStringHeader_enum KEVNM  = SAC_STRING_HDR_KEVNM;
                /*!< Location code. */
                static constexpr enum sacStringHeader_enum KHOLE  = SAC_STRING_HDR_KHOLE;
                /*!< Origin time identifier. */
                static constexpr enum sacStringHeader_enum K0     = SAC_STRING_HDR_KO;
                /*!< First arrival identifier. */
                static constexpr enum sacStringHeader_enum KA     = SAC_STRING_HDR_KA;
                /*!< Label for first pick time. */
                static constexpr enum sacStringHeader_enum KT0    = SAC_STRING_HDR_KT0;
                /*!< Label for second pick time. */
                static constexpr enum sacStringHeader_enum KT1    = SAC_STRING_HDR_KT1;
                /*!< Label for third pick time. */
                static constexpr enum sacStringHeader_enum KT2    = SAC_STRING_HDR_KT2;
                /*!< Label for fourth pick time. */
                static constexpr enum sacStringHeader_enum KT3    = SAC_STRING_HDR_KT3;
                /*!< Label for fifth pick time. */
                static constexpr enum sacStringHeader_enum KT4    = SAC_STRING_HDR_KT4;
                /*!< Label for sixth pick time. */
                static constexpr enum sacStringHeader_enum KT5    = SAC_STRING_HDR_KT5;
                /*!< Label for seventh pick time. */
                static constexpr enum sacStringHeader_enum KT6    = SAC_STRING_HDR_KT6;
                /*!< Label for eigth pick time. */
                static constexpr enum sacStringHeader_enum KT7    = SAC_STRING_HDR_KT7;
                /*!< Label for ninth pick time. */
                static constexpr enum sacStringHeader_enum KT8    = SAC_STRING_HDR_KT8;
                /*!< Label for tenth pick time. */
                static constexpr enum sacStringHeader_enum KT9    = SAC_STRING_HDR_KT9;
                /*!< End time identifier. */
                static constexpr enum sacStringHeader_enum KF     = SAC_STRING_HDR_KF;
                /*!< Extra character variable for user. */
                static constexpr enum sacStringHeader_enum KUSER0 = SAC_STRING_HDR_KUSER0;
                /*!< Extra character variable for user. */
                static constexpr enum sacStringHeader_enum KUSER1 = SAC_STRING_HDR_KUSER1;
                /*!< Extra character variable for user. */
                static constexpr enum sacStringHeader_enum KUSER2 = SAC_STRING_HDR_KUSER2;
                /*!< Component name. */
                static constexpr enum sacStringHeader_enum KCMPNM = SAC_STRING_HDR_KCMPNM;
                /*!< Network name. */
                static constexpr enum sacStringHeader_enum KNETWK = SAC_STRING_HDR_KNETWK;
                /*!< Data was read onto computer. */
                static constexpr enum sacStringHeader_enum KDATRD = SAC_STRING_HDR_KDATRD;
                /*!< Generic name of recording instrument. */
                static constexpr enum sacStringHeader_enum KINST  = SAC_STRING_HDR_KINST;
        };
        /*!
         * @brief Defines the character header variables.  These are equivalent
         *        to the string variables.
         * @ingroup SacHeaderpp
         */
        class Character
        {
            public:
                /*!< Station name. */
                static constexpr enum sacCharacterHeader_enum KSTNM  = SAC_CHAR_HDR_KSTNM;
                /*!< Event name. */
                static constexpr enum sacCharacterHeader_enum KEVNM  = SAC_CHAR_HDR_KEVNM;
                /*!< Location code. */
                static constexpr enum sacCharacterHeader_enum KHOLE  = SAC_CHAR_HDR_KHOLE;
                /*!< Origin time identifier. */
                static constexpr enum sacCharacterHeader_enum K0     = SAC_CHAR_HDR_KO;
                /*!< First arrival identifier. */
                static constexpr enum sacCharacterHeader_enum KA     = SAC_CHAR_HDR_KA;
                /*!< Label for first pick time. */
                static constexpr enum sacCharacterHeader_enum KT0    = SAC_CHAR_HDR_KT0;
                /*!< Label for second pick time. */
                static constexpr enum sacCharacterHeader_enum KT1    = SAC_CHAR_HDR_KT1;
                /*!< Label for third pick time. */
                static constexpr enum sacCharacterHeader_enum KT2    = SAC_CHAR_HDR_KT2;
                /*!< Label for fourth pick time. */
                static constexpr enum sacCharacterHeader_enum KT3    = SAC_CHAR_HDR_KT3;
                /*!< Label for fifth pick time. */
                static constexpr enum sacCharacterHeader_enum KT4    = SAC_CHAR_HDR_KT4;
                /*!< Label for sixth pick time. */
                static constexpr enum sacCharacterHeader_enum KT5    = SAC_CHAR_HDR_KT5;
                /*!< Label for seventh pick time. */
                static constexpr enum sacCharacterHeader_enum KT6    = SAC_CHAR_HDR_KT6;
                /*!< Label for eigth pick time. */
                static constexpr enum sacCharacterHeader_enum KT7    = SAC_CHAR_HDR_KT7;
                /*!< Label for ninth pick time. */
                static constexpr enum sacCharacterHeader_enum KT8    = SAC_CHAR_HDR_KT8;
                /*!< Label for tenth pick time. */
                static constexpr enum sacCharacterHeader_enum KT9    = SAC_CHAR_HDR_KT9;
                /*!< End time identifier. */
                static constexpr enum sacCharacterHeader_enum KF     = SAC_CHAR_HDR_KF;
                /*!< Extra character variable for user. */
                static constexpr enum sacCharacterHeader_enum KUSER0 = SAC_CHAR_HDR_KUSER0;
                /*!< Extra character variable for user. */
                static constexpr enum sacCharacterHeader_enum KUSER1 = SAC_CHAR_HDR_KUSER1;
                /*!< Extra character variable for user. */
                static constexpr enum sacCharacterHeader_enum KUSER2 = SAC_CHAR_HDR_KUSER2;
                /*!< Component name. */
                static constexpr enum sacCharacterHeader_enum KCMPNM = SAC_CHAR_HDR_KCMPNM;
                /*!< Network name. */
                static constexpr enum sacCharacterHeader_enum KNETWK = SAC_CHAR_HDR_KNETWK;
                /*!< Data was read onto computer. */
                static constexpr enum sacCharacterHeader_enum KDATRD = SAC_CHAR_HDR_KDATRD;
                /*!< Generic name of recording instrument. */
                static constexpr enum sacCharacterHeader_enum KINST  = SAC_CHAR_HDR_KINST;
        };
};

class SacData
{
    public:
        SacData(void);
        SacData(const SacData &sacData);
        SacData& operator=(const SacData &sacData);
        bool operator==(const SacData &sacData) const;
        bool operator!=(const SacData &sacData) const;
        ~SacData(void);
        void clear(void);
        int getNumberOfPoints(void) const;
        int readData(const std::string fileName, const int npts = 0);
        int readData(const char *fileName, const int npts = 0);
        int setData(const int npts, const double data[]);
        int setData(const int npts, const float data[]);
        int setData(const int npts, const int data[]);
        int getConstantDataPtr(const double *data[]) const;
        int getDataPtr(double *data[]) const;
        int getData(const int npts, double data[]) const;
        friend void swap(SacData &sac1, SacData &sac2);
    private:
        /*!
         * @brief Computes padding so that arrays ends are 64 bit aligned.
         *        This can mitigate vectorization uninitialized errors. 
         * @param[in] n          Number of points to allocate.
         * @param[in] alignment  Optionally sets the aligment.  This should
         *                       be a power of 2 and at least 64.
         * @result The padded length in bytes that should be allocated to the
         *         aligned array.
         */
        size_t computePaddedLength64f_(const int n, const size_t alignment = 64)
        {
            size_t mod, pad; 
            size_t ipad;
            pad = 0; 
            mod = (static_cast<size_t>(n)*sizeof(double))%alignment;
            if (mod != 0)
            {
                pad = (alignment - mod)/sizeof(double);
            }
            ipad = (static_cast<size_t> (n) + pad)*sizeof(double);
            return ipad; 
        }
        double *__attribute__((__aligned__(64))) data_ = nullptr; 
        size_t nbytes_ = 0;
        int npts_ = 0;
        int ulp_ = 4;
        friend class SacMPI;
};

class SacPoleZero
{
    public:
        SacPoleZero(void);
        SacPoleZero(const SacPoleZero &pz);
        SacPoleZero(const struct sacPoleZero_struct pz);
        SacPoleZero& operator=(const SacPoleZero &pz);
        ~SacPoleZero(void);
        void clear(void);
        int readPolesAndZeros(const std::string fileName);
        int readPolesAndZeros(const char *fileName);
        int getNumberOfPoles(int *npoles) const;
        int getNumberOfZeros(int *nzeros) const;
        int getConstant(double *constant) const;
        int getPoles(std::vector<std::complex<double>> &poles) const;
        int getZeros(std::vector<std::complex<double>> &zeros) const;
        int getPoles(const int npoles, double *re, double *im) const;
        int getZeros(const int nzeros, double *re, double *im) const;
        void getSacPZStruct(struct sacPoleZero_struct *pz) const;
        friend void swap(SacPoleZero &sac1, SacPoleZero &sac2);
    private:
        struct sacPoleZero_struct pz_; 
        friend class SacMPI;
    public:
    //------------------------------------------------------------------------//
    //                               Enumerated Types                         //
    //------------------------------------------------------------------------//
        /*!
         * @brief Defines the units on the SAC pole-zero structures.
         * @ingroup SacPZpp
         */
        class Units
        {
            public:
                /*!< Unknown units. */
                static constexpr enum sacUnits_enum UNKNOWN  = SAC_UNKNOWN_UNITS;
                /*!< Displacmeent: Meters. */
                static constexpr enum sacUnits_enum METERS  = SAC_METERS;
                /*!< Displacement: Nanometers. */
                static constexpr enum sacUnits_enum NANOMETERS  = SAC_NANOMETERS;
                /*!< Velocity: Meters/second. */
                static constexpr enum sacUnits_enum METERS_SECOND  = SAC_METERS_SECOND;
                /*!< Velocity: Nanometers/second. */
                static constexpr enum sacUnits_enum NANOMETERS_SECOND  = SAC_NANOMETERS_SECOND;
                /*!< Acceleration: Meters/second^2. */
                static constexpr enum sacUnits_enum METERS_SECOND_SECOND  = SAC_METERS_SECOND_SECOND;
                /*!< Acceleration: Nanometers/second^2. */
                static constexpr enum sacUnits_enum NANOMETERS_SECOND_SECOND = SAC_NANOMETERS_SECOND_SECOND;
        };

};

class Sac
{
    public:
        Sac(void);
        Sac(const struct sacData_struct sac);
        Sac(const Sac &sac);
        Sac& operator=(const Sac &sac);
        bool operator==(const Sac &sac) const;
        bool operator!=(const Sac &sac) const;
        ~Sac(void);
        void clear(void);
        int readFile(const std::string fileName);
        int readFile(const char *fileName);
        int readPolesAndZeros(const std::string fileName);
        int writeFile(const std::string fileName);
        int writeFile(const char *fileName);
        int getNumberOfPoints(int *npts) const;
        int getSamplingPeriod(double *dt) const;
        int printHeader(FILE *file = nullptr);
        // Sets time series data on the class.
        int setData(const int npts, const double data[]);
        // Gets pointers to the data 
        int getConstantDataPtr(const double *data[]) const;
        int getDataPtr(double *data[]) const;
        // Sets the epochal start time
        int setEpochalStartTime(const double t0); 
        // Get the epochal start/end time.
        int getEpochalStartTime(double *t0) const;
        int getEpochalEndTime(double *t1) const;
   
        // Integer
        int setHeader(const enum sacIntegerHeader_enum hvar, const int hdr);
        int getHeader(const enum sacIntegerHeader_enum hvar, int *hdr) const;
        int getIntegerHeader(const enum sacIntegerHeader_enum hvar,
                             int *hdr) const;
        int setIntegerHeader(const enum sacIntegerHeader_enum hvar,
                             const int hdr);
        // Boolean
        int setHeader(const enum sacBooleanHeader_enum hvar, const bool hdr);
        int getHeader(const enum sacBooleanHeader_enum hvar, bool *hdr) const;
        int getBooleanHeader(const enum sacBooleanHeader_enum hvar,
                             bool *hdr) const;
        int setBooleanHeader(const enum sacBooleanHeader_enum hvar,
                             const bool hdr);
        // Double
        int setHeader(const enum sacDoubleHeader_enum hvar, const double hdr);
        int getHeader(const enum sacDoubleHeader_enum hvar, double *hdr) const;
        int getDoubleHeader(const enum sacDoubleHeader_enum hvar,
                            double *hdr) const;
        int setDoubleHeader(const enum sacDoubleHeader_enum hvar,
                            const double hdr);
        // Float
        int setHeader(const enum sacFloatHeader_enum hvar, const float hdr);
        int getHeader(const enum sacFloatHeader_enum hvar, float *hdr) const;
        int getFloatHeader(const enum sacFloatHeader_enum hvar,
                           float *hdr) const;
        int setFloatHeader(const enum sacFloatHeader_enum hvar,
                           const float hdr);
        // String
        int setHeader(const enum sacStringHeader_enum hvar,
                      const std::string hdr);
        int getHeader(const enum sacStringHeader_enum hvar,
                      std::string &hdr) const;
        int getStringHeader(const enum sacStringHeader_enum hvar,
                            std::string &hdr) const;
        int setStringHeader(const enum sacStringHeader_enum hvar,
                            const std::string hdr);
        // Gets a SAC header class as a C data struture. 
        void getSacHeaderStruct(struct sacHeader_struct *header) const;
        // Gets poles and zeros
        int getPolesZerosAndConstant(std::vector<std::complex<double>> &zeros,
                                     std::vector<std::complex<double>> &poles,
                                     double *k) const;
        // Gets a SAC PZ class as a C data structure.
        void getSacPZStruct(struct sacPoleZero_struct *pz) const;
        // Gets a SAC data structure from the class.
        void getSacDataStruct(struct sacData_struct *sacData) const;
        /*!
         * @brief Initializes a SAC data structure.
         * @param[in] npts          Number of points in data.
         * @param[in] data          Time series.  This is an array of dimension [npts].
         * @param[in] dt            The sampling period in seconds.
         * @param[in] t0            The epochal start time in UTC seconds of the trace.
         * @param[in] networkName   The network name.
         * @param[in] stationName   The station name.
         * @param[in] channelName   The channel name.
         * @param[in] locationCode  The location code.
         * @result 0 indicates success.
         * @ingroup Sacpp
         */
/*
        int initializeFromStream(const int npts, const double data[],
                                 const double dt = 1,
                                 //const double t0 = 0,
                                 const char *networkName = nullptr,
                                 const char *stationName = nullptr,
                                 const char *channelName = nullptr,
                                 const char *locationCode = nullptr)
        {
            if (npts < 1 || dt <= 0 || data == nullptr)
            {
                if (npts < 1){fprintf(stderr, "%s: No points in signal\n", __func__);}
                if (dt <= 0){fprintf(stderr, "%s: Sampling period must be positive\n", __func__);}
                if (data == nullptr){fprintf(stderr, "%s: Data must be positive\n", __func__);}
                return -1;
            }
            int ierr = data_.setData(npts, data);
            if (ierr != 0)
            {
                fprintf(stderr, "%s: Failed to set data\n", __func__);
                return -1;
            } 
            ierr = header_.setDoubleHeader(SacHeader::Double::DELTA, dt); 
            if (ierr != 0)
            {
                fprintf(stderr, "%s: Failed to set sampling period\n", __func__);
                return -1;
            }
            ierr = header_.setIntegerHeader(SacHeader::Integer::NPTS, npts);
            if (ierr != 0)
            {
                fprintf(stderr, "%s: Failed to set number of points\n",
                        __func__);
                return -1;
            }
            if (networkName != nullptr)
            {
                ierr = header_.setCharacterHeader(SacHeader::Character::KNETWK, networkName);
                if (ierr != 0)
                {
                    fprintf(stderr, "%s: Failed to set network name\n", __func__);
                }
            }
            if (stationName != nullptr)
            {
                ierr = header_.setCharacterHeader(SacHeader::Character::KSTNM, stationName);
                if (ierr != 0)
                {
                    fprintf(stderr, "%s: Failed to set station name\n", __func__);
                }
            }
            if (channelName != nullptr)
            {
                ierr = header_.setCharacterHeader(SacHeader::Character::KCMPNM, channelName);
                if (ierr != 0)
                {
                    fprintf(stderr, "%s: Failed to set channel name\n", __func__);
                }
            }
            if (locationCode != nullptr)
            {
                ierr = header_.setCharacterHeader(SacHeader::Character::KHOLE, locationCode);
                if (ierr != 0)
                {
                    fprintf(stderr, "%s: Failed to set location code\n", __func__);
                }
            }
            return 0;
        }
*/
        // Swaps 2 Sac classes
        friend void swap(Sac &sac1, Sac &sac2);
    private:
        SacData data_;
        SacHeader header_;
        SacPoleZero pz_;
        friend class SacMPI;
};

#endif /* SACIO_HPP */
