#ifndef SACIO_MPI_H
#define SACIO_MPI_H 1
#include "sacio_config.h"
#include "sacio.h"
#ifdef SACIO_USE_MPI
#ifdef __clang__
#pragma clang diagnostic ignored "-Wall"
#pragma clang diagnostic ignored "-Wextra"
#pragma clang diagnostic ignored "-Weverything"
#endif
#include <mpi.h>
#ifdef __clang__
#pragma clang diagnostic pop
#endif

#ifdef __cplusplus
extern "C"
{
#endif
MPI_Datatype sacio_mpi_createHeader(void);
int sacio_mpi_broadcastPZ(struct sacPoleZero_struct *pz, const int npz,
                          const int root, const MPI_Comm comm);
int sacio_mpi_broadcastFAP(struct sacFAP_struct *fap, const int nfap,
                           const int root, const MPI_Comm comm);
int sacio_mpi_broadcastHeader(struct sacHeader_struct *sacHeader,
                              const int nheader,
                              const int root, const MPI_Comm comm);
int sacio_mpi_broadcast(struct sacData_struct *sac, const int nsac,
                        const int root, const MPI_Comm comm);
int sacio_mpi_send(struct sacData_struct *sac, const int nsac,
                   const int dest, const int tag, const MPI_Comm comm);
int sacio_mpi_recv(struct sacData_struct *sac, const int nsac,
                    const int source, const int tag, const MPI_Comm comm,
                    MPI_Status *status);


#ifdef __cplusplus
}
#endif
#endif /* USE_MPI */
#endif /* SACIO_MPI_H */
