#ifndef SACIO_STRUCT_H
#define SACIO_STRUCT_H 1
#include "sacio_enum.h"
struct sacHeader_struct
{
    /*!
     * @struct sacHeader_struct
     * @brief Defines the SAC header as a data structure.
     * @ingroup sac
     */
    double delta;      /*!< Increment between evenly spaced samples (nominal
                            value).  This is required. */
    double depmin;     /*!< Minimum value of dependent variable. */
    double depmax;     /*!< Maximum value of dependent variable. */
    double scale;      /*!< Multiplying scale factor for dependent variable.
                            This is currently not used. */
    double odelta;     /*!< Observed increment if different from nominal
                            value. */
    double b;          /*!< Beginning value of the independent variable.
                            This is required. */
    double e;          /*!< Ending value of the independent variable.
                            [required] */
    double o;          /*!< Event origin time (seconds relative to reference
                            time). */
    double a;          /*!< First arrival time seconds relative to reference
                            time). */
    double internal1;  /*!< SAC internal variable. */
    double t0;         /*!< First user defined pick time or marker (seconds
                            relative to reference time) */
    double t1;         /*!< Second user defined pick time or marker (seconds
                            relative to reference time) */
    double t2;         /*!< Third user defined pick time or marker (seconds
                            relative to reference time) */
    double t3;         /*!< Fourth user defined pick time or marker (seconds
                            relative to reference time) */
    double t4;         /*!< Fifth user defined pick time or marker (seconds
                            relative to reference time) */
    double t5;         /*!< Sixth user defined pick time or marker (seconds
                            relative to reference time) */
    double t6;         /*!< Seventh user defined pick time or marker (seconds
                            relative to reference time) */
    double t7;         /*!< Eighth user defined pick time or marker (seconds
                            relative to reference time) */
    double t8;         /*!< Ninth user defined pick time or marker (seconds
                            relative to reference time) */
    double t9;         /*!< Tenth user defined pick time or marker (seconds
                            relative to reference time) */
    double f;          /*!< End time of event (seconds relative to reference
                            time) */
    double resp0;      /*!< First instrument response parameter [unused] */
    double resp1;      /*!< Second instrument response parameter [unused] */
    double resp2;      /*!< Third instrument response parameter [unused] */
    double resp3;      /*!< Fourth instrument response parameter [unused] */
    double resp4;      /*!< Fifth instrument response parameter [unused] */
    double resp5;      /*!< Sixth instrument response parameter [unused] */
    double resp6;      /*!< Seventh instrument response parameter [unused] */
    double resp7;      /*!< Eigthth instrument response parameter [unused] */
    double resp8;      /*!< Ninth instrument response parameter [unused] */
    double resp9;      /*!< Tenth instrument response parameter [unused] */
    double stla;       /*!< Station latitude (degrees, north positive) */
    double stlo;       /*!< Station longitude (degrees, east positive) */
    double stel;       /*!< Station elevation (meters) */
    double stdp;       /*!< Station depth below surface (meters) */
    double evla;       /*!< Event latitude (degrees, north positive) */
    double evlo;       /*!< Event longitude (degrees, east positive) */
    double evel;       /*!< Event elevation (meters) [unused] */
    double evdp;       /*!< Event depth (kilometers) */
    double mag;        /*!< Event magnitude */
    double user0;      /*!< User defined */
    double user1;      /*!< User defined */
    double user2;      /*!< User defined */
    double user3;      /*!< User defined */
    double user4;      /*!< User defined */
    double user5;      /*!< User defined */
    double user6;      /*!< User defined */
    double user7;      /*!< User defined */
    double user8;      /*!< User defined */
    double user9;      /*!< User defined */
    double dist;       /*!< Station to event distance (km) */
    double az;         /*!< Event to station azimuth (degrees) */
    double baz;        /*!< Station to event azimuth (degrees) */
    double gcarc;      /*!< Station to eventgreat circle arc length (degrees) */
    double internal2;  /*!< SAC internal variable */
    double depmen;     /*!< Mean value of dependent variable */
    double cmpaz;      /*!< Component azimuth (degrees clockwise from north) */
    double cmpinc;     /*!< Component incident angle (degrees from vertical) */
    double xminimum;   /*!< Minimum value of x (spectral files only) */
    double xmaximum;   /*!< Maximum value of x (spectral files only) */
    double yminimum;   /*!< Minimum value of y (spectral files only) */
    double ymaximum;   /*!< Maximum value of y (spectral files only) */
    double unused0;    /*!< Extra space and unused by SAC */
    double unused1;    /*!< Extra space and unused by SAC */
    double unused2;    /*!< Extra space and unused by SAC */
    double unused3;    /*!< Extra space and unused by SAC */
    double unused4;    /*!< Extra space and unused by SAC */
    double unused5;    /*!< Extra space and unused by SAC */
    double unused6;    /*!< Extra space and unused by SAC */
    int nzyear;        /*!< GMT year corresponding to reference (zero)
                            time in file */
    int nzjday;        /*!< GMT julian day */
    int nzhour;        /*!< GMT hour */
    int nzmin;         /*!< GMT minute */
    int nzsec;         /*!< GMT second */
    int nzmsec;        /*!< GMT millisecond */
    int nvhdr;         /*!< Header version number.  Current value is 6. 
                           This is required. */
    int norid;         /*!< Origin ID (CSS 3.0) */
    int nevid;         /*!< Event ID (CSS 3.0) */
    int npts;          /*!< Number of points per data component.  This is
                            required. */
    int ninternal1;    /*!< SAC internal variable */
    int nwfid;         /*!< Waveform ID (CSS 3.0) */
    int nxsize;        /*!< Spectral length (spectral files only) */
    int nysize;        /*!< Spectral width (spectral files only) */
    int nunused0;      /*!< Extra space and unused by SAC */
    int iftype;        /*!< Type of file.  This is required.  Options are: \n
                             ITIME (01) -> time series file. \n
                             IRLIM (02) -> spectral file (real and imaginary).\n
                             IAMPH (03) -> spectral file; amplitude and phase.\n
                             IXY (04) -> general x versus y data. \n
                             IXYZ (05) -> general XYZ (3-D) file. */
    int idep;          /*!< Type of dependent variable. \n
                             IUNKN (unknown). \n
                             IDISP (displacmenet in nm). \n
                             IVEL (velocity in nm/sec). \n
                             IVOLTS (velocity in volts). \n
                             IACC (acceleration in nm/sec/sec). */
    int iztype;        /*!< Reference time equivalence. \n
                             IUNWN (unknown). \n
                             IB (begin time). \n
                             IDAY (midnight of reference GMT day). \n
                             IO (event origin time). \n
                             IA (first arrival time). \n
                             ITn (user defined pick time n,n=0,9). */
    int nunused1;      /*!< Extra space and unused by SAC */
    int iinst;         /*!< Type of recording instrument.  This is not used.*/
    int istreg;        /*!< Station geographic region.  This is not used. */
    int ievreg;        /*!< Event geographic region. This is not used. */
    int ievtyp;        /*!< Type of event: \n
                             IUNKN (Unknown). \n
                             INUCL (Nuclear event). \n
                             IPREN (Nuclear pre-shot event). \n
                             IPOSTN (Nuclear post-shot event). \n
                             IQUAKE (Earthquake). \n
                             IPREQ (Foreshock). \n
                             IPOSTQ (Aftershock). \n
                             ICHEM (Chemical explosion). \n
                             IQB (Quarry or mine blast confirmed by quarry). \n
                             IQB1 (Quarry/mine blast with designed shot
                                   info-ripple fired). \n
                             IQB2 (Quarry/mine blast with observed shot
                                   info-ripple fired). \n
                             IQBX (Quarry or mine blast - single shot). \n
                             IQMT (Quarry/mining-induced events:
                                   tremors and rockbursts). \n
                             IEQ (Earthquake). \n
                             IEQ1 (Earthquakes in a swarm or aftershock
                                   sequence). \n
                             IEQ2 (Felt earthquake). \n
                             IME (Marine explosion). \n
                             IEX (Other explosion). \n
                             INU (Nuclear explosion). \n
                             INC (Nuclear cavity collapse). \n
                             IO (Other source of known origin). \n
                             IL (Local event of unknown origin). \n
                             IR (Regional event of unknown origin). \n
                             IT (Teleseismic event of unknown origin). \n
                             IU (Undetermined or conflicting information). \n
                             IOTHER (Other). */
    int iqual;         /*!< Quality of data.  This is not used
                             IGOOD (good data). \n
                             IGLCH (glitches). \n
                             IDROP (dropouts). \n
                             ILOWSN (low signal to noise ratio). \n
                             IOTHER (other). */
    int isynth;        /*!< Synthetic data flag. This is not currently used. \n
                              IRLDTA (real data). \n
                              ?????? (flags for various synthetic seismogram
                                      codes) */
    int imagtyp;       /*!< Magnitude type. \n
                             IMB (body wave magnitude). \n
                             IMS (surfae wave magnitude). \n
                             IML (local magnitude). \n
                             IMW (moment magnitude). \n
                             IMD (duration magnitude). \n
                             IMX (user defined magnitude). */ 
    int imagsrc;       /*!< Source magnitude information:
                             INEIC (National Earthquake Information Center).
                             IPDE (Preliminary Determination of Epicenter).
                             IISC (Internation Seismological Center).
                             IREB (Reviewed Event Bulletin).
                             IUSGS (US Geological Survey).
                             IBRK (UC Berkeley).
                             ICALTECH (California Institute of Technology).
                             ILLNL (Lawrence Livermore National Lab).
                             IEVLOC (Event Location (computer program)).
                             IJSOP (Joint Seismic Observation Program).
                             IUSER (The indivudal using SAC2000).
                             IUNKNOWN (unknown). */
    int nunused2;      /*!< Extra space and unused by SAC. */
    int nunused3;      /*!< Extra space and unused by SAC. */
    int nunused4;      /*!< Extra space and unused by SAC. */
    int nunused5;      /*!< Extra space and unused by SAC. */
    int nunused6;      /*!< Extra space and unused by SAC. */
    int nunused7;      /*!< Extra space and unused by SAC. */
    int nunused8;      /*!< Extra space and unused by SAC. */
    int nunused9;      /*!< Extra space and unused by SAC. */
    int leven;         /*!< If true then data is evenly spaced.  This is
                            required. */
    int lpspol;        /*!< If true then station components have a positive
                            polarity (left-hand rule). */
    int lovrok;        /*!< If true then it is okay to overwrite this
                            file on disk. */
    int lcalda;        /*!< If true then dist, az, baz, and gcarc are to 
                            be calculated from the station and event
                            coordinates. */
    int lunused;       /*!< Extra space and unused by SAC */ 
    char kstnm[8];     /*!< Station name. */
    char kevnm[16];    /*!< Event name. */
    char khole[8];     /*!< Nuclear: hole identifier. \n
                            Other: location identifier. */
    char ko[8];        /*!< Event origin time identification. */
    char ka[8];        /*!< First arrival identification. */
    char kt0[8];       /*!< User defined pick time of t0. */
    char kt1[8];       /*!< User defined pick time of t1. */
    char kt2[8];       /*!< User defined pick time of t2. */
    char kt3[8];       /*!< User defined pick time of t3. */
    char kt4[8];       /*!< User defined pick time of t4. */
    char kt5[8];       /*!< User defined pick time of t5. */
    char kt6[8];       /*!< User defined pick time of t6. */
    char kt7[8];       /*!< User defined pick time of t7. */
    char kt8[8];       /*!< User defined pick time of t8. */
    char kt9[8];       /*!< user defined pick time of t9. */
    char kf[8];        /*!< End time identification. */
    char kuser0[8];    /*!< User defined. */
    char kuser1[8];    /*!< User defined. */
    char kuser2[8];    /*!< User defined */
    char kcmpnm[8];    /*!< Channel name.  SEED volumes use three character
                            names, and the third is the comopnent/orientation.
                            For horizontals, use 1/N and or 2/E. */
    char knetwk[8];    /*!< Name of seismic network. */
    char kdatrd[8];    /*!< Data was read onto computer. */
    char kinst[8];     /*!< Generic name of recording instrument. */
    bool lhaveHeader;  /*!< If true then the header is defined. */
    char pad[7];
};

struct sacPoleZero_struct
{
    /*! 
     * @struct sacPoleZero_struct
     * @brief Defines the SAC pole-zeros as a data structure.
     * @ingroup sac
     */
    char network[64];       /*!< Network name */
    char station[64];       /*!< Station name */
    char location[64];      /*!< Location name */
    char channel[64];       /*!< Channel name */
    char comment[64];       /*!< Comment (e.g. N/A) */
    char description[64];   /*!< Description (e.g. Isla Barro, Panama ) */
    char instrumentType[64];/*!< instrument type (e.g. STS-2 Standard gain) */
#ifdef __cplusplus
    void *poles;  /*!< Analog poles (rad/s) [npoles] */
    void *zeros;  /*!< Analog zeros (rad/s) [nzeros] */
#else
    double complex *poles;  /*!< Analog poles (rad/s) [npoles] */
    double complex *zeros;  /*!< Analog zeros (rad/s) [nzeros] */
#endif
    double constant;        /*!< System gain (=a0*instgain) */
    double created;         /*!< Time when file was created (seconds since
                                 epoch - UTC) */
    double start;           /*!< Earliest time that this metadata is valid
                                 (seconds since epoch - UTC) */
    double end;             /*!< Latest time that this metadat is valid
                                 (seconds since epoch - UTC) */
    double latitude;        /*!< Station latitude - degrees positive north */
    double longitude;       /*!< Station longitude - degrees positive east */
    double elevation;       /*!< Station elevation above sea level (meters) */
    double depth;           /*!< Station depth */
    double dip;             /*!< Component dip angle (degrees from vertical) */
    double azimuth;         /*!< Component azimuth (degrees from north) */
    double sampleRate;      /*!< Sampling rate (Hz) */
    double instgain;        /*!< Instrument gain */
    double sensitivity;     /*!< Instrument sensititivy */
    double a0;
    int npoles;             /*!< Number of poles */
    int nzeros;             /*!< Number of zeros */
    enum sacUnits_enum
         inputUnits;        /*!< Input units */
    enum sacUnits_enum
         instgainUnits;     /*!< Instrument gain units */
    enum sacUnits_enum
         sensitivityUnits;  /*!< Sensitivity units */
    bool lhavePZ;           /*!< If true then the response is defined */
    char pad[3];
};

struct sacFAP_struct
{
    /*! 
     * @struct sacFAP_struct
     * @brief Defines the SAC frequency, amplitude, and phase response as
     *        a data structure.
     * @ingroup sac
     */
    double *freqs;   /*!< Frequencies (Hz). This is in increasing order and
                          of dimension [nf]. */
    double *amp;     /*!< Amplitude corresponding to frequencies.
                          This is of dimension [nf]. */
    double *phase;   /*!< Phase angle (degrees) corresponding to frequencies.
                          This is of dimension [nf]. */
    int nf;          /*!< Number of frequencies. */
    bool lhaveFAP;   /*!< If true then the FAP response is defined. */
    char pad[3];
};

struct sacData_struct
{
    /*! 
     * @struct sacData_struct
     * @brief Holder for all data structures including the waveform data.
     * @ingroup sac
     */
    struct sacHeader_struct header; /*!< Holds the header values */
    struct sacPoleZero_struct pz;   /*!< Holds the pole-zero structure */
    struct sacFAP_struct fap;       /*!< Frequency, amplitude, phase
                                         structure. */
    double *data;                   /*!< Holds the data [npts] */
    int npts;                       /*!< Number of data points */
    char pad[4];
};
#endif
