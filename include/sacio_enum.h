#ifndef SACIO_ENUM_H
#define SACIO_ENUM_H 1

enum sacUnits_enum
{
    SAC_UNKNOWN_UNITS = 0,            /*!< No idea */
    SAC_METERS = 1,                   /*!< Displacement - Meters */
    SAC_NANOMETERS = 2,               /*!< Displacement - Nanometers */
    SAC_METERS_SECOND = 3,            /*!< Velocity - meters/second */
    SAC_NANOMETERS_SECOND = 4,        /*!< Velocity - nanometers/second */
    SAC_METERS_SECOND_SECOND = 5,     /*!< Acceleration - meters/second^2 */
    SAC_NANOMETERS_SECOND_SECOND = 6  /*!< Acceleration - nanometers/second^2 */
};



enum sacHeader_enum
{
    // floats
    SAC_FLOAT_DELTA = 0,  /*!< Sampling period. */
    SAC_FLOAT_DEPMIN = 1, /*!< Minimum value of dependent variable. */
    SAC_FLOAT_DEPMAX = 2, /*!< Maximum value of dependent variable. */
    SAC_FLOAT_SCALE = 3,  /*!< Scale factor multiplying dependent variable. */
    SAC_FLOAT_ODELTA = 4, /*!< Observed increment; if different than delta. */
    SAC_FLOAT_B = 5,      /*!< Beginning value of dependent variable. */
    SAC_FLOAT_E = 6,      /*!< Ending value of dependent variable. */
    SAC_FLOAT_O = 7,      /*!< Origin time (seconds relative to reference time. */
    SAC_FLOAT_A = 8,      /*!< First arrival time relative to reference time. */
    SAC_FLOAT_INTERNAL1 = 9, /*!< Internal variable. */
    SAC_FLOAT_T0 = 10,     /*!< First user defined pick time or marker (seconds
                           relative to reference time). */
    SAC_FLOAT_T1 = 11,     /*!< Second user defined pick time or marker. */
    SAC_FLOAT_T2 = 12,     /*!< Third user defined pick time or marker. */
    SAC_FLOAT_T3 = 13,     /*!< Fourth user defined pick time or marker. */
    SAC_FLOAT_T4 = 14,     /*!< Fifth user defined pick time or marker. */
    SAC_FLOAT_T5 = 15,     /*!< Sixth user defined pick time or marker. */
    SAC_FLOAT_T6 = 16,     /*!< Seventh user defined pick time or marker. */
    SAC_FLOAT_T7 = 17,     /*!< Eighth user defined pick time or marker. */
    SAC_FLOAT_T8 = 18,     /*!< Ninth user defined pick time or marker. */
    SAC_FLOAT_T9 = 19,     /*!< Tenth user defined pick time or marker. */
    SAC_FLOAT_F = 20,      /*!< End time of event. */
    SAC_FLOAT_RESP0 = 21,  /*!< First instrument response parameter. */
    SAC_FLOAT_RESP1 = 22,  /*!< Second instrument response parameter. */
    SAC_FLOAT_RESP2 = 23,  /*!< Third instrument response parameter. */
    SAC_FLOAT_RESP3 = 24,  /*!< Fourth instrument response parameter. */
    SAC_FLOAT_RESP4 = 25,  /*!< Fifth instrument response parameter. */
    SAC_FLOAT_RESP5 = 26,  /*!< Sixth instrument response parameter. */
    SAC_FLOAT_RESP6 = 27,  /*!< Seventh instrument response parameter. */
    SAC_FLOAT_RESP7 = 28,  /*!< Eigth instrument response parameter. */
    SAC_FLOAT_RESP8 = 29,  /*!< Ninth instrument response parameter. */
    SAC_FLOAT_RESP9 = 30,  /*!< Tenth instrument response parameter. */
    SAC_FLOAT_STLA = 31,   /*!< Station latitude. */
    SAC_FLOAT_STLO = 32,   /*!< Station longitude. */
    SAC_FLOAT_STEL = 33,   /*!< Station elevation. */
    SAC_FLOAT_STDP = 34,   /*!< Station depth. */
    SAC_FLOAT_EVLA = 35,   /*!< Event latitude. */
    SAC_FLOAT_EVLO = 36,   /*!< Event longitude. */
    SAC_FLOAT_EVEL = 37,   /*!< Event elevation. */
    SAC_FLOAT_EVDP = 38,   /*!< Event depth. */
    SAC_FLOAT_MAG = 39,    /*!< Event magnitude. */
    SAC_FLOAT_USER0 = 40,  /*!< First user defined variable. */
    SAC_FLOAT_USER1 = 41,  /*!< Second user defined variable. */
    SAC_FLOAT_USER2 = 42,  /*!< Third user defined variable. */
    SAC_FLOAT_USER3 = 43,  /*!< Fourth user defined variable. */
    SAC_FLOAT_USER4 = 44,  /*!< Fifth user defined variable. */
    SAC_FLOAT_USER5 = 45,  /*!< Sixth user defined variable. */
    SAC_FLOAT_USER6 = 46,  /*!< Seventh user defined variable. */
    SAC_FLOAT_USER7 = 47,  /*!< Eighth user defined variable. */
    SAC_FLOAT_USER8 = 48,  /*!< Ninth user defined variable. */
    SAC_FLOAT_USER9 = 49,  /*!< Tenth user defined variable. */
    SAC_FLOAT_DIST = 50,   /*!< Source-receiver distance. */
    SAC_FLOAT_AZ = 51,     /*!< Source-receiver azimuth. */
    SAC_FLOAT_BAZ = 52,    /*!< Source-receiver back-azimuth. */
    SAC_FLOAT_GCARC = 53,  /*!< Source-receiver great circle distance. */
    SAC_FLOAT_INTERNAL2 = 54, /*!< Internal variable. */
    SAC_FLOAT_DEPMEN = 55,    /*!< Mean value of dependent variable. */
    SAC_FLOAT_CMPAZ = 56,     /*!< Component azimuth. */
    SAC_FLOAT_CMPINC = 57,    /*!< Component incident angle. */
    SAC_FLOAT_XMINIMUM = 58,  /*!< Minimum value of x (spectral files only). */
    SAC_FLOAT_XMAXIMUM = 59,  /*!< Maximum value of x (spectral files only). */
    SAC_FLOAT_YMINIMUM = 60,  /*!< Minimum value of y (spectral files only). */
    SAC_FLOAT_YMAXIMUM = 61,  /*!< Maximum value of y (spectral files only). */
    SAC_FLOAT_UNUSED0 = 62,   /*!< Extra float variable. */
    SAC_FLOAT_UNUSED1 = 63,   /*!< Extra float variable. */
    SAC_FLOAT_UNUSED2 = 64,   /*!< Extra float variable. */
    SAC_FLOAT_UNUSED3 = 65,   /*!< Extra float variable. */
    SAC_FLOAT_UNUSED4 = 66,   /*!< Extra float variable. */
    SAC_FLOAT_UNUSED5 = 67,   /*!< Extra float variable. */
    SAC_FLOAT_UNUSED6 = 68,   /*!< Extra float variable. */
    // ints
    SAC_INT_NZYEAR = 69,    /*!< Year */
    SAC_INT_NZJDAY = 70,    /*!< Julian day */
    SAC_INT_NZHOUR = 71,    /*!< Hour */
    SAC_INT_NZMIN = 72,     /*!< Minute */
    SAC_INT_NZSEC = 73,     /*!< Second */
    SAC_INT_NZMSEC = 74,    /*!< Millisecond */
    SAC_INT_NVHDR = 75,     /*!< Header version number. */
    SAC_INT_NORID = 76,     /*!< CSS 3.0 origin ID. */
    SAC_INT_NEVID = 77,     /*!< CSS 3.0 event ID. */
    SAC_INT_NPTS = 78,      /*!< Number of points in time series. */
    SAC_INT_INTERNAL1 = 79, /*!< Internal variable. */
    SAC_INT_NWFID = 80,     /*!< CSS 3.0 Waveform ID. */
    SAC_INT_NXSIZE = 81,    /*!< Spectral length - for spectral files only. */
    SAC_INT_NYSIZE = 82,    /*!< Spectral width - for spectral files only. */
    SAC_INT_UNUSED0 = 83,   /*!< Extra integer variable. */
    SAC_INT_IFTYPE = 84,    /*!< Type of file.  */
    SAC_INT_IDEP = 85,      /*!< Type of dependent variable. */
    SAC_INT_IZTYPE = 86,    /*!< Reference time equivalence. */ 
    SAC_INT_UNUSED1 = 87,   /*!< Extra integer variable. */
    SAC_INT_IINST = 88,     /*!< Type of recording instrument. */
    SAC_INT_ISTREG = 89,    /*!< Station geographic region. */
    SAC_INT_IEVREG = 90,    /*!< Event geographic region. */
    SAC_INT_IEVTYP = 91,    /*!< Event type. */
    SAC_INT_IQUAL = 92,     /*!< Quality of data. */
    SAC_INT_ISYNTH = 93,    /*!< Synthetic data flag. */
    SAC_INT_IMAGTYP = 94,   /*!< Source magnitude type. */
    SAC_INT_IMAGSRC = 95,   /*!< Source magnitude information. */
    SAC_INT_UNUSED2 = 96,   /*!< Extra integer variable */
    SAC_INT_UNUSED3 = 97,   /*!< Extra integer variable */
    SAC_INT_UNUSED4 = 98,   /*!< Extra integer variable */
    SAC_INT_UNUSED5 = 99,   /*!< Extra integer variable */
    SAC_INT_UNUSED6 = 100,  /*!< Extra integer variable */
    SAC_INT_UNUSED7 = 101,  /*!< Extra integer variable */
    SAC_INT_UNUSED8 = 102,  /*!< Extra integer variable */
    SAC_INT_UNUSED9 = 103,  /*!< Extra integer variable */
    // bools
    SAC_BOOL_LEVEN = 104,   /*!< If true then data is evenly space. */
    SAC_BOOL_LPSPOL = 105,  /*!< If true then station conforms to
                                 left hand rule. */
    SAC_BOOL_LOVROK = 106,  /*!< If true then it is okay to overwrite file. */
    SAC_BOOL_LCALDA = 107,  /*!< If true then SAC wil compute dist, az, and
                                 gcarc. */
    SAC_BOOL_LUNUSED = 108, /*!< Extra logical variable. */ 
    // char
    SAC_CHAR_KSTNM = 109,   /*!< Station name. */
    SAC_CHAR_KEVNM = 110,   /*!< Event name. */
    SAC_CHAR_KHOLE = 111,   /*!< Location code. */
    SAC_CHAR_KO = 112,      /*!< Origin time identifier. */
    SAC_CHAR_KA = 113,      /*!< First arrival identifier. */
    SAC_CHAR_KT0 = 114,     /*!< Label for first pick time. */
    SAC_CHAR_KT1 = 115,     /*!< Label for second pick time. */ 
    SAC_CHAR_KT2 = 116,     /*!< Label for third pick time. */
    SAC_CHAR_KT3 = 117,     /*!< Label for fourth pick time. */
    SAC_CHAR_KT4 = 118,     /*!< Label for fifth pick time. */
    SAC_CHAR_KT5 = 119,     /*!< Label for sixth pick time. */
    SAC_CHAR_KT6 = 120,     /*!< Label for seventh pick time. */
    SAC_CHAR_KT7 = 121,     /*!< Label for eighth pick time. */
    SAC_CHAR_KT8 = 122,     /*!< Label for ninth pick time. */
    SAC_CHAR_KT9 = 123,     /*!< Label for tenth pick time. */
    SAC_CHAR_KF = 124,      /*!< End time identifier. */
    SAC_CHAR_KUSER0 = 125,  /*!< Extra character variable for user. */
    SAC_CHAR_KUSER1 = 126,  /*!< Extra character variable for user. */
    SAC_CHAR_KUSER2 = 127,  /*!< Extra character variable for user. */
    SAC_CHAR_KCMPNM = 128,  /*!< Component name. */
    SAC_CHAR_KNETWK = 129,  /*!< Network name. */
    SAC_CHAR_KDATRD = 130,  /*!< Data was read onto computer. */
    SAC_CHAR_KINST = 131,   /*!< Generic name of recording instrument. */
    // unknown header variable
    SAC_UNKNOWN_HDRVAR =-1  /*!< Unknown header variable. */
};

enum sacDoubleHeader_enum
{
    // floats
    SAC_DOUBLE_HDR_DELTA  = SAC_FLOAT_DELTA,  /*!< Sampling period. */
    SAC_DOUBLE_HDR_DEPMIN = SAC_FLOAT_DEPMIN, /*!< Minimum value of dependent variable. */
    SAC_DOUBLE_HDR_DEPMAX = SAC_FLOAT_DEPMAX, /*!< Maximum value of dependent variable. */
    SAC_DOUBLE_HDR_SCALE  = SAC_FLOAT_SCALE,  /*!< Scale factor multiplying dependent variable. */
    SAC_DOUBLE_HDR_ODELTA = SAC_FLOAT_ODELTA, /*!< Observed increment; if different than delta. */
    SAC_DOUBLE_HDR_B = SAC_FLOAT_B,      /*!< Beginning value of dependent variable. */
    SAC_DOUBLE_HDR_E = SAC_FLOAT_E,      /*!< Ending value of dependent variable. */
    SAC_DOUBLE_HDR_O = SAC_FLOAT_O,      /*!< Origin time (seconds relative to reference time. */
    SAC_DOUBLE_HDR_A = SAC_FLOAT_A,      /*!< First arrival time relative to reference time. */
    SAC_DOUBLE_HDR_INTERNAL1 = SAC_FLOAT_INTERNAL1, /*!< Internal variable. */
    SAC_DOUBLE_HDR_T0 = SAC_FLOAT_T0,     /*!< First user defined pick time or marker (seconds
                                               relative to reference time). */
    SAC_DOUBLE_HDR_T1 = SAC_FLOAT_T1,     /*!< Second user defined pick time or marker. */
    SAC_DOUBLE_HDR_T2 = SAC_FLOAT_T2,     /*!< Third user defined pick time or marker. */
    SAC_DOUBLE_HDR_T3 = SAC_FLOAT_T3,     /*!< Fourth user defined pick time or marker. */
    SAC_DOUBLE_HDR_T4 = SAC_FLOAT_T4,     /*!< Fifth user defined pick time or marker. */
    SAC_DOUBLE_HDR_T5 = SAC_FLOAT_T5,     /*!< Sixth user defined pick time or marker. */
    SAC_DOUBLE_HDR_T6 = SAC_FLOAT_T6,     /*!< Seventh user defined pick time or marker. */
    SAC_DOUBLE_HDR_T7 = SAC_FLOAT_T7,     /*!< Eighth user defined pick time or marker. */
    SAC_DOUBLE_HDR_T8 = SAC_FLOAT_T8,     /*!< Ninth user defined pick time or marker. */
    SAC_DOUBLE_HDR_T9 = SAC_FLOAT_T9,     /*!< Tenth user defined pick time or marker. */
    SAC_DOUBLE_HDR_F  = SAC_FLOAT_F,      /*!< End time of event. */
    SAC_DOUBLE_HDR_RESP0 = SAC_FLOAT_RESP0,  /*!< First instrument response parameter. */
    SAC_DOUBLE_HDR_RESP1 = SAC_FLOAT_RESP1,  /*!< Second instrument response parameter. */
    SAC_DOUBLE_HDR_RESP2 = SAC_FLOAT_RESP2,  /*!< Third instrument response parameter. */
    SAC_DOUBLE_HDR_RESP3 = SAC_FLOAT_RESP3,  /*!< Fourth instrument response parameter. */
    SAC_DOUBLE_HDR_RESP4 = SAC_FLOAT_RESP4,  /*!< Fifth instrument response parameter. */
    SAC_DOUBLE_HDR_RESP5 = SAC_FLOAT_RESP5,  /*!< Sixth instrument response parameter. */
    SAC_DOUBLE_HDR_RESP6 = SAC_FLOAT_RESP6,  /*!< Seventh instrument response parameter. */
    SAC_DOUBLE_HDR_RESP7 = SAC_FLOAT_RESP7,  /*!< Eigth instrument response parameter. */
    SAC_DOUBLE_HDR_RESP8 = SAC_FLOAT_RESP8,  /*!< Ninth instrument response parameter. */
    SAC_DOUBLE_HDR_RESP9 = SAC_FLOAT_RESP9,  /*!< Tenth instrument response parameter. */
    SAC_DOUBLE_HDR_STLA = SAC_FLOAT_STLA,   /*!< Station latitude. */
    SAC_DOUBLE_HDR_STLO = SAC_FLOAT_STLO,   /*!< Station longitude. */
    SAC_DOUBLE_HDR_STEL = SAC_FLOAT_STEL,   /*!< Station elevation. */
    SAC_DOUBLE_HDR_STDP = SAC_FLOAT_STDP,   /*!< Station depth. */
    SAC_DOUBLE_HDR_EVLA = SAC_FLOAT_EVLA,   /*!< Event latitude. */
    SAC_DOUBLE_HDR_EVLO = SAC_FLOAT_EVLO,   /*!< Event longitude. */
    SAC_DOUBLE_HDR_EVEL = SAC_FLOAT_EVEL,   /*!< Event elevation. */
    SAC_DOUBLE_HDR_EVDP = SAC_FLOAT_EVDP,   /*!< Event depth. */
    SAC_DOUBLE_HDR_MAG  = SAC_FLOAT_MAG,    /*!< Event magnitude. */
    SAC_DOUBLE_HDR_USER0 = SAC_FLOAT_USER0,  /*!< First user defined variable. */
    SAC_DOUBLE_HDR_USER1 = SAC_FLOAT_USER1,  /*!< Second user defined variable. */
    SAC_DOUBLE_HDR_USER2 = SAC_FLOAT_USER2,  /*!< Third user defined variable. */
    SAC_DOUBLE_HDR_USER3 = SAC_FLOAT_USER3,  /*!< Fourth user defined variable. */
    SAC_DOUBLE_HDR_USER4 = SAC_FLOAT_USER4,  /*!< Fifth user defined variable. */
    SAC_DOUBLE_HDR_USER5 = SAC_FLOAT_USER5,  /*!< Sixth user defined variable. */
    SAC_DOUBLE_HDR_USER6 = SAC_FLOAT_USER6,  /*!< Seventh user defined variable. */
    SAC_DOUBLE_HDR_USER7 = SAC_FLOAT_USER7,  /*!< Eighth user defined variable. */
    SAC_DOUBLE_HDR_USER8 = SAC_FLOAT_USER8,  /*!< Ninth user defined variable. */
    SAC_DOUBLE_HDR_USER9 = SAC_FLOAT_USER9,  /*!< Tenth user defined variable. */
    SAC_DOUBLE_HDR_DIST  = SAC_FLOAT_DIST,   /*!< Source-receiver distance. */
    SAC_DOUBLE_HDR_AZ    = SAC_FLOAT_AZ,     /*!< Source-receiver azimuth. */
    SAC_DOUBLE_HDR_BAZ   = SAC_FLOAT_BAZ,    /*!< Source-receiver back-azimuth. */
    SAC_DOUBLE_HDR_GCARC = SAC_FLOAT_GCARC,  /*!< Source-receiver great circle distance. */
    SAC_DOUBLE_HDR_INTERNAL2 = SAC_FLOAT_INTERNAL2, /*!< Internal variable. */
    SAC_DOUBLE_HDR_DEPMEN = SAC_FLOAT_DEPMEN,    /*!< Mean value of dependent variable. */
    SAC_DOUBLE_HDR_CMPAZ  = SAC_FLOAT_CMPAZ,     /*!< Component azimuth. */
    SAC_DOUBLE_HDR_CMPINC = SAC_FLOAT_CMPINC,    /*!< Component incident angle. */
    SAC_DOUBLE_HDR_XMINIMUM = SAC_FLOAT_XMINIMUM,  /*!< Minimum value of x (spectral files only). */
    SAC_DOUBLE_HDR_XMAXIMUM = SAC_FLOAT_XMAXIMUM,  /*!< Maximum value of x (spectral files only). */
    SAC_DOUBLE_HDR_YMINIMUM = SAC_FLOAT_YMINIMUM,  /*!< Minimum value of y (spectral files only). */
    SAC_DOUBLE_HDR_YMAXIMUM = SAC_FLOAT_YMAXIMUM,  /*!< Maximum value of y (spectral files only). */
    SAC_DOUBLE_HDR_UNUSED0 = SAC_FLOAT_UNUSED0,   /*!< Extra float variable. */
    SAC_DOUBLE_HDR_UNUSED1 = SAC_FLOAT_UNUSED1,   /*!< Extra float variable. */
    SAC_DOUBLE_HDR_UNUSED2 = SAC_FLOAT_UNUSED2,   /*!< Extra float variable. */
    SAC_DOUBLE_HDR_UNUSED3 = SAC_FLOAT_UNUSED3,   /*!< Extra float variable. */
    SAC_DOUBLE_HDR_UNUSED4 = SAC_FLOAT_UNUSED4,   /*!< Extra float variable. */
    SAC_DOUBLE_HDR_UNUSED5 = SAC_FLOAT_UNUSED5,   /*!< Extra float variable. */
    SAC_DOUBLE_HDR_UNUSED6 = SAC_FLOAT_UNUSED6   /*!< Extra float variable. */
};

enum sacFloatHeader_enum
{
    // floats
    SAC_FLOAT_HDR_DELTA  = SAC_FLOAT_DELTA,  /*!< Sampling period. */
    SAC_FLOAT_HDR_DEPMIN = SAC_FLOAT_DEPMIN, /*!< Minimum value of dependent variable. */
    SAC_FLOAT_HDR_DEPMAX = SAC_FLOAT_DEPMAX, /*!< Maximum value of dependent variable. */
    SAC_FLOAT_HDR_SCALE  = SAC_FLOAT_SCALE,  /*!< Scale factor multiplying dependent variable. */
    SAC_FLOAT_HDR_ODELTA = SAC_FLOAT_ODELTA, /*!< Observed increment; if different than delta. */
    SAC_FLOAT_HDR_B = SAC_FLOAT_B,      /*!< Beginning value of dependent variable. */
    SAC_FLOAT_HDR_E = SAC_FLOAT_E,      /*!< Ending value of dependent variable. */
    SAC_FLOAT_HDR_O = SAC_FLOAT_O,      /*!< Origin time (seconds relative to reference time. */
    SAC_FLOAT_HDR_A = SAC_FLOAT_A,      /*!< First arrival time relative to reference time. */
    SAC_FLOAT_HDR_INTERNAL1 = SAC_FLOAT_INTERNAL1, /*!< Internal variable. */
    SAC_FLOAT_HDR_T0 = SAC_FLOAT_T0,     /*!< First user defined pick time or marker (seconds
                                             relative to reference time). */
    SAC_FLOAT_HDR_T1 = SAC_FLOAT_T1,     /*!< Second user defined pick time or marker. */
    SAC_FLOAT_HDR_T2 = SAC_FLOAT_T2,     /*!< Third user defined pick time or marker. */
    SAC_FLOAT_HDR_T3 = SAC_FLOAT_T3,     /*!< Fourth user defined pick time or marker. */
    SAC_FLOAT_HDR_T4 = SAC_FLOAT_T4,     /*!< Fifth user defined pick time or marker. */
    SAC_FLOAT_HDR_T5 = SAC_FLOAT_T5,     /*!< Sixth user defined pick time or marker. */
    SAC_FLOAT_HDR_T6 = SAC_FLOAT_T6,     /*!< Seventh user defined pick time or marker. */
    SAC_FLOAT_HDR_T7 = SAC_FLOAT_T7,     /*!< Eighth user defined pick time or marker. */
    SAC_FLOAT_HDR_T8 = SAC_FLOAT_T8,     /*!< Ninth user defined pick time or marker. */
    SAC_FLOAT_HDR_T9 = SAC_FLOAT_T9,     /*!< Tenth user defined pick time or marker. */
    SAC_FLOAT_HDR_F  = SAC_FLOAT_F,      /*!< End time of event. */
    SAC_FLOAT_HDR_RESP0 = SAC_FLOAT_RESP0,  /*!< First instrument response parameter. */
    SAC_FLOAT_HDR_RESP1 = SAC_FLOAT_RESP1,  /*!< Second instrument response parameter. */
    SAC_FLOAT_HDR_RESP2 = SAC_FLOAT_RESP2,  /*!< Third instrument response parameter. */
    SAC_FLOAT_HDR_RESP3 = SAC_FLOAT_RESP3,  /*!< Fourth instrument response parameter. */
    SAC_FLOAT_HDR_RESP4 = SAC_FLOAT_RESP4,  /*!< Fifth instrument response parameter. */
    SAC_FLOAT_HDR_RESP5 = SAC_FLOAT_RESP5,  /*!< Sixth instrument response parameter. */
    SAC_FLOAT_HDR_RESP6 = SAC_FLOAT_RESP6,  /*!< Seventh instrument response parameter. */
    SAC_FLOAT_HDR_RESP7 = SAC_FLOAT_RESP7,  /*!< Eigth instrument response parameter. */
    SAC_FLOAT_HDR_RESP8 = SAC_FLOAT_RESP8,  /*!< Ninth instrument response parameter. */
    SAC_FLOAT_HDR_RESP9 = SAC_FLOAT_RESP9,  /*!< Tenth instrument response parameter. */
    SAC_FLOAT_HDR_STLA = SAC_FLOAT_STLA,   /*!< Station latitude. */
    SAC_FLOAT_HDR_STLO = SAC_FLOAT_STLO,   /*!< Station longitude. */
    SAC_FLOAT_HDR_STEL = SAC_FLOAT_STEL,   /*!< Station elevation. */
    SAC_FLOAT_HDR_STDP = SAC_FLOAT_STDP,   /*!< Station depth. */
    SAC_FLOAT_HDR_EVLA = SAC_FLOAT_EVLA,   /*!< Event latitude. */
    SAC_FLOAT_HDR_EVLO = SAC_FLOAT_EVLO,   /*!< Event longitude. */
    SAC_FLOAT_HDR_EVEL = SAC_FLOAT_EVEL,   /*!< Event elevation. */
    SAC_FLOAT_HDR_EVDP = SAC_FLOAT_EVDP,   /*!< Event depth. */
    SAC_FLOAT_HDR_MAG  = SAC_FLOAT_MAG,    /*!< Event magnitude. */
    SAC_FLOAT_HDR_USER0 = SAC_FLOAT_USER0,  /*!< First user defined variable. */
    SAC_FLOAT_HDR_USER1 = SAC_FLOAT_USER1,  /*!< Second user defined variable. */
    SAC_FLOAT_HDR_USER2 = SAC_FLOAT_USER2,  /*!< Third user defined variable. */
    SAC_FLOAT_HDR_USER3 = SAC_FLOAT_USER3,  /*!< Fourth user defined variable. */
    SAC_FLOAT_HDR_USER4 = SAC_FLOAT_USER4,  /*!< Fifth user defined variable. */
    SAC_FLOAT_HDR_USER5 = SAC_FLOAT_USER5,  /*!< Sixth user defined variable. */
    SAC_FLOAT_HDR_USER6 = SAC_FLOAT_USER6,  /*!< Seventh user defined variable. */
    SAC_FLOAT_HDR_USER7 = SAC_FLOAT_USER7,  /*!< Eighth user defined variable. */
    SAC_FLOAT_HDR_USER8 = SAC_FLOAT_USER8,  /*!< Ninth user defined variable. */
    SAC_FLOAT_HDR_USER9 = SAC_FLOAT_USER9,  /*!< Tenth user defined variable. */
    SAC_FLOAT_HDR_DIST  = SAC_FLOAT_DIST,   /*!< Source-receiver distance. */
    SAC_FLOAT_HDR_AZ    = SAC_FLOAT_AZ,     /*!< Source-receiver azimuth. */
    SAC_FLOAT_HDR_BAZ   = SAC_FLOAT_BAZ,    /*!< Source-receiver back-azimuth. */
    SAC_FLOAT_HDR_GCARC = SAC_FLOAT_GCARC,  /*!< Source-receiver great circle distance. */
    SAC_FLOAT_HDR_INTERNAL2 = SAC_FLOAT_INTERNAL2, /*!< Internal variable. */
    SAC_FLOAT_HDR_DEPMEN = SAC_FLOAT_DEPMEN,    /*!< Mean value of dependent variable. */
    SAC_FLOAT_HDR_CMPAZ  = SAC_FLOAT_CMPAZ,     /*!< Component azimuth. */
    SAC_FLOAT_HDR_CMPINC = SAC_FLOAT_CMPINC,    /*!< Component incident angle. */
    SAC_FLOAT_HDR_XMINIMUM = SAC_FLOAT_XMINIMUM,  /*!< Minimum value of x (spectral files only). */
    SAC_FLOAT_HDR_XMAXIMUM = SAC_FLOAT_XMAXIMUM,  /*!< Maximum value of x (spectral files only). */
    SAC_FLOAT_HDR_YMINIMUM = SAC_FLOAT_YMINIMUM,  /*!< Minimum value of y (spectral files only). */
    SAC_FLOAT_HDR_YMAXIMUM = SAC_FLOAT_YMAXIMUM,  /*!< Maximum value of y (spectral files only). */
    SAC_FLOAT_HDR_UNUSED0 = SAC_FLOAT_UNUSED0,   /*!< Extra float variable. */
    SAC_FLOAT_HDR_UNUSED1 = SAC_FLOAT_UNUSED1,   /*!< Extra float variable. */
    SAC_FLOAT_HDR_UNUSED2 = SAC_FLOAT_UNUSED2,   /*!< Extra float variable. */
    SAC_FLOAT_HDR_UNUSED3 = SAC_FLOAT_UNUSED3,   /*!< Extra float variable. */
    SAC_FLOAT_HDR_UNUSED4 = SAC_FLOAT_UNUSED4,   /*!< Extra float variable. */
    SAC_FLOAT_HDR_UNUSED5 = SAC_FLOAT_UNUSED5,   /*!< Extra float variable. */
    SAC_FLOAT_HDR_UNUSED6 = SAC_FLOAT_UNUSED6   /*!< Extra float variable. */
};

enum sacIntegerHeader_enum
{
    // ints
    SAC_INT_HDR_NZYEAR    = SAC_INT_NZYEAR,    /*!< Year */
    SAC_INT_HDR_NZJDAY    = SAC_INT_NZJDAY,    /*!< Julian day */
    SAC_INT_HDR_NZHOUR    = SAC_INT_NZHOUR,    /*!< Hour */
    SAC_INT_HDR_NZMIN     = SAC_INT_NZMIN,     /*!< Minute */
    SAC_INT_HDR_NZSEC     = SAC_INT_NZSEC,     /*!< Second */
    SAC_INT_HDR_NZMSEC    = SAC_INT_NZMSEC,    /*!< Millisecond */
    SAC_INT_HDR_NVHDR     = SAC_INT_NVHDR,     /*!< Header version number. */
    SAC_INT_HDR_NORID     = SAC_INT_NORID,     /*!< CSS 3.0 origin ID. */
    SAC_INT_HDR_NEVID     = SAC_INT_NEVID,     /*!< CSS 3.0 event ID. */
    SAC_INT_HDR_NPTS      = SAC_INT_NPTS,      /*!< Number of points in time series. */
    SAC_INT_HDR_INTERNAL1 = SAC_INT_INTERNAL1, /*!< Internal variable. */
    SAC_INT_HDR_NWFID     = SAC_INT_NWFID,     /*!< CSS 3.0 Waveform ID. */
    SAC_INT_HDR_NXSIZE    = SAC_INT_NXSIZE,    /*!< Spectral length - for spectral files only. */
    SAC_INT_HDR_NYSIZE    = SAC_INT_NYSIZE,    /*!< Spectral width - for spectral files only. */
    SAC_INT_HDR_UNUSED0   = SAC_INT_UNUSED0,   /*!< Extra integer variable. */
    SAC_INT_HDR_IFTYPE    = SAC_INT_IFTYPE,    /*!< Type of file.  */
    SAC_INT_HDR_IDEP      = SAC_INT_IDEP,      /*!< Type of dependent variable. */
    SAC_INT_HDR_IZTYPE    = SAC_INT_IZTYPE,    /*!< Reference time equivalence. */ 
    SAC_INT_HDR_UNUSED1   = SAC_INT_UNUSED1,   /*!< Extra integer variable. */
    SAC_INT_HDR_IINST     = SAC_INT_IINST,     /*!< Type of recording instrument. */
    SAC_INT_HDR_ISTREG    = SAC_INT_ISTREG,    /*!< Station geographic region. */
    SAC_INT_HDR_IEVREG    = SAC_INT_IEVREG,    /*!< Event geographic region. */
    SAC_INT_HDR_IEVTYP    = SAC_INT_IEVTYP,    /*!< Event type. */
    SAC_INT_HDR_IQUAL     = SAC_INT_IQUAL,     /*!< Quality of data. */
    SAC_INT_HDR_ISYNTH    = SAC_INT_ISYNTH,    /*!< Synthetic data flag. */
    SAC_INT_HDR_IMAGTYP   = SAC_INT_IMAGTYP,   /*!< Source magnitude type. */
    SAC_INT_HDR_IMAGSRC   = SAC_INT_IMAGSRC,   /*!< Source magnitude information. */
    SAC_INT_HDR_UNUSED2   = SAC_INT_UNUSED2,   /*!< Extra integer variable */
    SAC_INT_HDR_UNUSED3   = SAC_INT_UNUSED3,   /*!< Extra integer variable */
    SAC_INT_HDR_UNUSED4   = SAC_INT_UNUSED4,   /*!< Extra integer variable */
    SAC_INT_HDR_UNUSED5   = SAC_INT_UNUSED5,   /*!< Extra integer variable */
    SAC_INT_HDR_UNUSED6   = SAC_INT_UNUSED6,   /*!< Extra integer variable */
    SAC_INT_HDR_UNUSED7   = SAC_INT_UNUSED7,   /*!< Extra integer variable */
    SAC_INT_HDR_UNUSED8   = SAC_INT_UNUSED8,   /*!< Extra integer variable */
    SAC_INT_HDR_UNUSED9   = SAC_INT_UNUSED9    /*!< Extra integer variable */
};

enum sacBooleanHeader_enum
{
    // bools
    SAC_BOOL_HDR_LEVEN   = SAC_BOOL_LEVEN,   /*!< If true then data is evenly space. */
    SAC_BOOL_HDR_LPSPOL  = SAC_BOOL_LPSPOL,  /*!< If true then station conforms to
                                                  left hand rule. */
    SAC_BOOL_HDR_LOVROK  = SAC_BOOL_LOVROK,  /*!< If true then it is okay to overwrite file. */
    SAC_BOOL_HDR_LCALDA  = SAC_BOOL_LCALDA,  /*!< If true then SAC wil compute dist, az, and
                                                  gcarc. */
    SAC_BOOL_HDR_LUNUSED = SAC_BOOL_LUNUSED  /*!< Extra logical variable. */ 
};

enum sacCharacterHeader_enum
{
    // char
    SAC_CHAR_HDR_KSTNM  = SAC_CHAR_KSTNM,   /*!< Station name. */
    SAC_CHAR_HDR_KEVNM  = SAC_CHAR_KEVNM,   /*!< Event name. */
    SAC_CHAR_HDR_KHOLE  = SAC_CHAR_KHOLE,   /*!< Location code. */
    SAC_CHAR_HDR_KO     = SAC_CHAR_KO,      /*!< Origin time identifier. */
    SAC_CHAR_HDR_KA     = SAC_CHAR_KA,      /*!< First arrival identifier. */
    SAC_CHAR_HDR_KT0    = SAC_CHAR_KT0,     /*!< Label for first pick time. */
    SAC_CHAR_HDR_KT1    = SAC_CHAR_KT1,     /*!< Label for second pick time. */ 
    SAC_CHAR_HDR_KT2    = SAC_CHAR_KT2,     /*!< Label for third pick time. */
    SAC_CHAR_HDR_KT3    = SAC_CHAR_KT3,     /*!< Label for fourth pick time. */
    SAC_CHAR_HDR_KT4    = SAC_CHAR_KT4,     /*!< Label for fifth pick time. */
    SAC_CHAR_HDR_KT5    = SAC_CHAR_KT5,     /*!< Label for sixth pick time. */
    SAC_CHAR_HDR_KT6    = SAC_CHAR_KT6,     /*!< Label for seventh pick time. */
    SAC_CHAR_HDR_KT7    = SAC_CHAR_KT7,     /*!< Label for eighth pick time. */
    SAC_CHAR_HDR_KT8    = SAC_CHAR_KT8,     /*!< Label for ninth pick time. */
    SAC_CHAR_HDR_KT9    = SAC_CHAR_KT9,     /*!< Label for tenth pick time. */
    SAC_CHAR_HDR_KF     = SAC_CHAR_KF,      /*!< End time identifier. */
    SAC_CHAR_HDR_KUSER0 = SAC_CHAR_KUSER0,  /*!< Extra character variable for user. */
    SAC_CHAR_HDR_KUSER1 = SAC_CHAR_KUSER1,  /*!< Extra character variable for user. */
    SAC_CHAR_HDR_KUSER2 = SAC_CHAR_KUSER2,  /*!< Extra character variable for user. */
    SAC_CHAR_HDR_KCMPNM = SAC_CHAR_KCMPNM,  /*!< Component name. */
    SAC_CHAR_HDR_KNETWK = SAC_CHAR_KNETWK,  /*!< Network name. */
    SAC_CHAR_HDR_KDATRD = SAC_CHAR_KDATRD,  /*!< Data was read onto computer. */
    SAC_CHAR_HDR_KINST  = SAC_CHAR_KINST    /*!< Generic name of recording instrument. */
};

enum sacStringHeader_enum
{
    // char
    SAC_STRING_HDR_KSTNM  = SAC_CHAR_KSTNM,   /*!< Station name. */
    SAC_STRING_HDR_KEVNM  = SAC_CHAR_KEVNM,   /*!< Event name. */
    SAC_STRING_HDR_KHOLE  = SAC_CHAR_KHOLE,   /*!< Location code. */
    SAC_STRING_HDR_KO     = SAC_CHAR_KO,      /*!< Origin time identifier. */
    SAC_STRING_HDR_KA     = SAC_CHAR_KA,      /*!< First arrival identifier. */
    SAC_STRING_HDR_KT0    = SAC_CHAR_KT0,     /*!< Label for first pick time. */
    SAC_STRING_HDR_KT1    = SAC_CHAR_KT1,     /*!< Label for second pick time. */ 
    SAC_STRING_HDR_KT2    = SAC_CHAR_KT2,     /*!< Label for third pick time. */
    SAC_STRING_HDR_KT3    = SAC_CHAR_KT3,     /*!< Label for fourth pick time. */
    SAC_STRING_HDR_KT4    = SAC_CHAR_KT4,     /*!< Label for fifth pick time. */
    SAC_STRING_HDR_KT5    = SAC_CHAR_KT5,     /*!< Label for sixth pick time. */
    SAC_STRING_HDR_KT6    = SAC_CHAR_KT6,     /*!< Label for seventh pick time. */
    SAC_STRING_HDR_KT7    = SAC_CHAR_KT7,     /*!< Label for eighth pick time. */
    SAC_STRING_HDR_KT8    = SAC_CHAR_KT8,     /*!< Label for ninth pick time. */
    SAC_STRING_HDR_KT9    = SAC_CHAR_KT9,     /*!< Label for tenth pick time. */
    SAC_STRING_HDR_KF     = SAC_CHAR_KF,      /*!< End time identifier. */
    SAC_STRING_HDR_KUSER0 = SAC_CHAR_KUSER0,  /*!< Extra character variable for user. */
    SAC_STRING_HDR_KUSER1 = SAC_CHAR_KUSER1,  /*!< Extra character variable for user. */
    SAC_STRING_HDR_KUSER2 = SAC_CHAR_KUSER2,  /*!< Extra character variable for user. */
    SAC_STRING_HDR_KCMPNM = SAC_CHAR_KCMPNM,  /*!< Component name. */
    SAC_STRING_HDR_KNETWK = SAC_CHAR_KNETWK,  /*!< Network name. */
    SAC_STRING_HDR_KDATRD = SAC_CHAR_KDATRD,  /*!< Data was read onto computer. */
    SAC_STRING_HDR_KINST  = SAC_CHAR_KINST    /*!< Generic name of recording instrument. */
};

#endif
